@extends('laravel-launcher::layouts.backend')

@section('content')

	<form action="{{ route('backend.roles.store') }}" method="POST">

		@csrf

		<x-ll::backend.card>
			@if (Auth::user()->has_superpowers)
				<x-ll::form.input id="name" label="guard-name" name="name" description="guard" />
			@endif

			<x-ll::form.input id="label" label="label" name="label" required />
		</x-ll::backend.card>

		<x-ll::backend.card>
			@if ($permissions->count())
				<fieldset>
					<legend>@choice('laravel-launcher::model.permission', 2)</legend>
					<div class="table-scrollable">
						<table>
							<thead>
								<tr>
									<th>@choice('laravel-launcher::field.model', 2)</th>
									@foreach (config('laravel-launcher.permission.actions') as $action)
										<th>
											<input type="checkbox" id="{{ $action . 'All' }}" class="checkbox-all" name="all" value="permissions[{{ $action }}][]">
											<label for="{{ $action . 'All' }}">@lang('laravel-launcher::action.' . $action)</label>
										</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								@foreach ($permissions as $model => $permission)
									@if ($model)
										<tr>
											<th>
												{{ translate($model, 'model') }}
											</th>
											@foreach (config('laravel-launcher.permission.actions') as $action)
												<td>
													@include('laravel-launcher::backend.permissions.includes.checkbox', ['permission' => $permission->firstWhere('action', $action)])
												</td>
											@endforeach
										</tr>
									@else
										@foreach ($permission as $custom)
											<tr>
												<th>
													{{ $custom->name }}
												</th>
												<td>
													<input id="{{ $custom->slug }}" type="checkbox" name="permissions[view][]" value="{{ $custom->id }}" @if (in_array($custom->id, Arr::flatten(old('permissions', [])))) checked @endif>
													<label for="{{ $custom->slug }}">@lang('laravel-launcher::action.view')</label>
												</td>
												<td colspan="{{ count(config('laravel-launcher.permission.actions')) - 1 }}"></td>
											</tr>
										@endforeach
									@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</fieldset>
			@endif
		</x-ll::backend.card>

		<footer>
			<a href="{{ route('backend.roles.index') }}" role="button">@lang('laravel-launcher::action.cancel')</a>
			<button type="submit" class="primary">@lang('laravel-launcher::action.create')</button>
		</footer>
	</form>

@endsection
