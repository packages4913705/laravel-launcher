@extends('laravel-launcher::layouts.backend')

@section('buttons')
	@can('update', $role)
		<a class="primary" href="{{ route('backend.roles.edit', $role->id) }}" role="button">
			<x-ll::svg icon="pencil" />
			@lang('laravel-launcher::action.edit')
		</a>
	@endcan
@endsection

@section('grid')
	<x-ll::backend.card icon="lock" title="permissions" class="col-wider">
		<div class="table-scrollable">
			<table>
				<thead>
					<tr>
						<th>@choice('laravel-launcher::field.model', 2)</th>
						@foreach (config('laravel-launcher.permission.actions') as $action)
							<th>
								@lang('laravel-launcher::action.' . $action)
							</th>
						@endforeach
					</tr>
				</thead>
				<tbody>
					@foreach ($permissions as $model => $permission)
						@if ($model)
							<tr>
								<td>
									{{ translate($model, 'model') }}
								</td>
								@foreach (config('laravel-launcher.permission.actions') as $action)
									<td>
										@php
											$perm = $permission->firstWhere('action', $action);
										@endphp
										@if ($perm)
											@include('laravel-launcher::includes.badges.boolean', ['value' => in_array($perm->id ?? null, $role->permissions->modelKeys())])
										@else
											-
										@endif

									</td>
								@endforeach
							</tr>
						@else
							@foreach ($permission as $custom)
								<tr>
									<td>
										{{ $custom->name }}
									</td>
									<td>
										@include('laravel-launcher::includes.badges.boolean', ['value' => in_array($custom->id ?? null, $role->permissions->modelKeys())])
									</td>
									<td colspan="{{ count(config('laravel-launcher.permission.actions')) - 1 }}"></td>
								</tr>
							@endforeach
						@endif
					@endforeach
				</tbody>
			</table>
		</div>
	</x-ll::backend.card>

	<x-ll::backend.information :item="$role" />

	@include('laravel-launcher::includes.cards.loggable', ['item' => $role])
@endsection
