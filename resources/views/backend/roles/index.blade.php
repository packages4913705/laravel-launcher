@extends('laravel-launcher::layouts.backend')

@section('content')
	@livewire('datalist', [
	    'session' => 'roles',
	    'model' => 'role',
	    'path' => 'laravel-launcher::backend.roles.includes',
	    'columns' => ['label', 'used-by'],
	    'sorting' => ['label'],
	    'hasAdvancedSearch' => false,
	])
@endsection
