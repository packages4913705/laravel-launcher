@extends('laravel-launcher::layouts.backend')

@section('content')
	@livewire('datalist', [
	    'session' => 'users',
	    'model' => 'user',
	    'path' => 'laravel-launcher::backend.users.includes',
	    'columns' => ['name', 'email', 'role'],
	    'sorting' => ['name'],
	    'hasAdvancedSearch' => config('laravel-launcher.user.colorable') || config('laravel-launcher.user.categorizable') || config('laravel-launcher.user.roleable'),
	])
@endsection
