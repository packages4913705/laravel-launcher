@extends('laravel-launcher::layouts.backend')

@section('content')
	<form action="{{ route('backend.users.update', $user->id) }}" method="POST" enctype="multipart/form-data">

		@csrf
		@method('PATCH')

		<x-ll::backend.card>
			<x-ll::form.input id="name" label="username" name="name" :value="$user->name" required />

			<x-ll::form.input id="email" label="email" name="email" type="email" :value="$user->email" required />

			<x-ll::form.input id="password" label="password" name="password" type="password" description="password" />

			<x-ll::form.input id="passwordConfirmation" label="password-confirmation" name="password_confirmation" type="password" description="leave-empty" />

		</x-ll::backend.card>

		<x-ll::backend.form.roleable :current="$user->roles->modelKeys()" />

		<x-ll::backend.form.mediable :current="$user->media" />

		<footer>
			<a href="{{ route('backend.users.index') }}" role="button">@lang('laravel-launcher::action.cancel')</a>
			<button type="submit" class="primary">@lang('laravel-launcher::action.update')</button>
		</footer>

	</form>
@endsection
