@extends('laravel-launcher::layouts.backend')

@section('content')
	<form action="{{ route('backend.users.store') }}" method="POST" enctype="multipart/form-data">

		@csrf

		<x-ll::backend.card>
			<x-ll::form.input id="name" label="username" name="name" required />

			<x-ll::form.input id="email" label="email" name="email" type="email" required />

			<x-ll::form.input id="password" label="password" name="password" type="password" description="password" required />

			<x-ll::form.input id="passwordConfirmation" label="password-confirmation" name="password_confirmation" type="password" required />

		</x-ll::backend.card>

		<x-ll::backend.form.roleable />

		<x-ll::backend.form.mediable />

		<footer>
			<a href="{{ route('backend.users.index') }}" role="button">@lang('laravel-launcher::action.cancel')</a>
			<button type="submit" class="primary">@lang('laravel-launcher::action.create')</button>
		</footer>

	</form>
@endsection
