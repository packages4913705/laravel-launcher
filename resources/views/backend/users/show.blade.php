@extends('laravel-launcher::layouts.backend')

@section('buttons')
	@can('update', $user)
		<a class="primary" href="{{ route('backend.users.edit', $user->id) }}" role="button">
			<x-ll::svg icon="pencil" />
			@lang('laravel-launcher::action.edit')
		</a>
	@endcan
@endsection

@section('grid')
	<x-ll::backend.information :item="$user">
		@if ($user->name)
			<dt>@choice('laravel-launcher::field.username', 1)</dt>
			<dd>{{ $user->name }}</dd>
		@endif
		@if ($user->email)
			<dt>@choice('laravel-launcher::field.email', 1)</dt>
			<dd>{{ $user->email }}</dd>
		@endif
	</x-ll::backend.information>

	@php
		$item = $user;
	@endphp

	@include('laravel-launcher::includes.cards.mediable')

	@include('laravel-launcher::includes.cards.categorizable')

	@include('laravel-launcher::includes.cards.levelable')

	@include('laravel-launcher::includes.cards.roleable')

	@include('laravel-launcher::includes.cards.loggable')
@endsection
