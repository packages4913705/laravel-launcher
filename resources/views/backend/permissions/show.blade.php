@extends('laravel-launcher::layouts.backend')

@section('buttons')
	@can('update', $permission)
		<a class="primary" href="{{ route('backend.permissions.edit', $permission->id) }}" role="button">
			<x-ll::svg icon="pencil" />
			@lang('laravel-launcher::action.edit')
		</a>
	@endcan
@endsection

@section('grid')
	<x-ll::backend.information :item="$permission">
		@if ($permission->model)
			<dt>@choice('laravel-launcher::field.model', 1)</dt>
			<dd>{{ translate($permission->model, 'model') }}</dd>
		@endif
		@if ($permission->action)
			<dt>@choice('laravel-launcher::field.action', 1)</dt>
			<dd>@lang('laravel-launcher::action.' . $permission->action)</dd>
		@endif
	</x-ll::backend.information>

	@if ($permission->roles)
		<x-ll::backend.card :model="$permission->roles->count() > 1 ? 'roles' : 'role'" icon="key">
			<ul class="list flush">
				@foreach ($permission->roles as $role)
					<li>
						<b class="list-grow">{{ $role->label }}</b>
						@can('view', $role)
							<a class="rounded" href="{{ route('backend.roles.show', $role->id) }}" role="button" aria-label="@lang('laravel-launcher::action.view')" title="@lang('laravel-launcher::action.view')">
								<x-ll::svg icon="eye" />
							</a>
						@endcan
					</li>
				@endforeach
			</ul>
		</x-ll::backend.card>
	@endif

	@include('laravel-launcher::includes.cards.loggable', ['item' => $permission])
@endsection
