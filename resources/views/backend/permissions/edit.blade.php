@extends('laravel-launcher::layouts.backend')

@section('content')
	<form action="{{ route('backend.permissions.update', $permission->id) }}" method="POST">

		@csrf
		@method('PATCH')

		<x-ll::backend.card>
			@if (Auth::user()->has_superpowers)
				<x-ll::form.input id="name" label="guard-name" name="name" :value="$permission->name" description="guard" />
			@endif

			<x-ll::form.input id="label" label="label" name="label" :value="$permission->label" required />

			<x-ll::form.input id="model" label="model" name="model" :value="$permission->model" />

			<x-ll::form.select id="action" label="action" name="action">
				@foreach (config('laravel-launcher.permission.actions') as $action)
					<option value="{{ $action }}" @selected(old('action', $permission->action) == $action)>
						@lang('laravel-launcher::action.' . $action)
					</option>
				@endforeach
			</x-ll::form.select>
		</x-ll::backend.card>

		<footer>
			<a href="{{ route('backend.permissions.index') }}" role="button">@lang('laravel-launcher::action.cancel')</a>
			<button type="submit" class="primary">@lang('laravel-launcher::action.update')</button>
		</footer>

	</form>
@endsection
