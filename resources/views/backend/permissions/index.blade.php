@extends('laravel-launcher::layouts.backend')

@section('content')
	@livewire('datalist', [
	    'session' => 'permissions',
	    'model' => 'permission',
	    'path' => 'laravel-launcher::backend.permissions.includes',
	    'columns' => ['label', 'model', 'action'],
	    'sorting' => ['label', 'model', 'action'],
	    'hasAdvancedSearch' => true,
	])
@endsection
