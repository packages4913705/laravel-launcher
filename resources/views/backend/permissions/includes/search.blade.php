<x-ll::backend.search>

	<x-ll::form.select id="searchAction" name="searchAction" label="action" :options="$actions" wire:model="search.action" />

	<x-ll::form.select id="searchModel" name="searchModel" label="model" :options="$models" wire:model="search.model" />

</x-ll::backend.search>
