@if($permission)
    <input id="{{ $permission->action.$permission->id }}" type="checkbox" name="permissions[{{ $permission->action }}][]" value="{{ $permission->id }}" @if(in_array($permission->id, Arr::flatten(old('permissions', isset($role) ? $role->permissions->pluck('id') : [])))) checked @endif >
    <label for="{{ $permission->action.$permission->id }}">@lang('laravel-launcher::action.'.$permission->action )</label>
@else
    -
@endif