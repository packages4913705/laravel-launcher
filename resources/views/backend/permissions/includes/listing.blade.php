<td class="fit"><b>{{ $item->label }}</b></td>
<td class="fit">
	{{ $item->model ? translate($item->model, 'model') : '-' }}
</td>
<td class="fit">
	{{ $item->action ? translate($item->action, 'action') : '-' }}
</td>
