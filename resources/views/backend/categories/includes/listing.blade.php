<td><b>{{ $item->hierarchy ?? $item->label }}</b></td>
@if (config('laravel-launcher.category.colorable'))
	<td class="fit">
		@include('laravel-launcher::includes.badges.color', ['color' => $item->color])
	</td>
@endif
<td class="align-center">{{ $item->used_by }}</td>
