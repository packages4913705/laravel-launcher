@if (config('laravel-launcher.category.levelable') && $category->children->count())
	<li>
		<x-ll::form.checkbox id="{{ 'cat' . $category->id }}" :label="$category->label" name="categories[]" :value="$category->id" :default="$current" />

		<button type="button" class="rounded small" data-handle="tree" aria-expanded="false" aria-controls="{{ 'sublist' . $category->id }}" aria-label="@lang('laravel-launcher::action.toggle')">
			<x-ll::svg icon="chevron-right" />
		</button>

		<ul id="{{ 'sublist' . $category->id }}" hidden>
			@foreach ($category->children as $child)
				@include('laravel-launcher::backend.categories.includes.checkbox', ['category' => $child])
			@endforeach
		</ul>

	</li>
@else
	<li>
		<x-ll::form.checkbox id="{{ 'cat' . $category->id }}" :label="$category->label" name="categories[]" :value="$category->id" :default="$current" />
	</li>
@endif
