@extends('laravel-launcher::layouts.backend')

@section('content')
	<form action="{{ route('backend.categories.update', $category->id) }}" method="POST" enctype="multipart/form-data">

		@csrf
		@method('PATCH')

		<x-ll::backend.card>

			@if (Auth::user()->has_superpowers)
				<x-ll::form.input id="name" label="guard-name" name="name" :value="$category->name" description="guard" />
			@endif

			<x-ll::backend.form.typeable :current="$category->type" />

			<x-ll::backend.form.levelable :current="$category->parent_id" />

			<x-ll::form.input id="label" label="label" name="label" :value="$category->label" required />

			<x-ll::form.input id="slug" label="slug" name="slug" description="slug" :value="$category->slug" />

			<x-ll::backend.form.colorable :current="$category->color" />
		</x-ll::backend.card>

		<x-ll::backend.form.roleable :current="$category->roles->modelKeys()" />

		<x-ll::backend.form.mediable :current="$category->media" />

		<x-ll::backend.form.translatable>
			@foreach ($component->languages as $lang)
				<div id="{{ $lang }}" role="tabpanel">
					<x-ll::form.input id="{{ 'label' . $lang }}" label="{{ 'label' }}" name="translations[{{ $lang }}][label]" :value="$category->getTranslation($lang)->value['label'] ?? null" :info="$lang" />
				</div>
			@endforeach
		</x-ll::backend.form.translatable>

		<footer>
			<a href="{{ route('backend.categories.index') }}" role="button">@lang('laravel-launcher::action.cancel')</a>
			<button type="submit" class="primary">@lang('laravel-launcher::action.update')</button>
		</footer>

	</form>
@endsection
