@extends('laravel-launcher::layouts.backend')

@section('content')
	<form action="{{ route('backend.categories.store') }}" method="POST" enctype="multipart/form-data">

		@csrf

		<x-ll::backend.card>

			@if (Auth::user()->has_superpowers)
				<x-ll::form.input id="name" label="guard-name" name="name" description="guard" />
			@endif

			<x-ll::backend.form.typeable />

			<x-ll::backend.form.levelable />

			<x-ll::form.input id="label" label="label" name="label" required />

			<x-ll::form.input id="slug" label="slug" name="slug" description="slug" />

			<x-ll::backend.form.colorable />

		</x-ll::backend.card>

		<x-ll::backend.form.roleable />

		<x-ll::backend.form.mediable />

		<x-ll::backend.form.translatable>
			@foreach ($component->languages as $lang)
				<div id="{{ $lang }}" role="tabpanel">
					<x-ll::form.input id="{{ 'label' . $lang }}" label="{{ 'label' }}" name="translations[{{ $lang }}][label]" :info="$lang" />
				</div>
			@endforeach
		</x-ll::backend.form.translatable>

		<footer>
			<a href="{{ route('backend.categories.index') }}" role="button">@lang('laravel-launcher::action.cancel')</a>
			<button type="submit" class="primary">@lang('laravel-launcher::action.create')</button>
		</footer>

	</form>
@endsection
