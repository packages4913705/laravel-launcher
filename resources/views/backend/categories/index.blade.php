@extends('laravel-launcher::layouts.backend')

@section('content')
	@livewire('datalist', [
	    'session' => 'categories',
	    'model' => 'category',
	    'path' => 'laravel-launcher::backend.categories.includes',
	    'filters' => config('laravel-launcher.category.typeable'),
	    'columns' => array_filter(['label', config('laravel-launcher.category.colorable') ? 'color' : null, 'used-by']),
	    'sorting' => ['label'],
	    'hasAdvancedSearch' => config('laravel-launcher.category.colorable') || config('laravel-launcher.category.categorizable') || config('laravel-launcher.category.roleable'),
	])
@endsection
