@extends('laravel-launcher::layouts.backend')

@section('buttons')
	@can('update', $category)
		<a class="primary" href="{{ route('backend.categories.edit', $category->id) }}" role="button">
			<x-ll::svg icon="pencil" />
			@lang('laravel-launcher::action.edit')
		</a>
	@endcan
@endsection

@section('grid')
	<x-ll::backend.information :item="$category" />

	@php
		$item = $category;
	@endphp

	@include('laravel-launcher::includes.cards.mediable')

	@include('laravel-launcher::includes.cards.levelable')

	@include('laravel-launcher::includes.cards.roleable')

	@include('laravel-launcher::includes.cards.loggable')
@endsection
