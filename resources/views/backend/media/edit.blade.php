@extends('laravel-launcher::layouts.backend')

@section('content')
	<form action="{{ route('backend.media.update', $media->id) }}" method="POST" enctype="multipart/form-data">

		@csrf
		@method('PATCH')

		<x-ll::backend.card>
			<x-ll::form.input id="label" label="label" name="label" :value="$media->label" required />
			<x-ll::form.input id="description" label="description" name="description" :value="$media->description" description="image-description" />
			<x-ll::form.input id="file" label="file" name="udpate_media_file" type="file" :description="trans('laravel-launcher::form.helpers.format', ['format' => config('laravel-launcher.' . $media->mediable_type . '.mediable.' . $media->type . '.format') ?? 'jpg, png, pdf, doc, docx, xls, xlsx, csv, ics'])" />
		</x-ll::backend.card>

		<x-ll::backend.form.translatable model="media">
			@foreach ($component->languages as $lang)
				<div id="{{ $lang }}" role="tabpanel">
					<x-ll::form.input id="{{ 'label' . $lang }}" label="label" name="translations[{{ $lang }}][label]" :value="$media->getTranslation($lang)->value['label'] ?? null" :info="$lang" />
					<x-ll::form.textarea id="{{ 'description' . $lang }}" label="description" name="translations[{{ $lang }}][description]" :value="$media->getTranslation($lang)->value['description'] ?? null" :info="$lang" />
				</div>
			@endforeach
		</x-ll::backend.form.translatable>

		<footer>
			<a href="{{ route('backend.media.index') }}" role="button">@lang('laravel-launcher::action.cancel')</a>
			<button type="submit" class="primary">@lang('laravel-launcher::action.update')</button>
		</footer>

	</form>
@endsection
