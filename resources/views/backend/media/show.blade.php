@extends('laravel-launcher::layouts.backend')

@section('buttons')
	@can('download', $media)
		<a class=" outline" href="{{ $media->url }}" role="button" download target="_blank">
			<x-ll::svg icon="download" />
			@lang('laravel-launcher::action.download')
		</a>
	@endcan
	@can('update', $media)
		<a class="primary" href="{{ route('backend.media.edit', $media->id) }}" role="button">
			<x-ll::svg icon="pencil" />
			@lang('laravel-launcher::action.edit')
		</a>
	@endcan
@endsection

@section('grid')
	<x-ll::backend.card icon="info-circle" model="information">
		<dl>

			@if ($media->id)
				<dt>@choice('laravel-launcher::field.id', 1)</dt>
				<dd>{{ $media->id }}</dd>
			@endif

			@if ($media->created_at)
				<dt>@choice('laravel-launcher::field.created-at', 1)</dt>
				<dd>{{ $media->created_at->format(config('laravel-launcher.formats.datetime')) }}</dd>
			@endif

			@if ($media->updated_at)
				<dt>@choice('laravel-launcher::field.updated-at', 1)</dt>
				<dd>{{ $media->updated_at->format(config('laravel-launcher.formats.datetime')) }}</dd>
			@endif

			<dt>@choice('laravel-launcher::field.label', 1)</dt>
			<dd>{{ $media->label }}</dd>

			<dt>@choice('laravel-launcher::field.description', 1)</dt>
			<dd>
				@if (!$media->description && in_array($media->format, ['image', 'vector']))
					<span class="badge warning">
						<x-ll::svg icon="exclamation-triangle" />
						@lang('laravel-launcher::notification.error.media-description')
					</span>
				@else
					{{ $media->description ?? '-' }}
				@endif
			</dd>

			@if ($media->type)
				<dt>@choice('laravel-launcher::field.type', 1)</dt>
				<dd>{{ translate($media->type, 'field') }} - {{ translate($media->format, 'field') }} <small>({{ '.' . $media->extension }})</small></dd>
			@endif

			<dt>@choice('laravel-launcher::field.used-by', 1)</dt>
			<dd>
				{{ translate($media->mediable_type, 'model') . ' #' . $media->mediable_id }}
			</dd>

			<dt>@choice('laravel-launcher::field.url', 1)</dt>
			<dd>{{ $media->url }}</dd>

		</dl>
	</x-ll::backend.card>

	@if (in_array($media->format, ['image', 'vector']))
		<picture>
			<img src="{{ $media->url }}" alt="{{ $media->description ?? $media->label }}">
		</picture>
	@endif

	@php
		$item = $media;
	@endphp

	@include('laravel-launcher::includes.cards.loggable')
@endsection
