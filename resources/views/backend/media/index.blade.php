@extends('laravel-launcher::layouts.backend')

@section('content')
	@livewire('datalist', [
	    'session' => 'media',
	    'model' => 'media',
	    'path' => 'laravel-launcher::backend.media.includes',
	    'filters' => ['image', 'vector', 'pdf', 'word', 'excel', 'audio', 'video', 'code'],
	    'columns' => ['label', 'type', 'model'],
	    'sorting' => ['label'],
	    'hasAdvancedSearch' => false,
	    'hasPosition' => false,
	])
@endsection
