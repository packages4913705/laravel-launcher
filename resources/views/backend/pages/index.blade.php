@extends('laravel-launcher::layouts.backend')

@section('content')
	@livewire('datalist', [
	    'session' => 'pages',
	    'model' => 'page',
	    'path' => 'laravel-launcher::backend.pages.includes',
	    'filters' => array_merge(['in-menu', 'in-footer'], config('laravel-launcher.page.typeable') ?? [null]),
	    'columns' => array_filter(['label', 'published', config('laravel-launcher.page.colorable') ? 'color' : null]),
	    'sorting' => ['label'],
	    'hasAdvancedSearch' => config('laravel-launcher.page.colorable') || config('laravel-launcher.page.categorizable') || config('laravel-launcher.page.roleable'),
	])
@endsection
