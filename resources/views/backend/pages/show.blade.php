@extends('laravel-launcher::layouts.backend')

@section('buttons')
	@can('publish', $page)
		<form action="{{ route('backend.pages.publish', $page->id) }}" method="POST">
			@csrf
			@method('PATCH')
			<button type="submit" class="outline">
				<x-ll::svg icon="check-circle" />
				@lang('laravel-launcher::action.publish')
			</button>
		</form>
	@endcan
	@can('unpublish', $page)
		<form action="{{ route('backend.pages.unpublish', $page->id) }}" method="POST">
			@csrf
			@method('PATCH')
			<button type="submit" class="outline">
				<x-ll::svg icon="x-circle" />
				@lang('laravel-launcher::action.unpublish')
			</button>
		</form>
	@endcan
	@can('update', $page)
		<a class="primary" href="{{ route('backend.pages.edit', $page->id) }}" role="button">
			<x-ll::svg icon="pencil" />
			@lang('laravel-launcher::action.edit')
		</a>
	@endcan
@endsection

@section('grid')
	@if ($page->title || $page->description)
		<x-ll::backend.card icon="body-text" :title="$page->title" class="col-wider">
			{!! $page->description !!}
		</x-ll::backend.card>
	@endif

	<x-ll::backend.information :item="$page">
		<dt>@choice('laravel-launcher::field.in-menu', 1)</dt>
		<dd>@include('laravel-launcher::includes.badges.boolean', ['value' => $page->in_menu])</dd>
		<dt>@choice('laravel-launcher::field.in-footer', 1)</dt>
		<dd>@include('laravel-launcher::includes.badges.boolean', ['value' => $page->in_footer])</dd>
	</x-ll::backend.information>

	@php
		$item = $page;
	@endphp

	@include('laravel-launcher::includes.cards.mediable')

	@include('laravel-launcher::includes.cards.categorizable')

	@include('laravel-launcher::includes.cards.levelable')

	@include('laravel-launcher::includes.cards.roleable')

	@include('laravel-launcher::includes.cards.loggable')
@endsection
