@extends('laravel-launcher::layouts.backend')

@section('content')
	<form action="{{ route('backend.pages.update', $page->id) }}" method="POST" enctype="multipart/form-data">

		@csrf
		@method('PATCH')

		<x-ll::backend.card>
			@if (Auth::user()->has_superpowers)
				<x-ll::form.input id="name" label="guard-name" name="name" :value="$page->name" description="guard" />
			@endif

			<fieldset>
				<legend>@choice('laravel-launcher::field.option', 2)</legend>
				@if ($page->name !== 'index')
					<x-ll::form.switcher id="published" label="published" name="published" :default="$page->published" />
				@endif
				<x-ll::form.switcher id="inMenu" label="in-menu" name="in_menu" :default="$page->in_menu" />
				<x-ll::form.switcher id="inFooter" label="in-footer" name="in_footer" :default="$page->in_footer" />
			</fieldset>

			<x-ll::backend.form.typeable :current="$page->type" />

			<x-ll::backend.form.levelable :current="$page->parent_id" />

			<x-ll::form.input id="label" label="label" name="label" :value="$page->label" description="label-page" required />

			<x-ll::form.input id="slug" label="slug" name="slug" description="slug" :value="$page->slug" />

			<x-ll::form.input id="title" label="title" name="title" :value="$page->title" />

			<x-ll::form.editor id="description" label="description" name="description" :value="$page->description" />

			<x-ll::backend.form.colorable :current="$page->color" />

		</x-ll::backend.card>

		<x-ll::backend.form.roleable :current="$page->roles->modelKeys()" />

		<x-ll::backend.form.categorizable :current="$page->categories->modelKeys()" />

		<x-ll::backend.form.mediable :current="$page->media" />

		<x-ll::backend.form.translatable>
			@foreach ($component->languages as $lang)
				<div id="{{ $lang }}" role="tabpanel">
					<x-ll::form.input id="{{ 'label' . $lang }}" label="{{ 'label' }}" name="translations[{{ $lang }}][label]" :value="$page->getTranslation($lang)->value['label'] ?? null" :info="$lang" />
					<x-ll::form.input id="{{ 'title' . $lang }}" label="{{ 'title' }}" name="translations[{{ $lang }}][title]" :value="$page->getTranslation($lang)->value['title'] ?? null" :info="$lang" />
					<x-ll::form.editor id="{{ 'description' . $lang }}" label="{{ 'description' }}" name="translations[{{ $lang }}][description]" :value="$page->getTranslation($lang)->value['description'] ?? null" :info="$lang" />
				</div>
			@endforeach
		</x-ll::backend.form.translatable>

		<footer>
			<a href="{{ route('backend.pages.index') }}" role="button">@lang('laravel-launcher::action.cancel')</a>
			<button type="submit" class="primary">@lang('laravel-launcher::action.update')</button>
		</footer>

	</form>
@endsection
