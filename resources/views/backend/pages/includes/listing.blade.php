<td><b>{{ $item->hierarchy ?? $item->label }}</b></td>
<td class="fit">
	@include('laravel-launcher::includes.badges.boolean', ['value' => $item->published])
</td>
@if (config('laravel-launcher.page.colorable'))
	<td class="fit">
		@include('laravel-launcher::includes.badges.color', ['color' => $item->color])
	</td>
@endif
