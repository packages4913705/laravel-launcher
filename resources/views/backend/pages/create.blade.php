@extends('laravel-launcher::layouts.backend')

@section('content')
	<form action="{{ route('backend.pages.store') }}" method="POST" enctype="multipart/form-data">

		@csrf

		<x-ll::backend.card>

			@if (Auth::user()->has_superpowers)
				<x-ll::form.input id="name" label="guard-name" name="name" description="guard" />
			@endif

			<fieldset>
				<legend>@choice('laravel-launcher::field.option', 2)</legend>
				<x-ll::form.switcher id="published" label="published" name="published" checked />
				<x-ll::form.switcher id="inMenu" label="in-menu" name="in_menu" checked />
				<x-ll::form.switcher id="inFooter" label="in-footer" name="in_footer" />
			</fieldset>

			<x-ll::backend.form.typeable />

			<x-ll::backend.form.levelable />

			<x-ll::form.input id="label" label="label" name="label" required description="label-page" />

			<x-ll::form.input id="slug" label="slug" name="slug" description="slug" />

			<x-ll::form.input id="title" label="title" name="title" />

			<x-ll::form.editor id="description" label="description" name="description" />

			<x-ll::backend.form.colorable />

		</x-ll::backend.card>

		<x-ll::backend.form.roleable />

		<x-ll::backend.form.categorizable />

		<x-ll::backend.form.mediable />

		<x-ll::backend.form.translatable>
			@foreach ($component->languages as $lang)
				<div id="{{ $lang }}" role="tabpanel">
					<x-ll::form.input id="{{ 'label' . $lang }}" label="label" name="translations[{{ $lang }}][label]" :info="$lang" />
					<x-ll::form.input id="{{ 'title' . $lang }}" label="title" name="translations[{{ $lang }}][title]" :info="$lang" />
					<x-ll::form.editor id="{{ 'description' . $lang }}" label="{{ 'description' }}" name="translations[{{ $lang }}][description]" :info="$lang" />
				</div>
			@endforeach
		</x-ll::backend.form.translatable>

		<footer>
			<a href="{{ route('backend.pages.index') }}" role="button">@lang('laravel-launcher::action.cancel')</a>
			<button type="submit" class="primary">@lang('laravel-launcher::action.create')</button>
		</footer>

	</form>
@endsection
