<x-ll::backend.search>

	<x-ll::form.select id="searchEvent" name="searchEvent" label="event" :options="$events" wire:model="search.event" />

	<x-ll::form.select id="searchModel" name="searchModel" label="model" :options="$models" wire:model="search.model" />

</x-ll::backend.search>
