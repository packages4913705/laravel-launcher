@extends('laravel-launcher::layouts.backend')

@section('content')
	@livewire('datalist', [
	    'session' => 'logs',
	    'model' => 'activity-log',
	    'path' => 'laravel-launcher::backend.activity-logs.includes',
	    'filters' => ['today', 'week', 'month', 'last-month'],
	    'columns' => ['event', 'by', 'description'],
	    'sorting' => ['event'],
	    'sort' => ['field' => 'created_at', 'direction' => 'desc'],
	    'hasAdvancedSearch' => true,
	])
@endsection
