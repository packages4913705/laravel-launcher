@extends('laravel-launcher::layouts.backend')

@section('grid')
	<div class="card col-wider">
		<x-ll::svg class="card-icon" icon="filetype-json" />
		<h2>@choice('laravel-launcher::field.code', 1)</h2>
		<pre><code>{{ json_encode($log->value, JSON_PRETTY_PRINT) }}</code></pre>
	</div>

	<x-ll::backend.information :item="$log">
		@if ($log->model)
			<dt>@choice('laravel-launcher::field.model', 1)</dt>
			<dd>{{ translate($log->model, 'model') }} {{ '#' . $log->loggable_id }}</dd>
		@endif

		@if ($log->relation)
			<dt>@choice('laravel-launcher::field.relation', 1)</dt>
			<dd>{{ translate($log->relation, 'model') }}</dd>
		@endif

		<dt>@choice('laravel-launcher::field.by', 1)</dt>
		<dd>{{ $log->by }}</dd>

		@if ($log->comment)
			<dt>@choice('laravel-launcher::field.comment', 1)</dt>
			<dd>{{ $log->comment }}</dd>
		@endif
	</x-ll::backend.information>
@endsection
