@extends('laravel-launcher::layouts.backend')

@section('content')
	<form action="{{ route('backend.settings.update') }}" method="POST">

		@csrf
		@method('PATCH')

		@if (config('laravel-launcher.settings.seo'))
			<x-ll::backend.card icon="graph-up-arrow" model="seo">
				<x-ll::form.input id="seoDescription" label="description" name="settings[seo][description]" :value="$settings['seo']['description'] ?? null" description="seo" />
			</x-ll::backend.card>
		@endif

		@if (config('laravel-launcher.settings.social-network'))
			<x-ll::backend.card icon="share" model="social-network">
				<x-ll::form.input id="socialFacebook" label="Facebook" name="settings[social-network][facebook]" type="url" :value="$settings['social-network']['facebook'] ?? null" />
				<x-ll::form.input id="socialInstagram" label="Instagram" name="settings[social-network][instagram]" type="url" :value="$settings['social-network']['instagram'] ?? null" />
				<x-ll::form.input id="socialLinkedin" label="Linkedin" name="settings[social-network][linkedin]" type="url" :value="$settings['social-network']['linkedin'] ?? null" />
				<x-ll::form.input id="socialTiktok" label="Tiktok" name="settings[social-network][tiktok]" type="url" :value="$settings['social-network']['tiktok'] ?? null" />
				<x-ll::form.input id="socialTwitterX" label="X (Twitter)" name="settings[social-network][twitter-x]" type="url" :value="$settings['social-network']['twitter-x'] ?? null" />
				<x-ll::form.input id="youtube" label="Youtube" name="settings[social-network][youtube]" type="url" :value="$settings['social-network']['youtube'] ?? null" />
			</x-ll::backend.card>
		@endif

		@if (config('laravel-launcher.settings.contact'))
			<x-ll::backend.card icon="chat" model="contact">
				<x-ll::form.input id="contactPhone" label="phone" name="settings[contact][phone]" type="tel" :value="$settings['contact']['phone'] ?? null" />
				<x-ll::form.input id="contactEmail" label="email" name="settings[contact][email]" type="email" :value="$settings['contact']['email'] ?? null" />
				<x-ll::form.input id="contactAddress1" label="address" name="settings[contact][address][line-1]" type="text" :value="$settings['contact']['address']['line-1'] ?? null" />
				<x-ll::form.input id="contactAddress2" label="address" name="settings[contact][address][line-2]" type="text" :value="$settings['contact']['address']['line-2'] ?? null" />
				<x-ll::form.input id="contactZip" label="zip" name="settings[contact][address][zip]" type="text" :value="$settings['contact']['address']['zip'] ?? null" />
				<x-ll::form.input id="contactCity" label="city" name="settings[contact][address][city]" type="text" :value="$settings['contact']['address']['city'] ?? null" />
				<x-ll::form.input id="contactMap" label="map" name="settings[contact][map]" type="url" :value="$settings['contact']['map'] ?? null" />
			</x-ll::backend.card>
		@endif

		<footer>
			<a href="{{ route('backend.settings.index') }}" role="button">@lang('laravel-launcher::action.cancel')</a>
			<button type="submit" class="primary">@lang('laravel-launcher::action.update')</button>
		</footer>

	</form>
@endsection
