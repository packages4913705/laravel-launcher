@extends('laravel-launcher::layouts.backend')

@section('buttons')
	@can('update', Nh\LaravelLauncher\Models\Setting::class)
		<a class="primary" href="{{ route('backend.settings.edit') }}" role="button">
			<x-ll::svg icon="pencil" />
			@lang('laravel-launcher::action.edit')
		</a>
	@endcan
@endsection

@section('grid')
	@foreach ($settings as $setting)
		@if (config('laravel-launcher.settings.' . $setting->name) && array_filter($setting->value))
			@php
				switch ($setting->name) {
				    case 'seo':
				        $icon = 'graph-up-arrow';
				        break;

				    case 'contact':
				        $icon = 'chat';
				        break;

				    case 'social-network':
				        $icon = 'share';
				        break;

				    default:
				        $icon = null;
				        break;
				}
			@endphp

			<x-ll::backend.card :icon="$icon" :model="$setting->name">
				<dl>
					@foreach ($setting->value as $key => $value)
						@isset($value)
							<dt>
								{{ translate($key, 'field') }}
							</dt>
							<dd>

								@if (is_array($value))
									@php
										$values = $key === 'address' ? array_replace(array_flip(['line-1', 'line-2', 'zip', 'city']), array_filter($value)) : array_filter($value);
									@endphp

									{{ implode(' - ', $values) }}
								@else
									{{ $value ?? '-' }}
								@endif
							</dd>
						@endisset
					@endforeach
				</dl>
			</x-ll::backend.card>
		@endif
	@endforeach
@endsection
