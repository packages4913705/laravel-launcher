@extends('laravel-launcher::layouts.auth')

@section('title', trans('laravel-launcher::auth.forgot-your-password'))

@section('content')
	<form method="POST" action="{{ route('password.send') }}" no-validate>

		@csrf

		<div class="card">
			@if (session('success'))
				<p role="alert">{{ session('success') }}</p>
			@else
				<x-ll::form.input id="email" label="email" name="email" type="email" />
				<button type="submit" class="primary">@lang('laravel-launcher::action.send')</button>
			@endif
		</div>

	</form>
@endsection
