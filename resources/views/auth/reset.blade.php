@extends('laravel-launcher::layouts.auth')

@section('title', trans('laravel-launcher::auth.reset-your-password'))

@section('content')
	<form method="POST" action="{{ route('password.update') }}" no-validate>

		@csrf

		<div class="card">
			@if (session('success'))
				<p role="alert">{{ session('success') }}</p>
			@else
				<input type="hidden" name="token" value="{{ request()->route('token') }}">

				<x-ll::form.input id="email" label="email" name="email" type="email" :value="$email" />

				<x-ll::form.input id="password" label="password" name="password" type="password" />

				<x-ll::form.input id="passwordConfirmation" label="password-confirmation" name="password_confirmation" type="password" />

				<button type="submit" class="primary">@lang('laravel-launcher::action.update')</button>
			@endif
		</div>

	</form>
@endsection
