@extends('laravel-launcher::layouts.auth')

@section('title', trans('laravel-launcher::action.login'))

@section('content')
	<form method="POST" action="{{ route('authenticate') }}" no-validate>

		@csrf

		<div class="card">

			@session('success')
				<p role="alert">{{ $value }}</p>
			@endsession

			<x-ll::form.input id="email" label="email" name="email" type="email" />

			<x-ll::form.input id="password" label="password" name="password" type="password" />

			@if (Route::has('password.forgot'))
				<small>
					<a href="{{ route('password.forgot') }}">
						@lang('laravel-launcher::auth.forgot-your-password')
					</a>
				</small>
			@endif

			<button type="submit" class="primary">@lang('laravel-launcher::action.login')</button>

		</div>
	</form>
@endsection
