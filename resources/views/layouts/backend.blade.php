<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
			@if ($errors->any())
				@choice('laravel-launcher::form.error', count($errors)) ||
			@endif
			@yield('head-title', $layout['title'] ?? config('app.name'))
		</title>

		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300..800;1,300..800&display=swap" rel="stylesheet">

		@vite('resources/scss/backend.scss')
		@vite('resources/js/backend.js')
		<script>
			window.translations = @json(Lang::get('laravel-launcher::javascript'));
		</script>
	</head>

	<body data-preload>

		<a class="skip" href="#content">@lang('laravel-launcher::action.skip-to-content')</a>

		<div id="backdrop"></div>

		@include('laravel-launcher::includes.partials.header')

		@include('laravel-launcher::includes.partials.sidebar')

		<main>

			@include('laravel-launcher::includes.partials.breadcrumb')

			<header id="content">
				<h1>
					<span>
						@yield('model', $layout['model'] ?? '')
					</span>
					@yield('title', $layout['h1'] ?? '')
				</h1>
				@hasSection('buttons')
					<div>
						@yield('buttons')
					</div>
				@endif
			</header>

			@if ($errors->any())
				<x-ll::backend.card class="card-error" :title="trans_choice('laravel-launcher::form.error', count($errors))" icon="exclamation-circle">
					<ol>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ol>
				</x-ll::backend.card>
			@endif

			@yield('content')

			@hasSection('grid')
				<div id="contentGrid" class="flex-grid">
					@yield('grid')
				</div>
			@endif

			@hasSection('dashboard')
				<div id="dashboardGrid" class="flex-grid">
					@yield('dashboard')
				</div>
			@endif

		</main>

		@include('laravel-launcher::includes.dialogs.crud')
		@include('laravel-launcher::includes.dialogs.notification')
		@include('laravel-launcher::includes.dialogs.editor.link')
		@include('laravel-launcher::includes.dialogs.editor.abbr')
		@include('laravel-launcher::includes.dialogs.editor.help')

	</body>

</html>
