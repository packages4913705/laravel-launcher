<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>@yield('title') : {{ config('app.name') }}</title>

		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100..900&display=swap" rel="stylesheet">

		@vite('resources/scss/auth.scss')
		@vite('resources/js/auth.js')
	</head>

	<body>

		<main>
			<nav>
				<ul>
					@if (count(config('laravel-launcher.languages')) > 1)
						<li>
							<div class="dropdown">
								<button aria-controls="languageDropdown" aria-expanded="false" aria-pressed="false" aria-label="@lang('laravel-launcher::backend.topbar.languages')">
									<x-ll::svg icon="translate" />
									<span>
										{{ config('language.' . App::getLocale()) }}
									</span>
								</button>
								<ul id="languageDropdown" tabindex="0" hidden>
									@foreach (config('laravel-launcher.languages') as $lang)
										<li>
											<a href="{{ route('language.change', $lang) }}" role="button" @if (App::getLocale() == $lang) aria-current="true" @endif>
												{{ config('language.' . $lang) }}
											</a>
										</li>
									@endforeach
								</ul>
							</div>
						</li>
					@endif
					<li>
						<button class="toggle-theme" data-theme value="light">
							<x-ll::svg icon="sun" />
							<span>@lang('laravel-launcher::backend.topbar.theme-light')</span>
						</button>
						<button class="toggle-theme" data-theme value="dark">
							<x-ll::svg icon="moon-stars" />
							<span>@lang('laravel-launcher::backend.topbar.theme-dark')</span>
						</button>
					</li>
				</ul>
			</nav>
			<section>
				<h1>
					<span>
						@lang('laravel-launcher::backend.admin')
					</span>
					@yield('title')
				</h1>
				@yield('content')
			</section>
			<footer>
				@lang('laravel-launcher::backend.sidebar.copyright')<br>
				Copyright © 2024-{{ now()->year }} <a href="https://natachaherth.ch">Natacha Herth</a>
			</footer>
		</main>

		<picture>
			<img src="https://picsum.photos/1080?grayscale" alt="Picture from https://picsum.photos/">
		</picture>

	</body>

</html>
