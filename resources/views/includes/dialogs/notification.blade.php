@php
	$notificationType = null;
	$notificationMsg = null;
	if (Session::has('success')) {
	    $notificationType = 'success';
	    $notificationMsg = Session::get('success');
	} elseif (Session::has('error')) {
	    $notificationType = 'error';
	    $notificationMsg = Session::get('error');
	} elseif (Session::has('warning')) {
	    $notificationType = 'warning';
	    $notificationMsg = Session::get('warning');
	}
@endphp

<dialog id="notificationDialog" class="{{ $notificationType }}" @if ($notificationType && $notificationMsg) open @endif>
	<div id="notificationTimer"></div>
	<div id="notificationContent">
		<p>
			{!! $notificationMsg !!}
		</p>
		<button class="rounded" data-dialog-close aria-label="@lang('laravel-launcher::action.close')">
			<x-ll::svg icon="x-lg" />
		</button>
	</div>
</dialog>
