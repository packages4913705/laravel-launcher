<dialog id="editorLinkDialog" aria-modal="true">

	<button class="close-x" data-dialog-close aria-label="@lang('laravel-launcher::action.close')" title="@lang('laravel-launcher::action.close')">
		<x-ll::svg icon="x-lg" />
	</button>

	<form method="dialog">

		<header>
			<h2>@lang('laravel-launcher::editor.link.toggle')</h2>
		</header>

		<x-ll::form.select id="editorLinkType" label="type" name="tiptap_link_type">
			<option value="url">@choice('laravel-launcher::field.link', 1)</option>
			<option value="email">@choice('laravel-launcher::field.email', 1)</option>
			<option value="tel">@choice('laravel-launcher::field.phone', 1)</option>
		</x-ll::form.select>

		<x-ll::form.input id="editorLinkUrl" label="url" name="tiptap_link_url" />

		<fieldset>
			<legend>@choice('laravel-launcher::field.option', 1)</legend>
			<x-ll::form.switcher id="editorLinkTarget" :label="trans('laravel-launcher::editor.link.target-blank')" name="tiptap_link_target" />
		</fieldset>

		<footer>
			<button type="reset" data-dialog-close>@lang('laravel-launcher::action.cancel')</button>
			<button class="primary" type="submit">@lang('laravel-launcher::action.save')</button>
		</footer>

	</form>
</dialog>
