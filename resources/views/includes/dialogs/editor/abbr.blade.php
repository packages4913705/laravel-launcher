<dialog id="editorAbbrDialog" aria-modal="true">

	<button class="close-x" data-dialog-close aria-label="@lang('laravel-launcher::action.close')" title="@lang('laravel-launcher::action.close')">
		<x-ll::svg icon="x-lg" />
	</button>

	<form method="dialog">

		<header>
			<h2>@lang('laravel-launcher::editor.abbr.toggle')</h2>
		</header>

		<p>@lang('laravel-launcher::editor.abbr-description')</p>

		<x-ll::form.input id="editorAbbrTitle" label="description" name="tiptap_abbr_title" />

		<footer>
			<button type="reset" data-dialog-close>@lang('laravel-launcher::action.cancel')</button>
			<button class="primary" type="submit">@lang('laravel-launcher::action.save')</button>
		</footer>

	</form>
</dialog>
