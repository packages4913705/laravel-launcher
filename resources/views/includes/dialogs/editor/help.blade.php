<dialog id="editorHelpDialog" aria-modal="true">

	<button class="close-x" data-dialog-close aria-label="@lang('laravel-launcher::action.close')" title="@lang('laravel-launcher::action.close')">
		<x-ll::svg icon="x-lg" />
	</button>

	<h2>@lang('laravel-launcher::editor.help')</h2>

	<table>
		<thead>
			<tr>
				<th>@choice('laravel-launcher::field.description', 1)</th>
				<th>Windows/Linux</th>
				<th>macOS</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>@lang('laravel-launcher::editor.br')</td>
				<td><kbd>Shift</kbd> + <kbd>Enter</kbd>{{-- <kbd>Control</kbd> + <kbd>Enter</kbd> --}}</td>
				<td><kbd>Shift</kbd> + <kbd>Enter</kbd>{{-- <kbd>Cmd</kbd> + <kbd>Enter</kbd> --}}</td>
			</tr>
			<tr>
				<td>@lang('laravel-launcher::editor.bold')</td>
				<td><kbd>Control</kbd> + <kbd>B</kbd></td>
				<td><kbd>Cmd</kbd> + <kbd>B</kbd></td>
			</tr>
			<tr>
				<td>@lang('laravel-launcher::editor.italic')</td>
				<td><kbd>Control</kbd> + <kbd>I</kbd></td>
				<td><kbd>Cmd</kbd> + <kbd>I</kbd></td>
			</tr>
			<tr>
				<td>@lang('laravel-launcher::editor.underline')</td>
				<td><kbd>Control</kbd> + <kbd>U</kbd></td>
				<td><kbd>Cmd</kbd> + <kbd>U</kbd></td>
			</tr>
			<tr>
				<td>@lang('laravel-launcher::editor.strike')</td>
				<td><kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>S</kbd></td>
				<td><kbd>Cmd</kbd> + <kbd>Shift</kbd> + <kbd>S</kbd></td>
			</tr>
			<tr>
				<td>@lang('laravel-launcher::editor.paragraph')</td>
				<td><kbd>Control</kbd> + <kbd>Alt</kbd> + <kbd>0</kbd></td>
				<td><kbd>Cmd</kbd> + <kbd>Opt</kbd> + <kbd>0</kbd></td>
			</tr>
		</tbody>
	</table>

	<button class="primary" data-dialog-close>
		@lang('laravel-launcher::action.close')
	</button>

</dialog>
