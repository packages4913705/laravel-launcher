<dialog id="dataDeleteDialog" aria-modal="true">
	<form action="#" method="POST">
		@method('delete')
		@csrf
		<h1>@lang('laravel-launcher::action.confirm')</h1>
		<p>@lang('laravel-launcher::backend.dialog.delete.content')</p>
		<button type="reset" data-dialog-close>@lang('laravel-launcher::action.cancel')</button>
		<button class="primary" type="submit">@lang('laravel-launcher::backend.dialog.delete.confirm')</button>
	</form>
</dialog>

<dialog id="dataRestoreDialog" aria-modal="true">
	<form action="#" method="POST">
		@method('patch')
		@csrf
		<h1>@lang('laravel-launcher::action.confirm')</h1>
		<p>@lang('laravel-launcher::backend.dialog.restore.content')</p>
		<button type="reset" data-dialog-close>@lang('laravel-launcher::action.cancel')</button>
		<button class="primary" type="submit">@lang('laravel-launcher::backend.dialog.restore.confirm')</button>
	</form>
</dialog>

<dialog id="dataDestroyDialog" aria-modal="true">
	<form action="#" method="POST">
		@method('delete')
		@csrf
		<h1>@lang('laravel-launcher::action.confirm')</h1>
		<p>@lang('laravel-launcher::backend.dialog.destroy.content')</p>
		<button type="reset" data-dialog-close>@lang('laravel-launcher::action.cancel')</button>
		<button class="primary" type="submit">@lang('laravel-launcher::backend.dialog.destroy.confirm')</button>
	</form>
</dialog>
