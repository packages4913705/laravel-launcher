@php

	switch ($type) {
	    case 'update':
	        $name = 'media_to_update';
	        $item = $itemToUpdate;
	        break;

	    case 'delete':
	        $name = 'media_to_delete';
	        $item = $itemToDelete;
	        break;

	    default:
	        $name = 'media_to_add';
	        $item = $itemToAdd;
	        break;
	}

	$inputName = $name . '[' . ($misc['type'] ?? 'picture') . '][' . $index . ']';
	$labelFiles = ['field', 'form', 'model'];

@endphp

@if (in_array($type, ['update', 'delete']))
	<input type="hidden" name="{{ $inputName . '[id]' }}" value="{{ $item['id'] }}">
@endif

<div>
	<x-ll::form.input aria-label="{{ translate('label', $labelFiles) }}" id="{{ $name . '_label_' . $index }}" label="label" name="{{ $inputName . '[label]' }}" :value="$item['label'] ?? null" :readonly="$type == 'delete'" />
</div>

<div>
	<x-ll::form.input aria-label="{{ translate('description', $labelFiles) }}" id="{{ $name . '_description_' . $index }}" label="description" name="{{ $inputName . '[description]' }}" :value="$item['description'] ?? null" :readonly="$type == 'delete'" />
</div>

<div>
	<x-ll::form.input aria-label="{{ translate('file', $labelFiles) }}" id="{{ $name . '_file_' . $index }}" label="file" name="{{ $inputName . ($type == 'add' ? '[file]' : '[filename]') }}" :value="$item['filename'] ?? null" :type="$type == 'add' ? 'file' : 'text'" :readonly="$type != 'add'" aria-describedby="description{{ $dynID }}" />
</div>
