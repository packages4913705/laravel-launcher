<tr wire:key="{{ $item->id }}" id="{{ 'row' . $item->id }}" @if ($hasPosition) draggable="false" @endif @if ($hasLevel && $levelable) aria-level="{{ $item->level + 1 ?? 1 }}" @if ($item->children->count()) aria-controls="{{ $item->getChildrenString('row') }}" aria-expanded="false" @endif {{ $item->level ? 'hidden' : '' }} @endif>

	@if ($hasPosition)
		<td class="fit" @if (!$draggable) hidden @endif aria-hidden="true">
			<span data-handle="sortable" aria-label="@lang('laravel-launcher::action.drag-drop')">
				<x-ll::svg icon="grip-vertical" />
			</span>
		</td>
	@endif

	@if ($hasLevel)
		<td class="fit" @if (!$levelable) hidden @endif>
			@if ($item->children->count())
				<button type="button" data-handle="tree" aria-label="@lang('laravel-launcher::action.toggle')">
					<x-ll::svg icon="chevron-right" />
				</button>
			@endif
		</td>
	@endif

	<td class="fit align-center">{{ '#' . $item->id }}</td>

	@if ($hasPosition)
		<td class="fit align-center">{{ $item->position }}</td>
	@endif

	@includeIf($path . '.listing')

	<td class="fit">{{ $item->updated_at->format(config('laravel-launcher.formats.datetime')) }}</td>

	<td class="fit align-right">

		@if ($draggable)
			<button class="rounded" type="button" wire:click="goUp({{ $loop->index }})" aria-label="@lang('laravel-launcher::action.move-up')" title="@lang('laravel-launcher::action.move-up')" @if ($loop->first) disabled @endif><x-ll::svg icon="chevron-up" /></button>
			<button class="rounded" type="button" wire:click="goDown({{ $loop->index }})" aria-label="@lang('laravel-launcher::action.move-down')" title="@lang('laravel-launcher::action.move-down')" @if ($loop->last) disabled @endif><x-ll::svg icon="chevron-down" /></button>
		@else
			@includeIf($path . '.button')

			@can('view', $item)
				<a class="rounded" href="{{ route($route . '.show', $item->id) }}" role="button" aria-label="@lang('laravel-launcher::action.view')" title="@lang('laravel-launcher::action.view')">
					<x-ll::svg icon="eye" />
				</a>
			@endcan
			@can('update', $item)
				<a class="rounded" href="{{ route($route . '.edit', $item->id) }}" role="button" aria-label="@lang('laravel-launcher::action.edit')" title="@lang('laravel-launcher::action.edit')">
					<x-ll::svg icon="pencil" />
				</a>
			@endcan
			@can('delete', $item)
				<button class="rounded" wire:click="openDialog('delete',{{ $item->id }})" aria-label="@lang('laravel-launcher::action.delete')" title="@lang('laravel-launcher::action.delete')">
					<x-ll::svg icon="trash" />
				</button>
			@endcan
			@can('restore', $item)
				<button class="rounded" wire:click="openDialog('restore',{{ $item->id }})" aria-label="@lang('laravel-launcher::action.restore')" title="@lang('laravel-launcher::action.restore')">
					<x-ll::svg icon="arrow-counterclockwise" />
				</button>
			@endcan
			@can('destroy', $item)
				<button class="rounded" wire:click="openDialog('destroy',{{ $item->id }})" aria-label="@lang('laravel-launcher::action.delete')" title="@lang('laravel-launcher::action.delete')">
					<x-ll::svg icon="trash" />
				</button>
			@endcan
		@endif
	</td>

</tr>

@if ($levelable && $item->children->count())
	@foreach ($item->children as $child)
		@include('laravel-launcher::includes.livewire.datalist-row', ['item' => $child])
	@endforeach
@endif
