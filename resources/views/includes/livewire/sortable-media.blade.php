<b>{{ $item->label }}</b><br>
<small><i>{{ $item->filename }}</i> {{ $item->description ? ' - ' . $item->description : '' }}</small>
