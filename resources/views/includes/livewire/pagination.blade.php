@php
if (! isset($scrollTo)) {
    $scrollTo = 'body';
}

$scrollIntoViewJsSnippet = ($scrollTo !== false)
    ? <<<JS
       (\$el.closest('{$scrollTo}') || document.querySelector('{$scrollTo}')).scrollIntoView()
    JS
    : '';
@endphp

<small>
{{ $paginator->count().'/'.$paginator->total() }}
</small>

@if ($paginator->hasPages())
<nav class="group">
    @if (!$paginator->onFirstPage())
        <button type="button" dusk="previousPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}" wire:click="previousPage('{{ $paginator->getPageName() }}')" x-on:click="{{ $scrollIntoViewJsSnippet }}" wire:loading.attr="disabled" rel="prev" aria-label="@lang('pagination.previous')">
            <x-ll::svg icon="chevron-left" />
        </button>
    @endif

    @foreach ($elements as $element)

    @if (is_string($element))
        <button aria-disabled="true">{{ $element }}</button>
    @endif

    @if (is_array($element))
        @foreach ($element as $page => $url)
            @if ($page == $paginator->currentPage())
                <button wire:key="paginator-{{ $paginator->getPageName() }}-page-{{ $page }}" aria-current="page">{{ $page }}</button>
            @else
                <button type="button" wire:key="paginator-{{ $paginator->getPageName() }}-page-{{ $page }}" wire:click="gotoPage({{ $page }}, '{{ $paginator->getPageName() }}')" x-on:click="{{ $scrollIntoViewJsSnippet }}"wire:click="gotoPage({{ $page }}, '{{ $paginator->getPageName() }}')" x-on:click="{{ $scrollIntoViewJsSnippet }}">{{ $page }}</button>
            @endif
        @endforeach
    @endif
    @endforeach      

    @if($paginator->hasMorePages())
        <button type="button" dusk="nextPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}" wire:click="nextPage('{{ $paginator->getPageName() }}')" x-on:click="{{ $scrollIntoViewJsSnippet }}" wire:loading.attr="disabled" rel="next" aria-label="@lang('pagination.next')">
            <x-ll::svg icon="chevron-right" />
        </button>
    @endif

</nav>
@endif
