@if ($value)
	<span class="badge outline success">
		<x-ll::svg icon="check-circle" />
		@lang('laravel-launcher::field.yes')
	</span>
@else
	<span class="badge outline error">
		<x-ll::svg icon="x-circle" />
		@lang('laravel-launcher::field.no')
	</span>
@endif
