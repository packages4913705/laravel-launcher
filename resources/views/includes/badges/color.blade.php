@if ($color)
	<span class="badge {{ $color }}">
		{{ translate($color, 'color') }}
	</span>
@else
	-
@endif
