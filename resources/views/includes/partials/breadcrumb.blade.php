@if (count($crumbs))
	<nav aria-label="@lang('laravel-launcher::backend.topbar.breadcrumb')">
		<ol class="breadcrumb">
			@foreach ($crumbs as $crumb)
				<li @if ($loop->last) aria-current="location" @endif>
					@if ($crumb['url'])
						<a href="{{ $crumb['url'] }}">{{ $crumb['label'] }}</a>
					@else
						{{ $crumb['label'] }}
					@endif
				</li>
			@endforeach
		</ol>
	</nav>
@endif
