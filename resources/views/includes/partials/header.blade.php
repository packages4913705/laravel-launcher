<header>
	<button class="drawer-button" aria-expanded="false" aria-pressed="false" aria-controls="sidebar" aria-label="@lang('laravel-launcher::action.toggle-menu')" title="@lang('laravel-launcher::action.toggle-menu')">
		<svg aria-hidden="true" focusable="false" viewbox="0 0 100 100" width="100%">
			<rect fill="currentColor" width="100" height="10" x="0" y="10" rx="0"></rect>
			<rect fill="currentColor" width="100" height="10" x="0" y="45" rx="0"></rect>
			<rect fill="currentColor" width="100" height="10" x="0" y="80" rx="0"></rect>
		</svg>
	</button>
	<nav aria-label="@lang('laravel-launcher::backend.topbar.nav')">
		<ul>
			@if (count(config('laravel-launcher.languages')) > 1)
				<li>
					<div class="dropdown">
						<button aria-controls="languageDropdown" aria-expanded="false" aria-pressed="false" aria-label="@lang('laravel-launcher::backend.topbar.languages')">
							<x-ll::svg icon="translate" />
							<span>
								{{ config('language.' . App::getLocale()) }}
							</span>
						</button>
						<ul id="languageDropdown" tabindex="0" hidden>
							@foreach (config('laravel-launcher.languages') as $lang)
								<li>
									<a href="{{ route('language.change', $lang) }}" role="button" @if (App::getLocale() == $lang) aria-current="true" @endif>
										{{ config('language.' . $lang) }}
									</a>
								</li>
							@endforeach
						</ul>
					</div>
				</li>
			@endif
			<li>
				<button class="toggle-theme" data-theme value="light" aria-label="@lang('laravel-launcher::backend.topbar.theme-light')">
					<x-ll::svg icon="sun" />
					<span>@lang('laravel-launcher::backend.topbar.theme-light')</span>
				</button>
				<button class="toggle-theme" data-theme value="dark" aria-label="@lang('laravel-launcher::backend.topbar.theme-dark')">
					<x-ll::svg icon="moon-stars" />
					<span>@lang('laravel-launcher::backend.topbar.theme-dark')</span>
				</button>
			</li>
			<li>
				<a href="{{ url('/') }}" role="button" aria-label="@lang('laravel-launcher::backend.topbar.website')">
					<x-ll::svg icon="rocket-takeoff" />
					<span>@lang('laravel-launcher::backend.topbar.website')</span>
				</a>
			</li>
			<li>
				<form action="{{ route('logout') }}" method="POST">
					@csrf
					<button type="submit" aria-label="@lang('laravel-launcher::backend.topbar.logout')">
						<x-ll::svg icon="power" />
						<span>@lang('laravel-launcher::backend.topbar.logout')</span>
					</button>
				</form>
			</li>
		</ul>
	</nav>
</header>
