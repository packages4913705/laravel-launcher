<aside id="sidebar" class="drawer" tabindex="0" hidden>

	<header>
		<x-ll::svg icon="person-circle" />
		<p>
			<b>@lang('laravel-launcher::backend.sidebar.welcome', ['name' => Auth::user()->name])</b>
			<small>{{ Auth::user()->roles()->pluck('label')->implode(', ') }}</small>
		</p>
	</header>

	<nav aria-label="@lang('laravel-launcher::backend.sidebar.nav')">

		<ul>
			<li>
				<a href="{{ route('backend.dashboard') }}" @if (Route::is('backend.dashboard')) aria-current="page" @endif>
					@lang('laravel-launcher::backend.sidebar.dashboard')
				</a>
			</li>
			<li>
				<a href="{{ route('backend.users.edit', Auth::user()->id) }}" @if (Route::is('backend.users.edit') && request()->route('user') == Auth::user()) aria-current="page" @endif>
					@lang('laravel-launcher::backend.sidebar.edit-profile')
				</a>
			</li>
		</ul>

		<div>
			<b>@lang('laravel-launcher::backend.sidebar.website')</b>
			<ul>
				@if (Auth::user()->hasAccess('setting', 'view'))
					<li>
						<a href="{{ route('backend.settings.index') }}" @if (Route::is('backend.settings.*')) aria-current="page" @endif>
							@choice('laravel-launcher::model.setting', 2)
						</a>
					</li>
				@endif
				@if (Auth::user()->hasAccess('activity-log', 'view'))
					<li>
						<a href="{{ route('backend.activity-logs.index') }}" @if (Route::is('backend.activity-logs.*')) aria-current="page" @endif>
							@choice('laravel-launcher::model.activity-log', 2)
						</a>
					</li>
				@endif
			</ul>
		</div>

		<div>
			<b>@lang('laravel-launcher::backend.sidebar.access')</b>
			<ul>
				@if (Auth::user()->hasAccess('permission', 'view'))
					<li>
						<a href="{{ route('backend.permissions.index') }}" @if (Route::is('backend.permissions.*')) aria-current="page" @endif>
							@choice('laravel-launcher::model.permission', 2)
						</a>
					</li>
				@endif
				@if (Auth::user()->hasAccess('role', 'view'))
					<li>
						<a href="{{ route('backend.roles.index') }}" @if (Route::is('backend.roles.*')) aria-current="page" @endif>
							@choice('laravel-launcher::model.role', 2)
						</a>
					</li>
				@endif
				@if (Auth::user()->hasAccess('user', 'view'))
					<li>
						<a href="{{ route('backend.users.index') }}" @if (Route::is('backend.users.*')) aria-current="page" @endif>
							@choice('laravel-launcher::model.user', 2)
						</a>
					</li>
				@endif
			</ul>
		</div>

		@foreach (config('laravel-launcher.sidebar') as $sidebarKey => $sidebarLinks)
			<div>
				<b>@lang('laravel-launcher::backend.sidebar.' . $sidebarKey)</b>
				<ul>
					@foreach ($sidebarLinks as $link)
						@if (Auth::user()->hasAccess($link, 'view') && Route::has('backend.' . Str::pluralStudly($link) . '.index'))
							<li>
								<a href="{{ route('backend.' . Str::pluralStudly($link) . '.index') }}" @if (Route::is('backend.' . Str::pluralStudly($link) . '.*')) aria-current="page" @endif>
									{{ translate(Str::pluralStudly($link), 'model') }}
								</a>
							</li>
						@endif
					@endforeach
				</ul>
			</div>
		@endforeach

		<div>
			@lang('laravel-launcher::backend.sidebar.copyright')<br>
			Copyright © 2024-{{ now()->year }} <a href="https://natachaherth.ch">Natacha Herth</a>
		</div>
	</nav>

</aside>
