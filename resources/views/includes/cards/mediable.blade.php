@if (in_array('Nh\LaravelLauncher\Traits\Mediable', class_uses($item)) && $item->media->count())
	@foreach (config('laravel-launcher.page.mediable') as $type => $options)
		@php
			$mediaItems = $item->mediaByTypes($type);
			$mediaCount = $mediaItems->count();
		@endphp

		@if ($mediaCount)
			<x-ll::backend.card icon="file-earmark-richtext" :model="$mediaCount > 1 ? Str::pluralStudly($type) : $type">
				@livewire('sortable', [
				    'items' => $mediaItems,
				    'path' => 'laravel-launcher::includes.livewire.sortable-media',
				    'draggable' => Auth::user()->can('update', $item) && $mediaCount > 1,
				])
			</x-ll::backend.card>
		@endif
	@endforeach
@endif
