@if (in_array('Nh\LaravelLauncher\Traits\Loggable', class_uses($item)) && $item->activity_logs->count())
	<x-ll::backend.card icon="clock-history" model="activity-log">
		<ul class="list flush">
			@foreach ($item->activity_logs->take($take ?? 5) as $log)
				<li>
					<x-ll::svg :icon="$log->icon" />

					<span class="list-grow">
						<b>{{ $log->label }}</b>
						@if ($log->relation)
							{{ translate($log->relation, 'model') }}
						@endif
						<br>
						<small>@lang('laravel-launcher::field.by') {{ $log->by }} {{ $log->created_at->diffForHumans() }}</small>
					</span>

					@can('view', $log)
						<a class="rounded" href="{{ route('backend.activity-logs.show', $log->id) }}" role="button" aria-label="@lang('laravel-launcher::action.view')" title="@lang('laravel-launcher::action.view')">
							<x-ll::svg icon="eye" />
						</a>
					@endcan
				</li>
			@endforeach
		</ul>
	</x-ll::backend.card>
@endif
