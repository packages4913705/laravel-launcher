@if (in_array('Nh\LaravelLauncher\Traits\Roleable', class_uses($item)) && $item->roles->count())
	<x-ll::backend.card icon="key" :model="$item->roles->count() > 1 ? 'roles' : 'role'">
		<ul class="list flush">
			@foreach ($item->roles as $role)
				<li>
					<b class="list-grow">{{ $role->label }}</b>
					@can('view', $role)
						<a class="rounded" href="{{ route('backend.roles.show', $role->id) }}" role="button" aria-label="@lang('laravel-launcher::action.view')" title="@lang('laravel-launcher::action.view')">
							<x-ll::svg icon="eye" />
						</a>
					@endcan
				</li>
			@endforeach
		</ul>
	</x-ll::backend.card>
@endif
