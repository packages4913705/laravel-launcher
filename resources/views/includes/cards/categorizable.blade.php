@if (in_array('Nh\LaravelLauncher\Traits\Categorizable', class_uses($item)) && $item->categories->count())
	<x-ll::backend.card icon="tag" :model="$item->categories->count() > 1 ? 'categories' : 'category'">
		<ul class="list flush">
			@foreach ($item->categories as $category)
				<li>
					<b class="list-grow">{{ $category->label }}</b>
					@can('view', $category)
						<a class="rounded" href="{{ route('backend.categories.show', $category->id) }}" role="button" aria-label="@lang('laravel-launcher::action.view')" title="@lang('laravel-launcher::action.view')">
							<x-ll::svg icon="eye" />
						</a>
					@endcan
				</li>
			@endforeach
		</ul>
	</x-ll::backend.card>
@endif
