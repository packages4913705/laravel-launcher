	@php
		$childrenCount = $item->children ? $item->children->count() : 0;
	@endphp
	@if (in_array('Nh\LaravelLauncher\Traits\Levelable', class_uses($item)) && $childrenCount)
	<x-ll::backend.card icon="diagram-2" :title="trans('laravel-launcher::field.sub', ['item' => strtolower(translate($childrenCount > 1 ? getPluralModelName($item) : getSingleModelName($item), 'model'))])">
	@livewire('sortable', [
	    'items' => $item->children->sortBy('position'),
	    'draggable' => in_array('Nh\LaravelLauncher\Traits\Positionable', class_uses($item)) && Auth::user()->can('update', $item) && $childrenCount > 1,
	])
	</x-ll::backend.card>
	@endif
