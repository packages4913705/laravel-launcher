<dialog id="searchDialog" class="search-dialog" aria-modal="true">

	<header>
		<h2>@lang('laravel-launcher::backend.dialog.search.title')</h2>
	</header>

	@if ($options)

		@if ($options['colorable'] && $colors)
			<x-ll::form.select id="searchColor" name="searchColor" label="color" :options="$colors" wire:model="search.color" />
		@endif

		@if ($options['categorizable'])
			@if ($categories)
				<x-ll::form.select id="searchCategory" name="searchCategory" label="category" :options="$categories" wire:model="search.category" />
			@else
				<x-ll::form.input id="searchCategory" name="searchCategory" label="category" wire:model="search.category" />
			@endif
		@endif

		@if ($options['roleable'])
			@if ($roles)
				<x-ll::form.select id="searchRole" name="searchRole" label="role" :options="$roles" wire:model="search.role" />
			@else
				<x-ll::form.input id="searchRole" name="searchRole" label="role" wire:model="search.role" />
			@endif
		@endif

	@endif

	{{ $slot }}

	<footer>
		<button type="reset" data-dialog-close>
			@lang('laravel-launcher::action.cancel')
		</button>

		<button class="primary" data-dialog-close>
			<x-ll::svg icon="search" />
			@lang('laravel-launcher::action.search')
		</button>
	</footer>

</dialog>
