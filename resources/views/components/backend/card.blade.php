<div {{ $attributes->merge(['class' => 'card']) }}>

	@isset($icon)
		<x-ll::svg class="card-icon" :icon="$icon" />
	@endisset

	@if (!empty($title) || !empty($model))
		<h2>{{ $title ?? $model }}</h2>
	@endif

	{{ $slot }}

</div>
