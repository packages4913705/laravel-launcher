@if ($config && count($roles))
	<x-ll::backend.card>
		<fieldset>
			<legend>@choice('laravel-launcher::model.role', is_numeric($config) ? $config : 2)</legend>
			<input type="hidden" name="roleable" value="1">
			@foreach ($roles as $role)
				@if ($config === true || $config > 1)
					<x-ll::form.checkbox id="{{ 'role' . $role->id }}" :label="$role->label" name="roles[]" :value="$role->id" :default="$current" />
				@else
					<x-ll::form.radio id="{{ 'role' . $role->id }}" :label="$role->label" name="roles[]" :value="$role->id" :default="$current" />
				@endif
			@endforeach
		</fieldset>
	</x-ll::backend.card>
@endif
