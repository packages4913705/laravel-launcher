@if ($config && count($categories))
	<x-ll::backend.card>
		<input type="hidden" name="categorizable" value="1">
		@if ($config === true || $config > 1)
			<fieldset>
				<legend>@choice('laravel-launcher::model.category', is_numeric($config) ? $config : 2)</legend>
				<ul class="list-as-tree">
					@foreach ($categories as $category)
						@include('laravel-launcher::backend.categories.includes.checkbox')
					@endforeach
				</ul>
			</fieldset>
		@else
			<x-ll::form.select id="category" label="category" name="categories[]" :options="$categories" :value="$current" />
		@endif
	</x-ll::backend.card>
@endif
