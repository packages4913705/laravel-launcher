@if ($config && count($colors))
	<fieldset style="--form-border-size: 2px;">
		<legend>@choice('laravel-launcher::field.color', 1)</legend>
		<x-ll::form.radio id="noColor" :label="trans('laravel-launcher::color.default')" name="color" style="--form-border-color: currentColor; --form-active-background: currentColor;" />
		@foreach ($colors as $color)
			<x-ll::form.radio id="{{ $color . 'Color' }}" :label="translate($color, 'color')" name="color" :value="$color" :default="$current" style="--form-border-color: var(--color-{{ $color }}); --form-active-background: var(--color-{{ $color }}); --form-active-border-color: var(--color-{{ $color }}); " />
		@endforeach
	</fieldset>
@endif
