@if ($config && count($parents))
	<x-ll::form.select id="parent" label="parent" name="parent_id" :options="$parents" :value="$current" />
@endif
