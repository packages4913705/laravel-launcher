@if ($config && count($languages))
	<x-ll::backend.card>
		<fieldset>
			<legend>@choice('laravel-launcher::model.translation', 2)</legend>
			<div class="translations">
				<div class="group" role="tablist">
					@foreach ($languages as $lang)
						<button type="button" role="tab" aria-controls="{{ $lang }}">{{ config('language.' . $lang) }}</button>
					@endforeach
				</div>
				{{ $slot }}
			</div>
		</fieldset>
	</x-ll::backend.card>
@endif
