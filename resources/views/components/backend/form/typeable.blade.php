@if ($config && count($types))
	<x-ll::form.select id="type" label="type" name="type">
		@foreach ($types as $type)
			<option value="{{ $type }}" @selected(old('type', $current) == $type)>
				{{ translate($type, ['form.filters', 'form.types', 'field', 'model']) }}
			</option>
		@endforeach
	</x-ll::form.select>
@endif
