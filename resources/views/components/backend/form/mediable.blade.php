@if ($config)
	@foreach ($config as $type => $options)
		<x-ll::backend.card>
			@livewire('dynamic', [
			    'legend' => translate(isset($options['max']) && $options['max'] == 1 ? $type : Str::pluralStudly($type), 'field'),
			    'path' => 'laravel-launcher::includes.livewire.dynamic-media',
			    'min' => $options['min'] ?? null,
			    'max' => $options['max'] ?? null,
			    'items' => [
			        'add' => old('media_to_add.' . $type, []),
			        'update' => (session()->hasOldInput() ? old('media_to_update.' . $type, []) : !empty($current)) ? $current->whereIn('type', (array) $type)->toArray() : [],
			        'delete' => old('media_to_delete.' . $type, []),
			    ],
			    'misc' => [
			        'type' => $type,
			    ],
			    'description' => $getHelperMessage($options),
			    'draggable' => true,
			])
		</x-ll::backend.card>
	@endforeach
@endif
