<x-ll::backend.card icon="info-circle" model="information">
	<dl>

		@if ($item->id)
			<dt>@choice('laravel-launcher::field.id', 1)</dt>
			<dd>{{ $item->id }}</dd>
		@endif

		@if (Auth::user()->has_superpowers && $item->name)
			<dt>@choice('laravel-launcher::field.guard-name', 1)</dt>
			<dd>{{ $item->name }}</dd>
		@endif

		@if ($item->created_at)
			<dt>@choice('laravel-launcher::field.created-at', 1)</dt>
			<dd>{{ $item->created_at->format(config('laravel-launcher.formats.datetime')) }}</dd>
		@endif

		@if ($item->updated_at)
			<dt>@choice('laravel-launcher::field.updated-at', 1)</dt>
			<dd>{{ $item->updated_at->format(config('laravel-launcher.formats.datetime')) }}</dd>
		@endif

		@if ($item->deleted_at)
			<dt>@choice('laravel-launcher::field.deleted-at', 1)</dt>
			<dd>{{ $item->deleted_at->format(config('laravel-launcher.formats.datetime')) }}</dd>
		@endif

		@if ($item->label)
			<dt>@choice('laravel-launcher::field.label', 1)</dt>
			<dd>{{ $item->label }}</dd>
		@endif

		@if ($item->slug)
			<dt>@choice('laravel-launcher::field.slug', 1)</dt>
			<dd>{{ $item->slug }}</dd>
		@endif

		@if ($item->type)
			<dt>@choice('laravel-launcher::field.type', 1)</dt>
			<dd>{{ translate($item->type, ['form.filters', 'form.types', 'field', 'model']) }}</dd>
		@endif

		@if ($item->color)
			<dt>@choice('laravel-launcher::field.color', 1)</dt>
			<dd>@include('laravel-launcher::includes.badges.color', ['color' => $item->color])</dd>
		@endif

		@if ($item->parent_id)
			<dt>@choice('laravel-launcher::field.parent', 1)</dt>
			<dd>{{ $item->parent->label ?? ($item->parent->title ?? $item->parent->name) }}</dd>
		@endif

		@isset($item->published)
			<dt>@choice('laravel-launcher::field.published', 1)</dt>
			<dd>@include('laravel-launcher::includes.badges.boolean', ['value' => $item->published])</dd>
		@endisset

		{{ $slot }}

	</dl>
</x-ll::backend.card>
