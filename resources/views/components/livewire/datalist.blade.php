@section('buttons')
	@includeIf($path . '.action')
	@can('create', $model)
		<a class="primary" href="{{ route($route . '.create') }}" role="button">
			<x-ll::svg icon="plus-lg" />
			@lang('laravel-launcher::action.create')
		</a>
	@endcan
@endsection

<div>

	@if ($hasSearch)
		<search>
			<form wire:submit="runSearch">

				<div class="group">

					<input aria-label="@choice('laravel-launcher::field.keyword', 2)" type="search" placeholder="@choice('laravel-launcher::field.keyword', 2)..." wire:model="search.keyword">

					@if ($hasAdvancedSearch)
						<button type="button" aria-controls="searchDialog" aria-label="@lang('laravel-launcher::backend.dialog.search.title')" title="@lang('laravel-launcher::backend.dialog.search.title')">
							<x-ll::svg icon="sliders" />
						</button>
					@endif

					<button type="button" wire:click="runReset" @if (empty($this->search)) hidden @endif aria-label="@lang('laravel-launcher::action.reset')" title="@lang('laravel-launcher::action.reset')">
						<x-ll::svg icon="x-lg" />
					</button>

					<button type="submit">
						<x-ll::svg icon="search" />
						<span>@lang('laravel-launcher::action.search')</span>
					</button>

				</div>

				@includeIf($path . '.search')

				@if (count($this->searchBadges))
					@foreach ($this->searchBadges as $key => $val)
						<small class="badge">
							{{ translate($key, ['field', 'model']) }}: {{ $val }}
							<button type="button" wire:click="runReset('{{ $key }}')" aria-label="@lang('laravel-launcher::action.remove')">
								<x-ll::svg icon="x" />
							</button>
						</small>
					@endforeach
				@endif

			</form>
		</search>
	@endif

	<div class="datalist">

		<header>

			<div class="dropdown">
				<button aria-controls="filterDropdown" aria-expanded="false" aria-pressed="false" aria-label="@lang('laravel-launcher::action.filter')">
					<x-ll::svg icon="filter-circle" />
					{{ translate($filter, ['form.filters', 'form.types', 'field', 'model']) }} <small>({{ $this->totals[$filter] }})</small>
				</button>
				<ul id="filterDropdown" tabindex="0" hidden>
					@foreach ($filters as $buttonFilter)
						<li>
							<button type="button" wire:click="runFilter('{{ $buttonFilter }}')" @if ($filter == $buttonFilter) aria-pressed="true" @endif>
								{{ translate($buttonFilter, ['form.filters', 'form.types', 'field', 'model']) }}
								<small>({{ $this->totals[$buttonFilter] }})</small>
							</button>
						</li>
					@endforeach
				</ul>
			</div>

			<div class="group">
				@if ($filter == 'all' && empty($search))
					@if ($hasPosition && $this->items->count() > 1)
						<button type="button" wire:click="toggleDraggable" @if ($draggable) aria-pressed="true" @endif @if ($levelable) disabled @endif>
							<x-ll::svg icon="arrow-down-up" />
							<span>@lang('laravel-launcher::action.edit-position')</span>
						</button>
					@endif
					@if ($hasLevel)
						<button type="button" wire:click="toggleLevelable" @if ($levelable) aria-pressed="true" @endif @if ($draggable) disabled @endif>
							<x-ll::svg icon="diagram-2" />
							<span>@lang('laravel-launcher::action.tree-view')</span>
						</button>
					@endif
				@endif
			</div>

		</header>

		@if ($this->items->count())
			<div class="table-scrollable">
				<table>
					<thead>
						<tr>

							@if ($draggable || $levelable)
								<th></th>
							@endif

							<th class="table-sort align-center" wire:click="runSort('id')" @if ($sort['field'] == 'id') data-sorted-by="{{ $sort['direction'] }}" @endif>
								@choice('laravel-launcher::field.id', 1)
							</th>

							@if ($hasPosition)
								<th class="table-sort align-center" wire:click="runSort('position')" @if ($sort['field'] == 'position') data-sorted-by="{{ $sort['direction'] }}" @endif>
									@choice('laravel-launcher::field.position', 1)
								</th>
							@endif

							@foreach ($columns as $column)
								<th @if (in_array($column, $sorting)) class="table-sort" wire:click="runSort('{{ $column }}')" @if ($sort['field'] == $column) data-sorted-by="{{ $sort['direction'] }}" @endif @endif>
									{{ translate($column, ['field', 'model']) }}
								</th>
							@endforeach

							<th class="table-sort" wire:click="runSort('updated_at')" @if ($sort['field'] == 'updated_at') data-sorted-by="{{ $sort['direction'] }}" @endif>
								@choice('laravel-launcher::field.updated-at', 1)
							</th>

							<th class="align-right">@choice('laravel-launcher::field.action', 2)</th>
						</tr>
					</thead>
					<tbody @if ($hasPosition || $hasLevel) class="{{ $hasPosition ? 'as-sortable' : '' }} {{ $hasLevel ? 'as-tree' : '' }}" @endif>
						@foreach ($this->items as $item)
							@include('laravel-launcher::includes.livewire.datalist-row')
						@endforeach
					</tbody>
				</table>
			</div>

			<footer>
				@if ($hasPagination)
					{{ $this->items->links('laravel-launcher::includes.livewire.pagination') }}
				@endif
			</footer>
		@else
			<p>@lang('laravel-launcher::notification.no-item')</p>
		@endif
	</div>

</div>
