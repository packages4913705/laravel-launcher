<fieldset class="dynamic">

	<legend>
		{{ $legend }}
		@if ($this->total)
			<small>
				(
				{{ $this->total }} /
				@if ($max)
					{{ $max }}
				@else
					<x-ll::svg icon="infinity" />
				@endif
				)
			</small>
		@endif
	</legend>

	@isset($description)
		<small id="description{{ $dynID }}">
			{{ $description }}
		</small>
	@endisset

	@foreach ($items['update'] as $index => $itemToUpdate)
		<div wire:key="update-{{ $index }}" class="flex-grid">

			@includeIf($path, ['type' => 'update'])

			<button type="button" wire:click="deleteRow({{ $index }})" aria-label="@lang('laravel-launcher::action.delete')" title="@lang('laravel-launcher::action.delete')" @if ($total <= $min) disabled @endif>
				<x-ll::svg icon="dash-circle" />
			</button>

		</div>
	@endforeach

	@foreach ($items['add'] as $index => $itemToAdd)
		<div wire:key="add-{{ $index }}" class="flex-grid">

			@includeIf($path, ['type' => 'add'])

			<button type="button" wire:click="removeRow({{ $index }})" aria-label="@lang('laravel-launcher::action.remove')" title="@lang('laravel-launcher::action.remove')" @if ($total <= $min) disabled @endif>
				<x-ll::svg icon="dash-circle" />
			</button>

		</div>
	@endforeach

	@foreach ($items['delete'] as $index => $itemToDelete)
		<div class="flex-grid">

			@includeIf($path, ['type' => 'delete'])

			<button type="button" wire:click="restoreRow({{ $index }})" aria-label="@lang('laravel-launcher::action.restore')" title="@lang('laravel-launcher::action.restore')" @if ($max && $total == $max) disabled @endif>
				<x-ll::svg icon="plus-circle" />
			</button>

		</div>
	@endforeach

	<button type="button" wire:click="addRow" aria-label="@lang('laravel-launcher::action.add')" @if ($max && $total == $max) disabled @endif>
		<x-ll::svg icon="plus-circle" />
		@lang('laravel-launcher::action.add')
	</button>

</fieldset>
