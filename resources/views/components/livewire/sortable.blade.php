<ul class="list flush @if ($draggable) sortable @endif">
	@if ($this->items->count())
		@foreach ($this->items as $item)
			<li wire:key="{{ $item->id }}" @if ($draggable) draggable="false" @endif>
				@if ($draggable)
					<span data-handle="sortable" aria-hidden="true">
						<x-ll::svg icon="grip-vertical" />
					</span>
				@endif

				<span class="list-grow">
					@if ($path)
						@include($path)
					@else
						<b>{{ $item->label }}</b>
					@endif
				</span>

				<div>
					@if ($draggable)
						<button class="rounded" type="button" wire:click="goUp({{ $loop->index }})" aria-label="@lang('laravel-launcher::action.move-up')" title="@lang('laravel-launcher::action.move-up')" @if ($loop->first) disabled @endif><x-ll::svg icon="chevron-up" /></button>
						<button class="rounded" type="button" wire:click="goDown({{ $loop->index }})" aria-label="@lang('laravel-launcher::action.move-down')" title="@lang('laravel-launcher::action.move-down')" @if ($loop->last) disabled @endif><x-ll::svg icon="chevron-down" /></button>
					@endif

					@can('view', $item)
						<a class="rounded" href="{{ route('backend.' . getPluralModelName($item) . '.show', $item->id) }}" role="button" aria-label="@lang('laravel-launcher::action.view')" title="@lang('laravel-launcher::action.view')">
							<x-ll::svg icon="eye" />
						</a>
					@endcan

					@can('edit', $item)
						<a class="rounded" href="{{ route('backend.' . getPluralModelName($item) . '.edit', $item->id) }}" role="button" aria-label="@lang('laravel-launcher::action.edit')" title="@lang('laravel-launcher::action.edit')">
							<x-ll::svg icon="pencil" />
						</a>
					@endcan

					@can('download', $item)
						<a class="rounded" href="{{ $item->url }}" role="button" download target="_blank" aria-label="@lang('laravel-launcher::action.download')" title="@lang('laravel-launcher::action.download')">
							<x-ll::svg icon="download" />
						</a>
					@endcan
				</div>

			</li>
		@endforeach
	@endif
</ul>
