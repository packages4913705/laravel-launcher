<select {{ $attributes }} id="{{ $id }}" name="{{ $name }}" @if ($hasAnyDescription) aria-describedby="{{ $descriptionIds }}" @endif @error($cleanName) aria-invalid="true" @enderror @if ($required) aria-required="true" @endif @if ($disabled) disabled="true" @endif>
	<option value="">--</option>
	@forelse ($options as $key => $option)
		<option value="{{ $key }}" @selected(in_array($key, (array) old($cleanName, $value)))>
			{{ $option }}
		</option>
	@empty
		{{ $slot }}
	@endforelse
</select>
