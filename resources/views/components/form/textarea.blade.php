<textarea {{ $attributes }} id="{{ $id }}" name="{{ $name }}" @if ($placeholder) placeholder="{{ $placeholder }}" @endif @if ($hasAnyDescription) aria-describedby="{{ $descriptionIds }}" @endif @error($cleanName) aria-invalid="true" @enderror @if ($required) aria-required="true" @endif @if ($disabled) disabled="true" @endif @if ($readonly) readonly="true" @endif>
@if (old($cleanName, $value))
{!! old($cleanName, $value) !!}
@endif
</textarea>
