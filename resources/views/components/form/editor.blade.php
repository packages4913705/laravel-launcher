<div class="editor">

	<div class="editor-menu">
		<div class="group">
			@if (in_array('typography', $toolbar))
				<div class="dropdown">
					<button type="button" aria-controls="{{ $id . 'TypographyDropdown' }}" aria-expanded="false" aria-pressed="false" aria-label="@lang('laravel-launcher::editor.typography')" title="@lang('laravel-launcher::editor.typography')">
						<x-ll::svg icon="fonts" />
					</button>
					<ul id="{{ $id . 'TypographyDropdown' }}" tabindex="0" hidden>
						<li><button type="button" data-editor-paragraph>@lang('laravel-launcher::editor.paragraph')</button></li>
						@foreach ($headings as $heading)
							<li><button type="button" data-editor-heading value="{{ $heading }}">@lang('laravel-launcher::editor.heading-' . $heading)</button></li>
						@endforeach
					</ul>
				</div>
			@endif

			@if (in_array('div', $toolbar) && !empty($divs))
				<div class="dropdown">
					<button type="button" aria-controls="{{ $id . 'DivDropdown' }}" aria-expanded="false" aria-pressed="false" aria-label="@lang('laravel-launcher::editor.div')" title="@lang('laravel-launcher::editor.div')">
						<x-ll::svg icon="card-text" />
					</button>
					<ul id="{{ $id . 'DivDropdown' }}" tabindex="0" hidden>
						@foreach ($divs as $div)
							<li>
								<button type="button" data-editor-div value="{{ $div }}">
									{{ translate($div, 'editor') }}
								</button>
							</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if (in_array('span', $toolbar) && !empty($spans))
				<div class="dropdown">
					<button type="button" aria-controls="{{ $id . 'SpanDropdown' }}" aria-expanded="false" aria-pressed="false" aria-label="@lang('laravel-launcher::editor.span')" title="@lang('laravel-launcher::editor.span')">
						<x-ll::svg icon="highlighter" />
					</button>
					<ul id="{{ $id . 'SpanDropdown' }}" tabindex="0" hidden>
						@foreach ($spans as $span)
							<li>
								<button type="button" data-editor-span value="{{ $span }}">
									{{ translate($span, 'editor') }}
								</button>
							</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if (in_array('table', $toolbar))
				<button type="button" aria-label="@lang('laravel-launcher::editor.table.toggle')" title="@lang('laravel-launcher::editor.table.toggle')" data-editor-table>
					<x-ll::svg icon="table" />
				</button>
			@endif

			@if (in_array('blockquote', $toolbar))
				<button type="button" aria-label="@lang('laravel-launcher::editor.blockquote')" title="@lang('laravel-launcher::editor.blockquote')" data-editor-blockquote>
					<x-ll::svg icon="quote" />
				</button>
			@endif
		</div>

		@if (in_array('list', $toolbar))
			<div class="group">
				<button type="button" aria-label="@lang('laravel-launcher::editor.ul')" title="@lang('laravel-launcher::editor.ul')" data-editor-ul>
					<x-ll::svg icon="list-ul" />
				</button>
				<button type="button" aria-label="@lang('laravel-launcher::editor.ol')" title="@lang('laravel-launcher::editor.ol')" data-editor-ol>
					<x-ll::svg icon="list-ol" />
				</button>
			</div>
		@endif

		<div class="group">
			@if (in_array('format', $toolbar))
				<button type="button" aria-label="@lang('laravel-launcher::editor.bold')" title="@lang('laravel-launcher::editor.bold')" data-editor-bold>
					<x-ll::svg icon="type-bold" />
				</button>
				<button type="button" aria-label="@lang('laravel-launcher::editor.italic')" title="@lang('laravel-launcher::editor.italic')" data-editor-italic>
					<x-ll::svg icon="type-italic" />
				</button>
				<button type="button" aria-label="@lang('laravel-launcher::editor.underline')" title="@lang('laravel-launcher::editor.underline')" data-editor-underline>
					<x-ll::svg icon="type-underline" />
				</button>
				<button type="button" aria-label="@lang('laravel-launcher::editor.strike')" title="@lang('laravel-launcher::editor.strike')" data-editor-strike>
					<x-ll::svg icon="type-strikethrough" />
				</button>
			@endif

			@if (in_array('link', $toolbar))
				<button type="button" aria-label="@lang('laravel-launcher::editor.link.toggle')" title="@lang('laravel-launcher::editor.link.toggle')" data-editor-link>
					<x-ll::svg icon="link-45deg" />
				</button>
			@endif

			@if (in_array('abbr', $toolbar))
				<button type="button" aria-label="@lang('laravel-launcher::editor.abbr.toggle')" title="@lang('laravel-launcher::editor.abbr.toggle')" data-editor-abbr>
					<x-ll::svg icon="textarea-t" />
				</button>
			@endif
		</div>

		@if (in_array('align', $toolbar))
			<div class="group">
				<button type="button" aria-label="@lang('laravel-launcher::editor.align.left')" title="@lang('laravel-launcher::editor.align.left')" data-editor-align value="left">
					<x-ll::svg icon="text-left" />
				</button>
				<button type="button" aria-label="@lang('laravel-launcher::editor.align.center')" title="@lang('laravel-launcher::editor.align.center')" data-editor-align value="center">
					<x-ll::svg icon="text-center" />
				</button>
				<button type="button" aria-label="@lang('laravel-launcher::editor.align.right')" title="@lang('laravel-launcher::editor.align.right')" data-editor-align value="right">
					<x-ll::svg icon="text-right" />
				</button>
				<button type="button" aria-label="@lang('laravel-launcher::editor.align.justify')" title="@lang('laravel-launcher::editor.align.justify')" data-editor-align value="justify">
					<x-ll::svg icon="justify" />
				</button>
			</div>
		@endif

		<div class="group">
			<button type="button" aria-label="@lang('laravel-launcher::editor.erase')" title="@lang('laravel-launcher::editor.erase')" data-editor-erase>
				<x-ll::svg icon="eraser" />
			</button>
			<button type="button" aria-label="@lang('laravel-launcher::editor.help')" title="@lang('laravel-launcher::editor.help')" data-dialog-open aria-controls="editorHelpDialog">
				<x-ll::svg icon="patch-question" />
			</button>
		</div>
	</div>

	@if (in_array('table', $toolbar))
		<div class="group editor-bubble-menu editor-table-menu">
			<div class="dropdown">
				<button type="button" aria-controls="{{ $id . 'TableHeaderDropdown' }}" aria-expanded="false" aria-pressed="false">
					<x-ll::svg icon="border-style" />
					@lang('laravel-launcher::editor.table.headers')
				</button>
				<ul id="{{ $id . 'TableHeaderDropdown' }}" tabindex="0" hidden>
					<li>
						<button type="button" data-editor-table-row value="header">
							@lang('laravel-launcher::editor.table.row.header')
						</button>
					</li>
					<li>
						<button type="button" data-editor-table-col value="header">
							@lang('laravel-launcher::editor.table.col.header')
						</button>
					</li>
				</ul>
			</div>
			<div class="dropdown">
				<button type="button" aria-controls="{{ $id . 'TableInsertDropdown' }}" aria-expanded="false" aria-pressed="false">
					<x-ll::svg icon="plus-circle" />
					@lang('laravel-launcher::action.insert')
				</button>
				<ul id="{{ $id . 'TableInsertDropdown' }}" tabindex="0" hidden>
					<li>
						<button type="button" data-editor-table-row value="before">
							@lang('laravel-launcher::editor.table.row.before')
						</button>
					</li>
					<li>
						<button type="button" data-editor-table-row value="after">
							@lang('laravel-launcher::editor.table.row.after')
						</button>
					</li>
					<li>
						<button type="button" data-editor-table-col value="before">
							@lang('laravel-launcher::editor.table.col.before')
						</button>
					</li>
					<li>
						<button type="button" data-editor-table-col value="after">
							@lang('laravel-launcher::editor.table.col.after')
						</button>
					</li>
				</ul>
			</div>
			<div class="dropdown">
				<button type="button" aria-controls="{{ $id . 'TableRemoveDropdown' }}" aria-expanded="false" aria-pressed="false">
					<x-ll::svg icon="dash-circle" />
					@lang('laravel-launcher::action.remove')
				</button>
				<ul id="{{ $id . 'TableRemoveDropdown' }}" tabindex="0" hidden>
					<li>
						<button type="button" data-editor-table-row value="remove">
							@lang('laravel-launcher::editor.table.row.remove')
						</button>
					</li>
					<li>
						<button type="button" data-editor-table-col value="remove">
							@lang('laravel-launcher::editor.table.col.remove')
						</button>
					</li>
					<li>
						<button type="button" data-editor-table value="remove">
							@lang('laravel-launcher::editor.table.remove')
						</button>
					</li>
				</ul>
			</div>
		</div>
	@endif

	@if (in_array('link', $toolbar))
		<div class="group editor-bubble-menu editor-link-menu">
			<button type="button" aria-label="@lang('laravel-launcher::editor.link.edit')" title="@lang('laravel-launcher::editor.link.edit')" data-editor-link value="edit">
				<x-ll::svg icon="pencil" />
			</button>
			<button type="button" aria-label="@lang('laravel-launcher::editor.link.remove')" title="@lang('laravel-launcher::editor.link.remove')" data-editor-link value="remove">
				<x-ll::svg icon="trash" />
			</button>
		</div>
	@endif

	@if (in_array('abbr', $toolbar))
		<div class="group editor-bubble-menu editor-abbr-menu">
			<button type="button" aria-label="@lang('laravel-launcher::editor.abbr.edit')" title="@lang('laravel-launcher::editor.abbr.edit')" data-editor-abbr value="edit">
				<x-ll::svg icon="pencil" />
			</button>
			<button type="button" aria-label="@lang('laravel-launcher::editor.abbr.remove')" title="@lang('laravel-launcher::editor.abbr.remove')" data-editor-abbr value="remove">
				<x-ll::svg icon="trash" />
			</button>
		</div>
	@endif

	<div class="editor-content" @error($cleanName) aria-invalid="true" @enderror @if ($required) aria-required="true" @endif></div>

	<textarea id="{{ $id }}" class="editor-textarea" name="{{ $name }}" @if ($hasAnyDescription) aria-describedby="{{ $descriptionIds }}" @endif @error($cleanName) aria-invalid="true" @enderror @if ($required) aria-required="true" @endif @if ($disabled) disabled="true" @endif hidden>{!! old($cleanName, $value) ?? '' !!}</textarea>

</div>
