@php
	$hasAnyDescription = $description || $errors->has($cleanName);
	$descriptionIds = $description ? $descriptionId : '';
	$descriptionIds .= $errors->has($cleanName) ? ' ' . $invalidId : '';
@endphp

<div>
	@includeWhen($fieldIsBefore, 'laravel-launcher::components.form.' . $field)

	<label for="{{ $id }}">

		{{ translate($label, ['field', 'form', 'model']) }}

		@if ($info)
			<small>{{ $info }}</small>
		@endif

		@if ($required)
			<small aria-hidden="true">{{ translate('required', 'form.helpers') }}</small>
		@endif

	</label>

	@includeWhen(!$fieldIsBefore, 'laravel-launcher::components.form.' . $field)

	@error($cleanName)
		<small id="{{ $invalidId }}">
			<x-ll::svg icon="exclamation-circle" />
			<span aria-live="assertive">{{ $message }}</span>
		</small>
	@enderror

	@if ($description)
		<small id="{{ $descriptionId }}">
			{!! translate($description, 'form.helpers') !!}
		</small>
	@endif
</div>
