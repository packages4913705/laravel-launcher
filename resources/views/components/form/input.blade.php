@if ($type === 'password')
	<div class="group">
		<input {{ $attributes }} id="{{ $id }}" type="{{ $type }}" name="{{ $name }}" value="{{ old($cleanName, $value) }}" @if ($placeholder) placeholder="{{ $placeholder }}" @endif @if ($hasAnyDescription) aria-describedby="{{ $descriptionIds }}" @endif @error($cleanName) aria-invalid="true" @enderror @if ($required) aria-required="true" @endif @if ($disabled) disabled="true" @endif @if ($readonly) readonly="true" @endif>
		<button class="show-password" type="button" aria-controls="{{ $id }}" aria-pressed="false" aria-label="@lang('laravel-launcher::action.toggle-password')">
			<x-ll::svg icon="eye" />
			<x-ll::svg icon="eye-slash" />
		</button>
	</div>
@else
	<input {{ $attributes }} id="{{ $id }}" type="{{ $type }}" name="{{ $name }}" value="{{ old($cleanName, $value) }}" @if ($placeholder) placeholder="{{ $placeholder }}" @endif @if ($hasAnyDescription) aria-describedby="{{ $descriptionIds }}" @endif @error($cleanName) aria-invalid="true" @enderror @if ($required) aria-required="true" @endif @if ($disabled) disabled="true" @endif @if ($readonly) readonly="true" @endif>
@endif
