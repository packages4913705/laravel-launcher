<svg {{ $attributes }} aria-hidden="true" focusable="false" width="32" height="32" fill="currentColor">
	<use xlink:href="{{ asset('vectors/bootstrap-icons.svg#' . $icon) }}"></use>
</svg>
