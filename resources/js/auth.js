/**
 * ------------------------------------------------------------------
 * Auth
 * ------------------------------------------------------------------
 * This script is for the auth pages.
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

// Import global JS
import './components/_global'