/**
 * ------------------------------------------------------------------
 * Backend
 * ------------------------------------------------------------------
 * This script is for the backend.
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

// Notification
import { notification } from './components/_notification'

// Import global JS
import './components/_global'

// Add the Drawer to toggle the menu
import Drawer from "@natachah/vanilla-frontend/js/_drawer"
const sidebar = document.getElementById('sidebar')
if (sidebar) new Drawer(sidebar, { breakpoint: 960, cookie: '_backend-comfort' })

// Add sortable for datalist and some cards (Livewire components)
import Sortable from "@natachah/vanilla-frontend/js/_sortable"
const sortables = document.querySelectorAll('.sortable')
if (sortables) sortables.forEach(sortable => {
    new Sortable(sortable)
    initUpdatePositionEvent(sortable)
})

// Add Tree for categories checkboxes
import Tree from "@natachah/vanilla-frontend/js/_tree"
const trees = document.querySelectorAll('.list-as-tree')
if (trees) trees.forEach(tree => new Tree(tree))

// Add Tabpanel for translations
import Tabpanel from "@natachah/vanilla-frontend/js/_tabpanel"
const translations = document.querySelectorAll('.translations')
if (translations) translations.forEach(translation => new Tabpanel(translation))

// Add CheckAll mostly for role->permissions
import CheckAll from "@natachah/vanilla-frontend/js/_check-all"
const checkboxes = document.querySelectorAll('.checkbox-all')
if (checkboxes) {
    checkboxes.forEach(checkboxe => new CheckAll(checkboxe))
}

// Add CRUD Dialog
import Dialog from "@natachah/vanilla-frontend/js/_dialog"
const dataDeleteDialog = document.getElementById('dataDeleteDialog')
const dataRestoreDialog = document.getElementById('dataRestoreDialog')
const dataDestroyDialog = document.getElementById('dataDestroyDialog')
const searchDialogs = document.querySelectorAll('.search-dialog')
if (searchDialogs) searchDialogs.forEach(searchDialog => new Dialog(searchDialog))

// Add Editor
import Editor from './components/_editor'
const editors = document.querySelectorAll('.editor')
if (editors) editors.forEach(editor => new Editor(editor))

// Define a function to trigger the updatePosition into Livewire File
function initUpdatePositionEvent(item) {
    item.addEventListener('sortable:drop', (e) => {
        if (e.detail.items) {
            const values = [...e.detail.items].map(x => x.getAttribute('wire:key'))
            const component = Livewire.find(e.detail.current.closest('[wire\\:id]').getAttribute('wire:id'))
            component.call('updatePosition', values, true)
        }
    })
}

// Things to do after Livewire is initilized
document.addEventListener('livewire:initialized', () => {

    // Delete dialog
    if (dataDeleteDialog) {
        const deleteDialog = new Dialog(dataDeleteDialog)
        Livewire.on('open-delete-dialog', (e) => {
            dataDeleteDialog.querySelector('form').action = e.url
            deleteDialog.open()
        })
    }

    // Restore dialog
    if (dataRestoreDialog) {
        const restoreDialog = new Dialog(dataRestoreDialog)
        Livewire.on('open-restore-dialog', (e) => {
            dataRestoreDialog.querySelector('form').action = e.url
            restoreDialog.open()
        })
    }

    // Destroy dialog
    if (dataDestroyDialog) {
        const destroyDialog = new Dialog(dataDestroyDialog)
        Livewire.on('open-destroy-dialog', (e) => {
            dataDestroyDialog.querySelector('form').action = e.url
            destroyDialog.open()
        })
    }

    // On close dialog, reset action url
    document.querySelectorAll('#dataDeleteDialog, #dataRestoreDialog, #dataDestroyDialog').forEach(dialog => {
        dialog.addEventListener('dialog:closed', () => dialog.querySelector('form').action = null)
    })

    // Init the tree for livewire Datalist
    let datalistTreeObjects = []
    const dataTrees = document.querySelectorAll('.as-tree')
    if (dataTrees) dataTrees.forEach(tree => datalistTreeObjects.push(new Tree(tree)))

    // Reset the tree for livewire Datalist
    Livewire.on('resetTreeJS', e => {
        setTimeout(() => datalistTreeObjects.forEach(obj => obj.resetEvents()), 0)
    })

    // Init the sortable for livewire Datalist
    let datalistSortableObject
    const dataSortable = document.querySelector('.as-sortable')
    if (dataSortable) {
        datalistSortableObject = new Sortable(dataSortable)
        initUpdatePositionEvent(dataSortable)
    }

    // Reset the sortable for livewire Datalist
    Livewire.on('resetSortableJS', e => {
        setTimeout(() => datalistSortableObject.resetEvents(), 0)
    })

    // Run notification for position update
    if (notificationDialog && notification) {
        Livewire.on('position-updated', (e) => {
            notificationDialog.classList.add('success')
            notificationDialog.querySelector('p').innerHTML = e
            notification.open()
        })
    }

})
