/**
 * ------------------------------------------------------------------
 * Editor
 * ------------------------------------------------------------------
 * This script is for the backend editor.
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import { Editor as TiptapEditor } from '@tiptap/core'
import StarterKit from '@tiptap/starter-kit'
import Underline from '@tiptap/extension-underline'
import Table from '@tiptap/extension-table'
import TableCell from '@tiptap/extension-table-cell'
import TableHeader from '@tiptap/extension-table-header'
import TableRow from '@tiptap/extension-table-row'
import Link from '@tiptap/extension-link'
import BubbleMenu from '@tiptap/extension-bubble-menu'
import TextAlign from '@tiptap/extension-text-align'

// Custom
import Dialog from "@natachah/vanilla-frontend/js/_dialog"
import CustomSpan from './_editor-span'
import CustomDiv from './_editor-div'
import CustomAbbr from './_editor-abbr'

export default class Editor {

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @constructor
     */
    constructor(el) {

        if (!el) throw new Error('The parameter el is missing')

        this._element = el

        this._content = el.querySelector('.editor-content')

        this._textarea = el.querySelector('.editor-textarea')

        this._menu = {
            main: el.querySelector('.editor-menu'),
            table: el.querySelector('.editor-table-menu') ?? null,
            link: el.querySelector('.editor-link-menu') ?? null,
            abbr: el.querySelector('.editor-abbr-menu') ?? null,
        }

        this._dialog = {
            link: new Dialog(document.getElementById('editorLinkDialog')),
            abbr: new Dialog(document.getElementById('editorAbbrDialog')),
            help: new Dialog(document.getElementById('editorHelpDialog'))
        }

        this._buttons = {
            paragraph: this._menu.main.querySelector('[data-editor-paragraph]'),
            headings: this._menu.main.querySelectorAll('[data-editor-heading]'),
            blockquote: this._menu.main.querySelector('[data-editor-blockquote]'),
            ul: this._menu.main.querySelector('[data-editor-ul]'),
            ol: this._menu.main.querySelector('[data-editor-ol]'),
            tables: [...this._menu.main.querySelectorAll('[data-editor-table]'), ...(this._menu.table ? this._menu.table.querySelectorAll('[data-editor-table]') : [])],
            tableRows: this._menu.table ? this._menu.table.querySelectorAll('[data-editor-table-row]') : [],
            tableCols: this._menu.table ? this._menu.table.querySelectorAll('[data-editor-table-col]') : [],
            bold: this._menu.main.querySelector('[data-editor-bold]'),
            italic: this._menu.main.querySelector('[data-editor-italic]'),
            underline: this._menu.main.querySelector('[data-editor-underline]'),
            strike: this._menu.main.querySelector('[data-editor-strike]'),
            aligns: this._menu.main.querySelectorAll('[data-editor-align]'),
            links: [...this._menu.main.querySelectorAll('[data-editor-link]'), ...(this._menu.link ? this._menu.link.querySelectorAll('[data-editor-link]') : [])],
            abbrs: [...this._menu.main.querySelectorAll('[data-editor-abbr]'), ...(this._menu.abbr ? this._menu.abbr.querySelectorAll('[data-editor-abbr]') : [])],
            erase: this._menu.main.querySelector('[data-editor-erase]'),
            spans: this._menu.main.querySelectorAll('[data-editor-span]'),
            divs: this._menu.main.querySelectorAll('[data-editor-div]')
        }

        this._inputs = {
            type: this._dialog.link._form.querySelector('[name="tiptap_link_type"]'),
            url: this._dialog.link._form.querySelector('[name="tiptap_link_url"]'),
            target: this._dialog.link._form.querySelector('[name="tiptap_link_target"]'),
            abbr: this._dialog.abbr._form.querySelector('[name="tiptap_abbr_title"]'),
        }

        this._editor = null

        if (this._buttons.links.length) this.#initLinkValidation()

        this.#initEditor()

        if (this._buttons.links.length) this.#initDialogEvent()

    }

    /**
     * Init the editor
     * 
     * @private
     */
    #initEditor() {

        // Put this in that to use it inside the TipTap 
        const that = this

        // Initialize the TipTap
        this._editor = new TiptapEditor({

            element: that._content,

            extensions: [StarterKit, Underline, Table, TableRow, TableHeader, TableCell, CustomSpan, CustomDiv, CustomAbbr,

                TextAlign.configure({
                    types: ['paragraph'],
                    alignments: ['left', 'center', 'right', 'justify'],
                }),

                Link.configure({
                    openOnClick: false,
                    autolink: false,
                    defaultProtocol: 'https',
                }),

                BubbleMenu.configure({
                    pluginKey: 'tableBubbleMenu',
                    element: that._menu.table,
                    shouldShow: ({ editor, view, state, oldState, from, to }) => {
                        return editor.isActive('table')
                    },
                }),

                BubbleMenu.configure({
                    pluginKey: 'linkBubbleMenu',
                    element: that._menu.link,
                    shouldShow: ({ editor, view, state, oldState, from, to }) => {
                        return editor.isActive('link')
                    },
                }),

                BubbleMenu.configure({
                    pluginKey: 'abbrBubbleMenu',
                    element: that._menu.abbr,
                    shouldShow: ({ editor, view, state, oldState, from, to }) => {
                        return editor.isActive('customAbbr')
                    },
                }),

            ],

            content: that._textarea.value,

            injectCSS: false,

            editorProps: {
                transformPastedHTML(html) {
                    // Make sure to get ONLY some text on paste
                    const div = document.createElement('div')
                    div.innerHTML = html
                    return div.textContent || div.innerText || ''
                },
            },

            onCreate({ editor }) {
                const editorLabel = that._content.closest('.editor').previousElementSibling.textContent ?? 'Rich editor text'
                editor.view.dom.setAttribute('aria-label', editorLabel)
            },

            onBlur({ editor, event }) {
                // Only if click outside
                if (!event.relatedTarget || !that._element.contains(event.relatedTarget)) {
                    that._textarea.value = editor.getHTML()
                    that._menu.main.querySelectorAll('[aria-pressed]').forEach(button => button.setAttribute('aria-pressed', false))
                }
            },

            onSelectionUpdate({ editor }) {
                that.#checkIsActive(editor)
            },

        })

        // Create the event listener
        if (this._buttons.paragraph) this._buttons.paragraph.addEventListener('click', () => this._editor.chain().focus().setParagraph().run())

        if (this._buttons.headings) this._buttons.headings.forEach(heading => heading.addEventListener('click', () => this._editor.chain().focus().toggleHeading({ level: parseInt(heading.value) }).run()))

        if (this._buttons.blockquote) this._buttons.blockquote.addEventListener('click', () => this._editor.chain().focus().toggleBlockquote().run())

        if (this._buttons.ul) this._buttons.ul.addEventListener('click', () => this._editor.chain().focus().toggleBulletList().run())

        if (this._buttons.ol) this._buttons.ol.addEventListener('click', () => this._editor.chain().focus().toggleOrderedList().run())

        if (this._buttons.bold) this._buttons.bold.addEventListener('click', () => this._editor.chain().focus().toggleBold().run())

        if (this._buttons.italic) this._buttons.italic.addEventListener('click', () => this._editor.chain().focus().toggleItalic().run())

        if (this._buttons.underline) this._buttons.underline.addEventListener('click', () => this._editor.chain().focus().toggleUnderline().run())

        if (this._buttons.strike) this._buttons.strike.addEventListener('click', () => this._editor.chain().focus().toggleStrike().run())

        if (this._buttons.aligns) this._buttons.aligns.forEach(align => align.addEventListener('click', () => this._editor.chain().focus().setTextAlign(align.value).run()))

        if (this._buttons.tables) this._buttons.tables.forEach(table => {

            table.addEventListener('click', () => {
                switch (table.value) {
                    case 'remove':
                        this._editor.chain().focus().deleteTable().run()
                        const dropdown = this._menu.table.querySelector('.dropdown:last-child')
                        dropdown.querySelector('button').setAttribute('aria-expanded', false)
                        dropdown.querySelector('button').setAttribute('aria-pressed', false)
                        dropdown.querySelector('ul').hidden = true
                        break
                    default:
                        this._editor.isActive('table') ? this._editor.chain().focus().deleteTable().run() : this._editor.chain().focus().insertTable({ rows: 3, cols: 3, withHeaderRow: true }).run()
                        break
                }
            })

        })

        if (this._buttons.tableRows) {
            this._buttons.tableRows.forEach(row => {
                row.addEventListener('click', () => {
                    switch (row.value) {
                        case 'header':
                            this._editor.chain().focus().toggleHeaderRow().run()
                            break
                        case 'before':
                            if (!this._editor.isActive('tableHeader')) this._editor.chain().focus().addRowBefore().run()
                            break
                        case 'after':
                            this._editor.chain().focus().addRowAfter().run()
                            break
                        default:
                            this._editor.chain().focus().deleteRow().run()
                            break
                    }
                })
            })
        }

        if (this._buttons.tableCols) {
            this._buttons.tableCols.forEach(col => {
                col.addEventListener('click', () => {
                    switch (col.value) {
                        case 'header':
                            this._editor.chain().focus().toggleHeaderColumn().run()
                            break
                        case 'before':
                            if (!this._editor.isActive('tableHeader')) this._editor.chain().focus().addColumnBefore().run()
                            break
                        case 'after':
                            this._editor.chain().focus().addColumnAfter().run()
                            break

                        default:
                            this._editor.chain().focus().deleteColumn().run()
                            break
                    }
                })
            })
        }

        if (this._buttons.links && this._dialog.link) this._buttons.links.forEach(link => {
            link.addEventListener('click', () => {
                switch (link.value) {
                    case 'edit':
                        this._dialog.link.open()
                        break
                    case 'remove':
                        this._editor.chain().focus().unsetLink().run()
                        break
                    default:
                        this._editor.isActive('link') ? this._editor.chain().focus().unsetLink().run() : this._dialog.link.open()
                        break
                }
            })
        })

        if (this._buttons.abbrs && this._dialog.abbr) this._buttons.abbrs.forEach(abbr => {
            abbr.addEventListener('click', () => {
                switch (abbr.value) {
                    case 'edit':
                        this._dialog.abbr.open()
                        break
                    case 'remove':
                        this._editor.chain().focus().unsetCustomAbbr().run()
                        break
                    default:
                        this._editor.isActive('customAbbr') ? this._editor.chain().focus().unsetCustomAbbr().run() : this._dialog.abbr.open()
                        break
                }
            })
        })

        if (this._buttons.erase) {
            this._buttons.erase.addEventListener('click', () => {
                this._editor.commands.clearNodes()
                this._editor.commands.unsetAllMarks()
            })
        }

        if (this._buttons.spans) this._buttons.spans.forEach(span => span.addEventListener('click', () => this._editor.chain().focus().toggleCustomSpan(span.value).run()))

        if (this._buttons.divs) this._buttons.divs.forEach(div => div.addEventListener('click', () => this._editor.chain().focus().toggleCustomDiv(div.value).run()))

    }

    /**
     * Check wiche button is active
     * 
     * @private
     */
    #checkIsActive(editor) {

        if (this._buttons.paragraph) this._buttons.paragraph.setAttribute('aria-pressed', editor.isActive('paragraph'))

        if (this._buttons.headings) this._buttons.headings.forEach(heading => heading.setAttribute('aria-pressed', editor.isActive('heading', { level: parseInt(heading.value) })))

        if (this._buttons.blockquote) this._buttons.blockquote.setAttribute('aria-pressed', editor.isActive('blockquote'))

        if (this._buttons.ul) this._buttons.ul.setAttribute('aria-pressed', editor.isActive('bulletList'))

        if (this._buttons.ol) this._buttons.ol.setAttribute('aria-pressed', editor.isActive('orderedList'))

        if (this._buttons.bold) this._buttons.bold.setAttribute('aria-pressed', editor.isActive('bold'))

        if (this._buttons.italic) this._buttons.italic.setAttribute('aria-pressed', editor.isActive('italic'))

        if (this._buttons.underline) this._buttons.underline.setAttribute('aria-pressed', editor.isActive('underline'))

        if (this._buttons.strike) this._buttons.strike.setAttribute('aria-pressed', editor.isActive('strike'))

        if (this._buttons.aligns) this._buttons.aligns.forEach(align => align.setAttribute('aria-pressed', editor.isActive({ textAlign: align.value })))

        if (this._buttons.tables) this._buttons.tables.forEach(table => !table.value ? table.setAttribute('aria-pressed', editor.isActive('table')) : null)

        if (this._buttons.tableRows && this._buttons.tableCols && editor.isActive('table')) {

            const { state } = editor
            const { doc, selection } = state
            const { $from } = selection

            // Find the Table to the node
            let node = $from.node()
            let pos = $from.pos
            while (node && node.type.name !== 'table') {
                pos = doc.resolve(pos).before()
                node = doc.nodeAt(pos)
            }

            const firstRow = node.content.firstChild
            const lastRow = node.content.lastChild

            const isRowHeader = firstRow && firstRow.content.lastChild.type.name === 'tableHeader'
            const isColHeader = lastRow && lastRow.content.firstChild.type.name === 'tableHeader'

            const tableRowHeader = [...this._buttons.tableRows].find(button => button.value === 'header')
            const tableColHeader = [...this._buttons.tableCols].find(button => button.value === 'header')

            tableRowHeader.setAttribute('aria-pressed', isRowHeader)
            tableColHeader.setAttribute('aria-pressed', isColHeader)

            // Disabled the insert before buttons if in header
            const y = [...this._buttons.tableRows].find(button => button.value === 'before')
            const x = [...this._buttons.tableCols].find(button => button.value === 'before')
            x.disabled = editor.isActive('tableHeader')
            y.disabled = editor.isActive('tableHeader')

        }

        if (this._buttons.links) this._buttons.links.forEach(link => !link.value ? link.setAttribute('aria-pressed', editor.isActive('link')) : null)

        if (this._buttons.abbrs) this._buttons.abbrs.forEach(abbr => !abbr.value ? abbr.setAttribute('aria-pressed', editor.isActive('customAbbr')) : null)

        if (this._buttons.spans) this._buttons.spans.forEach(span => span.setAttribute('aria-pressed', editor.isActive('customSpan', { class: span.value })))

        if (this._buttons.divs) this._buttons.divs.forEach(div => div.setAttribute('aria-pressed', editor.isActive('customDiv', { class: div.value })))

    }

    /**
     * Init the form dialog validation
     * 
     * @private
     */
    #initLinkValidation() {

        this._inputs.url.addEventListener('change', () => {

            let regex, errorMsg

            switch (this._inputs.type.value) {
                case 'email':
                    regex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
                    errorMsg = window.translations.error.email
                    break

                case 'tel':
                    regex = /^\+?([0-9\-\s]){9,}$/
                    errorMsg = window.translations.error.tel
                    break

                default:
                    regex = /^(https?:\/\/)?(www\.)?([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,}(\/[^\s]*)?$/
                    errorMsg = window.translations.error.url
                    break
            }

            this._inputs.url.setCustomValidity(regex.test(this._inputs.url.value) ? '' : errorMsg)

        })

    }

    /**
     * Init the event listener for Dialog
     * 
     * @private
     */
    #initDialogEvent() {

        this._dialog.link._element.addEventListener('dialog:opening', () => {
            const href = this._editor.getAttributes('link').href ?? ''
            const target = this._editor.getAttributes('link').target ?? '_blank'
            const type = href.startsWith('mailto:') ? 'email' : href.startsWith('tel:') ? 'tel' : 'url'
            this._inputs.type.value = type
            this._inputs.target.checked = target == '_blank'
            this._inputs.url.value = href.replace('mailto:', '').replace('tel:', '')
        })

        this._dialog.link._element.addEventListener('dialog:submited', (e) => {
            const type = this._inputs.type.value
            const target = this._inputs.target.checked ? '_blank' : '_self'
            const url = (type == 'email' ? 'mailto:' : type == 'tel' ? 'tel:' : '') + this._inputs.url.value
            if (url) this._editor.chain().focus().extendMarkRange('link').setLink({ href: url, target: target }).run()
        })

        this._dialog.abbr._element.addEventListener('dialog:opening', () => {
            const title = this._editor.getAttributes('customAbbr').title ?? ''
            this._inputs.abbr.value = title
        })

        this._dialog.abbr._element.addEventListener('dialog:submited', (e) => {
            const value = this._inputs.abbr.value
            if (value) this._editor.chain().focus().extendMarkRange('customAbbr').setCustomAbbr({ title: value }).run()
        })

    }

}
