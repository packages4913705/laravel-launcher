/**
 * ------------------------------------------------------------------
 * Notification
 * ------------------------------------------------------------------
 * This script is for the backend notification.
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import Dialog from "@natachah/vanilla-frontend/js/_dialog"

// Create a function to set a timer bar
function setNotificationTimer(duration) {

    const progressBar = document.getElementById('notificationTimer')
    const interval = 10
    const totalIntervals = duration / interval
    let currentInterval = 0
    clearInterval(timer)

    timer = setInterval(function () {
        currentInterval++
        const progress = (currentInterval / totalIntervals) * 100
        progressBar.style.width = progress + "%"

        if (currentInterval >= totalIntervals) {
            notification.close()
        }

    }, interval)

}

// Get the <dialog> element
const notificationDialog = document.getElementById('notificationDialog')

// Store the interval timer to easily clear it after
let timer = null

// Store the notification Object to re-use
export let notification = null

if (notificationDialog) {

    notification = new Dialog(notificationDialog)

    notificationDialog.addEventListener('dialog:opened', () => {
        setNotificationTimer(5000)
    })

    notificationDialog.addEventListener('dialog:closed', () => {
        clearInterval(timer)
    })

    if (notificationDialog.open) setNotificationTimer(5000)

}