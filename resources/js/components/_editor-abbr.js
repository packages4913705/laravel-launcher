/**
 * ------------------------------------------------------------------
 * Editor - Abbr
 * ------------------------------------------------------------------
 * This script is for the backend editor to create some abbreviation.
 * Generated via ChatGPT
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import { Mark, mergeAttributes } from '@tiptap/core'

const CustomAbbr = Mark.create({

    name: 'customAbbr',

    keepOnSplit: false,

    exitable: true,

    inclusive: false,

    addAttributes() {
        return {
            title: {
                default: null,
            },
        }
    },

    parseHTML() {
        return [
            {
                tag: 'abbr[title]',
            },
        ]
    },

    renderHTML({ HTMLAttributes }) {
        return ['abbr', mergeAttributes(HTMLAttributes), 0]
    },

    addCommands() {
        return {
            setCustomAbbr:
                (values) =>
                    ({ commands }) => {
                        return commands.setMark(this.name, { title: values.title })
                    },
            unsetCustomAbbr:
                () =>
                    ({ commands }) => {
                        return commands.unsetMark(this.name)
                    },
            toggleCustomAbbr:
                (values) =>
                    ({ commands }) => {
                        return commands.toggleMark(this.name, { title: values.title })
                    },
        }
    },

})

export default CustomAbbr