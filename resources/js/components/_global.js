/**
 * ------------------------------------------------------------------
 * Global
 * ------------------------------------------------------------------
 * This script is for the global backend JS used in all views.
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

// Set a timeout to remove data-preload
// This is for avoid animation on first load
setTimeout(() => {
    document.body.removeAttribute('data-preload')
}, 1000)

// Add the Comfort to switch themes
import Comfort from "@natachah/vanilla-frontend/js/_comfort"
const myComfort = new Comfort('_backend-comfort')
if (!myComfort.has('theme')) myComfort.setTheme(window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light')

// Add the Dropdown for each .dropdown
import Dropdown from "@natachah/vanilla-frontend/js/_dropdown"
const dropdowns = document.querySelectorAll('.dropdown')
if (dropdowns) dropdowns.forEach(dropdown => new Dropdown(dropdown))

// Add the show-password function
import FormHelper from "@natachah/vanilla-frontend/js/utilities/_form-helper"
const passwordButtons = document.querySelectorAll('.show-password')
if (passwordButtons) passwordButtons.forEach(button => button.addEventListener('click', () => FormHelper.togglePassword(button))) 