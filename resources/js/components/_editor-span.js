/**
 * ------------------------------------------------------------------
 * Editor - Span
 * ------------------------------------------------------------------
 * This script is for the backend editor to create some span with a class.
 * Generated via ChatGPT
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import { Mark, mergeAttributes } from '@tiptap/core'

const CustomSpan = Mark.create({

    name: 'customSpan',

    addAttributes() {
        return {
            class: {
                default: null,
            },
        }
    },

    parseHTML() {
        return [
            {
                tag: 'span[class]',
            },
        ]
    },

    renderHTML({ HTMLAttributes }) {
        return ['span', mergeAttributes(HTMLAttributes), 0]
    },

    addCommands() {
        return {
            setCustomSpan:
                (className) =>
                    ({ commands }) => {
                        return commands.setMark(this.name, { class: className })
                    },
            unsetCustomSpan:
                () =>
                    ({ commands }) => {
                        return commands.unsetMark(this.name)
                    },
            toggleCustomSpan:
                (className) =>
                    ({ commands }) => {
                        return commands.toggleMark(this.name, { class: className })
                    },
        }
    },

})

export default CustomSpan