/**
 * ------------------------------------------------------------------
 * Editor - Div
 * ------------------------------------------------------------------
 * This script is for the backend editor to create some div with a class.
 * Generated via ChatGPT
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import { Node, mergeAttributes } from '@tiptap/core'

export const CustomDiv = Node.create({

    name: 'customDiv',
    group: 'block',
    content: 'block+',
    defining: true,

    addAttributes() {
        return {
            class: {
                default: null,
            },
        }
    },

    parseHTML() {
        return [
            {
                tag: 'div[class]',
            },
        ]
    },

    renderHTML({ HTMLAttributes }) {
        return ['div', mergeAttributes(HTMLAttributes), 0]
    },

    addCommands() {

        return {
            setCustomDiv:
                (className) =>
                    ({ commands }) => {
                        return commands.wrapIn(this.name, { class: className })
                    },

            unsetCustomDiv:
                () =>
                    ({ commands }) => {
                        return commands.lift(this.name)
                    },

            toggleCustomDiv:
                (className) =>
                    ({ editor, commands }) => {
                        if (editor.isActive(this.name)) {
                            return commands.updateAttributes(this.name, { class: className })

                        } else {
                            return commands.toggleWrap(this.name, { class: className })
                        }
                    },
        }

    },

})


export default CustomDiv