<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Notification Language Lines
    |--------------------------------------------------------------------------
    */

    'no-item' => 'There are no items.',

    'success' => [
        'created' => 'The item was <b>successfully</b> created!',
        'updated' => 'The item was <b>successfully</b> updated!',
        'deleted' => 'The item was <b>successfully</b> moved to the trash!',
        'restored' => 'The item was <b>successfully</b> restored!',
        'destroyed' => 'The item was <b>successfully</b> deleted (permanently)!',
        'sorted' => 'The item order was <b>successfully</b> changed!',
        'published' => 'The item was <b>successfully</b> published!',
        'unpublished' => 'The item was <b>successfully</b> unpublished!',
    ],

    'error' => [
        'max-levelable' => 'The parent <b>could not be saved</b> because you have reached the maximum level.',
        'media' => 'There was an <b>error</b> while importing media.',
        'model' => 'The :model model is not found.',
        'media-description' => 'The media description is missing !',
    ],

];
