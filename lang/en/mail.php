<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Mail Language Lines
    |--------------------------------------------------------------------------
    |
    | These are use for the mail views.
    |
    */

    'reset-password' => [
        'subject' => 'Password Reset Request',
        'greeting' => 'Hello,',
        'description' => 'We have received a request to reset your password.',
        'action' => 'Reset my password',
        'ignor' => 'If you did not request this, you can ignore this email.',
        'salutation' => 'Best regards',
    ],

];
