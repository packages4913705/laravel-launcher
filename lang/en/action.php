<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Action Language Lines
    |--------------------------------------------------------------------------
    |
    | These are used for buttons or links.
    |
    */

    'save' => 'Save',
    'send' => 'Send',
    'submit' => 'Submit',
    'cancel' => 'Cancel',
    'search' => 'Search',
    'reset' => 'Reset',
    'confirm' => 'Confirm',
    'filter' => 'Filter',
    'open' => 'Open',
    'close' => 'Close',
    'toggle' => 'Toggle',
    'insert' => 'Insert',
    'decline' => 'Decline',
    'apply' => 'Apply',

    /*
    |--------------------------------------------------------------------------
    | Auth Language Lines
    |--------------------------------------------------------------------------
    */

    'login' => 'Login',
    'logout' => 'Logout',

    /*
    |--------------------------------------------------------------------------
    | CRUD Language Lines
    |--------------------------------------------------------------------------
    */

    'add' => 'Add',
    'remove' => 'Remove',
    'create' => 'Create',
    'edit' => 'Edit',
    'update' => 'Update',
    'delete' => 'Delete',
    'destroy' => 'Destroy (permanently)',
    'restore' => 'Restore',
    'view' => 'View',
    'activity' => 'View History',
    'index' => 'List',
    'show' => 'Details',
    'publish' => 'Publish',
    'unpublish' => 'Unpublish',
    'validate' => 'Validate',
    'unvalidate' => 'Invalidate',

    /*
    |--------------------------------------------------------------------------
    | File Language Lines
    |--------------------------------------------------------------------------
    */

    'upload' => 'Upload',
    'download' => 'Download',

    /*
    |--------------------------------------------------------------------------
    | Position Language Lines
    |--------------------------------------------------------------------------
    */

    'edit-position' => 'Edit position',
    'drag-drop' => 'Drag and drop',
    'tree-view' => 'Tree view',
    'move-up' => 'Move up',
    'move-down' => 'Move down',

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    */

    'go-next' => 'Next',
    'go-prev' => 'Previous',
    'go-top' => 'Go to top',
    'go-end' => 'Go to bottom',
    'go-back' => 'Go back',
    'learn-more' => 'Learn more',
    'see-more' => 'See more',

    /*
    |--------------------------------------------------------------------------
    | SKIP action
    |--------------------------------------------------------------------------
    */

    'skip-to-content' => 'Skip to content',
    'skip-to-search' => 'Skip to search',
    'skip-to-list' => 'Skip to list',

    /*
    |--------------------------------------------------------------------------
    | SEND action
    |--------------------------------------------------------------------------
    */

    'send-email' => 'Send email',

    /*
    |--------------------------------------------------------------------------
    | Toggle action
    |--------------------------------------------------------------------------
    */

    'toggle-password' => 'Show/Hide password',
    'toggle-menu' => 'Open/Close menu',

];
