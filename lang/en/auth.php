<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Auth Language Lines
    |--------------------------------------------------------------------------
    |
    | These are use for the authenfication views.
    |
    */

    'forgot-your-password' => 'Forgot your password?',
    'reset-your-password' => 'Reset password',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
