<?php

return [

    /*
    |--------------------------------------------------------------------------
    | JS Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the javascript messages.
    | -> Used by the Tiptap Editor to set an url
    |
    */

    'error' => [
        'url' => 'Please enter a valid URL.',
        'email' => 'Please enter a valid email address.',
        'tel' => 'Please enter a valid phone number.',
    ],

];
