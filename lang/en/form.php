<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Form Language Lines
    |--------------------------------------------------------------------------
    |
    | These are specific for the forms and helpers.
    |
    */

    'helpers' => [
        'required' => 'Required field',
        'slug' => 'This field is used to create a URL like: www.mysite.com/<b>URL</b>',
        'leave-empty' => 'Leave empty if you don\'t want to change it.',
        'format' => 'Format(s): :format',
        'size' => 'Recommended size: :size px',
        'width' => 'Min width: :width px',
        'height' => 'Min height: :height px',
        'weight' => 'Max weight: :weight MB',
        'seo' => 'Enter a concise summary of your content (max. 160 characters) with keywords for SEO.',
        'guard' => 'This field is for admin use only, please enter a unique name in lowercase, without spaces or special characters.',
        'password' => 'The password must contain at least 6 characters and a number.',
        'label-page' => 'This field is used for links in the menus.',
        'image-description' => 'Image descriptions improve accessibility for visually impaired people by allowing them to understand visual content.',
    ],

    'filters' => [
        'today' => 'Today',
        'week' => 'This week',
        'month' => 'This month',
        'last-month' => 'Last month',
        'trashed' => 'Trashed|Trashed',
    ],

];
