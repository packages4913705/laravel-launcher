<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Log Language Lines
    |--------------------------------------------------------------------------
    |
    | These are used for activity log.
    |
    */

    'added' => 'Addition',
    'created' => 'Creation',
    'edited' => 'Edit',
    'updated' => 'Update',
    'deleted' => 'Deletion',
    'destroyed' => 'Destruction (permanent)',
    'restored' => 'Restoration',
    'synchronized' => 'Synchronization',
    'published' => 'Publication',
    'validated' => 'Validation',

];
