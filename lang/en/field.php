<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Field Language Lines
    |--------------------------------------------------------------------------
    |
    | These are global used in forms or in details.
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Single Field
    |--------------------------------------------------------------------------
    */

    'yes' => 'Yes',
    'no' => 'No',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'in-menu' => 'In menu',
    'in-footer' => 'In footer',
    'by' => 'By',
    'sub' => 'Sub-:item',
    'all' => 'All',
    'used-by' => 'Used by',
    'seo' => 'SEO',

    /*
    |--------------------------------------------------------------------------
    | Status Field
    |--------------------------------------------------------------------------
    */

    'available' => 'Available|Available',
    'unavailable' => 'Unavailable|Unavailable',
    'visible' => 'Visible|Visible',
    'invisible' => 'Invisible|Invisible',
    'empty' => 'Empty|Empty',
    'published' => 'Published|Published',
    'unpublished' => 'Unpublished|Unpublished',
    'validated' => 'Validated|Validated',
    'invalided' => 'Invalid|Invalid',
    'used' => 'Used|Used',
    'unused' => 'Unused|Unused',

    /*
    |--------------------------------------------------------------------------
    | Global Field
    |--------------------------------------------------------------------------
    */

    'information' => 'Information|Information',
    'option' => 'Option|Options',
    'keyword' => 'Keyword|Keywords',
    'event' => 'Event|Events',
    'model' => 'Model|Models',
    'relation' => 'Relation|Relations',
    'action' => 'Action|Actions',
    'parent' => 'Parent|Parents',
    'child' => 'Child|Children',
    'subpage' => 'Subpage|Subpages',

    /*
    |--------------------------------------------------------------------------
    | Person Field
    |--------------------------------------------------------------------------
    */

    'firstname' => 'First name|First names',
    'lastname' => 'Last name|Last names',
    'author' => 'Author|Authors',
    'birthday' => 'Birthday|Birthdays',
    'birthdate' => 'Birth date|Birth dates',
    'age' => 'Age|Ages',
    'username' => 'Username|Usernames',
    'password' => 'Password|Passwords',
    'password-confirmation' => 'Password confirmation|Password confirmations',

    /*
    |--------------------------------------------------------------------------
    | Contact Field
    |--------------------------------------------------------------------------
    */

    'contact' => 'Contact|Contacts',
    'email' => 'Email|Emails',
    'phone' => 'Phone|Phones',
    'mobile' => 'Mobile|Mobiles',
    'address' => 'Address|Addresses',
    'zip' => 'Postal code|Postal codes',
    'city' => 'City|Cities',
    'country' => 'Country|Countries',
    'street' => 'Street and number|Streets and numbers',
    'state' => 'State|States',
    'social-network' => 'Social network|Social networks',
    'map' => 'Map|Maps',

    /*
    |--------------------------------------------------------------------------
    | Attributes Field
    |--------------------------------------------------------------------------
    */

    'id' => 'ID|IDs',
    'guard-name' => 'Guard name|Guard names',
    'slug' => 'Simplified URL|Simplified URLs',
    'name' => 'Name|Names',
    'label' => 'Label|Labels',
    'type' => 'Type|Types',
    'title' => 'Title|Titles',
    'subtitle' => 'Subtitle|Subtitles',
    'description' => 'Description|Descriptions',
    'message' => 'Message|Messages',
    'comment' => 'Comment|Comments',
    'position' => 'Position|Positions',
    'color' => 'Color|Colors',
    'number' => 'Number|Numbers',
    'reference' => 'Reference|References',
    'level' => 'Level|Levels',
    'link' => 'Link|Links',
    'url' => 'URL|URLs',
    'price' => 'Price|Prices',
    'size' => 'Size|Sizes',
    'weight' => 'Weight|Weights',
    'height' => 'Height|Heights',
    'length' => 'Length|Lengths',
    'depth' => 'Depth|Depths',
    'place' => 'Place|Places',

    /*
    |--------------------------------------------------------------------------
    | Date Field
    |--------------------------------------------------------------------------
    */

    'date' => 'Date|Dates',
    'day' => 'Day|Days',
    'week' => 'Week|Weeks',
    'month' => 'Month|Months',
    'year' => 'Year|Years',
    'hour' => 'Hour|Hours',
    'minute' => 'Minute|Minutes',
    'second' => 'Second|Seconds',
    'created-at' => 'Creation date|Creation dates',
    'updated-at' => 'Update date|Update dates',
    'deleted-at' => 'Deletion date|Deletion dates',
    'published-at' => 'Publication date|Publication dates',

    /*
    |--------------------------------------------------------------------------
    | File Field
    |--------------------------------------------------------------------------
    */

    'file' => 'File|Files',
    'picture' => 'Picture|Pictures',
    'image' => 'Image|Images',
    'document' => 'Document|Documents',
    'filename' => 'File name|File names',
    'icon' => 'Icon|Icons',
    'vector' => 'Vector|Vectors',
    'pdf' => 'PDF|PDFs',
    'word' => 'Word file|Word files',
    'excel' => 'Excel file|Excel files',
    'audio' => 'Audio|Audios',
    'video' => 'Video|Videos',
    'code' => 'Code|Codes',

];
