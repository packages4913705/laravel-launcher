<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backend Language Lines
    |--------------------------------------------------------------------------
    |
    | These are used for the Backend views.
    |
    */

    'admin' => 'Administration',

    'sidebar' => [
        'nav' => 'Navigation menu',
        'welcome' => 'Welcome, :Name!',
        'dashboard' => 'Dashboard',
        'edit-profile' => 'Edit my profile',
        'website' => 'My website',
        'access' => 'Access',
        'settings' => 'Settings',
        'content' => 'Content',
        'copyright' => 'Published under the MIT License.',
    ],

    'topbar' => [
        'nav' => 'Navigation menu for options',
        'languages' => 'Change language',
        'theme-light' => 'Light theme',
        'theme-dark' => 'Dark theme',
        'website' => 'View my website',
        'logout' => 'Logout',
        'breadcrumb' => 'Breadcrumb',
    ],

    'help' => [
        'title' => 'Need help?',
        'message' => 'If you need help or encounter a bug, no worries! You can send me an email, and I will get back to you as soon as possible.',
    ],

    'log' => [
        'system' => 'System',
        'no-user' => 'User deleted',
        'no-item' => 'Item deleted',
    ],

    /*
|--------------------------------------------------------------------------
| Dialog
|--------------------------------------------------------------------------
*/

    'dialog' => [
        'delete' => [
            'title' => 'Delete confirmation',
            'content' => 'Are you sure you want to delete this item?',
            'confirm' => 'Yes, delete the item',
        ],
        'restore' => [
            'title' => 'Restore confirmation',
            'content' => 'Are you sure you want to restore this item?',
            'confirm' => 'Yes, restore the item',
        ],
        'destroy' => [
            'title' => 'Permanent delete confirmation',
            'content' => 'Are you sure you want to <b>permanently</b> delete this item?',
            'confirm' => 'Yes, permanently delete the item',
        ],
        'search' => [
            'title' => 'Advanced search',
        ],
    ],

];
