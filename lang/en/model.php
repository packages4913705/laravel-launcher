<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
    */

    'activity-log' => 'Activity log|Activity logs',
    'setting' => 'Setting|Settings',
    'media' => 'Media|Media',
    'translation' => 'Translation|Translations',
    'user' => 'User|Users',
    'role' => 'Role|Roles',
    'permission' => 'Permission|Permissions',
    'media' => 'Media|Media',
    'category' => 'Category|Categories',
    'page' => 'Page|Pages',

];
