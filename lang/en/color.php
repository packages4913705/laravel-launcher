<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Color Language Lines
    |--------------------------------------------------------------------------
    */

    'default' => 'Default',

    'primary' => 'Primary color',
    'secondary' => 'Secondary color',
    'tertiary' => 'Tertiary color',

    'black' => 'Black',
    'white' => 'White',
    'blue' => 'Blue',
    'red' => 'Red',
    'yellow' => 'Yellow',
    'purple' => 'Purple',
    'green' => 'Green',
    'orange' => 'Orange',

];
