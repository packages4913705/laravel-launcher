<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Editor Language Lines
    |--------------------------------------------------------------------------
    |
    | These are used for the TIPTAP editor.
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Font
    |--------------------------------------------------------------------------
    */

    'typography' => 'Font type',
    'paragraph' => 'Paragraph',
    'heading-1' => 'Heading 1',
    'heading-2' => 'Heading 2',
    'heading-3' => 'Heading 3',
    'heading-4' => 'Heading 4',
    'heading-5' => 'Heading 5',
    'heading-6' => 'Heading 6',

    /*
    |--------------------------------------------------------------------------
    | Custom
    |--------------------------------------------------------------------------
    */

    'div' => 'Block type',
    'span' => 'Text type',

    /*
    |--------------------------------------------------------------------------
    | Quote
    |--------------------------------------------------------------------------
    */

    'blockquote' => 'Quote',

    /*
    |--------------------------------------------------------------------------
    | List
    |--------------------------------------------------------------------------
    */

    'ul' => 'Unordered list',
    'ol' => 'Ordered list',

    /*
    |--------------------------------------------------------------------------
    | Format
    |--------------------------------------------------------------------------
    */

    'bold' => 'Bold',
    'italic' => 'Italic',
    'underline' => 'Underline',
    'strike' => 'Strikethrough',

    /*
    |--------------------------------------------------------------------------
    | Align
    |--------------------------------------------------------------------------
    */

    'align' => [
        'left' => 'Align left',
        'center' => 'Align center',
        'right' => 'Align right',
        'justify' => 'Justify',
    ],

    /*
    |--------------------------------------------------------------------------
    | Link
    |--------------------------------------------------------------------------
    */

    'link' => [
        'toggle' => 'Link',
        'edit' => 'Edit link',
        'remove' => 'Remove link',
        'target-blank' => 'Open in a new page',
    ],

    /*
    |--------------------------------------------------------------------------
    | Abbreviation
    |--------------------------------------------------------------------------
    */

    'abbr' => [
        'toggle' => 'Abbreviation',
        'edit' => 'Edit abbreviation',
        'remove' => 'Remove abbreviation',
    ],

    /*
    |--------------------------------------------------------------------------
    | Table
    |--------------------------------------------------------------------------
    */

    'table' => [
        'toggle' => 'Table',
        'remove' => 'Remove table',
        'headers' => 'Headers',
        'row' => [
            'header' => 'Header row',
            'before' => 'One row above',
            'after' => 'One row below',
            'remove' => 'Remove row',
        ],
        'col' => [
            'header' => 'Header column',
            'before' => 'One column to the left',
            'after' => 'One column to the right',
            'remove' => 'Remove column',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Other
    |--------------------------------------------------------------------------
    */

    'erase' => 'Remove style',
    'help' => 'Help and shortcuts',
    'br' => 'Line break',

    'abbr-description' => 'This field is used to indicate that a word or phrase is an abbreviation. It allows showing the full version of the abbreviation when hovering over it.',

];
