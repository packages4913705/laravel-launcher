<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Log Language Lines
    |--------------------------------------------------------------------------
    |
    | These are used for activity log.
    |
    */

    'added' => 'Ajout',
    'created' => 'Création',
    'edited' => 'Modification',
    'updated' => 'Mise à jour',
    'deleted' => 'Suppression',
    'destroyed' => 'Suppression (définitivement)',
    'restored' => 'Restauration',
    'synchronized' => 'Synchronisation',
    'published' => 'Publication',
    'validated' => 'Validation',

];
