<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Mail Language Lines
    |--------------------------------------------------------------------------
    |
    | These are use for the mail views.
    |
    */

    'reset-password' => [
        'subject' => 'Réinitialisation de votre mot de passe',
        'greeting' => 'Bonjour,',
        'description' => 'Nous avons reçu une demande pour réinitialiser votre mot de passe.',
        'action' => 'Réinitialiser mon mot de passe',
        'ignor' => 'Si vous n\'êtes pas à l\'origine de cette demande, vous pouvez ignorer cet email.',
        'salutation' => 'Meilleures salutations',
    ],

];
