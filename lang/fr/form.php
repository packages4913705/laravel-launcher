<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Form Language Lines
    |--------------------------------------------------------------------------
    |
    | These are specific for the forms and helpers.
    |
    */

    'error' => '{1}:count erreur|[2,*] :count erreurs',

    'helpers' => [
        'required' => 'Champ requis',
        'slug' => 'Ce champ est utilisé pour créer une URL du type : www.monsite.com/<b>URL</b>',
        'leave-empty' => 'Laissez vide si vous ne souhaitez pas le modifier.',
        'format' => 'Format(s) : :format',
        'size' => 'Taille recommandée : :size px',
        'width' => 'Largeur min. : :width px',
        'height' => 'Hauteur min. : :height px',
        'weight' => 'Poids max. : :weight MB',
        'seo' => 'Entrez un résumé concis de votre contenu (max. 160 caractères) avec des mots-clés pour le référencement.',
        'guard' => 'Ce champ est réservé à l\'administration, veuillez entrer un nom unique en minuscules, sans espaces ni caractères spéciaux.',
        'password' => 'Le mot de passe doit contenir au moins 6 caractères et un chiffre.',
        'label-page' => 'Ce champ est utilisé pour les liens dans les menus.',
        'image-description' => 'La description d\'image améliore l\'accessibilité pour les personnes malvoyantes en leur permettant de comprendre le contenu visuel.',
    ],

    'filters' => [
        'today' => 'Aujourd\'hui',
        'week' => 'Cette semaine',
        'month' => 'Ce mois-ci',
        'last-month' => 'Le mois dernier',
        'trashed' => 'Poubelle|Poubelles',
    ],

    'types' => [
    ],

];
