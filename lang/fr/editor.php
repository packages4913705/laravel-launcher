<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Editor Language Lines
    |--------------------------------------------------------------------------
    |
    | These are used for the TIPTAP editor.
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Font
    |--------------------------------------------------------------------------
    */

    'typography' => 'Type de police',
    'paragraph' => 'Paragraphe',
    'heading-1' => 'Titre 1',
    'heading-2' => 'Titre 2',
    'heading-3' => 'Titre 3',
    'heading-4' => 'Titre 4',
    'heading-5' => 'Titre 5',
    'heading-6' => 'Titre 6',

    /*
    |--------------------------------------------------------------------------
    | Custom
    |--------------------------------------------------------------------------
    */

    'div' => 'Type de bloc',
    'span' => 'Type de texte',

    /*
    |--------------------------------------------------------------------------
    | Quote
    |--------------------------------------------------------------------------
    */

    'blockquote' => 'Citation',

    /*
    |--------------------------------------------------------------------------
    | List
    |--------------------------------------------------------------------------
    */

    'ul' => 'Liste à puces',
    'ol' => 'Liste numérotée',

    /*
    |--------------------------------------------------------------------------
    | Format
    |--------------------------------------------------------------------------
    */

    'bold' => 'Gras',
    'italic' => 'Italique',
    'underline' => 'Souligné',
    'strike' => 'Barré',

    /*
    |--------------------------------------------------------------------------
    | Align
    |--------------------------------------------------------------------------
    */

    'align' => [
        'left' => 'Aligner à gauche',
        'center' => 'Aligner au centre',
        'right' => 'Aligner à droite',
        'justify' => 'Justifier',
    ],

    /*
    |--------------------------------------------------------------------------
    | Link
    |--------------------------------------------------------------------------
    */

    'link' => [
        'toggle' => 'Lien',
        'edit' => 'Modifier le lien',
        'remove' => 'Supprimer le lien',
        'target-blank' => 'Ouvrir dans une nouvelle page',
    ],

    /*
    |--------------------------------------------------------------------------
    | Abbreviation
    |--------------------------------------------------------------------------
    */

    'abbr' => [
        'toggle' => 'Abbreviation',
        'edit' => 'Modifier l\'abbreviation',
        'remove' => 'Supprimer l\'abbreviation',
    ],

    /*
    |--------------------------------------------------------------------------
    | Table
    |--------------------------------------------------------------------------
    */

    'table' => [
        'toggle' => 'Tableau',
        'remove' => 'Supprimer le tableau',
        'headers' => 'En-têtes',
        'row' => [
            'header' => 'Ligne d\'entête',
            'before' => 'Une ligne au-dessus',
            'after' => 'Une ligne en dessous',
            'remove' => 'Supprimer la ligne',
        ],
        'col' => [
            'header' => 'Colonne d\'entête',
            'before' => 'Une colonne à gauche',
            'after' => 'Une colonne à droite',
            'remove' => 'Supprimer la colonne',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Other
    |--------------------------------------------------------------------------
    */

    'erase' => 'Enlever le style',
    'help' => 'Aide et raccourcis',
    'br' => 'Saut à la ligne',
    'abbr-description' => 'Ce champ est utilisée pour indiquer qu\'un mot ou une phrase est une abréviation. Elle permet de montrer la version complète de l\'abréviation lorsqu\'on passe la souris dessus.',

];
