<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Field Language Lines
    |--------------------------------------------------------------------------
    |
    | These are global used in forms or in details.
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Single Field
    |--------------------------------------------------------------------------
    */

    'yes' => 'Oui',
    'no' => 'Non',
    'asc' => 'Ascendant',
    'desc' => 'Descendant',
    'in-menu' => 'Dans le menu',
    'in-footer' => 'Dans le pied de page',
    'by' => 'Par',
    'sub' => 'Sous-:item',
    'all' => 'Tous',
    'used-by' => 'Utilisé par',
    'seo' => 'SEO',

    /*
    |--------------------------------------------------------------------------
    | Status Field
    |--------------------------------------------------------------------------
    */

    'available' => 'Disponible|Disponibles',
    'unavailable' => 'Indisponible|Indisponibles',
    'visible' => 'Visible|Visibles',
    'invisible' => 'Invisible|Invisibles',
    'empty' => 'Vide|Vides',
    'published' => 'Publié|Publiés',
    'unpublished' => 'Non publié|Non publiés',
    'validated' => 'Validé|Validés',
    'invalided' => 'Non validé|Non validés',
    'used' => 'Utilisé|Utilisés',
    'unused' => 'Inutilisé|Inutilisés',

    /*
    |--------------------------------------------------------------------------
    | Global Field
    |--------------------------------------------------------------------------
    */

    'information' => 'Information|Informations',
    'option' => 'Option|Options',
    'keyword' => 'Mot clé|Mots clés',
    'event' => 'Événement|Événements',
    'model' => 'Modèle|Modèles',
    'relation' => 'Relation|Relations',
    'action' => 'Action|Actions',
    'parent' => 'Parent|Parents',
    'child' => 'Enfant|Enfants',
    'subpage' => 'Sous-page|Sous-pages',

    /*
    |--------------------------------------------------------------------------
    | Person Field
    |--------------------------------------------------------------------------
    */

    'firstname' => 'Prénom|Prénoms',
    'lastname' => 'Nom|Noms',
    'author' => 'Auteur|Auteurs',
    'birthday' => 'Date d\'anniversaire|Dates d\'anniversaire',
    'birthdate' => 'Date de naissance|Dates de naissance',
    'age' => 'Âge|Âges',
    'username' => 'Identifiant|Identifiants',
    'password' => 'Mot de passe|Mots de passe',
    'password-confirmation' => 'Confirmation du mot de passe|Confirmation des mots de passe',

    /*
    |--------------------------------------------------------------------------
    | Contact Field
    |--------------------------------------------------------------------------
    */

    'contact' => 'Contact|Contacts',
    'email' => 'Email|Emails',
    'phone' => 'Téléphone|Téléphones',
    'mobile' => 'Tél. portable|Tél. portables',
    'address' => 'Adresse|Adresses',
    'zip' => 'Code postal|Codes postaux',
    'city' => 'Ville|Villes',
    'country' => 'Pays|Pays',
    'street' => 'Rue et numéro|Rues et numéros',
    'state' => 'État|États',
    'social-network' => 'Réseau social|Réseaux sociaux',
    'map' => 'Carte|Cartes',

    /*
    |--------------------------------------------------------------------------
    | Attributes Field
    |--------------------------------------------------------------------------
    */

    'id' => 'ID|IDs',
    'guard-name' => 'Nom protégé|Noms protégés',
    'slug' => 'URL simplifié|URLs simplifiés',
    'name' => 'Nom|Noms',
    'label' => 'Nom|Noms',
    'type' => 'Type|Types',
    'title' => 'Titre|Titres',
    'subtitle' => 'Sous-titre|Sous-titres',
    'description' => 'Description|Descriptions',
    'message' => 'Message|Messages',
    'comment' => 'Commentaire|Commentaires',
    'position' => 'Position|Positions',
    'color' => 'Couleur|Couleurs',
    'number' => 'Numéro|Numéros',
    'reference' => 'Référence|Références',
    'level' => 'Niveau|Niveaux',
    'link' => 'Lien|Liens',
    'url' => 'URL|URLs',
    'price' => 'Prix|Prix',
    'size' => 'Taille|Tailles',
    'weight' => 'Poids|Poids',
    'height' => 'Hauteur|Hauteurs',
    'length' => 'Longueur|Longueurs',
    'depth' => 'Profondeur|Profondeurs',
    'place' => 'Lieu|Lieux',

    /*
    |--------------------------------------------------------------------------
    | Date Field
    |--------------------------------------------------------------------------
    */

    'date' => 'Date|Dates',
    'day' => 'Jour|Jours',
    'week' => 'Semaine|Semaines',
    'month' => 'Mois|Mois',
    'year' => 'Année|Années',
    'hour' => 'Heure|Heures',
    'minute' => 'Minute|Minutes',
    'second' => 'Seconde|Secondes',
    'created-at' => 'Date de création|Dates de création',
    'updated-at' => 'Date de modification|Dates de modification',
    'deleted-at' => 'Date de suppression|Dates de suppression',
    'published-at' => 'Date de publication|Dates de publication',

    /*
    |--------------------------------------------------------------------------
    | File Field
    |--------------------------------------------------------------------------
    */

    'file' => 'Fichier|Fichiers',
    'picture' => 'Photo|Photos',
    'image' => 'Image|Images',
    'document' => 'Document|Documents',
    'filename' => 'Nom du fichier|Noms des fichiers',
    'icon' => 'Icône|Icônes',
    'vector' => 'Vectoriel|Vectoriels',
    'pdf' => 'PDF|PDFs',
    'word' => 'Fichier Word|Fichiers Word',
    'excel' => 'Fichier Excel|Fichiers Excel',
    'audio' => 'Audio|Audios',
    'video' => 'Vidéo|Vidéos',
    'code' => 'Code|Codes',

];
