<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
    */

    'activity-log' => 'Journal d\'activité|Journaux d\'activité',
    'setting' => 'Paramètre|Paramètres',
    'media' => 'Média|Médias',
    'translation' => 'Traduction|Traductions',
    'user' => 'Utilisateur|Utilisateurs',
    'role' => 'Rôle|Rôles',
    'permission' => 'Permission|Permissions',
    'media' => 'Média|Médias',
    'category' => 'Catégorie|Catégories',
    'page' => 'Page|Pages',

];
