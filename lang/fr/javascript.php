<?php

return [

    /*
    |--------------------------------------------------------------------------
    | JS Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the javascript messages.
    | -> Used by the Tiptap Editor to set an url
    |
    */

    'error' => [
        'url' => 'Veuillez entrer une URL valide.',
        'email' => 'Veuillez entrer une adresse e-mail valide.',
        'tel' => 'Veuillez entrer un numéro de téléphone valide.',
    ],

];
