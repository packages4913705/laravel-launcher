<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Auth Language Lines
    |--------------------------------------------------------------------------
    |
    | These are use for the authenfication views.
    |
    */

    'forgot-your-password' => 'Vous avez oublié votre mot de passe ?',
    'reset-your-password' => 'Réinitialiser le mot de passe',
    'failed' => 'Ces identifiants ne correspondent pas à nos enregistrements.',
    'throttle' => 'Trop de tentatives de connexion. Veuillez réessayer dans :seconds secondes.',

];
