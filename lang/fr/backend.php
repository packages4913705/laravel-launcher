<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backend Language Lines
    |--------------------------------------------------------------------------
    |
    | These are used for the Backend views.
    |
    */

    'admin' => 'Administration',

    'sidebar' => [
        'nav' => 'Menu de navigation',
        'welcome' => 'Bienvenue :Name !',
        'dashboard' => 'Tableau de bord',
        'edit-profile' => 'Modifier mon profil',
        'website' => 'Mon site',
        'access' => 'Accès',
        'settings' => 'Paramètres',
        'content' => 'Contenu',
        'copyright' => 'Publié sous la licence MIT.',
    ],

    'topbar' => [
        'nav' => 'Menu de navigation pour les options',
        'languages' => 'Changer la langue',
        'theme-light' => 'Thème clair',
        'theme-dark' => 'Thème sombre',
        'website' => 'Voir mon site',
        'logout' => 'Déconnexion',
        'breadcrumb' => 'Fil d\'Ariane',
    ],

    'help' => [
        'title' => 'Besoin d\'aide ?',
        'message' => 'Si vous avez besoin d’aide ou si vous détectez un bug, pas de souci ! Vous pouvez m’envoyer un email, et je vous recontacterai dans les plus brefs délais.',
    ],

    'log' => [
        'system' => 'Système',
        'no-user' => 'Utilisateur supprimé',
        'no-item' => 'Élément supprimé',
    ],

    /*
    |--------------------------------------------------------------------------
    | Dialog
    |--------------------------------------------------------------------------
    */

    'dialog' => [
        'delete' => [
            'title' => 'Confirmation de la suppression',
            'content' => 'Voulez-vous vraiment supprimer cet élément ?',
            'confirm' => 'Oui, supprimer l\'élément',
        ],
        'restore' => [
            'title' => 'Confirmation de la restauration',
            'content' => 'Voulez-vous vraiment restaurer cet élément ?',
            'confirm' => 'Oui, restaurer l\'élément',
        ],
        'destroy' => [
            'title' => 'Confirmation de la suppression définitive',
            'content' => 'Voulez-vous vraiment supprimer <b>définitivement</b> cet élément ?',
            'confirm' => 'Oui, supprimer définitivement l\'élément',
        ],
        'search' => [
            'title' => 'Recherche avancée',
        ],
    ],

];
