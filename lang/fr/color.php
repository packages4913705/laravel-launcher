<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Color Language Lines
    |--------------------------------------------------------------------------
    */

    'default' => 'Par défaut',

    'primary' => 'Couleur principale',
    'secondary' => 'Couleur secondaire',
    'tertiary' => 'Couleur tertiaire',

    'black' => 'Noir',
    'white' => 'Blanc',
    'blue' => 'Bleu',
    'red' => 'Rouge',
    'yellow' => 'Jaune',
    'purple' => 'Violet',
    'green' => 'Vert',
    'orange' => 'Orange',

];
