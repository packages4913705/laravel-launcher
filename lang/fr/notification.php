<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Notification Language Lines
    |--------------------------------------------------------------------------
    */

    'no-item' => 'Il n\'y a aucun élément.',

    'success' => [
        'created' => 'L\'élément a été créé avec <b>succès</b> !',
        'updated' => 'L\'élément a été modifié avec <b>succès</b> !',
        'deleted' => 'L\'élément a été mis à la poubelle avec <b>succès</b> !',
        'restored' => 'L\'élément a été restauré avec <b>succès</b> !',
        'destroyed' => 'L\'élément a été supprimé (définitivement) avec <b>succès</b> !',
        'sorted' => 'L\'ordre des éléments a été modifié avec <b>succès</b> !',
        'published' => 'L\'élément a été publié avec <b>succès</b> !',
        'unpublished' => 'L\'élément a été dépublié avec <b>succès</b> !',
    ],

    'error' => [
        'max-levelable' => 'Le parent <b>n\'a pas pu être sauvegardé</b> car vous avez atteint le niveau maximum.',
        'media' => 'Il y a eu une <b>erreur</b> lors de l\'importation d\'un média.',
        'model' => 'Le modèle :model est introuvable.',
        'media-description' => 'Il manque une description de l\'image !',
    ],

];
