<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Action Language Lines
    |--------------------------------------------------------------------------
    |
    | These are used for buttons or links.
    |
    */

    'save' => 'Sauvegarder',
    'send' => 'Envoyer',
    'submit' => 'Soumettre',
    'cancel' => 'Annuler',
    'search' => 'Rechercher',
    'reset' => 'Réinitialiser',
    'confirm' => 'Confirmer',
    'filter' => 'Filtrer',
    'open' => 'Ouvrir',
    'close' => 'Fermer',
    'toggle' => 'Ouvrir/Fermer',
    'insert' => 'Insérer',
    'decline' => 'Refuser',
    'apply' => 'Postuler',

    /*
    |--------------------------------------------------------------------------
    | Auth Language Lines
    |--------------------------------------------------------------------------
    */

    'login' => 'Se connecter',
    'logout' => 'Se déconnecter',

    /*
    |--------------------------------------------------------------------------
    | CRUD Language Lines
    |--------------------------------------------------------------------------
    */

    'add' => 'Ajouter',
    'remove' => 'Supprimer',
    'create' => 'Créer',
    'edit' => 'Modifier',
    'update' => 'Mettre à jour',
    'delete' => 'Supprimer',
    'destroy' => 'Supprimer (définitivement)',
    'restore' => 'Restaurer',
    'view' => 'Voir',
    'activity' => 'Voir l’historique',
    'index' => 'Liste',
    'show' => 'Détails',
    'publish' => 'Publier',
    'unpublish' => 'Dépublier',
    'validate' => 'Valider',
    'unvalidate' => 'Invalider',

    /*
    |--------------------------------------------------------------------------
    | File Language Lines
    |--------------------------------------------------------------------------
    */

    'upload' => 'Téléverser',
    'download' => 'Télécharger',

    /*
    |--------------------------------------------------------------------------
    | Position Language Lines
    |--------------------------------------------------------------------------
    */

    'edit-position' => 'Modifier la position',
    'drag-drop' => 'Glisser-déposer',
    'tree-view' => 'Vue arborescente',
    'move-up' => 'Déplacer au-dessus',
    'move-down' => 'Déplacer en-dessous',

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    */

    'go-next' => 'Suivant',
    'go-prev' => 'Précédent',
    'go-top' => 'Aller en haut',
    'go-end' => 'Aller en bas',
    'go-back' => 'Retour',
    'learn-more' => 'En savoir plus',
    'see-more' => 'En voir plus',

    /*
    |--------------------------------------------------------------------------
    | SKIP action
    |--------------------------------------------------------------------------
    */

    'skip-to-content' => 'Aller au contenu',
    'skip-to-search' => 'Aller à la recherche',
    'skip-to-list' => 'Aller à la liste',

    /*
    |--------------------------------------------------------------------------
    | SEND action
    |--------------------------------------------------------------------------
    */

    'send-email' => 'Envoyer un email',

    /*
    |--------------------------------------------------------------------------
    | Toggle action
    |--------------------------------------------------------------------------
    */

    'toggle-password' => 'Afficher/masquer le mot de passe',
    'toggle-menu' => 'Ouvrir/fermer le menu',

];
