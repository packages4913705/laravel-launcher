<?php

namespace Nh\LaravelLauncher\Tests;

use Database\Seeders\LauncherAccessSeeder;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Schema;
use Nh\LaravelLauncher\Models\Permission;
use Nh\LaravelLauncher\Models\Role;
use Nh\LaravelLauncher\Models\User;

abstract class TestBackendCase extends BaseTestCase
{
    public User $superadmin;

    public User $admin;

    public User $fake;

    public Role $superadminRole;

    public Role $adminRole;

    public Role $fakeRole;

    public Permission $fakePermission;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(LauncherAccessSeeder::class);

        $this->superadmin = User::factory()->create([
            'name' => 'Superadmin',
            'email' => 'superadmin@test.com',
            'password' => '123456',
        ]);

        $this->admin = User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'password' => '123456',
        ]);

        $this->fake = User::factory()->create([
            'name' => 'Fake',
            'email' => 'fake@test.com',
            'password' => '123456',
        ]);

        $this->superadminRole = Role::firstWhere('name', 'superadmin');
        $this->adminRole = Role::firstWhere('name', 'admin');

        $this->fakeRole = Role::factory()->create([
            'name' => 'fake',
            'label' => 'fake',
        ]);

        // Will automatically be assigned to the superadmin
        $this->fakePermission = Permission::factory()->create([
            'name' => 'fake-permission',
            'label' => 'Fake permission',
        ]);

        $this->superadmin->roles()->attach($this->superadminRole);
        $this->admin->roles()->attach($this->adminRole);
        $this->fake->roles()->attach($this->fakeRole);

        $this->assertTrue(Schema::hasTable('pages'));

    }
}
