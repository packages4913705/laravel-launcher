<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Nh\LaravelLauncher\Models\User;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class UserTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/users';

    private $testUser;

    private $testDeletedUser;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testUser = User::factory()->create([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => '123456',
        ]);

        $this->testDeletedUser = User::factory()->create([
            'name' => 'Test deleted',
            'email' => 'deleted@test.com',
            'password' => '123456',
            'deleted_at' => '2024-07-03 01:00:00',
        ]);
    }

    /**
     * Test index view exist.
     */
    public function test_user_index(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route);
        $response->assertStatus(200);
    }

    /**
     * Test index view failed.
     */
    public function test_user_index_failed(): void
    {
        $response = $this->actingAs($this->fake)->get($this->route);
        $response->assertStatus(403);
    }

    /**
     * Test show view exist.
     */
    public function test_user_show(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testUser->id);
        $response->assertStatus(200);
    }

    /**
     * Test create view exist.
     */
    public function test_user_create(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/create');
        $response->assertStatus(200);
    }

    /**
     * Test store model.
     * -> Insert in database
     * -> Check password hash
     */
    public function test_user_store_success(): void
    {
        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => '123456',
            'password_confirmation' => '123456',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.created'));

        $this->assertDatabaseHas('users', [
            'name' => 'John',
            'email' => 'john@doe.com',
        ]);

        $this->assertTrue(Hash::check('123456', $this->testUser->password)); // Test password hash since the password is same as $fakeUser
    }

    /**
     * Test store model.
     * -> Name is required
     * -> Email is required, unique and must be of type email
     * -> Password is required & confirmed (+ Password rules)
     * -> Password confirmation is required is a password is set
     */
    public function test_user_store_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => null, // Cannot be null
            'email' => 'error', // Must be an email
            'password' => null,
        ]);

        $response->assertStatus(302)->assertInvalid([
            'name', 'email', 'password',
        ]);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'Ok',
            'email' => 'fake@test.com', // Already in use
            'password' => '123456', // Must be confirmed
            'password_confirmation' => null, // Cannot be null if password is set
        ]);

        $response->assertStatus(302)->assertInvalid([
            'email', 'password', 'password_confirmation',
        ]);
    }

    /**
     * Test edit view exist.
     */
    public function test_user_edit(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testUser->id.'/edit');
        $response->assertStatus(200);
    }

    /**
     * Test update model.
     * -> Update all fields in database
     * -> If password is null => keep old one
     * -> If password not null => check password hash
     */
    public function test_user_update_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testUser->id, [
            'name' => 'John',
            'email' => 'john@doe.com',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('users', [
            'id' => $this->testUser->id,
            'name' => 'John',
            'email' => 'john@doe.com',
        ]);

        $this->assertTrue(Hash::check('123456', $this->testUser->password)); // Password should NOT be updated if not passed

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testUser->id, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => 'testhash1',
            'password_confirmation' => 'testhash1',
        ]);

        $user = User::find($this->testUser->id);
        $this->assertTrue(Hash::check('testhash1', $user->password));
    }

    /**
     * Test update model.
     * -> Name is required
     * -> Email is required, unique and must be of type email
     * -> Password is required & confirmed (+ Password rules)
     * -> Password confirmation is required is a password is set
     */
    public function test_user_update_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testUser->id, [
            'name' => null,
            'email' => null,
            'password' => null, // Can be null on update
        ]);

        $response->assertStatus(302)->assertInvalid([
            'name', 'email',
        ]);

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testUser->id, [
            'name' => 'test',
            'email' => 'superadmin@test.com', // Already in use
            'password' => '123456', // Must be confirmed
            'password_confirmation' => null, // Cannot be null
        ]);

        $response->assertStatus(302)->assertInvalid([
            'email', 'password', 'password_confirmation',
        ]);

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testDeletedUser->id, [
            'name' => 'John',
            'email' => 'john@test.com',
        ]);

        $response->assertStatus(403);
    }

    /**
     * Test delete model.
     * -> Set as soft delete
     */
    public function test_user_delete_success(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testUser->id.'/delete');
        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.deleted'));
        $this->assertSoftDeleted($this->testUser);
    }

    /**
     * Test delete model.
     * -> Cannot delete is own account
     * -> Cannot delete a superadmin if not himself a "superadmin"
     */
    public function test_user_delete_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->superadmin->id.'/delete');
        $response->assertStatus(403);
        $this->assertNotSoftDeleted($this->superadmin);

        $response = $this->actingAs($this->admin)->delete($this->route.'/'.$this->superadmin->id.'/delete');
        $response->assertStatus(403);
        $this->assertNotSoftDeleted($this->superadmin);
    }

    /**
     * Test restore model.
     * -> Restore a deleted model
     */
    public function test_user_restore_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testDeletedUser->id.'/restore');
        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.restored'));
        $this->assertNotSoftDeleted($this->testDeletedUser);
    }

    /**
     * Test restore model.
     * -> Cannot restore if not a deleted model
     */
    public function test_user_restore_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testUser->id.'/restore');
        $response->assertStatus(403);
    }

    /**
     * Test destroy model.
     * -> Removed from database
     */
    public function test_user_destroy_success(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testDeletedUser->id);
        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.destroyed'));
        $this->assertDatabaseMissing('users', [
            'id' => $this->testDeletedUser->id,
        ]);
    }

    /**
     * Test destroy model.
     * -> Cannot destroy if not a deleted model
     */
    public function test_user_destroy_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testUser->id);
        $response->assertStatus(403);
        $this->assertDatabaseHas('users', [
            'id' => $this->testUser->id,
        ]);
    }
}
