<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Models\Category;
use Nh\LaravelLauncher\Models\Page;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class PageCategoryTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/pages';

    private $testPage;

    private $fakeCategory;

    private $testCategory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testPage = Page::factory()->create([
            'slug' => 'my-page',
            'name' => 'my-page',
            'label' => 'My page',
        ]);

        $this->fakeCategory = Category::factory()->create([
            'name' => 'fake-category',
            'label' => 'Fake category',
        ]);

        $this->testPage->categories()->attach($this->fakeCategory);

        $this->testCategory = Category::factory()->create([
            'name' => 'test-category',
            'label' => 'test category',
        ]);

        Config::set('laravel-launcher.page.categorizable', true);

    }

    /**
     * Test store model.
     * -> Insert in database
     * -> Set the categorizable relation
     */
    public function test_page_category_store_success(): void
    {

        $count = DB::table('pages')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'a test',
            'title' => 'my title',
            'description' => 'my description',
            'categorizable' => 1,
            'categories' => [$this->testCategory->id],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('pages', [
            'label' => 'a test',
            'title' => 'my title',
            'description' => 'my description',
        ]);

        $this->assertDatabaseHas('categorizables', [
            'category_id' => $this->testCategory->id,
            'categorizable_type' => 'page',
            'categorizable_id' => $count + 1,
        ]);
    }

    /**
     * Test store model.
     * -> Cannot set role if no "categorizable" value
     */
    public function test_page_category_store_fail(): void
    {
        $count = DB::table('pages')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'a test',
            'title' => 'my title',
            'description' => 'my description',
            'categories' => [$this->testCategory->id],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseMissing('categorizables', [
            'category_id' => $this->testCategory->id,
            'categorizable_type' => 'page',
            'categorizable_id' => $count + 1,
        ]);

    }

    /**
     * Test update model.
     * -> Update in database
     * -> Update the categorizable relation
     */
    public function test_page_category_update_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPage->id, [
            'label' => 'Updated',
            'title' => 'My page',
            'categorizable' => 1,
            'categories' => [$this->testCategory->id],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('pages', [
            'id' => $this->testPage->id,
            'label' => 'Updated',
            'title' => 'My page',
        ]);

        $this->assertDatabaseHas('categorizables', [
            'category_id' => $this->testCategory->id,
            'categorizable_type' => 'page',
            'categorizable_id' => $this->testPage->id,
        ]);

        $this->assertDatabaseMissing('categorizables', [
            'category_id' => $this->fakeCategory->id,
            'categorizable_type' => 'page',
            'categorizable_id' => $this->testPage->id,
        ]);
    }

    /**
     * Test update model.
     * -> Cannot set role if no "categorizable" value
     * -> If Config is set to 1 (only 1 per page)
     */
    public function test_page_category_update_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPage->id, [
            'label' => 'Updated',
            'title' => 'My page',
            'categories' => [$this->testCategory->id],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('pages', [
            'id' => $this->testPage->id,
            'label' => 'Updated',
            'title' => 'My page',
        ]);

        $this->assertDatabaseMissing('categorizables', [
            'category_id' => $this->testCategory->id,
            'categorizable_type' => 'page',
            'categorizable_id' => $this->testPage->id,
        ]);

    }

    /**
     * Test destroy model.
     * -> Destroy in database
     * -> Destroy the categorizable relation
     */
    public function test_page_category_destroy_success(): void
    {

        // Soft delete first
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id.'/delete');
        $this->assertSoftDeleted($this->testPage);
        $response->assertValid()->assertRedirect($this->route);

        // Check translations still exist
        $this->assertDatabaseHas('categorizables', [
            'categorizable_id' => $this->testPage->id,
            'categorizable_type' => 'page',
        ]);

        // Force delete
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id);
        $this->assertDatabaseMissing('pages', [
            'id' => $this->testPage->id,
        ]);
        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseMissing('categorizables', [
            'categorizable_id' => $this->testPage->id,
            'categorizable_type' => 'page',
        ]);
    }

    /**
     * Test destroy model relation.
     * -> Keep relation with category
     */
    public function test_page_category_destroy_fail(): void
    {
        Config::set('laravel-launcher.page.protected', 'my-page');

        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id);

        $response->assertStatus(403);

        $this->assertDatabaseHas('pages', [
            'id' => $this->testPage->id,
        ]);

        $this->assertDatabaseHas('categorizables', [
            'categorizable_id' => $this->testPage->id,
            'categorizable_type' => 'page',
        ]);

    }

    /**
     * Test categorizable is false.
     */
    public function test_page_categorizable_false_fail(): void
    {

        Config::set('laravel-launcher.page.categorizable', false);

        $count = DB::table('pages')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'test',
            'title' => 'test',
            'description' => 'test',
            'categories' => [$this->testCategory->id],
            'categorizable' => 1,
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseMissing('categorizables', [
            'category_id' => $this->testCategory->id,
            'categorizable_type' => 'page',
            'categorizable_id' => $count + 1,
        ]);

    }

    /**
     * Test categorizable is limited.
     */
    public function test_page_categorizable_number_fail(): void
    {

        Config::set('laravel-launcher.page.categorizable', 1);

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPage->id, [
            'label' => 'Updated',
            'title' => 'My page',
            'categories' => [$this->fakeCategory->id, $this->testCategory->id],
        ]);

        $response->assertStatus(302)->assertInvalid([
            'categories',
        ]);

        $this->assertDatabaseHas('categorizables', [
            'category_id' => $this->fakeCategory->id,
            'categorizable_type' => 'page',
            'categorizable_id' => $this->testPage->id,
        ]);

        $this->assertDatabaseMissing('categorizables', [
            'category_id' => $this->testCategory->id,
            'categorizable_type' => 'page',
            'categorizable_id' => $this->testPage->id,
        ]);

    }
}
