<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Models\Permission;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class PermissionTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/permissions';

    private $testPermission;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testPermission = Permission::factory()->create([
            'name' => 'test-permission',
            'label' => 'Test permission',
        ]);
    }

    /**
     * Test index view exist.
     */
    public function test_permission_index(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route);
        $response->assertStatus(200);
    }

    /**
     * Test index view failed.
     */
    public function test_permission_index_failed(): void
    {
        $response = $this->actingAs($this->fake)->get($this->route);
        $response->assertStatus(403);
    }

    /**
     * Test show view exist.
     */
    public function test_permission_show(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testPermission->id);
        $response->assertStatus(200);
    }

    /**
     * Test create view exist.
     */
    public function test_permission_create(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/create');
        $response->assertStatus(200);
    }

    /**
     * Test store model.
     * -> Insert in database
     * -> Set the permission to the superadmin role
     * -> If name is null, it's created from label
     */
    public function test_permission_store_success(): void
    {
        $count = DB::table('permissions')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'test',
            'label' => 'test',
            'model' => 'my model',
            'action' => 'test',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.created'));

        $this->assertDatabaseHas('permissions', [
            'name' => 'test',
            'label' => 'test',
            'model' => 'my model',
            'action' => 'test',
        ]);

        $this->assertDatabaseHas('permission_role', [
            'permission_id' => $count + 1,
            'role_id' => $this->superadminRole->id,
        ]);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'Test auto slug',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.created'));

        $this->assertDatabaseHas('permissions', [
            'name' => 'test-auto-slug',
            'label' => 'Test auto slug',
            'model' => null,
            'action' => null,
        ]);

        $this->assertDatabaseHas('permission_role', [
            'permission_id' => $count + 2,
            'role_id' => $this->superadminRole->id,
        ]);
    }

    /**
     * Test store model.
     * -> Name is a "slug" and unique
     * -> Label is required
     */
    public function test_permission_store_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'UPPERCASE', // Cannot be uppercase
            'label' => null, // Cannot be null
        ]);

        $response->assertStatus(302)->assertInvalid([
            'name', 'label',
        ]);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'fake-permission', // Cannot be already use
            'label' => 'ok',
        ]);

        $response->assertStatus(302)->assertInvalid([
            'name',
        ]);
    }

    /**
     * Test edit view exist.
     */
    public function test_permission_edit(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testPermission->id.'/edit');
        $response->assertStatus(200);
    }

    /**
     * Test update model.
     * -> Update all fields in database
     * -> The permission for the superadmin didn't change
     * -> If name is null, take previous
     */
    public function test_permission_update_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPermission->id, [
            'name' => 'update-test',
            'label' => 'Update test',
            'model' => 'fake',
            'action' => 'test',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('permissions', [
            'name' => 'update-test',
            'label' => 'Update test',
            'model' => 'fake',
            'action' => 'test',
        ]);

        $this->assertDatabaseHas('permission_role', [
            'permission_id' => $this->testPermission->id,
            'role_id' => $this->superadminRole->id,
        ]);

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPermission->id, [
            'name' => null,
            'label' => 'New update fake',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('permissions', [
            'name' => 'update-test',
            'label' => 'New update fake',
        ]);

        $this->assertDatabaseHas('permission_role', [
            'permission_id' => $this->testPermission->id,
            'role_id' => $this->superadminRole->id,
        ]);
    }

    /**
     * Test update model.
     * -> Name is a "slug" and unique
     * -> Label is required
     */
    public function test_permission_update_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPermission->id, [
            'name' => 'fake-permission', // Already in use
            'label' => null,
        ]);

        $response->assertStatus(302)->assertInvalid([
            'name', 'label',
        ]);
    }

    /**
     * Test destroy model.
     * -> Removed from database
     * -> Removed relation with superadmin
     */
    public function test_permission_destroy_success(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPermission->id);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.destroyed'));

        $this->assertDatabaseMissing('permissions', [
            'id' => $this->testPermission->id,
        ]);

        $this->assertDatabaseMissing('permission_role', [
            'permission_id' => $this->testPermission->id,
            'role_id' => $this->superadminRole->id,
        ]);
    }

    /**
     * Test destroy model.
     * -> Cannot destroy if set in "protected" into config laravel-launcher
     * -> Keep relation with superadmin
     */
    public function test_permission_destroy_fail(): void
    {
        Config::set('laravel-launcher.permission.protected', 'fake-protected');

        $testProtectedPermission = Permission::factory()->create([
            'name' => 'fake-protected',
            'label' => 'Fake permission',
        ]);

        $response = $this->actingAs($this->admin)->delete($this->route.'/'.$testProtectedPermission->id);

        $response->assertStatus(403);

        $this->assertDatabaseHas('permissions', [
            'id' => $testProtectedPermission->id,
        ]);

        $this->assertDatabaseHas('permission_role', [
            'permission_id' => $testProtectedPermission->id,
            'role_id' => $this->superadminRole->id,
        ]);
    }
}
