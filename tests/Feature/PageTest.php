<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Models\Page;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class PageTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/pages';

    private $testPage;

    private $testDeletedPage;

    private $testProtectedPage;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testPage = Page::factory()->create([
            'slug' => 'fake-page',
            'name' => 'fake-page',
            'label' => 'Fake page',
            'published' => true,
            'in_menu' => true,
            'in_footer' => false,
        ]);

        $this->testDeletedPage = Page::factory()->create([
            'slug' => 'deleted-page',
            'name' => 'deleted-page',
            'label' => 'Deleted page',
            'deleted_at' => '2024-07-03 01:00:00',
        ]);

        $this->testProtectedPage = Page::factory()->create([
            'slug' => 'protected-page',
            'name' => 'protected-page',
            'label' => 'Protected page',
            'published' => false,
        ]);

        Config::set('laravel-launcher.page.protected', 'protected-page');
    }

    /**
     * Test index view exist.
     */
    public function test_page_index(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route);
        $response->assertStatus(200);
    }

    /**
     * Test index view failed.
     */
    public function test_page_index_failed(): void
    {
        $response = $this->actingAs($this->fake)->get($this->route);
        $response->assertStatus(403);
    }

    /**
     * Test show view exist.
     */
    public function test_page_show(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testPage->id);
        $response->assertStatus(200);
    }

    /**
     * Test create view exist.
     */
    public function test_page_create(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/create');
        $response->assertStatus(200);
    }

    /**
     * Test store model.
     * -> Insert in database
     * -> Insert position on created
     * -> If name/slug is null, it's created from label
     * -> Set the published and in_menu property as boolean
     */
    public function test_page_store_success(): void
    {
        $max = DB::table('pages')->max('position');

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'test',
            'slug' => 'test',
            'label' => 'testing',
            'title' => 'title',
            'description' => 'description',
            'published' => true,
            'in_menu' => true,
            'in_footer' => true,
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.created'));

        $this->assertDatabaseHas('pages', [
            'position' => $max + 1,
            'name' => 'test',
            'slug' => 'test',
            'label' => 'testing',
            'title' => 'title',
            'description' => 'description',
            'published' => true,
            'in_menu' => true,
            'in_footer' => true,
        ]);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'Test auto slug',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.created'));

        $this->assertDatabaseHas('pages', [
            'name' => 'test-auto-slug',
            'slug' => 'test-auto-slug',
            'label' => 'Test auto slug',
            'title' => null,
            'description' => null,
            'published' => false,
            'in_menu' => false,
            'in_footer' => false,
        ]);
    }

    /**
     * Test store model.
     * -> Parent id exist
     * -> Name/Slug is a "slug" and unique
     * -> Label is required
     * -> Published is boolean
     * -> In-menu is boolean
     * -> In-footer is boolean
     */
    public function test_page_store_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'parent_id' => 46,
            'name' => 'UPPERCASE', // Cannot be uppercase
            'slug' => 'test test', // Cannot be with space
            'label' => null, // Cannot be null
            'published' => 'string',
            'in_menu' => 'string',
            'in_footer' => 'string',
        ]);

        $response->assertStatus(302)->assertInvalid([
            'parent_id', 'name', 'slug', 'label', 'published', 'in_menu', 'in_footer',
        ]);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'fake-page', // Cannot be already use
            'label' => 'ok',
        ]);

        $response->assertStatus(302)->assertInvalid([
            'name',
        ]);
    }

    /**
     * Test edit view exist.
     */
    public function test_page_edit(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testPage->id.'/edit');
        $response->assertStatus(200);
    }

    /**
     * Test update model.
     * -> Update all fields in database
     * -> If name is null, take previous
     * -> If slug is null, take previous
     */
    public function test_page_update_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPage->id, [
            'name' => 'update-test',
            'slug' => 'update-slug',
            'label' => 'Update test',
            'title' => 'title',
            'description' => 'description',
            'published' => false,
            'in_menu' => true,
            'in_footer' => true,
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('pages', [
            'name' => 'update-test', // Changed
            'slug' => 'update-slug', // Changed
            'label' => 'Update test',
            'title' => 'title',
            'description' => 'description',
            'published' => false,
            'in_menu' => true,
            'in_footer' => true,
        ]);

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPage->id, [
            'name' => null,
            'slug' => null,
            'label' => 'New update fake',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('pages', [
            'name' => 'update-test', // NOT changed
            'slug' => 'update-slug', // NOT changed
            'label' => 'New update fake',
        ]);
    }

    /**
     * Test update model.
     * -> Parent exist
     * -> Name/Slug is a "slug" and unique
     * -> Label is required
     * -> Published is boolean
     * -> In-menu is boolean
     * -> In-footer is boolean
     */
    public function test_page_update_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPage->id, [
            'parent_id' => 46,
            'name' => 'protected-page', // Already in use
            'label' => null,
            'published' => 'string',
            'in_menu' => 'string',
            'in_footer' => 'string',
        ]);

        $response->assertStatus(302)->assertInvalid([
            'parent_id', 'name', 'label', 'published', 'in_menu', 'in_footer',
        ]);
    }

    /**
     * Test delete model.
     * -> Set as soft delete
     */
    public function test_page_delete_success(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id.'/delete');
        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.deleted'));
        $this->assertSoftDeleted($this->testPage);
    }

    /**
     * Test delete model.
     * -> Cannot delete if set in "protected" into config laravel-launcher
     */
    public function test_page_delete_fail(): void
    {

        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testProtectedPage->id);
        $response->assertStatus(403);
        $this->assertDatabaseHas('pages', [
            'id' => $this->testProtectedPage->id,
        ]);
    }

    /**
     * Test restore model.
     * -> Restore a deleted model
     */
    public function test_page_restore_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testDeletedPage->id.'/restore');
        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.restored'));
        $this->assertNotSoftDeleted($this->testDeletedPage);
    }

    /**
     * Test restore model.
     * -> Cannot restore if not a deleted model
     */
    public function test_page_restore_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPage->id.'/restore');
        $response->assertStatus(403);
    }

    /**
     * Test destroy model.
     * -> Removed from database
     */
    public function test_page_destroy_success(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testDeletedPage->id);
        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.destroyed'));
        $this->assertDatabaseMissing('pages', [
            'id' => $this->testDeletedPage->id,
        ]);
    }

    /**
     * Test destroy model.
     * -> Cannot destroy if set in "protected" into config laravel-launcher
     */
    public function test_page_destroy_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id);
        $response->assertStatus(403);
        $this->assertDatabaseHas('pages', [
            'id' => $this->testPage->id,
        ]);
    }

    /**
     * Test publish model.
     */
    public function test_page_publish_success(): void
    {
        $this->assertDatabaseHas('pages', [
            'id' => $this->testProtectedPage->id,
            'published' => false,
        ]);

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testProtectedPage->id.'/publish');
        $response->assertValid()->assertRedirect($this->route.'/'.$this->testProtectedPage->id)->with('status', trans('laravel-launcher::notification.published'));
        $this->assertDatabaseHas('pages', [
            'id' => $this->testProtectedPage->id,
            'published' => true,
        ]);
    }

    /**
     * Test unpublish model.
     */
    public function test_page_unublish_success(): void
    {
        $this->assertDatabaseHas('pages', [
            'id' => $this->testPage->id,
            'published' => true,
        ]);

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPage->id.'/unpublish');
        $response->assertValid()->assertRedirect($this->route.'/'.$this->testPage->id)->with('status', trans('laravel-launcher::notification.unpublished'));
        $this->assertDatabaseHas('pages', [
            'id' => $this->testPage->id,
            'published' => false,
        ]);
    }
}
