<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Models\Category;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class CategoryTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/categories';

    private $testCategory;

    private $testDeletedCategory;

    private $testProtectedCategory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testCategory = Category::factory()->create([
            'name' => 'fake-category',
            'label' => 'Fake category',
        ]);

        $this->testDeletedCategory = Category::factory()->create([
            'name' => 'deleted-category',
            'label' => 'Deleted category',
            'deleted_at' => '2024-07-03 01:00:00',
        ]);

        $this->testProtectedCategory = Category::factory()->create([
            'name' => 'protected-category',
            'label' => 'Protected category',
        ]);

        Config::set('laravel-launcher.category.protected', 'protected-category');
        Config::set('laravel-launcher.category.colorable', ['red', 'blue']);
        Config::set('laravel-launcher.category.typeable', ['typeA', 'typeB']);
    }

    /**
     * Test index view exist.
     */
    public function test_category_index(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route);
        $response->assertStatus(200);
    }

    /**
     * Test index view failed.
     */
    public function test_category_index_failed(): void
    {
        $response = $this->actingAs($this->fake)->get($this->route);
        $response->assertStatus(403);
    }

    /**
     * Test show view exist.
     */
    public function test_category_show(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testCategory->id);
        $response->assertStatus(200);
    }

    /**
     * Test create view exist.
     */
    public function test_category_create(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/create');
        $response->assertStatus(200);
    }

    /**
     * Test store model.
     * -> Insert in database
     * -> Insert position on created
     * -> If name/slug is null, it's created from label
     */
    public function test_category_store_success(): void
    {
        $max = DB::table('categories')->max('position');

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'test',
            'slug' => 'test',
            'label' => 'testing',
            'color' => 'red',
            'type' => 'typeA',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.created'));

        $this->assertDatabaseHas('categories', [
            'position' => $max + 1,
            'name' => 'test',
            'slug' => 'test',
            'label' => 'testing',
            'color' => 'red',
            'type' => 'typeA',
        ]);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'Test auto slug',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.created'));

        $this->assertDatabaseHas('categories', [
            'name' => 'test-auto-slug',
            'slug' => 'test-auto-slug',
            'label' => 'Test auto slug',
            'color' => null,
            'type' => null,
        ]);
    }

    /**
     * Test store model.
     * -> Parent id exist
     * -> Name/Slug is a "slug" and unique
     * -> Label is required
     * -> Color must be in config
     * -> Type must be in config
     */
    public function test_category_store_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'parent_id' => 46,
            'name' => 'UPPERCASE', // Cannot be uppercase
            'slug' => 'test test', // Cannot be with space
            'label' => null, // Cannot be null
            'color' => 'orange',
            'type' => 'typeC',
        ]);

        $response->assertStatus(302)->assertInvalid([
            'parent_id', 'name', 'slug', 'label', 'color', 'type',
        ]);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'fake-category', // Cannot be already use
            'label' => 'ok',
        ]);

        $response->assertStatus(302)->assertInvalid([
            'name',
        ]);
    }

    /**
     * Test edit view exist.
     */
    public function test_category_edit(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testCategory->id.'/edit');
        $response->assertStatus(200);
    }

    /**
     * Test update model.
     * -> Update all fields in database
     * -> If name is null, take previous
     * -> If slug is null, take previous
     */
    public function test_category_update_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testCategory->id, [
            'name' => 'update-name',
            'slug' => 'update-slug',
            'label' => 'Update test',
            'color' => 'blue',
            'type' => 'typeB',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('categories', [
            'position' => $this->testCategory->position,
            'name' => 'update-name', // Changed
            'slug' => 'update-slug', // Changed
            'label' => 'Update test',
            'color' => 'blue',
            'type' => 'typeB',
        ]);

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testCategory->id, [
            'name' => null,
            'slug' => null,
            'label' => 'New update fake',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('categories', [
            'name' => 'update-name', // Not changed
            'slug' => 'update-slug', // Not changed
            'label' => 'New update fake',
        ]);
    }

    /**
     * Test update model.
     * -> Parent exist
     * -> Name/Slug is a "slug" and unique
     * -> Label is required
     */
    public function test_category_update_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testCategory->id, [
            'parent_id' => 46,
            'name' => 'protected-category', // Already in use
            'label' => null,
        ]);

        $response->assertStatus(302)->assertInvalid([
            'parent_id', 'name', 'label',
        ]);
    }

    /**
     * Test delete model.
     * -> Set as soft delete
     */
    public function test_category_delete_success(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testCategory->id.'/delete');
        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.deleted'));
        $this->assertSoftDeleted($this->testCategory);
    }

    /**
     * Test delete model.
     * -> Cannot delete if set in "protected" into config laravel-launcher
     */
    public function test_category_delete_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testProtectedCategory->id);
        $response->assertStatus(403);
        $this->assertDatabaseHas('categories', [
            'id' => $this->testProtectedCategory->id,
        ]);
    }

    /**
     * Test restore model.
     * -> Restore a deleted model
     */
    public function test_category_restore_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testDeletedCategory->id.'/restore');
        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.restored'));
        $this->assertNotSoftDeleted($this->testDeletedCategory);
    }

    /**
     * Test restore model.
     * -> Cannot restore if not a deleted model
     */
    public function test_category_restore_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testCategory->id.'/restore');
        $response->assertStatus(403);
    }

    /**
     * Test destroy model.
     * -> Removed from database
     */
    public function test_category_destroy_success(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testDeletedCategory->id);
        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.destroyed'));
        $this->assertDatabaseMissing('categories', [
            'id' => $this->testDeletedCategory->id,
        ]);
    }

    /**
     * Test destroy model.
     * -> Cannot destroy if set in "protected" into config laravel-launcher
     */
    public function test_category_destroy_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testCategory->id);
        $response->assertStatus(403);
        $this->assertDatabaseHas('categories', [
            'id' => $this->testCategory->id,
        ]);
    }
}
