<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Nh\LaravelLauncher\Models\Page;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class MediaTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/media';

    private $testPage;

    private $testMedia;

    protected function setUp(): void
    {
        parent::setUp();

        Config::set('laravel-launcher.page.mediable', [
            'fake' => [
                'format' => 'jpg,pdf',
                'max' => 1,
                'sizes' => [
                    'fit' => [250],
                ],
            ],
        ]);

        $this->testPage = Page::factory()->create([
            'name' => 'fake-page',
        ]);

        $this->testMedia = $this->testPage->media()->create([
            'type' => 'fake',
            'label' => 'fake picture',
            'description' => 'a fake picture',
            'extension' => 'jpg',
            'format' => 'image',
        ]);

        Storage::fake('mediatest');

        Storage::putFileAs('pages/images/', UploadedFile::fake()->image('fake.jpg'), '1-fake-picture.jpg');
        Storage::putFileAs('pages/images/f-250/', UploadedFile::fake()->image('fake.jpg'), '1-fake-picture.jpg');

    }

    /**
     * Test index view exist.
     */
    public function test_media_index(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route);
        $response->assertStatus(200);
    }

    /**
     * Test index view failed.
     */
    public function test_media_index_failed(): void
    {
        $response = $this->actingAs($this->fake)->get($this->route);
        $response->assertStatus(403);
    }

    /**
     * Test show view exist.
     */
    public function test_media_show(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testMedia->id);
        $response->assertStatus(200);
    }

    /**
     * Test edit view exist.
     */
    public function test_media_edit(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testMedia->id.'/edit');
        $response->assertStatus(200);
    }

    /**
     * Test update model.
     * -> Update all fields in database
     * -> Uppdate label will rename all files
     */
    public function test_media_update_fields_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testMedia->id, [
            'label' => 'Updated',
            'description' => 'Updated',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('media', [
            'label' => 'Updated', // changed
            'description' => 'Updated', // changed
            'extension' => 'jpg', // not changed
            'format' => 'image', // not changed
            'type' => 'fake', // not changed
            'mediable_type' => 'page', // not changed
            'mediable_id' => $this->testPage->id, // not changed
        ]);

        Storage::disk('mediatest')->assertMissing('pages/images/1-fake-picture.jpg');
        Storage::disk('mediatest')->assertMissing('pages/images/f-250/1-fake-picture.jpg');

        Storage::disk('mediatest')->assertExists('pages/images/1-updated.jpg');
        Storage::disk('mediatest')->assertExists('pages/images/f-250/1-updated.jpg');

    }

    /**
     * Test update model.
     * -> Update the file and Database
     */
    public function test_media_update_file_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testMedia->id, [
            'label' => 'fake picture',
            'udpate_media_file' => UploadedFile::fake()->image('fake.pdf'),
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('media', [
            'label' => 'fake picture', // not changed
            'extension' => 'pdf', // changed
            'format' => 'pdf', // changed
            'type' => 'fake', // not changed
            'mediable_type' => 'page', // not changed
            'mediable_id' => $this->testPage->id, // not changed
        ]);

        Storage::disk('mediatest')->assertMissing('pages/images/1-fake-picture.jpg');
        Storage::disk('mediatest')->assertMissing('pages/images/f-250/1-fake-picture.jpg');

        Storage::disk('mediatest')->assertExists('pages/pdfs/1-fake-picture.pdf');

    }

    /**
     * Test update model.
     * -> Not correct format
     */
    public function test_page_update_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testMedia->id, [
            'label' => null,
            'udpate_media_file' => UploadedFile::fake()->image('fake.png'),
        ]);

        $response->assertStatus(302)->assertInvalid([
            'label', 'udpate_media_file',
        ]);
    }
}
