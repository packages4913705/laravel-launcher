<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Models\Permission;
use Nh\LaravelLauncher\Models\Role;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class RolePermissionTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/roles';

    private $testRole;

    private $testPermission;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testRole = Role::factory()->create([
            'name' => 'test',
            'label' => 'Test role',
        ]);

        // Add permission to admin role to test
        $permissions = Permission::select('id')->where('model', 'role')->get()->modelKeys();
        $this->adminRole->permissions()->attach($permissions);
    }

    /**
     * Test store model.
     * -> Insert in database
     * -> Set relation between role and permission
     */
    public function test_role_permission_store_success(): void
    {
        $count = DB::table('roles')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'designer',
            'label' => 'designer',
            'permissions' => $this->fakePermission->id,
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('roles', [
            'name' => 'designer',
            'label' => 'designer',
        ]);

        $this->assertDatabaseHas('permission_role', [
            'permission_id' => $this->fakePermission->id,
            'role_id' => $count + 1,
        ]);
    }

    /**
     * Test store model.
     * -> Failed if user has not himself the permission
     */
    public function test_role_permission_store_fail(): void
    {
        $count = DB::table('roles')->count();

        $response = $this->actingAs($this->admin)->post($this->route, [
            'name' => 'designer',
            'label' => 'designer',
            'permissions' => $this->fakePermission->id,
        ]);

        $response->assertStatus(403);

        $this->assertDatabaseMissing('permission_role', [
            'permission_id' => $this->fakePermission->id,
            'role_id' => $count + 1,
        ]);
    }

    /**
     * Test store model.
     * -> Update in database
     * -> Update the relation between role and permission
     */
    public function test_role_permission_update_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testRole->id, [
            'name' => 'test-update',
            'label' => 'Test update',
            'permissions' => $this->fakePermission->id,
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('roles', [
            'name' => 'test-update',
            'label' => 'Test update',
        ]);

        $this->assertDatabaseHas('permission_role', [
            'permission_id' => $this->fakePermission->id,
            'role_id' => $this->testRole->id,
        ]);

        $newPermission = Permission::factory()->create([
            'name' => 'new-permission',
            'label' => 'New permission',
        ]);

        $this->adminRole->permissions()->attach($newPermission->id);

        $response = $this->actingAs($this->admin)->patch($this->route.'/'.$this->testRole->id, [
            'name' => 'test-update',
            'label' => 'Test update',
            'permissions' => $newPermission->id,
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('permission_role', [
            'permission_id' => $this->fakePermission->id,
            'role_id' => $this->testRole->id,
        ]);

        $this->assertDatabaseHas('permission_role', [
            'permission_id' => $newPermission->id,
            'role_id' => $this->testRole->id,
        ]);
    }

    /**
     * Test store model.
     * -> Failed if user has not himself the permission
     */
    public function test_role_permission_update_fail(): void
    {
        $response = $this->actingAs($this->admin)->patch($this->route.'/'.$this->testRole->id, [
            'name' => 'test-update',
            'label' => 'Test update',
            'permissions' => $this->fakePermission->id,
        ]);

        $response->assertStatus(403);

        $this->assertDatabaseMissing('permission_role', [
            'permission_id' => $this->fakePermission->id,
            'role_id' => $this->testRole->id,
        ]);
    }
}
