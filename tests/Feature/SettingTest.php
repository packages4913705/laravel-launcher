<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class SettingTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/settings';

    /**
     * Test index view exist.
     */
    public function test_setting_index(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route);
        $response->assertStatus(200);
    }

    /**
     * Test index view failed.
     */
    public function test_setting_index_failed(): void
    {
        $response = $this->actingAs($this->fake)->get($this->route);
        $response->assertStatus(403);
    }

    /**
     * Test edit view exist.
     */
    public function test_setting_edit(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/edit');
        $response->assertStatus(200);
    }

    /**
     * Test update/create model.
     * -> Update or create all fields in database
     */
    public function test_setting_update_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/update', [
            'settings' => [
                'meta' => [
                    'description' => 'some description',
                ],
                'social' => [
                    'facebook' => 'http://facebook.com',
                    'linkedin' => 'http://linkedin.com',
                ],
            ],
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('settings', [
            'name' => 'meta',
            'value->description' => 'some description',
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'social',
            'value->facebook' => 'http://facebook.com',
            'value->linkedin' => 'http://linkedin.com',
        ]);
    }

    /**
     * Test update model.
     * -> Social link must be URL
     * -> Contact email must be an email
     * -> Contact phone must be a phone number
     */
    public function test_setting_update_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/update', [
            'settings' => [
                'social' => [
                    'facebook' => 'error',
                ],
                'contact' => [
                    'email' => 'error',
                    'phone' => 'error',
                ],
            ],
        ]);

        $response->assertStatus(302)->assertInvalid([
            'settings.social.facebook', 'settings.contact.email', 'settings.contact.phone',
        ]);
    }
}
