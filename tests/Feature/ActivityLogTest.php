<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Models\User;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class ActivityLogTest extends TestBackendCase
{
    use RefreshDatabase;

    /**
     * Test that an activity log is created by system.
     */
    public function test_created_by_system(): void
    {
        $user = User::factory()->create([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => '123456',
        ]);

        $this->assertDatabaseHas('activity_logs', [
            'event' => 'created',
            'value->name' => $user->name,
            'value->email' => $user->email,
            'comment' => '[system]',
            'loggable_type' => 'user',
            'loggable_id' => $user->id,
            'user_id' => null,
        ]);

        $this->assertTrue($user->activity_logs()->exists());

        $this->assertTrue($user->activity_log->by == trans('laravel-launcher::backend.log.system'));
    }

    /**
     * Test the addLog() method.
     */
    public function test_add_log_method(): void
    {
        $this->fake->addlog('custom', ['1' => 'test'], 'A comment');

        $this->assertDatabaseHas('activity_logs', [
            'event' => 'custom',
            'value->1' => 'test',
            'comment' => '[system] - A comment',
            'loggable_type' => 'user',
            'loggable_id' => $this->fake->id,
            'user_id' => null,
        ]);
    }

    /**
     * Check that the observer will run the addLog() method on created model.
     */
    public function test_observer_created(): void
    {
        $count = DB::table('users')->count();

        $this->actingAs($this->superadmin)->post('/backend/users', [
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'roleable' => 1,
            'roles' => [$this->adminRole->id],
        ]);

        $this->assertDatabaseHas('activity_logs', [
            'event' => 'created',
            'value->name' => 'John',
            'value->email' => 'john@doe.com',
            'comment' => null,
            'loggable_type' => 'user',
            'loggable_id' => $count + 1,
            'user_id' => $this->superadmin->id,
        ]);

        $this->assertDatabaseHas('activity_logs', [
            'event' => 'synchronized',
            'value->roles->updated' => null,
            'value->roles->attached' => '['.$this->adminRole->id.']',
            'value->roles->detached' => null,
            'comment' => null,
            'loggable_type' => 'user',
            'loggable_id' => $count + 1,
            'user_id' => $this->superadmin->id,
        ]);
    }

    /**
     * Check that the observer will run the addLog() method on updated model.
     */
    public function test_observer_updated(): void
    {
        $this->actingAs($this->superadmin)->patch('/backend/users/'.$this->fake->id, [
            'name' => 'Update fake',
            'email' => 'fake@test.com',
            'roleable' => 1,
            'roles' => [$this->adminRole->id],
        ]);

        $this->assertDatabaseHas('activity_logs', [
            'event' => 'updated',
            'value->name' => 'Update fake',
            'comment' => null,
            'loggable_type' => 'user',
            'loggable_id' => $this->fake->id,
            'user_id' => $this->superadmin->id,
        ]);

        $this->assertDatabaseHas('activity_logs', [
            'event' => 'synchronized',
            'value->roles->updated' => null,
            'value->roles->attached' => '['.$this->adminRole->id.']',
            'value->roles->detached' => '['.$this->fakeRole->id.']',
            'comment' => null,
            'loggable_type' => 'user',
            'loggable_id' => $this->fake->id,
            'user_id' => $this->superadmin->id,
        ]);
    }

    /**
     * Check that the observer will run the addLog() method on deleted model.
     */
    public function test_observer_deleted(): void
    {
        $this->actingAs($this->superadmin)->delete('/backend/users/'.$this->fake->id.'/delete');

        $this->assertDatabaseHas('activity_logs', [
            'event' => 'deleted',
            'value' => '[]',
            'comment' => null,
            'loggable_type' => 'user',
            'loggable_id' => $this->fake->id,
            'user_id' => $this->superadmin->id,
        ]);
    }

    /**
     * Check that the observer will run the addLog() method on restored model.
     */
    public function test_observer_restored(): void
    {
        $this->actingAs($this->superadmin)->delete('/backend/users/'.$this->fake->id.'/delete');
        $this->actingAs($this->superadmin)->patch('/backend/users/'.$this->fake->id.'/restore');

        $this->assertDatabaseHas('activity_logs', [
            'event' => 'restored',
            'value->deleted_at' => null,
            'comment' => null,
            'loggable_type' => 'user',
            'loggable_id' => $this->fake->id,
            'user_id' => $this->superadmin->id,
        ]);
    }

    /**
     * Check that the observer will run the addLog() method on destroyed model.
     */
    public function test_observer_destroyed(): void
    {
        $this->actingAs($this->superadmin)->delete('/backend/users/'.$this->fake->id.'/delete');
        $this->actingAs($this->superadmin)->delete('/backend/users/'.$this->fake->id);

        $this->assertDatabaseHas('activity_logs', [
            'event' => 'destroyed',
            'value' => '[]',
            'comment' => null,
            'loggable_type' => 'user',
            'loggable_id' => $this->fake->id,
            'user_id' => $this->superadmin->id,
        ]);
    }
}
