<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Models\Permission;
use Nh\LaravelLauncher\Models\User;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class UserRoleTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/users';

    private $testUser;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testUser = User::factory()->create([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => '123456',
        ]);

        // Add permission to admin role to test
        $permissions = Permission::select('id')->where('model', 'user')->get()->modelKeys();
        $this->adminRole->permissions()->attach($permissions);
        $this->fakeRole->permissions()->attach($permissions);

        Config::set('laravel-launcher.user.roleable', true);
    }

    /**
     * Test store model.
     * -> Insert in database
     * -> Set the user and role relation
     */
    public function test_user_role_store_success(): void
    {
        $count = DB::table('users')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'roleable' => 1,
            'roles' => [$this->adminRole->id],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('users', [
            'name' => 'John',
            'email' => 'john@doe.com',
        ]);

        $this->assertDatabaseHas('roleables', [
            'role_id' => $this->adminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $count + 1,
        ]);
    }

    /**
     * Test store model.
     * -> Cannot set role if no "roleable" value
     * -> Cannot set role "superadmin" if user is not himself "superadmin"
     * -> Cannot set role "admin" if user is not himself "superadmin" or "admin"
     */
    public function test_user_role_store_fail(): void
    {
        $count = DB::table('users')->count();

        $response = $this->actingAs($this->admin)->post($this->route, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'roles' => [$this->adminRole->id], // Required the roleable value
        ]);

        $this->assertDatabaseMissing('roleables', [
            'role_id' => $this->adminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $count + 1,
        ]);

        $response = $this->actingAs($this->admin)->post($this->route, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'roleable' => 1,
            'roles' => [$this->superadminRole->id], // Only a superadmin can set a superadmin
        ]);

        $this->assertDatabaseMissing('roleables', [
            'role_id' => $this->superadminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $count + 1,
        ]);

        $response = $this->actingAs($this->fake)->post($this->route, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'roleable' => 1,
            'roles' => [$this->adminRole->id], // If not superadmin or admin, user can only set role that he own
        ]);

        $this->assertDatabaseMissing('roleables', [
            'role_id' => $this->adminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $count + 1,
        ]);
    }

    /**
     * Test update model.
     * -> Update in database
     * -> Update the user and role relation
     * -> Update not impact "protected" roles
     */
    public function test_user_role_update_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testUser->id, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'roleable' => 1,
            'roles' => [$this->adminRole->id],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('roleables', [
            'role_id' => $this->adminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $this->testUser->id,
        ]);

        $response = $this->actingAs($this->fake)->patch($this->route.'/'.$this->testUser->id, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'roleable' => 1,
            'roles' => [$this->fakeRole->id],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('roleables', [
            'role_id' => $this->adminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $this->testUser->id,
        ]);

        $this->assertDatabaseHas('roleables', [
            'role_id' => $this->fakeRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $this->testUser->id,
        ]);
    }

    /**
     * Test store model.
     * -> Cannot set role if no "roleable" value
     * -> Cannot set role "superadmin" if user is not himself "superadmin"
     * -> Cannot set role "admin" if user is not himself "superadmin" or "admin"
     */
    public function test_user_role_update_fail(): void
    {

        $response = $this->actingAs($this->admin)->patch($this->route.'/'.$this->testUser->id, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'roles' => [$this->superadminRole->id], // Required the roleable value
        ]);

        $this->assertDatabaseMissing('roleables', [
            'role_id' => $this->superadminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $this->testUser->id,
        ]);

        $response = $this->actingAs($this->admin)->patch($this->route.'/'.$this->testUser->id, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'roleable' => 1,
            'roles' => [$this->superadminRole->id], // Only a superadmin can set a superadmin
        ]);

        $this->assertDatabaseMissing('roleables', [
            'role_id' => $this->superadminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $this->testUser->id,
        ]);

        $response = $this->actingAs($this->fake)->patch($this->route.'/'.$this->testUser->id, [
            'name' => 'John',
            'email' => 'john@doe.com',
            'roleable' => 1,
            'roles' => [$this->adminRole->id], // If not superadmin or admin, user can only set role that he own
        ]);

        $this->assertDatabaseMissing('roleables', [
            'role_id' => $this->adminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $this->testUser->id,
        ]);
    }

    /**
     * Test roleable is false.
     */
    public function test_page_roleable_false_fail(): void
    {

        Config::set('laravel-launcher.user.roleable', false);

        $count = DB::table('users')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'Tom',
            'email' => 'tom@doe.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'roleable' => 1,
            'roles' => [$this->adminRole->id],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseMissing('roleables', [
            'role_id' => $this->adminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $count + 1,
        ]);

    }

    /**
     * Test roleable is limited.
     */
    public function test_page_roleable_number_fail(): void
    {

        Config::set('laravel-launcher.user.roleable', 1);

        $this->testUser->roles()->attach($this->adminRole);

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testUser->id, [
            'name' => 'Anne',
            'email' => 'anne@doe.com',
            'roleable' => 1,
            'roles' => [$this->adminRole->id, $this->fakeRole->id],
        ]);

        $response->assertStatus(302)->assertInvalid([
            'roles',
        ]);

        $this->assertDatabaseHas('roleables', [
            'role_id' => $this->adminRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $this->testUser->id,
        ]);

        $this->assertDatabaseMissing('roleables', [
            'role_id' => $this->fakeRole->id,
            'roleable_type' => 'user',
            'roleable_id' => $this->testUser->id,
        ]);

    }
}
