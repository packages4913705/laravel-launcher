<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Nh\LaravelLauncher\Models\User;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class AuthTest extends TestBackendCase
{
    use RefreshDatabase;

    private $testUser;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testUser = User::factory()->create([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => '123456',
        ]);
    }

    /**
     * Test login view exist.
     */
    public function test_login_view(): void
    {
        $response = $this->get(route('login'));
        $response->assertStatus(200);
    }

    /**
     * Test authenticate success.
     */
    public function test_authenticate_success(): void
    {
        $response = $this->post(route('login'), [
            'email' => $this->testUser->email,
            'password' => '123456',
        ]);

        $response->assertRedirect('/backend');
        $this->assertAuthenticatedAs($this->testUser);
    }

    /**
     * Test authenticate failed.
     */
    public function test_authenticate_fail(): void
    {
        $response = $this->post(route('login'), [
            'email' => 'nonexistent@example.com',
            'password' => 'wrongpassword',
        ]);

        $response->assertSessionHasErrors('email');
        $this->assertGuest();
    }

    /**
     * Test logout.
     */
    public function test_logout(): void
    {
        $this->actingAs($this->testUser);
        $response = $this->post(route('logout'));
        $response->assertRedirect('/');
        $this->assertGuest();
    }

    /**
     * Test forgot password view exist.
     */
    public function test_forgot_view(): void
    {
        $response = $this->get(route('password.forgot'));
        $response->assertStatus(200);
    }

    /**
     * Test forgot password send an email.
     */
    public function test_password_send(): void
    {

        Password::shouldReceive('sendResetLink')
            ->once()
            ->with(['email' => $this->testUser->email])
            ->andReturn(Password::RESET_LINK_SENT);

        $response = $this->post(route('password.send'), [
            'email' => $this->testUser->email,
        ]);

        $response->assertSessionHas('success', __('passwords.sent'));
    }

    /**
     * Test reset password view exist.
     */
    public function test_reset_view(): void
    {
        $token = Str::random(60);
        $response = $this->get(route('password.reset', ['token' => $token]));
        $response->assertStatus(200);
        $response->assertViewHas('token', $token);
    }

    /**
     * Test the password update with success.
     */
    public function test_password_update_success(): void
    {
        $token = Password::broker()->createToken($this->testUser);

        $response = $this->post(route('password.update'), [
            'email' => $this->testUser->email,
            'password' => 'newpassword123',
            'password_confirmation' => 'newpassword123',
            'token' => $token,
        ]);

        $response->assertRedirect(route('login'));
        $this->assertTrue(Hash::check('newpassword123', $this->testUser->fresh()->password));
    }

    /**
     * Test the password update fail.
     */
    public function test_password_update_fail(): void
    {
        $response = $this->post(route('password.update'), [
            'email' => $this->testUser->email,
            'password' => 'newpassword123',
            'password_confirmation' => 'newpassword123',
            'token' => 'invalid_token',
        ]);

        $response->assertSessionHasErrors('email');
    }
}
