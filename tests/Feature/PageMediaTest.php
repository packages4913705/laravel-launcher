<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Nh\LaravelLauncher\Models\Media;
use Nh\LaravelLauncher\Models\Page;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class PageMediaTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/pages';

    private $testPage;

    protected function setUp(): void
    {
        parent::setUp();

        Config::set('laravel-launcher.page.mediable', [
            'picture' => [
                'format' => 'jpg,png',
                'max' => 2,
                'sizes' => [
                    'fit' => [250],
                    'height' => [250],
                    'width' => [250],
                ],
            ],
            'document' => [
                'format' => 'pdf',
            ],
        ]);

        $this->testPage = Page::factory()->create([
            'slug' => 'page',
            'name' => 'page',
            'label' => 'My page',
        ]);

        $this->testPage->media()->create([
            'type' => 'fake',
            'label' => 'fake picture',
            'description' => 'a fake picture',
            'extension' => 'jpg',
            'format' => 'image',
        ]);

        $this->testPage->media()->create([
            'type' => 'fake',
            'label' => 'fake file',
            'description' => 'a fake file',
            'extension' => 'pdf',
            'format' => 'pdf',
        ]);

        Storage::fake('mediatest');

        Storage::putFileAs('pages/images/', UploadedFile::fake()->image('fake.jpg'), '1-fake-picture.jpg');
        Storage::putFileAs('pages/images/f-250/', UploadedFile::fake()->image('fake.jpg'), '1-fake-picture.jpg');
        Storage::putFileAs('pages/pdfs/', UploadedFile::fake()->image('fake.pdf'), '2-fake-file.pdf');
    }

    /**
     * Test store model relation.
     * -> Store the media in database
     * -> The media has been uploaded in storage
     * -> The media attribute are correct
     */
    public function test_page_media_store_success(): void
    {
        $count = DB::table('pages')->count();
        $countMedia = DB::table('media')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'testing media',
            'media_to_add' => [
                'document' => [
                    0 => [
                        'label' => 'my document',
                        'description' => 'a document',
                        'file' => UploadedFile::fake()->image('file.pdf'),
                    ],
                    1 => [
                        'file' => UploadedFile::fake()->image('custom-document.pdf'),
                    ],
                ],
            ],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('media', [
            'id' => $countMedia + 1,
            'type' => 'document',
            'label' => 'my document',
            'description' => 'a document',
            'extension' => 'pdf',
            'format' => 'pdf',
            'mediable_type' => 'page',
            'mediable_id' => $count + 1,
        ]);

        $this->assertDatabaseHas('media', [
            'id' => $countMedia + 2,
            'type' => 'document',
            'label' => 'custom-document',
            'description' => null,
            'extension' => 'pdf',
            'format' => 'pdf',
            'mediable_type' => 'page',
            'mediable_id' => $count + 1,
        ]);

        $media = Media::find($countMedia + 1);
        $this->assertTrue($media->filename == $media->id.'-'.Str::slug($media->label).'.'.$media->extension);
        $this->assertTrue($media->folder == 'pages/pdfs/');
        $this->assertTrue($media->format == 'pdf');

        Storage::disk('mediatest')->assertExists('pages/pdfs/'.$media->filename);
        Storage::disk('mediatest')->assertExists('pages/pdfs/'.($countMedia + 2).'-custom-document.pdf');
    }

    /**
     * Test store fail model relation.
     * -> Not store the media in database
     * -> The media has been NOT uploaded in storage
     * -> Test the validation (format + min/max)
     */
    public function test_page_media_store_fail(): void
    {
        $countMedia = DB::table('media')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'testing file',
            'media_to_add' => [
                'error' => [
                    0 => [
                        'label' => 'my file',
                    ],
                ],
            ],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseMissing('media', [
            'id' => $countMedia + 1,
            'type' => 'error',
            'label' => 'my file',
        ]);

        Storage::disk('mediatest')->assertMissing('pages/pdfs/'.($countMedia + 1).'-my-file.pdf');

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'testing format',
            'media_to_add' => [
                'picture' => [
                    0 => [
                        'label' => 'test doc',
                        'file' => UploadedFile::fake()->image('custom-document.pdf'),
                    ],
                ],
            ],
        ]);

        $response->assertStatus(302)->assertInvalid([
            'media_to_add.picture.0.file',
        ]);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'testing min max',
            'media_to_add' => [
                'picture' => [
                    0 => [
                        'label' => 'test img',
                        'file' => UploadedFile::fake()->image('custom-img.jpg'),
                    ],
                    1 => [
                        'label' => 'test file',
                        'file' => UploadedFile::fake()->image('custom-file.jpg'),
                    ],
                    2 => [
                        'label' => 'test pic',
                        'file' => UploadedFile::fake()->image('custom-pic.jpg'),
                    ],
                ],
            ],
        ]);

        $response->assertStatus(302)->assertInvalid([
            'media_to_add.picture',
        ]);
    }

    /**
     * Test store model relation.
     * -> The media has been uploaded in multiple storage folder by size
     */
    public function test_page_media_resize_success(): void
    {

        $count = DB::table('pages')->count();
        $countMedia = DB::table('media')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'testing picture',
            'media_to_add' => [
                'picture' => [
                    0 => [
                        'label' => 'my picture',
                        'description' => 'a picture',
                        'file' => UploadedFile::fake()->image('photo1.jpg'),
                    ],
                ],
            ],

        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('media', [
            'id' => $countMedia + 1,
            'type' => 'picture',
            'label' => 'my picture',
            'description' => 'a picture',
            'extension' => 'jpg',
            'format' => 'image',
            'mediable_type' => 'page',
            'mediable_id' => $count + 1,
        ]);

        $media = Media::find($countMedia + 1);

        Storage::disk('mediatest')->assertExists('pages/images/'.$media->filename);
        Storage::disk('mediatest')->assertExists('pages/images/f-250/'.$media->filename);
        Storage::disk('mediatest')->assertExists('pages/images/h-250/'.$media->filename);
        Storage::disk('mediatest')->assertExists('pages/images/w-250/'.$media->filename);
    }

    /**
     * Test update model relation.
     * -> Some file has been added in database & storage
     * -> Some file has been updated in database
     * -> Some file has been deleted in database & storage
     */
    public function test_page_media_update_success(): void
    {
        $countMedia = DB::table('media')->count();

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPage->id, [
            'label' => $this->testPage->name,
            'title' => 'updated',
            'media_to_add' => [
                'document' => [
                    0 => [
                        'file' => UploadedFile::fake()->create('new.pdf'),
                    ],
                ],
            ],
            'media_to_update' => [
                'picture' => [
                    0 => [
                        'id' => 1,
                        'label' => 'updated',
                        'description' => 'updated',
                    ],
                ],
            ],
            'media_to_delete' => [
                'picture' => [
                    0 => [
                        'id' => 2,
                    ],
                ],
            ],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('media', [
            'id' => $countMedia + 1,
            'label' => 'new',
            'extension' => 'pdf',
            'format' => 'pdf',
            'mediable_type' => 'page',
            'mediable_id' => $this->testPage->id,
        ]);

        Storage::disk('mediatest')->assertExists('pages/pdfs/'.($countMedia + 1).'-new.pdf');

        $this->assertDatabaseHas('media', [
            'id' => 1,
            'label' => 'updated',
            'description' => 'updated',
            'mediable_type' => 'page',
            'mediable_id' => $this->testPage->id,
        ]);

        Storage::disk('mediatest')->assertExists('pages/images/1-updated.jpg');
        Storage::disk('mediatest')->assertExists('pages/images/f-250/1-updated.jpg');

        $this->assertDatabaseMissing('media', [
            'id' => 2,
        ]);

        Storage::disk('mediatest')->assertMissing('pages/pdfs/2-fake-file.pdf');
    }

    /**
     * Test destroy model relation.
     * -> Removed from database
     * -> Removed files from storage
     */
    public function test_page_media_destroy_success(): void
    {
        // Soft delete first
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id.'/delete');
        $this->assertSoftDeleted($this->testPage);
        $response->assertValid()->assertRedirect($this->route);

        // Check media still exist
        $this->assertDatabaseHas('media', [
            'label' => 'fake picture',
            'mediable_type' => 'page',
            'mediable_id' => $this->testPage->id,
        ]);

        $this->assertDatabaseHas('media', [
            'label' => 'fake file',
            'mediable_type' => 'page',
            'mediable_id' => $this->testPage->id,
        ]);

        Storage::disk('mediatest')->assertExists('pages/images/1-fake-picture.jpg');
        Storage::disk('mediatest')->assertExists('pages/images/f-250/1-fake-picture.jpg');
        Storage::disk('mediatest')->assertExists('pages/pdfs/2-fake-file.pdf');

        // Force delete
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id);
        $this->assertDatabaseMissing('pages', [
            'id' => $this->testPage->id,
        ]);
        $response->assertValid()->assertRedirect($this->route);

        // Check media are gone
        $this->assertDatabaseMissing('media', [
            'label' => 'fake picture',
            'mediable_type' => 'page',
            'mediable_id' => $this->testPage->id,
        ]);

        $this->assertDatabaseMissing('media', [
            'label' => 'fake file',
            'mediable_type' => 'page',
            'mediable_id' => $this->testPage->id,
        ]);

        Storage::disk('mediatest')->assertMissing('pages/images/1-fake-picture.jpg');
        Storage::disk('mediatest')->assertMissing('pages/images/f-250/1-fake-picture.jpg');
        Storage::disk('mediatest')->assertMissing('pages/pdfs/2-fake-file.pdf');

    }

    /**
     * Test destroy model relation.
     * -> Keep relation with media
     * -> Keep files in storage
     */
    public function test_page_media_destroy_fail(): void
    {
        Config::set('laravel-launcher.page.protected', 'page');

        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id);

        $response->assertStatus(403);

        $this->assertDatabaseHas('pages', [
            'id' => $this->testPage->id,
        ]);

        $this->assertDatabaseHas('media', [
            'label' => 'fake picture',
            'mediable_type' => 'page',
            'mediable_id' => $this->testPage->id,
        ]);

        $this->assertDatabaseHas('media', [
            'label' => 'fake file',
            'mediable_type' => 'page',
            'mediable_id' => $this->testPage->id,
        ]);

        Storage::disk('mediatest')->assertExists('pages/images/1-fake-picture.jpg');
        Storage::disk('mediatest')->assertExists('pages/images/f-250/1-fake-picture.jpg');
        Storage::disk('mediatest')->assertExists('pages/pdfs/2-fake-file.pdf');
    }
}
