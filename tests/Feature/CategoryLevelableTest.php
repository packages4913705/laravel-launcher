<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Models\Category;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class CategoryLevelableTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/categories';

    private $testParentCategory;

    private $testChildCategory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testParentCategory = Category::factory()->create([
            'name' => 'fake-parent',
            'label' => 'Fake parent',
        ]);

        $this->testChildCategory = Category::factory()->create([
            'parent_id' => $this->testParentCategory->id,
            'level' => 1,
            'name' => 'fake-child',
            'label' => 'Fake child',
        ]);
    }

    /**
     * Test add the parent and level when config is true.
     */
    public function test_category_levelable_true_success(): void
    {
        Config::set('laravel-launcher.category.levelable', true);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'parent_id' => $this->testParentCategory->id,
            'name' => 'test-1',
            'label' => 'test 1',
        ]);

        $response->assertValid();

        $this->assertDatabaseHas('categories', [
            'parent_id' => $this->testParentCategory->id,
            'level' => 1,
            'name' => 'test-1',
            'label' => 'test 1',
        ]);
    }

    /**
     * Test add the parent and level when level is under config.
     */
    public function test_category_levelable_number_success(): void
    {
        Config::set('laravel-launcher.category.levelable', 2);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'parent_id' => $this->testParentCategory->id,
            'name' => 'test-1',
            'label' => 'test 1',
        ]);

        $response->assertValid();

        $this->assertDatabaseHas('categories', [
            'parent_id' => $this->testParentCategory->id,
            'level' => 1,
            'name' => 'test-1',
            'label' => 'test 1',
        ]);
    }

    /**
     * Test add the parent and level fail if config is set to false.
     */
    public function test_category_levelable_false_fail(): void
    {
        $count = DB::table('categories')->count();

        Config::set('laravel-launcher.category.levelable', false);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'parent_id' => $this->testParentCategory->id,
            'name' => 'test-1',
            'label' => 'test 1',
        ]);

        $response->assertValid();

        $this->assertDatabaseHas('categories', [
            'id' => $count + 1,
            'parent_id' => null,
            'level' => 0,
            'name' => 'test-1',
            'label' => 'test 1',
        ]);
    }

    /**
     * Test add the parent and level fail if config is under config.
     */
    public function test_category_levelable_number_fail(): void
    {
        Config::set('laravel-launcher.category.levelable', 1);

        $count = DB::table('categories')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'parent_id' => $this->testChildCategory->id,
            'name' => 'test-1',
            'label' => 'test 1',
        ]);

        $response->assertValid()->with('status', trans('laravel-launcher::notification.error.max-levelable'));

        $this->assertDatabaseHas('categories', [
            'id' => $count + 1,
            'parent_id' => null,
            'level' => 0,
            'name' => 'test-1',
            'label' => 'test 1',
        ]);
    }
}
