<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Models\Page;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class PageTranslationTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/pages';

    private $testPage;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testPage = Page::factory()->create([
            'slug' => 'my-page',
            'name' => 'my-page',
            'label' => 'My page',
        ]);

        $this->testPage->translations()->create([
            'lang' => 'fr',
            'value' => ['label' => 'Ma page'],
        ]);
    }

    /**
     * Test store model.
     * -> Insert in database
     * -> Set the translation relation
     */
    public function test_page_translation_store_success(): void
    {
        $count = DB::table('pages')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'a test',
            'title' => 'my title',
            'description' => 'my description',
            'translations' => [
                'fr' => [
                    'label' => 'un test',
                    'title' => 'mon titre',
                    'description' => 'ma description',
                ],
            ],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('pages', [
            'label' => 'a test',
            'title' => 'my title',
            'description' => 'my description',
        ]);

        $this->assertDatabaseHas('translations', [
            'translatable_id' => $count + 1,
            'translatable_type' => 'page',
            'lang' => 'fr',
            'value->label' => 'un test',
            'value->title' => 'mon titre',
            'value->description' => 'ma description',
        ]);
    }

    /**
     * Test store model.
     * -> Don't insert in database if empty value and not already exist
     */
    public function test_page_translation_store_fail(): void
    {
        $count = DB::table('pages')->count();

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'a test',
            'title' => 'my title',
            'description' => 'my description',
            'translations' => [
                'fr' => [
                    'label' => null,
                ],
            ],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseMissing('translations', [
            'translatable_id' => $count + 1,
            'translatable_type' => 'page',
            'lang' => 'fr',
            'value->label' => null,
        ]);
    }

    /**
     * Test update model.
     * -> Update in database
     * -> Update the translations relation
     */
    public function test_page_translation_update_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testPage->id, [
            'label' => 'Updated',
            'title' => 'My page',
            'translations' => [
                'fr' => [
                    'label' => 'Modifié',
                    'title' => 'Ma page',
                ],
            ],
        ]);

        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseHas('pages', [
            'id' => $this->testPage->id,
            'label' => 'Updated',
            'title' => 'My page',
        ]);

        $this->assertDatabaseHas('translations', [
            'translatable_id' => $this->testPage->id,
            'translatable_type' => 'page',
            'lang' => 'fr',
            'value->label' => 'Modifié',
            'value->title' => 'Ma page',
        ]);
    }

    /**
     * Test destroy model.
     * -> Destroy in database
     * -> Destroy the translations relation
     */
    public function test_page_translation_destroy_success(): void
    {

        // Soft delete first
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id.'/delete');
        $this->assertSoftDeleted($this->testPage);
        $response->assertValid()->assertRedirect($this->route);

        // Check translations still exist
        $this->assertDatabaseHas('translations', [
            'translatable_id' => $this->testPage->id,
            'translatable_type' => 'page',
        ]);

        // Force delete
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id);
        $this->assertDatabaseMissing('pages', [
            'id' => $this->testPage->id,
        ]);
        $response->assertValid()->assertRedirect($this->route);

        $this->assertDatabaseMissing('translations', [
            'translatable_id' => $this->testPage->id,
            'translatable_type' => 'page',
        ]);
    }

    /**
     * Test destroy model relation.
     * -> Keep relation with translation
     */
    public function test_page_translation_destroy_fail(): void
    {
        Config::set('laravel-launcher.page.protected', 'my-page');

        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testPage->id);

        $response->assertStatus(403);

        $this->assertDatabaseHas('pages', [
            'id' => $this->testPage->id,
        ]);

        $this->assertDatabaseHas('translations', [
            'translatable_id' => $this->testPage->id,
            'translatable_type' => 'page',
        ]);

    }
}
