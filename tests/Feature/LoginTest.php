<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class LoginTest extends TestBackendCase
{
    use RefreshDatabase;

    /**
     * Redirect to the backend when logged.
     */
    public function test_login_true(): void
    {
        $this->post('login', [
            'email' => 'superadmin@test.com',
            'password' => '123456',
        ])->assertRedirect('/backend');
    }

    /**
     * Redirect back if error on login.
     */
    public function test_login_false(): void
    {
        $this->post('login', [
            'email' => 'test@test.com',
            'password' => 'error',
        ])->assertRedirect('/');
    }

    /**
     * Cannot access backend if not logged.
     */
    public function test_access_backend_if_not_auth(): void
    {
        $this->get('/backend')->assertRedirect('/login');
    }

    /**
     * Can access backend if logged.
     */
    public function test_access_backend_if_auth(): void
    {
        $this->actingAs($this->superadmin)->get('/backend')->assertStatus(200);
    }

    /**
     * Redirect to backend if already logged.
     */
    public function test_redirect_backend_if_auth(): void
    {
        $this->actingAs($this->superadmin)->get('/login')->assertRedirect('/backend');
    }
}
