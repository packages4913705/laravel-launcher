<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Nh\LaravelLauncher\Models\Page;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class LocalizationTest extends TestBackendCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Config::set('laravel-launcher.languages', ['fr', 'en', 'de', 'it']);
    }

    /**
     * Test that the localization route will change the session and the locale.
     */
    public function test_localization_true(): void
    {
        $this->get('/')->assertStatus(200);

        $this->assertTrue(App::getLocale() === config('app.locale'));

        $this->get('language/de')->assertRedirect('/');

        $this->assertTrue(session('locale') === 'de');
        $this->assertTrue(app()->getLocale() === 'de');
    }

    /**
     * Test that:
     * -> The translation attribute return in the current language
     * -> The method getTranslation('lang') return in the specified language
     * -> Change the locale will change the default label attribute
     */
    public function test_translation_true(): void
    {
        $page = Page::factory()->create([
            'label' => 'My page',
        ]);

        $page->translations()->create([
            'lang' => 'fr',
            'value' => ['label' => 'Ma page'],
        ]);

        app()->setLocale('en');
        $this->assertNull($page->translation);
        $this->assertNull($page->getTranslation('en'));

        $this->assertTrue($page->label === 'My page');
        $this->assertTrue($page->getTranslation('fr')->value['label'] === 'Ma page');

        app()->setLocale('fr');
        $this->assertTrue($page->label === 'Ma page');
        $this->assertTrue($page->translation->value['label'] === 'Ma page');
    }
}
