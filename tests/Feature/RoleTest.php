<?php

namespace Nh\LaravelLauncher\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Nh\LaravelLauncher\Models\Role;
use Nh\LaravelLauncher\Tests\TestBackendCase;

class RoleTest extends TestBackendCase
{
    use RefreshDatabase;

    private $route = '/backend/roles';

    private $testRole;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testRole = Role::factory()->create([
            'name' => 'test',
            'label' => 'Test role',
        ]);
    }

    /**
     * Test index view exist.
     */
    public function test_role_index(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route);
        $response->assertStatus(200);
    }

    /**
     * Test index view failed.
     */
    public function test_role_index_failed(): void
    {
        $response = $this->actingAs($this->fake)->get($this->route);
        $response->assertStatus(403);
    }

    /**
     * Test show view exist.
     */
    public function test_role_show(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testRole->id);
        $response->assertStatus(200);
    }

    /**
     * Test create view exist.
     */
    public function test_role_create(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/create');
        $response->assertStatus(200);
    }

    /**
     * Test store model.
     * -> Insert in database
     * -> If name is null, it's created from label
     */
    public function test_role_store_success(): void
    {
        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'designer',
            'label' => 'designer',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.created'));

        $this->assertDatabaseHas('roles', [
            'name' => 'designer',
            'label' => 'designer',
        ]);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'label' => 'Web designer',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.created'));

        $this->assertDatabaseHas('roles', [
            'name' => 'web-designer',
            'label' => 'Web designer',
        ]);
    }

    /**
     * Test store model.
     * -> Name is a "slug" and unique
     * -> Label is required
     */
    public function test_role_store_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'UPPERCASE', // Cannot be uppercase
            'label' => null, // Cannot be null
        ]);

        $response->assertStatus(302)->assertInvalid([
            'name', 'label',
        ]);

        $response = $this->actingAs($this->superadmin)->post($this->route, [
            'name' => 'superadmin', // Cannot be already use
            'label' => 'ok',
        ]);

        $response->assertStatus(302)->assertInvalid([
            'name',
        ]);
    }

    /**
     * Test edit view exist.
     */
    public function test_role_edit(): void
    {
        $response = $this->actingAs($this->superadmin)->get($this->route.'/'.$this->testRole->id.'/edit');
        $response->assertStatus(200);
    }

    /**
     * Test update model.
     * -> Update all fields in database
     * -> If name is null, take previous
     */
    public function test_role_update_success(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testRole->id, [
            'name' => 'update-test',
            'label' => 'Update test',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('roles', [
            'name' => 'update-test',
            'label' => 'Update test',
        ]);

        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testRole->id, [
            'name' => null,
            'label' => 'New update test',
        ]);

        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.updated'));

        $this->assertDatabaseHas('roles', [
            'name' => 'update-test',
            'label' => 'New update test',
        ]);
    }

    /**
     * Test update model.
     * -> Name is a "slug" and unique
     * -> Label is required
     */
    public function test_role_update_fail(): void
    {
        $response = $this->actingAs($this->superadmin)->patch($this->route.'/'.$this->testRole->id, [
            'name' => 'superadmin', // Already in use
            'label' => null,
        ]);

        $response->assertStatus(302)->assertInvalid([
            'name', 'label',
        ]);
    }

    /**
     * Test destroy model.
     * -> Removed from database
     */
    public function test_role_destroy_success(): void
    {
        $response = $this->actingAs($this->superadmin)->delete($this->route.'/'.$this->testRole->id);
        $response->assertValid()->assertRedirect($this->route)->with('status', trans('laravel-launcher::notification.success.destroyed'));
        $this->assertDatabaseMissing('roles', [
            'id' => $this->testRole->id,
        ]);
    }

    /**
     * Test destroy model.
     * -> Cannot destroy if set in "protected" into config laravel-launcher
     */
    public function test_role_destroy_fail(): void
    {
        $response = $this->actingAs($this->admin)->delete($this->route.'/'.$this->superadminRole->id);
        $response->assertStatus(403);
        $this->assertDatabaseHas('roles', [
            'id' => $this->superadminRole->id,
        ]);
    }
}
