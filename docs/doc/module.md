# Modules

The Launcher come with some default modules as:
- Setting
- Permission
- Role
- User
- Media
- Category
- Page

## Create a new module

To create a new module simply run this command

```
php artisan module:new {name : The name of the new module lowercase and singular}
```

Follow the steps to configure your module, and then customize the files if necessary

When you are ready, run the migration

```
php artisan migrate
```
