# Components

The Launcher come with some helpful components:

## Backend

- Card
- Information
- Search

- Form / Categorizable
- Form / Colorable
- Form / Levelable
- Form / Mediable
- Form / Roleable
- Form / Translatable
- Form / Typeable

::: danger TODO
Make the doc
:::


## Global

- SVG

- Form / Template
- Form / Checkbox
- Form / Editor
- Form / Inout
- Form / Radio
- Form / Select
- Form / Switch
- Form / Textarea

- Livewire / Datalist
- Livewire / Dynamic
- Livewire / Sortable


::: danger TODO
Make the doc
:::
