# Config

Most of Launcher can be configure via the **config/laravel-launcher.php**

## Localization

The system authorize you to make the website multilingual via

```
'languages' => ['fr', 'en', 'de', 'it'],
```

## Dates

You can change all the dates format via

```
'formats' => [
    'datetime' => 'd/m/Y H:i:s',
    'date' => 'd/m/Y',
],
```

## WYSIWYG Editor

The Launcher come with a **Tiptap** editor for content. You can customize the global toolbar into the config

```
'editor' => [
    'toolbar' => ['typography', 'div', 'span', 'table', 'blockquote', 'list', 'format', 'abbr', 'link', 'align'],
    'headings' => [2, 3, 4, 5, 6],
    'divs' => [],
    'spans' => [],
],
```

::: info Note
The **divs** & **spans** are some array of string classes.
:::

## Settings

You can decide what the user can change into the setting via some booleans

```
'settings' => [
    'seo' => true,
    'social-network' => false,
    'contact' => true,
],
```

## Sidebar

Into the `sidebar` you can customize the order of the modules into your sidebar

```
'sidebar' => [
    'settings' => [
        'category',
    ],
    'content' => [
        'page',
         // !NAME_PLACEHOLDER
    ],
],
```

::: warning
Keep the  `// !NAME_PLACEHOLDER` into the list to make sure that the command for creating a new module will add the name into the list.
:::

## Relations

Each module can have some customizable trait/relation as:
- Categorizable
- Levelable
- Roleable
- Translatable
- Mediable
- Colorable
- Typeable
- Protected

::: info Note
The **Loggable** & **Positionable** traits are directly added to the Model.
:::

Here an exemple of the config

```
'my-module' => [
    'categorizable' => false,           // Can be boolean or a number to set the maximum of related categories
    'levelable' => false,               // Can be boolean or a number to set the maximum of level
    'roleable' => false,                // Can be boolean or a number to set the maximum of role per model
    'translatable' => false,            // Is a boolean
    'mediable' => [                     // Can be false or an array with each type of media
        'picture' => [      
            'format' => 'jpg,png',      // String of the valide format
            'min' => 1,                 // Minimum number of media, if not set is 0
            'max' => 3,                 // Maximum number of media, if not set is infinit
            'size' => '1800 x 800',     // String to display for the helper into the admin
            'weight' => '16000',        // String to display for the helper into the admin
            'sizes' => [                // Arrays of sizes to resize the pictures when uploaded
                'fit' => [100],         
                'height' => [200],
                'width' => [300],
            ],
            'watermark' => [            // Setting for picture that require a watermark
                'path' => null,
                'position' => 'bottom-right',
                'padding' => ['x' => 5, 'y' => 5],
                'opacity' => 100,
            ],
        ],
    ],            
    'typeable' => [],                   // Array of types
    'colorable' => [],                  // Array of colors
    'protected' => []                   // Array of name, to protect some items for delete (≠ superadmin)
],
```