# Helpers

The Launcher come with some helpers.

## Launcher

### getPluralModelName

This method return the correct plural model name as:
FooBar => foo-bars

```php
$item = FooBar::first();
getPluralModelName($item); // return 'foo-bars'
```

### getSingleModelName

This method return the correct singular model name as:
FooBar => foo-bar

```php
$item = FooBar::first();
getSingleModelName($item); // return 'foo-bar'
```

### cleanArray

This method return a clean array (without null and empty array) as:

```php
$array = [
    0 => null,
    1 => [
        0 => 1,
        1 => null
    ],
    2 => 2
];

cleanArray($array); // return [1 => [0 => 1], 2 => 2]
```

### deleteFilesInStorage

This method delete all files into the storage by name, even in the subfolders.

```php
deleteFilesInStorage('file-to-delete.jpg');
```

### renameFilesInStorage

This method rename all files into the storage by name, even in the subfolders.

```php
deleteFilesInStorage('file-old-name.jpg', 'file-new-name.jpg');
```

## Translate

### translate

This helper return a translation, and it will check in multiple files.

```php
translate('my-key', ['first-file', 'seconde-file'], 'package-prepend::')
````

This method will:
1) Search into the 'first-file'
2) If none, search into 'package-prepend::first-file'
3) If there is a seconde file, search into 'seconde-file'
4) If none, search into 'package-prepend::seconde-file'

You can pass a string or an array as second argument.

By default, the third argument is `laravel-launcher::` package.

:::tip
The method will return the plural translation if the **key** is plural.
:::

::: warning
If the first argument start with an uppercase it will skip the translation !
:::


