# Rules

The Launcher come with some helpful rules.

## Launcher

This Rule comme with some preset validation between settings into the config file and the modules.

For exemple, with a module **my-module**:

```php
use Nh\LaravelLauncher\Rules\LauncherRules;

public function rules(): array
{
    return array_merge(LauncherRules::rules('my-module'), [
        'name' => ['required'],
    ]);
}
```

This will add the default validation for:
- Categorizable (array, maximum)
- Mediable (array, file, max:16000, mimes)
- Roleable (array, maximum)
- Typeable (nullable, in_array)
- Colorable (nullable, in_array)

## Phone

Verify that the input is a swiss phone number as:
- 000 000 00 00
- +00 00 000 00 00
- 000-000-00-00 
- +00-00-000-00-00 

```php
use Nh\LaravelLauncher\Rules\Phone;

public function rules(): array
{
    return [
        'phone' => [new Phone],
    ];
}
```

## Slug

Verify that the input is a slug:
- is lowercase
- without space
- without accent

```php
use Nh\LaravelLauncher\Rules\Slug;

public function rules(): array
{
    return [
        'slug' => [new Slug],
    ];
}
```

## WithoutHtml

Verify that the input as NO HTML tags.

```php
use Nh\LaravelLauncher\Rules\WithoutHtml;

public function rules(): array
{
    return [
        'description' => [new WithoutHtml],
    ];
}
```

## WithoutUrl

Verify that the input as NO URL.

```php
use Nh\LaravelLauncher\Rules\WithoutUrl;

public function rules(): array
{
    return [
        'description' => [new WithoutUrl],
    ];
}
```
