# Let's do some cleaning

Not the best part, but it is required to have a nice and beautiful project !

## GIT

Add these lines into your **.gitignore** file

```
.DS_Store
composer.lock
composer.phar 
package-lock.json
```

And remove the lines `/public/build` to make sure that the build JS and CSS are saved in GIT and easy to publish on server


If needed clean the commited files

```
git rm -r --cached .
git add .
git commit -m "fixed untracked files"
```

## Config

Change this config into your `config/app.php`:

```
'timezone' => 'Europe/Zurich',
```


## ENV

You must change/add these lines into your `.env.php` file

Make the website on the right timezone

```
APP_LOCALE=fr
APP_FALLBACK_LOCALE=en
APP_FAKER_LOCALE=fr_CH
```

Make sure that the User model is from the package

```
AUTH_MODEL=\Nh\LaravelLauncher\Models\User
```

Make sure that the files are saved into the storage folder

```
FILESYSTEM_DISK=public
```

And of course change your `APP_NAME` and database variables !

Also, don't forget to change these informations into the `.env.exemple` for the Pipelines !

## Database

Remove all the unnecessary migration as **jobs**, and in the **database/migrations/0001_01_01_000000_create_users_table.php** migration add the soft delete for the users

```
$table->softDeletes();
```

## Files

Remove these files:

- app/Models/User.php
- database/factories/UserFactory.php
- resources/css/*
- resources/js/*
- resources/views/*
- routes/console.php
- tests/Feature/ExempleTest.php
- tests/Unit/ExempleTest.php

## Composer

Remove these packages (exept if you need them):

- laravel/sail
- laravel/pail