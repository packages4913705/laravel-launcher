# Pipeline CI/CD

The Launcher come with a default pipeline for **Gitlab** to make sure every push into your Git server is perfect !

The pipeline will:

- Install the dependencies and keep them into the cache + as an artifact
- Verify your code with **Laravel Pint**
- Run your Unit test suite stocked as **LaravelLauncher** and **Feature**

::: info
The pipeline work with PHP 8.2 and Mysql 5.7
:::