## NPM

Now let's take care of the frontend part with NPM !

Here the list of the required NPM packages that must be into your **package.json**

```
"laravel-vite-plugin": "^1.0",
"vite": "^6.0",
"sass": "^1.77.6",
"@natachah/vanilla-frontend": "^0.1.21",
"@tiptap/core": "^2.10.3",
"@tiptap/extension-bubble-menu": "^2.10.3",
"@tiptap/extension-gapcursor": "^2.10.3",
"@tiptap/extension-link": "^2.10.3",
"@tiptap/extension-table": "^2.10.3",
"@tiptap/extension-table-cell": "^2.10.3",
"@tiptap/extension-table-header": "^2.10.3",
"@tiptap/extension-table-row": "^2.10.3",
"@tiptap/extension-text-align": "^2.10.3",
"@tiptap/extension-underline": "^2.10.3",
"@tiptap/pm": "^2.10.3",
"@tiptap/starter-kit": "^2.10.3"
```

::: info Note
There are automatically added with the Launcher Preset
:::

You need to install them all !

```
npm install
```

You should change the **Cookies** name into the **resources/js/app.js** to make them nicer and related to your project

## Commandes

For develloping, use the commande

```
npm run dev
```

And for the production

```
npm run build
```
