# Install Laravel Launcher

Into your Laravel project, install the Launcher via composer:

```
composer require nh/laravel-launcher
composer dump-autoload
```

To use this packages you will need at least:

- php <Badge type="info" text="^8.2" />
- laravel/framework <Badge type="info" text="^11.9" />
- laravel/livewire <Badge type="info" text="^3.00" />
- laravel/pint <Badge type="info" text="^1.13" />
- fakerphp/faker <Badge type="info" text="^1.24" />
- intervention/image-laravel <Badge type="info" text="dev-main" />
- phpunit/phpunit <Badge type="info" text="^11.5" />


## Publish the files

The Launcher come with a bunch of default files to help you to be faster at creating a new Website.
Let's publish them !

```
php artisan vendor:publish --tag=laravel-launcher-preset --force
```

## Databases

If necessary change the default User in **database/seeders/LauncherAccessSeeder.php**, otherwise you will not have access to the admin !

```
$user = User::create([
    'name' => 'Admin',
    'email' => 'exemple@exemple.com',
    'password' => '123456',
]);
```

Then migrate and seed the default data !

```
php artisan migrate --seed
```

## Providers

To customize the rules for the password, change the **Password::defaults** into the `boot()` method in the **app/Providers/AppServiceProviders.php** file

```
use Illuminate\Validation\Rules\Password;

Password::defaults(function () {
    return Password::min(6)
        //->letters()
        ->numbers();
    //->mixedCase()
    //->uncompromised();
});
```