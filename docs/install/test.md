# Test

The Launcher come with some default Unit test to make sure that your project work as it should !

```
php artisan test
```

::: info Note
When creating a new module, the test files are automatically created !
:::