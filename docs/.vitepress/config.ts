import { defineConfig } from 'vitepress'

// https://vitepress.vuejs.org/config/app-configs
export default defineConfig({

    title: 'Laravel Launcher',
    description: 'Documentation officielle de mon package',

    outDir: '../public',

    themeConfig: {

        socialLinks: [
            {
                icon: {
                    svg: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gitlab" viewBox="0 0 16 16"><path d="m15.734 6.1-.022-.058L13.534.358a.57.57 0 0 0-.563-.356.6.6 0 0 0-.328.122.6.6 0 0 0-.193.294l-1.47 4.499H5.025l-1.47-4.5A.572.572 0 0 0 2.47.358L.289 6.04l-.022.057A4.044 4.044 0 0 0 1.61 10.77l.007.006.02.014 3.318 2.485 1.64 1.242 1 .755a.67.67 0 0 0 .814 0l1-.755 1.64-1.242 3.338-2.5.009-.007a4.05 4.05 0 0 0 1.34-4.668Z" /></svg>'
                },
                link: 'https://gitlab.com/packages4913705/laravel-launcher',
                ariaLabel: 'Gitlab'
            }
        ],

        footer: {
            message: 'Released under the MIT License.',
            copyright: 'Copyright © 2024-present <a href="https://natachaherth.ch">Natacha Herth</a>'
        },

        sidebar: [

            {
                text: 'Install',
                items: [
                    { text: 'Clean', link: '/install/clean' },
                    { text: 'Getting Started', link: '/install/launcher' },
                    { text: 'NPM', link: '/install/npm' },
                    { text: 'Test', link: '/install/test' },
                    { text: 'Pipeline', link: '/install/pipeline' },
                ]
            },
            {
                text: 'Doc',
                items: [
                    { text: 'Config', link: '/doc/config' },
                    { text: 'Modules', link: '/doc/module' },
                    { text: 'Component', link: '/doc/component' },
                    { text: 'Rules', link: '/doc/rule' },
                    { text: 'Helpers', link: '/doc/helper' },
                ]
            },
            {
                text: 'Production',
                items: [
                    { text: 'Git', link: '/prod/git' },
                    { text: 'Prepare', link: '/prod/step-1' },
                    { text: 'Push', link: '/prod/step-2' },
                ]
            },
            {
                text: 'Tips',
                items: [
                    { text: 'Commands', link: '/tip/commands' },
                    { text: 'Best practice', link: '/tip/best' },
                    { text: 'More', link: '/tip/more' },
                ]
            }
        ]
    }
})
