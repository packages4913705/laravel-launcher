# More tips !

Here some global tips that can be usefull

## Generate a list with all attributes

you can get all attribute into a dl as this

```
<dl>
    @foreach ($page->only(['label', 'name', 'slug', 'type', 'color', 'parent']) as $key => $value)
        @if (!empty($value))
            <dt>@choice('laravel-launcher::field.' . $key, 1)</dt>
            <dd>{{ $value }}</dd>
        @endif
    @endforeach
</dl>
```
