# Commands

Here some commands that can be usefull

## Clear

::: danger TODO
Make the doc
:::

## Maintenance

To put your website in maintenance

```
php artisan down --render=errors.maintenance --secret=nhdev
```

And to remove the maintenance mode

```
php artisan up
```