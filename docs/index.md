---
layout: home

hero:
  name: Laravel Launcher
  text: To create quickly a website with a simply CMS.
  tagline: This enable all the default files for frontend and backend to start your project !
  actions:
    - theme: brand
      text: Get Started
      link: /install/clean
    - theme: alt
      text: View on GitLab
      link: https://gitlab.com/packages4913705/laravel-launcher
---