<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->integer('position')->unsigned()->nullable();
            $table->string('label');
            $table->string('description')->nullable();
            $table->string('extension');
            $table->enum('format', ['image', 'vector', 'pdf', 'word', 'excel', 'audio', 'video', 'code'])->nullable();
            $table->string('type')->nullable();
            $table->morphs('mediable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('media');
    }
};
