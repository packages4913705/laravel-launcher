 <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->integer('level')->unsigned()->default(0);
            $table->foreignId('parent_id')->nullable()->constrained('pages', 'id')->nullOnDelete();
            $table->integer('position')->unsigned()->nullable();
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->string('label');
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->boolean('published')->default(1);
            $table->boolean('in_menu')->default(1);
            $table->boolean('in_footer')->default(0);
            $table->string('type')->nullable();
            $table->string('color')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pages');
    }
};
