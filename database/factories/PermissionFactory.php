<?php

namespace Nh\LaravelLauncher\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Nh\LaravelLauncher\Models\Permission;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\LaravelLauncher\Models\Permission>
 */
class PermissionFactory extends Factory
{
    protected $model = Permission::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $label = fake()->name();
        $name = Str::slug($label);

        return [
            'label' => $label,
            'name' => $name,
        ];
    }
}
