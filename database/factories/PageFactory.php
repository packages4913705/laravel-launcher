<?php

namespace Nh\LaravelLauncher\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Nh\LaravelLauncher\Models\Page;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\LaravelLauncher\Models\Page>
 */
class PageFactory extends Factory
{
    protected $model = Page::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $label = fake()->name();
        $slug = Str::slug($label);

        $paragraphs = fake()->paragraphs(rand(2, 6));
        $description = '';
        foreach ($paragraphs as $para) {
            $description .= "<p>{$para}</p>";
        }

        return [
            'slug' => $slug,
            'name' => $slug,
            'label' => $label,
            'title' => fake()->title(),
            'description' => $description,
            'published' => fake()->boolean(),
            'in_menu' => fake()->boolean(),
            'in_footer' => fake()->boolean(),
        ];
    }
}
