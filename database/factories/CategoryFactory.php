<?php

namespace Nh\LaravelLauncher\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Nh\LaravelLauncher\Models\Category;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\LaravelLauncher\Models\Category>
 */
class CategoryFactory extends Factory
{
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $label = fake()->name();
        $slug = Str::slug($label);

        return [
            'slug' => $slug,
            'name' => $slug,
            'label' => $label,
        ];
    }
}
