<?php

namespace Nh\LaravelLauncher\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Nh\LaravelLauncher\Models\Role;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\LaravelLauncher\Models\Role>
 */
class RoleFactory extends Factory
{
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $label = fake()->name();
        $name = Str::slug($label);

        return [
            'label' => $label,
            'name' => $name,
        ];
    }
}
