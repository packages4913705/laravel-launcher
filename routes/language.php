<?php

use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Http\Controllers\LanguageController;

Route::middleware('web')->get('/language/{lang}', LanguageController::class)->name('language.change');
