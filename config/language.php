<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Languages list by there native name
    |--------------------------------------------------------------------------
    */

    'fr' => 'Français',
    'de' => 'Deutsch',
    'en' => 'English',
    'it' => 'Italiano',
    'es' => 'Español',
    'pt' => 'Português',

];
