<?php

namespace Nh\LaravelLauncher\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPasswordNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public string $token) {}

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {

        $url = url('/password/reset/'.$this->token.'?email='.urlencode($notifiable->getEmailForPasswordReset()));

        return (new MailMessage)
            ->subject(trans('laravel-launcher::mail.reset-password.subject'))
            ->greeting(trans('laravel-launcher::mail.reset-password.greeting'))
            ->line(trans('laravel-launcher::mail.reset-password.description'))
            ->action(trans('laravel-launcher::mail.reset-password.action'), $url)
            ->line(trans('laravel-launcher::mail.reset-password.ignor'))
            ->salutation(trans('laravel-launcher::mail.reset-password.salutation'))
            ->markdown('laravel-launcher::notifications.email');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
