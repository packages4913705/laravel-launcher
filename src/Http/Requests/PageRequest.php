<?php

namespace Nh\LaravelLauncher\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Nh\LaravelLauncher\Rules\LauncherRules;
use Nh\LaravelLauncher\Rules\Slug;
use Nh\LaravelLauncher\Rules\WithoutUrl;

class PageRequest extends FormRequest
{
    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'name' => $this->name ?? $this->page->name ?? Str::slug($this->label),
            'slug' => $this->slug ?? $this->page->slug ?? Str::slug($this->label),
            'published' => $this->published ?? 0,
            'in_menu' => $this->in_menu ?? 0,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return array_merge(LauncherRules::rules('page'), [
            'parent_id' => ['nullable', 'exists:pages,id'],
            'name' => [new Slug, Rule::unique('pages', 'name')->ignore($this->page)],
            'slug' => [new Slug, Rule::unique('pages', 'slug')->ignore($this->page)],
            'label' => ['required'],
            'published' => ['boolean'],
            'in_menu' => ['boolean'],
            'in_footer' => ['boolean'],
            'title' => [new WithoutUrl],
        ]);
    }
}
