<?php

namespace Nh\LaravelLauncher\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;
use Nh\LaravelLauncher\Rules\LauncherRules;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $isNew = $this->getMethod() == 'POST';

        return array_merge(LauncherRules::rules('user'), [
            'name' => ['required', Rule::unique('users', 'name')->ignore($this->user)],
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($this->user)],
            'password' => [($isNew ? 'required' : 'nullable'), Password::defaults(), 'confirmed'],
            'password_confirmation' => ['required_with:password'],
        ]);

    }
}
