<?php

namespace Nh\LaravelLauncher\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Nh\LaravelLauncher\Rules\Slug;

class PermissionRequest extends FormRequest
{
    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'name' => $this->name ?? $this->permission->name ?? Str::slug($this->label),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => [new Slug, Rule::unique('permissions', 'name')->ignore($this->permission)],
            'label' => ['required'],
            'model' => ['nullable'],
            'action' => ['nullable'],
        ];
    }
}
