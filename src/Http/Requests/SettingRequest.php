<?php

namespace Nh\LaravelLauncher\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Nh\LaravelLauncher\Rules\Phone;

class SettingRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'settings.seo.description' => ['nullable', 'max:160'],
            'settings.social.*' => ['nullable', 'url'],
            'settings.contact.email' => ['nullable', 'email'],
            'settings.contact.phone' => ['nullable', new Phone],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'settings.seo.description' => strtolower(translate('description', 'field')),
            'settings.contact.phone' => strtolower(translate('phone', 'field')),
            'settings.contact.email' => strtolower(translate('phone', 'field')),
        ];
    }
}
