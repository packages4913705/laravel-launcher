<?php

namespace Nh\LaravelLauncher\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MediaRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {

        $formats = config('laravel-launcher.'.$this->media->mediable_type.'.mediable.'.$this->media->type.'.format') ?? 'jpg,png,svg,pdf,doc,docx,xls,xlsx,csv,ics';

        return [
            'label' => ['required'],
            'udpate_media_file' => ['file', 'max:16000', 'mimes:'.$formats],
        ];
    }
}
