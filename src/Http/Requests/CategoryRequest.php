<?php

namespace Nh\LaravelLauncher\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Nh\LaravelLauncher\Rules\LauncherRules;
use Nh\LaravelLauncher\Rules\Slug;

class CategoryRequest extends FormRequest
{
    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'name' => $this->name ?? $this->category->name ?? Str::slug($this->label),
            'slug' => $this->slug ?? $this->category->slug ?? Str::slug($this->label),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return array_merge(LauncherRules::rules('category'), [
            'parent_id' => ['nullable', 'exists:categories,id'],
            'name' => [new Slug, Rule::unique('categories', 'name')->ignore($this->category)],
            'slug' => [new Slug, Rule::unique('categories', 'slug')->ignore($this->category)],
            'label' => ['required'],
        ]);
    }
}
