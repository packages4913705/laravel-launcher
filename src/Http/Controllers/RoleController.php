<?php

namespace Nh\LaravelLauncher\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Nh\LaravelLauncher\Http\Requests\RoleRequest;
use Nh\LaravelLauncher\Models\Permission;
use Nh\LaravelLauncher\Models\Role;

class RoleController extends Controller
{
    /**
     * Define the available fields that the requests can use.
     *
     * @var array
     */
    private $fields = ['name', 'label'];

    /**
     * Define the view path.
     *
     * @var string
     */
    private $view = 'laravel-launcher::backend.roles.';

    /**
     * Define the redirect route name.
     *
     * @var string
     */
    private $redirect = 'backend.roles.index';

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        // Authorization
        Gate::authorize('viewAny', Role::class);

        // Return the view
        return view($this->view.'index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        // Authorization
        Gate::authorize('create', Role::class);

        // Get permissions
        $permissions = Auth::user()->has_superpowers ? Permission::get()->groupBy('model') : Auth::user()->permissions->groupBy('model');

        // Return the view
        return view($this->view.'create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RoleRequest $request): RedirectResponse
    {
        // Flatten the permissions array
        if ($request->has('permissions')) {
            $request->merge([
                'permissions' => Arr::flatten((array) $request->permissions),
            ]);
        }

        // Authorization
        Gate::authorize('create', Role::class);
        Gate::authorize('set-permissions', $request);

        // Create the resource
        $role = Role::create($request->only($this->fields));

        if ($request->has('permissions')) {

            // Attach the permissions
            $role->permissions()->attach($request->permissions);

            // Add the activity log
            if (in_array('Nh\LaravelLauncher\Traits\Loggable', class_uses($role)) && ! empty(array_filter($request->permissions))) {
                $role->addLog('attached', ['permissions' => $request->permissions]);
            }

        }

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.created'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role): View
    {
        // Authorization
        Gate::authorize('view', $role);

        // Get permissions
        $permissions = Permission::get()->groupBy('model');

        // Return the view
        return view($this->view.'show', compact('role', 'permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Role $role): View
    {
        // Authorization
        Gate::authorize('update', $role);

        // Get permissions
        $permissions = Auth::user()->has_superpowers ? Permission::get()->groupBy('model') : Auth::user()->permissions->groupBy('model');

        // Return the view
        return view($this->view.'edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RoleRequest $request, Role $role): RedirectResponse
    {
        // Flatten the permissions array to use in Gate
        if ($request->has('permissions')) {
            $request->merge([
                'permissions' => Arr::flatten((array) $request->permissions),
            ]);
        }

        // Authorization
        Gate::authorize('create', $role);
        Gate::authorize('set-permissions', $request);

        // Update the resource
        $role->update($request->only($this->fields));

        // Define permissions by request
        $permissions = $request->has('permissions') ? $request->permissions : [];

        // GET protected permissions
        if (! Auth::user()->has_superpowers) {

            // GET protected permissions
            $protected = $role->permissions->whereNotIn('id', Auth::user()->permissions->modelKeys())->modelKeys();

            // Merge request and protected
            if ($protected) {
                $permissions = array_merge($permissions, $protected);
            }
        }

        // Sync the permissions
        $sync = $role->permissions()->sync($permissions);

        // Add the activity log
        if (in_array('Nh\LaravelLauncher\Traits\Loggable', class_uses($role)) && ! empty(array_filter($sync))) {
            $role->addLog('synchronized', ['permissions' => $sync]);
        }

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role): RedirectResponse
    {
        // Authorization
        Gate::authorize('destroy', $role);

        // Delete the resource
        $role->delete();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.destroyed'));
    }
}
