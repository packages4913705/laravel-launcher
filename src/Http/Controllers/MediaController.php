<?php

namespace Nh\LaravelLauncher\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Nh\LaravelLauncher\Http\Requests\MediaRequest;
use Nh\LaravelLauncher\Models\Media;

class MediaController extends Controller
{
    /**
     * Define the available fields that the requests can use.
     *
     * @var array
     */
    private $fields = ['position', 'label', 'description', 'extension'];

    /**
     * Define the view path.
     *
     * @var string
     */
    private $view = 'laravel-launcher::backend.media.';

    /**
     * Define the redirect route name.
     *
     * @var string
     */
    private $redirect = 'backend.media.index';

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        // Authorization
        Gate::authorize('viewAny', Media::class);

        // Return the view
        return view($this->view.'index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Media $media): View
    {
        // Authorization
        Gate::authorize('view', $media);

        // Return the view
        return view($this->view.'show', compact('media'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Media $media): View
    {
        // Authorization
        Gate::authorize('update', $media);

        // Return the view
        return view($this->view.'edit', compact('media'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MediaRequest $request, Media $media): RedirectResponse
    {
        // Authorization
        Gate::authorize('update', $media);

        if ($request->has('udpate_media_file')) {

            // If there is a new file

            try {
                // Update the extension
                $request->merge([
                    'extension' => $request->udpate_media_file->getClientOriginalExtension(),
                ]);

                // Delete the old file
                deleteFilesInStorage($media->getOriginal('filename'));

                // Update the new media model
                $media->update($request->only($this->fields));

                // Upload the new file
                $media->uploadFile($request->udpate_media_file);

            } catch (Exception $e) {
                Session::flash('error', trans('laravel-launcher::notification.error.media'));
            }

        } else {
            // If there is NO new file
            $media->update($request->only($this->fields));
        }

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.updated'));
    }
}
