<?php

namespace Nh\LaravelLauncher\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Nh\LaravelLauncher\Http\Requests\CategoryRequest;
use Nh\LaravelLauncher\Models\Category;

class CategoryController extends Controller
{
    /**
     * Define the available fields that the requests can use.
     *
     * @var array
     */
    private $fields = ['position', 'name', 'slug',  'label', 'color', 'type'];

    /**
     * Define the view path.
     *
     * @var string
     */
    private $view = 'laravel-launcher::backend.categories.';

    /**
     * Define the redirect route name.
     *
     * @var string
     */
    private $redirect = 'backend.categories.index';

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        // Authorization
        Gate::authorize('viewAny', Category::class);

        // Return the view
        return view($this->view.'index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        // Authorization
        Gate::authorize('create', Category::class);

        // Return the view
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CategoryRequest $request): RedirectResponse
    {
        // Authorization
        Gate::authorize('create', Category::class);

        // Create the resource
        Category::create($request->only($this->fields));

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.created'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category): View
    {
        // Authorization
        Gate::authorize('view', $category);

        // Return the view
        return view($this->view.'show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category): View
    {
        // Authorization
        Gate::authorize('update', $category);

        // Return the view
        return view($this->view.'edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CategoryRequest $request, Category $category): RedirectResponse
    {
        // Authorization
        Gate::authorize('update', $category);

        // Update the resource
        $category->update($request->only($this->fields));

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.updated'));
    }

    /**
     * Soft delete the specified resource from storage.
     */
    public function delete(Category $category): RedirectResponse
    {
        // Authorization
        Gate::authorize('delete', $category);

        // Delete the resource
        $category->delete();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.deleted'));
    }

    /**
     * Restore the specified resource from storage.
     */
    public function restore(Category $category): RedirectResponse
    {
        // Authorization
        Gate::authorize('restore', $category);

        // Restore the resource
        $category->restore();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.restored'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category): RedirectResponse
    {
        // Authorization
        Gate::authorize('destroy', $category);

        // Delete the resource
        $category->forceDelete();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.destroyed'));
    }
}
