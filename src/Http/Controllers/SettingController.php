<?php

namespace Nh\LaravelLauncher\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Nh\LaravelLauncher\Http\Requests\SettingRequest;
use Nh\LaravelLauncher\Models\Setting;

class SettingController extends Controller
{
    /**
     * Define the view path.
     *
     * @var string
     */
    private $view = 'laravel-launcher::backend.settings.';

    /**
     * Define the redirect route name.
     *
     * @var string
     */
    private $redirect = 'backend.settings.index';

    /**
     * Display a the resource.
     */
    public function index(): View
    {
        // Authorization
        Gate::authorize('viewAny', Setting::class);

        // Get all the settings
        $settings = Setting::get();

        // Return the view
        return view($this->view.'index', compact('settings'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(): View
    {
        // Authorization
        Gate::authorize('update', Setting::class);

        // Get all the settings
        $settings = Setting::get()->pluck('value', 'name')->toArray();

        // Return the view
        return view($this->view.'edit', compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SettingRequest $request): RedirectResponse
    {
        // Authorization
        Gate::authorize('update', Setting::class);

        // Update the resource
        if ($request->has('settings')) {
            foreach (cleanArray($request->settings) as $key => $values) {
                Setting::updateOrCreate([
                    'name' => $key,
                ], [
                    'value' => $values,
                ]);
            }
        }

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.updated'));
    }
}
