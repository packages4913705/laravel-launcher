<?php

namespace Nh\LaravelLauncher\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Nh\LaravelLauncher\Models\ActivityLog;

class ActivityLogController extends Controller
{
    /**
     * Define the view path.
     *
     * @var string
     */
    private $view = 'laravel-launcher::backend.activity-logs.';

    /**
     * Display a the resource.
     */
    public function index(): View
    {
        // Authorization
        Gate::authorize('viewAny', ActivityLog::class);

        // Return the view
        return view($this->view.'index');
    }

    /**
     * Display the specified resource.
     */
    public function show(ActivityLog $log): View
    {

        // Authorization
        Gate::authorize('view', $log);

        // Return the view
        return view($this->view.'show', compact('log'));
    }
}
