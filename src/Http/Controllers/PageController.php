<?php

namespace Nh\LaravelLauncher\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Nh\LaravelLauncher\Http\Requests\PageRequest;
use Nh\LaravelLauncher\Models\Page;

class PageController extends Controller
{
    /**
     * Define the available fields that the requests can use.
     *
     * @var array
     */
    private $fields = ['position', 'name', 'slug', 'label', 'title', 'description', 'published', 'in_menu', 'in_footer', 'type', 'color'];

    /**
     * Define the view path.
     *
     * @var string
     */
    private $view = 'laravel-launcher::backend.pages.';

    /**
     * Define the redirect route name.
     *
     * @var string
     */
    private $redirect = 'backend.pages.index';

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        // Authorization
        Gate::authorize('viewAny', Page::class);

        // Return the view
        return view($this->view.'index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        // Authorization
        Gate::authorize('create', Page::class);

        // Return the view
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PageRequest $request): RedirectResponse
    {
        // Authorization
        Gate::authorize('create', Page::class);

        // Create the resource
        Page::create($request->only($this->fields));

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.created'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Page $page): View
    {
        // Authorization
        Gate::authorize('view', $page);

        // Return the view
        return view($this->view.'show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Page $page): View
    {
        // Authorization
        Gate::authorize('update', $page);

        // Return the view
        return view($this->view.'edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PageRequest $request, Page $page): RedirectResponse
    {
        // Make sure index can't be unpublished
        if ($page->name === 'index') {
            $request->merge([
                'published' => 1,
            ]);
        }

        // Authorization
        Gate::authorize('update', $page);

        // Update the resource
        $page->update($request->only($this->fields));

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.updated'));
    }

    /**
     * Soft delete the specified resource from storage.
     */
    public function delete(Page $page): RedirectResponse
    {
        // Authorization
        Gate::authorize('delete', $page);

        // Delete the resource
        $page->delete();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.deleted'));
    }

    /**
     * Restore the specified resource from storage.
     */
    public function restore(Page $page): RedirectResponse
    {
        // Authorization
        Gate::authorize('restore', $page);

        // Restore the resource
        $page->restore();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.restored'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Page $page): RedirectResponse
    {
        // Authorization
        Gate::authorize('destroy', $page);

        // Delete the resource
        $page->forceDelete();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.destroyed'));
    }

    /**
     * Publish a model.
     */
    public function publish(Page $page): RedirectResponse
    {
        // Authorization
        Gate::authorize('publish', $page);

        // Update the resource
        $page->update(['published' => 1]);

        // Redirect
        return redirect()->route('backend.pages.show', $page->id)->with('success', trans('laravel-launcher::notification.published'));
    }

    /**
     * Unpublish a model.
     */
    public function unpublish(Page $page): RedirectResponse
    {
        // Authorization
        Gate::authorize('unpublish', $page);

        // Update the resource
        $page->update(['published' => 0]);

        // Redirect
        return redirect()->route('backend.pages.show', $page->id)->with('success', trans('laravel-launcher::notification.unpublished'));
    }
}
