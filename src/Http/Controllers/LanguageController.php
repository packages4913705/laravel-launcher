<?php

namespace Nh\LaravelLauncher\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    /**
     * Invoke the language controller.
     */
    public function __invoke(string $lang): RedirectResponse
    {
        if (in_array($lang, config('laravel-launcher.languages'))) {

            // Set local in session to use in Middleware
            Session::put('locale', $lang);

            // As fallback change the local
            App::setLocale($lang);

        }

        return redirect()->back();

    }
}
