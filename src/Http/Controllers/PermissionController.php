<?php

namespace Nh\LaravelLauncher\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Nh\LaravelLauncher\Http\Requests\PermissionRequest;
use Nh\LaravelLauncher\Models\Permission;

class PermissionController extends Controller
{
    /**
     * Define the available fields that the requests can use.
     *
     * @var array
     */
    private $fields = ['name', 'label', 'model', 'action'];

    /**
     * Define the view path.
     *
     * @var string
     */
    private $view = 'laravel-launcher::backend.permissions.';

    /**
     * Define the redirect route name.
     *
     * @var string
     */
    private $redirect = 'backend.permissions.index';

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        // Authorization
        Gate::authorize('viewAny', Permission::class);

        // Return the view
        return view($this->view.'index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        // Authorization
        Gate::authorize('create', Permission::class);

        // Return the view
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PermissionRequest $request): RedirectResponse
    {
        // Authorization
        Gate::authorize('create', Permission::class);

        // Create the resource
        Permission::create($request->only($this->fields));

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.created'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Permission $permission): View
    {
        // Authorization
        Gate::authorize('view', $permission);

        // Return the view
        return view($this->view.'show', compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Permission $permission): View
    {
        // Authorization
        Gate::authorize('update', $permission);

        // Return the view
        return view($this->view.'edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PermissionRequest $request, Permission $permission): RedirectResponse
    {
        // Authorization
        Gate::authorize('update', $permission);

        // Update the resource
        $permission->update($request->only($this->fields));

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Permission $permission): RedirectResponse
    {
        // Authorization
        Gate::authorize('destroy', $permission);

        // Delete the resource
        $permission->delete();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.destroyed'));
    }
}
