<?php

namespace Nh\LaravelLauncher\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Nh\LaravelLauncher\Http\Requests\UserRequest;
use Nh\LaravelLauncher\Models\User;

class UserController extends Controller
{
    /**
     * Define the available fields that the requests can use.
     *
     * @var array
     */
    private $fields = ['name', 'email', 'password'];

    /**
     * Define the view path.
     *
     * @var string
     */
    private $view = 'laravel-launcher::backend.users.';

    /**
     * Define the redirect route name.
     *
     * @var string
     */
    private $redirect = 'backend.users.index';

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        // Authorization
        Gate::authorize('viewAny', User::class);

        // Return the view
        return view($this->view.'index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        // Authorization
        Gate::authorize('create', User::class);

        // Return the view
        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserRequest $request): RedirectResponse
    {
        // Authorization
        Gate::authorize('create', User::class);
        Gate::authorize('set-roles', $request);

        // Create the resource
        User::create($request->only($this->fields));

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.created'));
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user): View
    {
        // Authorization
        Gate::authorize('view', $user);

        // Return the view
        return view($this->view.'show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user): View
    {
        // Authorization
        Gate::authorize('update', $user);

        // Return the view
        return view($this->view.'edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserRequest $request, User $user): RedirectResponse
    {
        // Authorization
        Gate::authorize('update', $user);
        Gate::authorize('set-roles', $request);

        // Update the resource
        // Use array_filter to remove NULL value to avoid change in password
        $user->update(array_filter($request->only($this->fields)));

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.updated'));
    }

    /**
     * Soft delete the specified resource from storage.
     */
    public function delete(User $user): RedirectResponse
    {
        // Authorization
        Gate::authorize('delete', $user);

        // Delete the resource
        $user->delete();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.deleted'));
    }

    /**
     * Restore the specified resource from storage.
     */
    public function restore(User $user): RedirectResponse
    {
        // Authorization
        Gate::authorize('restore', $user);

        // Restore the resource
        $user->restore();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.restored'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user): RedirectResponse
    {
        // Authorization
        Gate::authorize('destroy', $user);

        // Delete the resource
        $user->forceDelete();

        // Redirect
        return redirect()->route($this->redirect)->with('success', trans('laravel-launcher::notification.success.destroyed'));
    }
}
