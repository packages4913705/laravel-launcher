<?php

namespace Nh\LaravelLauncher\Livewire;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Livewire\Attributes\Computed;
use Livewire\Attributes\Locked;
use Livewire\Attributes\Session;
use Livewire\Component;
use Livewire\WithoutUrlPagination;
use Livewire\WithPagination;
use Nh\LaravelLauncher\Traits\Livewire\Sortable;

class Datalist extends Component
{
    use Sortable, WithoutUrlPagination, WithPagination;

    /**
     * This is the name of the session.
     * ! You must passed the value via the HTML component
     *
     * @var string
     */
    #[Locked]
    public $session;

    /**
     * Get the model to use to get the data.
     *
     * @var Illuminate\Database\Model
     */
    #[Locked]
    public $model;

    /**
     * Route to define the actions (edit/delete/restore/destroy).
     * -> Used in Dialog
     *
     * @var string
     */
    #[Locked]
    public $route;

    /**
     * Path to the custom views (listing/search/filter).
     *
     * @var string
     */
    #[Locked]
    public $path;

    /**
     * Is the datalist have some filter.
     * -> Defined by the method 'scopeFilter'
     *
     * @var bool
     */
    public $hasFilter = false;

    /**
     * Is the datalist have some search.
     * -> Defined by the method 'scopeSearch'
     *
     * @var bool
     */
    public $hasSearch = false;

    /**
     * Is the datalist have some advanced search.
     * -> Defined by the view 'models.includes.search'
     *
     * @var bool
     */
    public $hasAdvancedSearch = false;

    /**
     * Is the datalist items have some level.
     * -> To make them levelable and toggle the parent/children
     *
     * @var bool
     */
    public $hasLevel = false;

    /**
     * Is the datalist have some pagination.
     *
     * @var bool
     */
    public $hasPagination = true;

    /**
     * Array of filters available.
     *
     * @var array
     */
    public $filters;

    /**
     * Array of columns available.
     *
     * @var array
     */
    public $columns = [];

    /**
     * Array of sorting columns available.
     *
     * @var array
     */
    public $sorting = [];

    /**
     * Number of items per page.
     *
     * @var int
     */
    public $pagination = 10;

    /**
     * Array of sort values saved in session.
     *
     * @var array
     */
    #[Session(key: 'datalist-{session}-sort')]
    public $sort;

    /**
     * The page number saved in session.
     *
     * @var int
     */
    #[Session(key: 'datalist-{session}-page')]
    public $page;

    /**
     * Current view filter saved in session.
     *
     * @var string
     */
    #[Session(key: 'datalist-{session}-filter')]
    public $filter = 'all';

    /**
     * Array of search values saved in session.
     *
     * @var array
     */
    #[Session(key: 'datalist-{session}-search')]
    public $search = [];

    /**
     * Is the datalist draggable.
     *
     * @var bool
     */
    public $draggable = false;

    /**
     * Is the datalist levelable.
     *
     * @var bool
     */
    public $levelable = false;

    /**
     * Get the collection of Model.
     *
     * @return Illuminate\Pagination\LengthAwarePaginator
     * @return Illuminate\Database\Eloquent\Collection
     */
    #[Computed]
    public function items(): mixed
    {

        // Define items
        $items = $this->model;

        // Get only parent if levelable or if draggable
        if ($this->levelable || ($this->hasLevel && $this->draggable)) {
            $items = $items->with(['children' => fn ($children) => $children->orderBy($this->sort['field'], $this->sort['direction'])])->whereNull('parent_id');
        }

        // Filter use direct SCOPE method in MODEL
        if ($this->hasFilter && ! empty($this->filter)) {
            $items = $items->filter($this->filter);
        }

        // Search use direct SCOPE method in MODEL
        if ($this->hasSearch && ! empty($this->search)) {
            $items = $items->search($this->search);
        }

        // Order the items by field and direction
        if (! empty($this->sort)) {
            $items = $items->orderBy($this->sort['field'], $this->sort['direction']);
        }

        // Return the collection
        return $this->hasPagination ? $items->paginate($this->pagination) : $items->get();
    }

    /**
     * Define the total of items per filter.
     */
    #[Computed]
    public function totals(): array
    {

        $total = [];

        $items = $this->model;

        if ($this->hasSearch && ! empty($this->search)) {
            $items = $items->search($this->search);
        }

        if ($this->hasFilter) {

            foreach ($this->filters as $filter) {
                $filtered = clone $items;
                $total[$filter] = $filtered->filter($filter)->count();
            }

        } else {
            $total['all'] = $items->count();
        }

        return $total;

    }

    /**
     * Define the badges of the advanced search.
     */
    #[Computed]
    public function searchBadges(): array
    {

        $badges = [];

        foreach (Arr::except($this->search, ['keyword']) as $key => $val) {

            if ($val == 'empty') {
                $badges[$key] = trans_choice('laravel-launcher::field.empty', 1);
            } else {
                switch ($key) {
                    case 'color':
                        $badges[$key] = translate($val, 'color');
                        break;
                    default:
                        if (is_numeric($val)) {
                            $modelClass = Relation::getMorphedModel($key);
                            $badges[$key] = $modelClass::find($val)->label ?? $val;
                        } else {
                            $badges[$key] = $val;
                        }
                        break;
                }
            }

        }

        return $badges;

    }

    /**
     * Create the component instance.
     */
    public function mount($model, $route = null, $path = null, $filters = [], $sort = [], $hasAdvancedSearch = null, $hasPosition = null, $hasLevel = null): void
    {

        // Get the full class (App\Models\Foo or Nh\LaraveLauncher\Models\Foo)
        $modelClass = Relation::getMorphedModel($model);

        // Get the plural as 'foo' => 'foos'
        $modelPlural = Str::pluralStudly($model);

        // Define the basics
        $this->model = new $modelClass;
        $this->route = $route ?? 'backend.'.$modelPlural;
        $this->path = $path ?? $this->route.'.includes';

        // Options
        $this->hasFilter = method_exists($this->model, 'scopeFilter');
        $this->hasSearch = method_exists($this->model, 'scopeSearch');
        $this->hasAdvancedSearch = is_null($hasAdvancedSearch) ? view()->exists($this->path.'.search') : $hasAdvancedSearch;
        $this->hasPosition = is_null($hasPosition) ? in_array('Nh\LaravelLauncher\Traits\Positionable', class_uses($this->model)) && in_array('position', $this->model->getFillable()) : $hasPosition;
        $this->hasLevel = is_null($hasLevel) ? in_array('Nh\LaravelLauncher\Traits\Levelable', class_uses($this->model)) && in_array('level', $this->model->getFillable()) && config('laravel-launcher.'.$model.'.levelable') : $hasLevel;

        // Define the filters (all|published|trashed|...)
        $trashed = in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($this->model)) && Auth::user()->hasAccess($model, ['restore', 'destroy']) ? ['trashed'] : [];
        $published = in_array('published', $this->model->getFillable()) ? ['published'] : [];
        $this->filters = $this->hasFilter ? array_merge(['all'], $trashed, $published, (array) $filters) : ['all'];

        // Init the default sorting
        $this->sort = ! empty($this->sort) ? $this->sort : (! empty($sort) ? $sort : ['field' => $this->hasPosition ? 'position' : 'id', 'direction' => 'asc']);

        // Go to the page saved in session or the last page
        if ($this->hasPagination) {
            $this->page = $this->page > $this->items()->lastPage() ? $this->items()->lastPage() : $this->page;
            $this->setPage($this->page);
        }

    }

    /**
     * Get the view that represent the component.
     */
    public function render(): View
    {
        return view('laravel-launcher::components.livewire.datalist');
    }

    /**
     * Action trigger after the page updated.
     * -> Save in session the page number.
     */
    public function updatedPage($page): void
    {
        if ($this->hasPagination) {
            $this->page = $page;
        }

        if ($this->filter != 'all' || $this->search) {
            $this->draggable = false;
            $this->levelable = false;
        }
    }

    /**
     * Search method: reinitialize the page.
     */
    public function runSearch(): void
    {
        if ($this->hasSearch) {
            $this->search = array_filter($this->search);
            $this->resetPage();
        }
    }

    /**
     * Reset the search property.
     * -> If passing a key, only reset one property
     */
    public function runReset(?string $key = null): void
    {
        if ($this->hasSearch) {
            if (is_null($key)) {
                $this->reset('search');
            } else {
                unset($this->search[$key]);
            }
        }
    }

    /**
     * Filter method: change property and reinitialize the page.
     */
    public function runFilter(string $value): void
    {
        if ($this->hasFilter) {
            $this->filter = $value;
            $this->resetPage();
        }
    }

    /**
     * Defined the sort properties.
     */
    public function runSort(string $field): void
    {
        if (Schema::hasColumn($this->model->getTable(), $field) && ! $this->draggable) {
            $this->sort['direction'] = $this->sort['field'] === $field && $this->sort['direction'] === 'asc' ? 'desc' : 'asc';
            $this->sort['field'] = $field;
        }
    }

    /**
     * Open the dialog method.
     * -> This method will fire an event that will be used in Javascript to open a Dialog
     */
    public function openDialog($method, $id): void
    {
        switch ($method) {
            case 'delete':
                $url = route($this->route.'.delete', $id);
                break;
            case 'restore':
                $url = route($this->route.'.restore', $id);
                break;
            case 'destroy':
                $url = route($this->route.'.destroy', $id);
                break;
        }

        $this->dispatch('open-'.$method.'-dialog', url: $url);
    }

    /**
     * Toggle the draggable property.
     */
    public function toggleDraggable(): void
    {

        // Avoid if no position option
        if (! $this->hasPosition) {
            return;
        }

        // Change properties
        $this->draggable = ! $this->draggable;
        $this->hasPagination = ! $this->draggable;
        $this->levelable = false;
        $this->search = [];

        $this->sort = [
            'field' => 'position',
            'direction' => 'asc',
        ];

        // And reset the component
        $this->resetPage();

        // Run the JS
        if ($this->draggable) {
            $this->dispatch('resetSortableJS');
        }

    }

    /**
     * Toggle the draggable property.
     */
    public function toggleLevelable(): void
    {

        // Change properties
        $this->draggable = false;
        $this->levelable = ! $this->levelable;
        $this->search = [];

        // And reset the component
        $this->resetPage();

        // Run the JS
        if ($this->levelable) {
            $this->dispatch('resetTreeJS');
        }

    }
}
