<?php

namespace Nh\LaravelLauncher\Livewire;

use Illuminate\View\View;
use Livewire\Attributes\Locked;
use Livewire\Component;
use Nh\LaravelLauncher\Traits\Livewire\Sortable as LivewireSortable;

class Sortable extends Component
{
    use LivewireSortable;

    /**
     * Path to the custom views.
     *
     * @var string
     */
    #[Locked]
    public $path;

    /**
     * The items.
     *
     * @var Collection
     */
    public $items;

    /**
     * Create the component instance.
     */
    public function mount($items): void
    {
        $this->items = $items->values(); // This reset the keys to correct order [0,2,3] => [0,1,2]
        $model = $this->items->first();
        $this->hasPosition = in_array('Nh\LaravelLauncher\Traits\Positionable', class_uses($model)) && in_array('position', $model->getFillable());
    }

    /**
     * Get the view that represent the component.
     */
    public function render(): View
    {
        return view('laravel-launcher::components.livewire.sortable');
    }
}
