<?php

namespace Nh\LaravelLauncher\Livewire;

use Illuminate\View\View;
use Livewire\Attributes\Locked;
use Livewire\Component;

class Dynamic extends Component
{
    /**
     * Get a unique ID for the component
     *
     * @var string
     */
    public $dynID;

    /**
     * Path to the custom views.
     *
     * @var string
     */
    #[Locked]
    public $path;

    /**
     * The legend of the fieldset.
     *
     * @var string
     */
    public $legend;

    /**
     * The description of the fieldset.
     *
     * @var string
     */
    public $description;

    /**
     * The items to add/update/delete.
     *
     * @var array
     */
    public $items = [];

    /**
     * The total items available.
     *
     * @var int
     */
    public $total = 0;

    /**
     * The minimum items available.
     *
     * @var int
     */
    public $min = null;

    /**
     * The maximum items available.
     *
     * @var int
     */
    public $max = null;

    /**
     * An array to store some extra information passing to the views.
     *
     * @var array
     */
    public $misc = [];

    /**
     * Create the component instance.
     */
    public function mount($items = null): void
    {

        // Define the ID
        $this->dynID = 'dynamic'.uniqid();

        // Add default items
        $defaultItems = [
            'add' => [],
            'update' => [],
            'delete' => [],
        ];

        // Merge the items
        $this->items = array_merge($defaultItems, (array) $items);

        // Get the total
        $this->getTotal();

        // Add the default rows if the is a minimum
        if ($this->min) {
            $default = $this->min - $this->total;
            for ($i = 1; $i <= $default; $i++) {
                $this->addRow();
            }
        }

    }

    /**
     * Get the view that represent the component.
     */
    public function render(): View
    {
        return view('laravel-launcher::components.livewire.dynamic');
    }

    /**
     * Get the total of items.
     */
    public function getTotal(): void
    {
        $this->total = count($this->items['add']) + count($this->items['update']);
    }

    /**
     * Add a row to the listing.
     */
    public function addRow(): void
    {
        // Check if not already at maximum
        if ($this->max && $this->total == $this->max) {
            return;
        }

        // Add empty item to the add array
        $this->items['add'][] = '';

        // Get the total number
        $this->getTotal();
    }

    /**
     * Remove a row to the listing.
     */
    public function removeRow(int $index): void
    {
        // Check if not already at minimum
        if ($this->min && $this->total <= $this->min) {
            return;
        }

        // Remove from array
        unset($this->items['add'][$index]);

        // Reset index in order
        $this->items['add'] = array_values($this->items['add']);

        // Get the total number
        $this->getTotal();
    }

    /**
     * Delete a row to the listing.
     */
    public function deleteRow(int $index): void
    {
        // Check if not already at minimum
        if ($this->min && $this->total <= $this->min) {
            return;
        }

        // Add to the delete array
        $this->items['delete'][] = $this->items['update'][$index];

        // Remove from array
        unset($this->items['update'][$index]);

        // Get the total number
        $this->getTotal();
    }

    /**
     * Restore a row to the listing.
     */
    public function restoreRow(int $index): void
    {
        // Check if not already at maximum
        if ($this->max && $this->total == $this->max) {
            return;
        }

        // Add to the update array
        $this->items['update'][] = $this->items['delete'][$index];

        // Remove from array
        unset($this->items['delete'][$index]);

        // Get the total number
        $this->getTotal();
    }
}
