<?php

namespace Nh\LaravelLauncher;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Nh\LaravelLauncher\Console\Commands\AddModule;
use Nh\LaravelLauncher\Livewire\Datalist;
use Nh\LaravelLauncher\Livewire\Dynamic;
use Nh\LaravelLauncher\Livewire\Sortable;
use Nh\LaravelLauncher\Models\Role;
use Nh\LaravelLauncher\Models\User;
use Nh\LaravelLauncher\View\Composers\LayoutComposer;
use Nh\LaravelLauncher\View\Composers\SearchActivityLogComposer;
use Nh\LaravelLauncher\View\Composers\SearchPermissionComposer;
use Nh\LaravelLauncher\View\Composers\SearchUserComposer;

class LaravelLauncherServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     */
    public function register(): void
    {

        $this->mergeConfigFrom(
            __DIR__.'/../config/language.php', 'language'
        );

    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

        // COMMANDS
        if ($this->app->runningInConsole()) {
            $this->commands([
                AddModule::class,
            ]);
        }

        // VIEWS
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'laravel-launcher');

        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/laravel-launcher'),
        ], 'laravel-launcher-views');

        // TRANSLATIONS
        $this->loadTranslationsFrom(__DIR__.'/../lang', 'laravel-launcher');

        $this->publishes([
            __DIR__.'/../lang' => lang_path('vendor/laravel-launcher'),
        ], 'laravel-launcher-translations');

        // ROUTES
        $this->loadRoutesFrom(__DIR__.'/../routes/language.php');

        // MIGRATIONS
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        // STUBS
        // These are file that MUST be per project
        $stubs = __DIR__.'/../stubs/preset/';
        $this->publishes([
            $stubs.'/app' => app_path(),
            $stubs.'/bootstrap' => base_path('bootstrap'),
            $stubs.'/config/laravel-launcher.php' => config_path('laravel-launcher.php'),
            $stubs.'/database/seeders' => database_path('seeders'),
            $stubs.'/lang' => base_path('lang'),
            $stubs.'/public' => public_path(),
            $stubs.'/resources' => resource_path(),
            $stubs.'/routes' => base_path('routes'),
            $stubs.'/.gitlab-ci.yml' => base_path('.gitlab-ci.yml'),
            $stubs.'/package.json' => base_path('package.json'),
            $stubs.'/phpunit.xml' => base_path('phpunit.xml'),
            $stubs.'/vite.config.js' => base_path('vite.config.js'),
        ], 'laravel-launcher-preset');

        // LIVEWIRE COMPONENTS
        // Run the livewire components to use them on all the project
        Livewire::component('datalist', Datalist::class);
        Livewire::component('dynamic', Dynamic::class);
        Livewire::component('sortable', Sortable::class);

        // BLADE COMPONENTS
        // Run the blade components to use them on all the project
        Blade::componentNamespace('Nh\\LaravelLauncher\\View\\Components', 'll');

        // VIEW COMPOSERS
        // Run the default composer
        View::composer(['laravel-launcher::layouts.backend'], LayoutComposer::class);
        View::composer(['laravel-launcher::backend.activity-logs.includes.search'], SearchActivityLogComposer::class);
        View::composer(['laravel-launcher::backend.permissions.includes.search'], SearchPermissionComposer::class);
        View::composer(['laravel-launcher::backend.users.includes.search'], SearchUserComposer::class);

        // GATES
        // Define some gates for roles and permissions
        Gate::define('set-roles', function (User $user, Request $request) {

            // If user have superpower => true
            if ($user->has_superpowers) {
                return true;
            }

            // Only Superadmin can set another superadmin
            $superadmin = Role::select('id')->firstWhere('name', 'superadmin');
            if (in_array($superadmin->id, (array) $request->roles)) {
                return false;
            }

            // If user have administrator role => true
            if ($user->is_administrator) {
                return true;
            }

            // Check if user has role
            return $user->hasRoles((array) $request->roles, 'id', true);

        });

        Gate::define('set-permissions', function (User $user, Request $request) {

            // If user have superpower => true
            if ($user->has_superpowers) {
                return true;
            }

            // Check for each roles
            return $user->hasPermissions((array) $request->permissions, 'id', true);

        });

    }
}
