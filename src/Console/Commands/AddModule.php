<?php

namespace Nh\LaravelLauncher\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Nh\LaravelLauncher\Models\Permission;

class AddModule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:new {name : The name of the new module lowercase and singular}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new module with all CRUD files';

    /**
     * Name of the model.
     * -> Singular
     * -> Lowercase
     *
     * @var string
     */
    protected $name;

    /**
     * Name of the model.
     * -> Plural
     * -> Lowercase
     *
     * @var string
     */
    protected $pname;

    /**
     * Name of the model.
     * -> Singular
     * -> Uppercase
     *
     * @var string
     */
    protected $ucname;

    /**
     * Name of the model.
     * -> Plural
     * -> Uppercase
     *
     * @var string
     */
    protected $ucpname;

    /**
     * List of traits/options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * Stubs path.
     *
     * @var string
     */
    protected $stub;

    /**
     * Execute the console command.
     */
    public function handle(): void
    {

        // Define the name of the module to replace in all files
        $this->name = $this->argument('name');
        $this->pname = Str::pluralStudly($this->name);
        $this->ucname = ucfirst($this->name);
        $this->ucpname = ucfirst($this->pname);

        // Define the options (SoftDeletes|Levelable|Roleable|Mediable|Translatable|Categorizable|Positionable)
        $this->options = $this->choice(
            'What options does your module have ?',
            ['SoftDeletes', 'Levelable', 'Roleable', 'Mediable', 'Translatable', 'Categorizable', 'Positionable', 'Typeable', 'Colorable', 'Sluggable'],
            '0,1,2,3,4,5,6,7,8,9',
            null,
            true
        );

        // Define the stub path
        $this->stub = __DIR__.'/../../../stubs/module/';

        // Get current date for migration file
        $date = date('Y_m_d_His');

        try {
            // Copy the files
            $this->copyFile('factory', database_path('factories'), "{$this->ucname}Factory.php");
            $this->copyFile('model', app_path('Models'), "{$this->ucname}.php");
            $this->copyFile('migration', database_path('migrations'), "{$date}_create_{$this->pname}_table.php");
            $this->copyFile('controller', app_path('Http/Controllers/Backend'), "{$this->ucname}Controller.php");
            $this->copyFile('request', app_path('Http/Requests'), "{$this->ucname}Request.php");
            $this->copyFile('policy', app_path('Policies'), "{$this->ucname}Policy.php");
            $this->copyFile('test', base_path('tests/Feature'), "{$this->ucname}Test.php");

            // Copy the views
            $this->copyViews();

            // Add some content to existing files
            $this->addToFile('config', config_path('laravel-launcher.php'));
            $this->addToFile('seeder', database_path('seeders/LauncherAccessSeeder.php'));
            $this->addToFile('route', base_path('routes/backend.php'));
            $this->addToFile('provider', app_path('Providers/AppServiceProvider.php'));

            // Create the permission
            $askCreatePermission = $this->confirm('Do you want to create the permissions for this model ? [yes|no]', true);
            if ($askCreatePermission) {
                $actions = in_array('SoftDeletes', $this->options) ? config('laravel-launcher.permission.actions') : array_diff(config('laravel-launcher.permission.actions'), ['delete', 'restore']);
                foreach ($actions as $action) {
                    Permission::create([
                        'name' => $this->name.'-'.$action,
                        'label' => $this->name.' '.$action,
                        'model' => $this->name,
                        'action' => $action,
                    ]);
                    $this->info("The Permission for {$action} the {$this->name} has been created with success !");
                }
            }

        } catch (\Throwable $th) {
            throw $th;
        }

    }

    /**
     * Replace the name into the file
     */
    protected function replaceName($filePath): string
    {
        $filePath = str_replace('{{ NAME }}', $this->name, $filePath);
        $filePath = str_replace('{{ PNAME }}', $this->pname, $filePath);
        $filePath = str_replace('{{ UCNAME }}', $this->ucname, $filePath);
        $filePath = str_replace('{{ UCCPNAME }}', $this->ucpname, $filePath);

        return $filePath;
    }

    /**
     * Run Pint to clean files
     */
    protected function runPintOnFile($filePath): void
    {
        if (file_exists(base_path('vendor/bin/pint'))) {
            exec(base_path("vendor/bin/pint {$filePath}"), $output, $resultCode);

            if ($resultCode !== 0) {
                $this->error("There is an error on Pint : {$filePath}");
            }
        } else {
            $this->error('Laravel Pint is not installed.');
        }
    }

    /**
     * Copy a file and replace the name and by option
     * -> $name is the name of the stub file
     * -> $path is the path where to save the new file
     * -> $filename is the copied filename
     */
    protected function copyFile(string $name, string $path, string $filename): void
    {

        // Get the stub file
        $stub = File::get($this->stub.$name.'.stub');

        // Replace the placeholders {{ NAME }}, {{ PNAME }}, {{ UCNAME }} and {{ UCPNAME }}
        $stub = $this->replaceName($stub);

        // Compile with blade for customization per option
        $compiledStub = Blade::render($stub, [
            'traits' => $this->options ? ', '.implode(', ', $this->options) : '',
            'softDeletes' => in_array('SoftDeletes', $this->options),
            'levelable' => in_array('Levelable', $this->options),
            'roleable' => in_array('Roleable', $this->options),
            'mediable' => in_array('Mediable', $this->options),
            'translatable' => in_array('Translatable', $this->options),
            'categorizable' => in_array('Categorizable', $this->options),
            'positionable' => in_array('Positionable', $this->options),
            'typeable' => in_array('Typeable', $this->options),
            'colorable' => in_array('Colorable', $this->options),
            'sluggable' => in_array('Sluggable', $this->options),
        ]);

        // Add the PHP balise at the top
        $compiledStub = "<?php\n\n".$compiledStub;

        // Check if the folder exist, otherwise create it !
        if (! File::exists($path)) {
            File::makeDirectory($path, 0755, true);
        }

        // Save the file
        File::put($path.'/'.$filename, $compiledStub);

        // Execute Pint to clean the file
        $this->runPintOnFile($path);

        // Return message
        $this->info("The {$this->name} {$name} file has been created with success !");
    }

    /**
     * Copy the view files and replace the name
     * -> $name is the name of the stub file
     * -> $path is the path where to save the new file
     * -> $filename is the copied filename
     */
    protected function copyViews(): void
    {

        $files = ['create', 'edit', 'index', 'show', 'listing', 'search'];

        foreach ($files as $file) {

            // Get the stub file
            $stub = File::get("{$this->stub}/views/{$file}.stub");
            $path = in_array($file, ['listing', 'search']) ? resource_path("views/backend/{$this->pname}/includes") : resource_path("views/backend/{$this->pname}");
            $filename = "{$file}.blade.php";

            // Replace the placeholders per options
            if (in_array('Levelable', $this->options)) {

                switch ($file) {
                    case 'edit':
                        $component = '<x-ll::backend.form.levelable :current="${{ NAME }}->parent_id" />';
                        break;
                    case 'create':
                        $component = '<x-ll::backend.form.levelable />';
                        break;
                    default:
                        $component = "@include('laravel-launcher::includes.cards.levelable')";
                        break;
                }

                $stub = str_replace(
                    ['{{ LEVELABLE }}'],
                    [$component],
                    $stub
                );
            }

            if (in_array('Roleable', $this->options)) {

                switch ($file) {
                    case 'edit':
                        $component = '<x-ll::backend.form.roleable :current="${{ NAME }}->roles->modelKeys()" />';
                        break;
                    case 'create':
                        $component = '<x-ll::backend.form.roleable />';
                        break;
                    default:
                        $component = "@include('laravel-launcher::includes.cards.roleable')";
                        break;
                }

                $stub = str_replace(
                    ['{{ ROLEABLE }}'],
                    [$component],
                    $stub
                );
            }

            if (in_array('Categorizable', $this->options)) {

                switch ($file) {
                    case 'edit':
                        $component = '<x-ll::backend.form.categorizable :current="${{ NAME }}->categories->modelKeys()" />';
                        break;
                    case 'create':
                        $component = '<x-ll::backend.form.categorizable />';
                        break;
                    default:
                        $component = "@include('laravel-launcher::includes.cards.categorizable')";
                        break;
                }

                $stub = str_replace(
                    ['{{ CATEGORIZABLE }}'],
                    [$component],
                    $stub
                );
            }

            if (in_array('Mediable', $this->options)) {

                switch ($file) {
                    case 'edit':
                        $component = '<x-ll::backend.form.mediable :current="${{ NAME }}->media" />';
                        break;
                    case 'create':
                        $component = '<x-ll::backend.form.mediable />';
                        break;
                    default:
                        $component = "@include('laravel-launcher::includes.cards.mediable')";
                        break;
                }

                $stub = str_replace(
                    ['{{ MEDIABLE }}'],
                    [$component],
                    $stub
                );
            }

            if (in_array('Translatable', $this->options)) {
                $substub = "{$this->stub}/views/placeholder/{$file}-translatable.stub";
                if (File::exists($substub)) {
                    $stub = str_replace(
                        ['{{ TRANSLATABLE }}'],
                        [File::get($substub)],
                        $stub
                    );
                }
            }

            if (in_array('Typeable', $this->options)) {
                $component = $file == 'edit' ? '<x-ll::backend.form.typeable :current="${{ NAME }}->type" />' : '<x-ll::backend.form.typeable />';
                $stub = str_replace(
                    ['{{ TYPEABLE }}'],
                    [$component],
                    $stub
                );
            }

            if (in_array('Colorable', $this->options)) {

                switch ($file) {
                    case 'edit':
                        $component = '<x-ll::backend.form.colorable :current="${{ NAME }}->color" />';
                        break;
                    case 'create':
                        $component = '<x-ll::backend.form.colorable />';
                        break;
                    case 'listing':
                        $component = "{$this->stub}/views/placeholder/listing-colorable.stub";
                        break;
                }

                $stub = str_replace(
                    ['{{ COLORABLE }}'],
                    [$file == 'listing' && File::exists($component) ? File::get($component) : $component],
                    $stub
                );

            }

            if (in_array('Sluggable', $this->options)) {
                $component = $file == 'edit' ? '<x-ll::form.input id="slug" label="slug" name="slug" description="slug" :value="${{ NAME }}->slug" />' : '<x-ll::form.input id="slug" label="slug" name="slug" description="slug" />';
                $stub = str_replace(
                    ['{{ SLUGGABLE }}'],
                    [$component],
                    $stub
                );
            }

            // Clean not used placeholder
            $stub = str_replace(
                ['{{ LEVELABLE }}', '{{ ROLEABLE }}', '{{ CATEGORIZABLE }}', '{{ MEDIABLE }}', '{{ TRANSLATABLE }}', '{{ TYPEABLE }}', '{{ COLORABLE }}', '{{ SLUGGABLE }}'],
                ['', '', '', '', '', '', '', ''],
                $stub
            );

            // Replace the placeholders {{ NAME }}, {{ PNAME }}, {{ UCNAME }} and {{ UCPNAME }}
            $stub = $this->replaceName($stub);

            // Check if the folder exist, otherwise create it !
            if (! File::exists($path)) {
                File::makeDirectory($path, 0755, true);
            }

            // Save the file
            File::put($path.'/'.$filename, $stub);

            // Execute Pint to clean the file
            $this->runPintOnFile($path);

            // Return message
            $this->info("The {$this->name} {$file} view file has been created with success !");

        }

    }

    /**
     * Add some stub content into an existing file
     * -> $name is the name of the stub file
     * -> $path is the path of the existing file
     */
    protected function addToFile(string $name, string $path): void
    {

        // Get the file
        $file = File::get($path);

        // Get the stub
        $stub = File::get("{$this->stub}placeholder/{$name}.stub");

        // Replace the placeholders {{ NAME }}, {{ PNAME }}, {{ UCNAME }} and {{ UCPNAME }}
        $stub = $this->replaceName($stub);

        // Compile with blade for customization per option
        $compiledStub = Blade::render($stub, [
            'softDeletes' => in_array('SoftDeletes', $this->options),
            'levelable' => in_array('Levelable', $this->options),
            'roleable' => in_array('Roleable', $this->options),
            'mediable' => in_array('Mediable', $this->options),
            'translatable' => in_array('Translatable', $this->options),
            'categorizable' => in_array('Categorizable', $this->options),
            'positionable' => in_array('Positionable', $this->options),
            'typeable' => in_array('Typeable', $this->options),
            'colorable' => in_array('Colorable', $this->options),
            'sluggable' => in_array('Sluggable', $this->options),
        ]);

        // Add the stub to the original file
        $updatedFile = str_replace('// !PLACEHOLDER', $compiledStub, $file);

        // Add the name as placeholder (e.g. in config)
        if ($name == 'config') {
            $updatedFile = str_replace('// !NAME_PLACEHOLDER', "'{$this->name}',\n// !NAME_PLACEHOLDER", $updatedFile);
        }

        // Add the use controller on top
        if ($name == 'route') {
            $updatedFile = str_replace('// !USE_PLACEHOLDER', "use App\Http\Controllers\Backend\\{$this->ucname}Controller;\n// !USE_PLACEHOLDER", $updatedFile);
        }

        // Save the file
        File::put($path, $updatedFile);

        // Execute Pint to clean the file
        $this->runPintOnFile($path);

        // Return message
        $this->info("The {$name} file has been updated with success !");

    }
}
