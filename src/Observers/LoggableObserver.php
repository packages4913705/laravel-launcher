<?php

namespace Nh\LaravelLauncher\Observers;

class LoggableObserver
{
    /**
     * Handle the Model "created" event.
     * -> Add an activity log
     */
    public function created($model): void
    {
        $model->addLog('created');
    }

    /**
     * Handle the Model "updated" event.
     * -> Add an activity log
     */
    public function updated($model): void
    {
        // Ignor if it's the remember_token that is updated
        // * Weird behaviour from Laravel on logout
        if ($model->isDirty('remember_token') && count($model->getDirty()) === 1) {
            return;
        }

        if (in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($model)) && $model->isDirty('deleted_at')) {
            $model->addLog('restored');
        } else {
            $model->addLog('updated');
        }
    }

    /**
     * Handle the Model "deleted" event.
     * -> Add an activity log
     */
    public function deleted($model): void
    {
        if (! in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($model)) || $model->isForceDeleting()) {
            $model->addLog('destroyed');
        } else {
            $model->addLog('deleted');
        }
    }
}
