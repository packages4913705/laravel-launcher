<?php

namespace Nh\LaravelLauncher\Observers;

use Nh\LaravelLauncher\Models\Permission;
use Nh\LaravelLauncher\Models\Role;

class PermissionObserver
{
    /**
     * Handle the Permission "created" event.
     * -> Attach new permission to the superadmin
     */
    public function created(Permission $permission): void
    {
        Role::firstWhere('name', 'superadmin')->permissions()->attach($permission);
    }
}
