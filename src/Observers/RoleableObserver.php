<?php

namespace Nh\LaravelLauncher\Observers;

use Illuminate\Support\Facades\Auth;

class RoleableObserver
{
    /**
     * Handle the Model "saved" event.
     * -> Sync some roles
     * -> Add activity log for this relation
     */
    public function saved($model)
    {

        // Check if config is set
        $config = config('laravel-launcher.'.getSingleModelName($model).'.roleable');

        // Ignore if there is no a roleable
        if (! request()->has('roleable') || ! $config) {
            return;
        }

        // Define roles by request
        $roles = request()->has('roles') ? request()->roles : [];

        // GET protected roles
        if ($model->hasRoles() && ! Auth::user()->has_superpowers && ! Auth::user()->is_administrator) {
            $protected = $model->roles()->whereNotIn('id', Auth::user()->roles()->pluck('id'))->pluck('id')->toArray();
            if ($protected) {
                $roles = array_merge((array) $protected, (array) $roles);
            }
        }

        // Check number
        if (is_numeric($config) && count($roles) >= $config) {
            array_slice($roles, 0, $config);
        }

        // Sync roles
        $sync = $model->roles()->sync($roles);

        // Add the activity log
        if (in_array('Nh\LaravelLauncher\Traits\Loggable', class_uses($model)) && ! empty(array_filter($sync))) {
            $model->addLog('synchronized', ['roles' => array_filter($sync)]);
        }

    }

    /**
     * Handle the Model "deleting" event.
     * -> Detach the roles
     */
    public function deleting($model): void
    {
        if (! in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($model))) {
            $model->roles()->detach();
        }
    }

    /**
     * Handle the Model "forceDeleting" event.
     * -> Detach the roles
     */
    public function forceDeleting($model): void
    {
        $model->roles()->detach();
    }
}
