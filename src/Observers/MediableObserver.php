<?php

namespace Nh\LaravelLauncher\Observers;

use Exception;
use Illuminate\Support\Facades\Session;
use Nh\LaravelLauncher\Models\Media;

class MediableObserver
{
    /**
     * Handle the Model "saved" event.
     * -> Add/update/delete some media
     * -> Add activity log for this relation
     */
    public function saved(mixed $model): void
    {

        // Define an array to keep all changes
        $changes = [];

        // Add some Media
        if (request()->has('media_to_add')) {

            foreach (request()->media_to_add as $mediaType => $medias) {

                foreach ($medias as $key => $values) {

                    // Check that the file is not empty
                    if (empty($values['file'])) {
                        continue;
                    }

                    // Create a Media in Database
                    $new = $model->media()->create([
                        'position' => $values['position'] ?? $model->media()->exists() ? null : $key,
                        'label' => $values['label'] ?? pathinfo($values['file']->getClientOriginalName(), PATHINFO_FILENAME),
                        'description' => $values['description'] ?? null,
                        'extension' => $values['file']->getClientOriginalExtension(),
                        'type' => $mediaType,
                    ]);

                    // Upload the file into the storage, if not then delete into the database
                    try {

                        // Try to upload the file
                        $new->uploadFile($values['file']);

                        // Add the changes into the array
                        $changes['added'][] = $new->getAttributes();

                    } catch (Exception $e) {
                        Session::flash('error', trans('laravel-launcher::notification.error.media'));
                        $new->delete();
                    }

                }

            }

        }

        // Update some Media
        if (request()->has('media_to_update')) {

            foreach (request()->media_to_update as $mediaType => $medias) {

                foreach ($medias as $key => $values) {

                    $media = Media::find($values['id']);

                    try {

                        // Fill with the new values
                        $media->fill($values);

                        // If dirty
                        if (! empty(array_filter($media->getDirty()))) {

                            // Save changes for activity log
                            $changes['updated'][] = $media->getDirty();

                            // Save in database
                            $media->save();

                        }

                    } catch (Exception $e) {
                    }

                }

            }

        }

        // Delete some Media
        if (request()->has('media_to_delete')) {

            foreach (request()->media_to_delete as $mediaType => $medias) {

                foreach ($medias as $key => $values) {
                    $media = Media::find($values['id']);
                    $media->delete();
                    $changes['deleted'][] = $values['id'];
                }

            }

        }

        // Add the activity log
        if (in_array('Nh\LaravelLauncher\Traits\Loggable', class_uses($model)) && ! empty(array_filter($changes))) {
            $model->addLog('synchronized', ['media' => $changes]);
        }

    }

    /**
     * Handle the Model "deleting" event.
     * -> Delete the media
     */
    public function deleting($model): void
    {
        if (! in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($model))) {
            foreach ($model->media as $media) {
                $media->delete();
            }
        }
    }

    /**
     * Handle the Model "forceDeleting" event.
     * -> Delete the media
     */
    public function forceDeleting($model): void
    {
        foreach ($model->media as $media) {
            $media->delete();
        }
    }
}
