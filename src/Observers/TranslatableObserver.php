<?php

namespace Nh\LaravelLauncher\Observers;

class TranslatableObserver
{
    /**
     * Handle the Model "saved" event.
     * -> Create/update translation
     */
    public function saved($model): void
    {
        $translations = request()->has('translations') ? request()->translations : [];
        if (! empty($translations)) {
            $model->setTranslations($translations);
        }
    }

    /**
     * Handle the Model "deleting" event.
     * -> Delete the translation
     */
    public function deleting($model): void
    {
        if (! in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($model))) {
            $model->translations()->delete();
        }
    }

    /**
     * Handle the Model "forceDeleting" event.
     * -> Delete the translation
     */
    public function forceDeleting($model): void
    {
        $model->translations()->delete();
    }
}
