<?php

namespace Nh\LaravelLauncher\Observers;

class CategorizableObserver
{
    /**
     * Handle the Model "saved" event.
     * -> Sync some categories
     * -> Add activity log for this relation
     */
    public function saved($model): void
    {

        // Check if config is set
        $config = config('laravel-launcher.'.getSingleModelName($model).'.categorizable');

        // Ignore if there is no a categorizable
        if (! request()->has('categorizable') || ! $config) {
            return;
        }

        // Define categories by request
        $categories = request()->has('categories') ? request()->categories : [];

        // Check number
        if (is_numeric($config) && count($categories) >= $config) {
            array_slice($categories, 0, $config);
        }

        // Sync categories
        $sync = $model->categories()->sync($categories);

        // Add the activity log
        if (in_array('Nh\LaravelLauncher\Traits\Loggable', class_uses($model)) && ! empty(array_filter($sync))) {
            $model->addLog('synchronized', ['categories' => array_filter($sync)]);
        }

    }

    /**
     * Handle the Model "deleting" event.
     * -> Detach the categories
     */
    public function deleting($model): void
    {
        if (! in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($model))) {
            $model->categories()->detach();
        }
    }

    /**
     * Handle the Model "forceDeleting" event.
     * -> Detach the categories
     */
    public function forceDeleting($model): void
    {
        $model->categories()->detach();
    }
}
