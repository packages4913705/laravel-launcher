<?php

namespace Nh\LaravelLauncher\Observers;

class PositionableObserver
{
    /**
     * Handle the Model "creating" event.
     * -> Set the default position
     */
    public function creating($model): void
    {
        if (is_null($model->position)) {
            $model->position = $model->max('position') + 1;
        }
    }
}
