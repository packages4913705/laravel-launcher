<?php

namespace Nh\LaravelLauncher\Observers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;

class LevelableObserver
{
    /**
     * Handle the Model "saving" event.
     * -> Check the maximum level
     */
    public function saving($model): void
    {

        if (request()->has('parent_id')) {

            // Get the parent
            $parent = DB::table($model->getTable())->find(request()->parent_id);

            // Check if config is set
            $config = config('laravel-launcher.'.getSingleModelName($model).'.levelable');

            if ($config && Schema::hasColumn($model->getTable(), 'level')) {

                // Get the new level
                $level = $parent ? $parent->level + 1 : 0;

                if (is_numeric($config) && $level > $config) {
                    // If the new level is over the maximum set in config, dont save the parent and level
                    $model->parent_id = null;
                    Session::flash('error', trans('laravel-launcher::notification.error.max-levelable'));
                } else {
                    // If the new level is under the max or it's unlimited, set the parent and the level
                    $model->parent_id = $parent->id ?? null;
                    $model->level = $level;
                }

            }

        }

    }
}
