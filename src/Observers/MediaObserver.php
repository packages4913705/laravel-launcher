<?php

namespace Nh\LaravelLauncher\Observers;

use Illuminate\Support\Facades\Storage;
use Nh\LaravelLauncher\Models\Media;

class MediaObserver
{
    /**
     * Handle the Media "saving" event.
     * -> Save the format by extension
     */
    public function saving(Media $media): void
    {

        if ($media->isDirty('extension')) {
            $format = null;

            switch ($media->extension) {
                case 'jpeg':
                case 'jpg':
                case 'png':
                    $format = 'image';
                    break;

                case 'svg':
                    $format = 'vector';
                    break;

                case 'pdf':
                    $format = 'pdf';
                    break;

                case 'doc':
                case 'docx':
                    $format = 'word';
                    break;

                case 'xlsx':
                case 'xls':
                case 'csv':
                    $format = 'excel';
                    break;

                case 'mp3':
                case 'mpga':
                    $format = 'audio';
                    break;

                case 'mp4':
                    $format = 'video';
                    break;

                default:
                    $format = 'code';
                    break;
            }

            $media->format = $format;
        }

    }

    /**
     * Handle the Media "updating" event.
     * -> Rename the file if only the label change
     */
    public function updating(Media $media): void
    {
        if ($media->isDirty('label') && ! request()->has('udpate_media_file')) {
            renameFilesInStorage($media->getOriginal('filename'), $media->filename);
        }
    }

    /**
     * Handle the Media "deleting" event.
     * -> Remove Files into the storage (helper in LauncherHelper)
     * ! Not work on multiple deletes as in $model->media()->delete()
     */
    public function deleting(Media $media): void
    {
        deleteFilesInStorage($media->filename);
    }
}
