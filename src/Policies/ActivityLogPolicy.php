<?php

namespace Nh\LaravelLauncher\Policies;

use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Models\ActivityLog;
use Nh\LaravelLauncher\Models\User;

class ActivityLogPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return Route::has('backend.activity-logs.index') && $user->hasAccess('activity-log', 'view');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, ActivityLog $model): bool
    {
        return Route::has('backend.activity-logs.show') && $user->hasAccess('activity-log', 'view');
    }
}
