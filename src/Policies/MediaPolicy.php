<?php

namespace Nh\LaravelLauncher\Policies;

use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Models\Media;
use Nh\LaravelLauncher\Models\User;

class MediaPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return Route::has('backend.media.index') && $user->hasAccess('media', 'view');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Media $model): bool
    {
        return Route::has('backend.media.show') && $user->hasAccess('media', 'view');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Media $model): bool
    {
        return Route::has('backend.media.edit') && $user->hasAccess('media', 'update');
    }

    /**
     * Determine whether the user can download the model.
     * -> This is usefull for the mediable card list
     */
    public function download(User $user, Media $model): bool
    {
        return true;
    }
}
