<?php

namespace Nh\LaravelLauncher\Policies;

use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Models\Page;
use Nh\LaravelLauncher\Models\User;

class PagePolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return Route::has('backend.pages.index') && $user->hasAccess('page', 'view');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Page $model): bool
    {
        return Route::has('backend.pages.show') && $user->hasAccess('page', 'view');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return Route::has('backend.pages.create') && $user->hasAccess('page', 'create');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Page $model): bool
    {
        return Route::has('backend.pages.edit') && $user->hasAccess('page', 'update');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Page $model): bool
    {
        return Route::has('backend.pages.delete') && $user->hasAccess('page', 'delete') && ! $model->trashed() && (! $model->is_protected || $user->has_superpowers);
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Page $model): bool
    {
        return Route::has('backend.pages.restore') && $user->hasAccess('page', 'restore') && $model->trashed();
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function destroy(User $user, Page $model): bool
    {
        return Route::has('backend.pages.destroy') && $user->hasAccess('page', 'destroy') && $model->trashed();
    }

    /**
     * Determine whether the user can publish the model.
     */
    public function publish(User $user, Page $model): bool
    {
        return Route::has('backend.pages.publish') && $user->hasAccess('page', 'update') && ! $model->published && $model->name !== 'index';
    }

    /**
     * Determine whether the user can unpublish the model.
     */
    public function unpublish(User $user, Page $model): bool
    {
        return Route::has('backend.pages.unpublish') && $user->hasAccess('page', 'update') && $model->published && $model->name !== 'index';
    }
}
