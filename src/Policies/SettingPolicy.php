<?php

namespace Nh\LaravelLauncher\Policies;

use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Models\Setting;
use Nh\LaravelLauncher\Models\User;

class SettingPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return Route::has('backend.settings.index') && $user->hasAccess('setting', 'view');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Setting $model): bool
    {
        return false;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user): bool
    {
        return Route::has('backend.settings.edit') && $user->hasAccess('setting', 'update');
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function destroy(User $user, Setting $model): bool
    {
        return false;
    }
}
