<?php

namespace Nh\LaravelLauncher\Policies;

use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Models\Role;
use Nh\LaravelLauncher\Models\User;

class RolePolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return Route::has('backend.roles.index') && $user->hasAccess('role', 'view');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Role $model): bool
    {
        return Route::has('backend.roles.show') && $user->hasAccess('role', 'view');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return Route::has('backend.roles.create') && $user->hasAccess('role', 'create');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Role $model): bool
    {
        return Route::has('backend.roles.edit') && $user->hasAccess('role', 'update');
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function destroy(User $user, Role $model): bool
    {
        return Route::has('backend.roles.destroy') && $user->hasAccess('role', 'destroy') && (! $model->is_protected || $user->has_superpowers);
    }
}
