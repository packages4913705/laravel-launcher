<?php

namespace Nh\LaravelLauncher\Policies;

use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Models\Category;
use Nh\LaravelLauncher\Models\User;

class CategoryPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return Route::has('backend.categories.index') && $user->hasAccess('category', 'view');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Category $model): bool
    {
        return Route::has('backend.categories.show') && $user->hasAccess('category', 'view');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return Route::has('backend.categories.create') && $user->hasAccess('category', 'create');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Category $model): bool
    {
        return Route::has('backend.categories.edit') && $user->hasAccess('category', 'update');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Category $model): bool
    {
        return Route::has('backend.categories.delete') && $user->hasAccess('category', 'delete') && ! $model->trashed() && (! $model->is_protected || $user->has_superpowers);
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Category $model): bool
    {
        return Route::has('backend.categories.restore') && $user->hasAccess('category', 'restore') && $model->trashed();
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function destroy(User $user, Category $model): bool
    {
        return Route::has('backend.categories.destroy') && $user->hasAccess('category', 'destroy') && $model->trashed();
    }
}
