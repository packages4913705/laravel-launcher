<?php

namespace Nh\LaravelLauncher\Policies;

use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Models\Permission;
use Nh\LaravelLauncher\Models\User;

class PermissionPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return Route::has('backend.permissions.index') && $user->hasAccess('permission', 'view');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Permission $model): bool
    {
        return Route::has('backend.permissions.show') && $user->hasAccess('permission', 'view');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return Route::has('backend.permissions.create') && $user->hasAccess('permission', 'create');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Permission $model): bool
    {
        return Route::has('backend.permissions.edit') && $user->hasAccess('permission', 'update');
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function destroy(User $user, Permission $model): bool
    {
        return Route::has('backend.permissions.destroy') && $user->hasAccess('permission', 'destroy') && (! $model->is_protected || $user->has_superpowers);
    }
}
