<?php

namespace Nh\LaravelLauncher\Policies;

use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Models\User;

class UserPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return Route::has('backend.users.index') && $user->hasAccess('user', 'view');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, User $model): bool
    {
        return Route::has('backend.users.show') && $user->hasAccess('user', 'view');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return Route::has('backend.users.create') && $user->hasAccess('user', 'create');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, User $model): bool
    {

        // Always authorize if it's own profile
        if ($user->id == $model->id) {
            return true;
        }

        return Route::has('backend.users.edit') && $user->hasAccess('user', 'update') && ! $model->trashed() && (! $model->has_superpowers || $user->has_superpowers);

    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, User $model): bool
    {
        // Always refuse if it's own profile
        if ($user->id == $model->id) {
            return false;
        }

        return Route::has('backend.users.delete') && $user->hasAccess('user', 'delete') && ! $model->trashed() && (! $model->has_superpowers || $user->has_superpowers);

    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, User $model): bool
    {
        return Route::has('backend.users.restore') && $user->hasAccess('user', 'restore') && $model->trashed();
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function destroy(User $user, User $model): bool
    {
        return Route::has('backend.users.destroy') && $user->hasAccess('user', 'destroy') && $model->trashed();
    }
}
