<?php

namespace Nh\LaravelLauncher\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Database\Factories\PageFactory;
use Nh\LaravelLauncher\Traits\Categorizable;
use Nh\LaravelLauncher\Traits\Colorable;
use Nh\LaravelLauncher\Traits\Levelable;
use Nh\LaravelLauncher\Traits\Loggable;
use Nh\LaravelLauncher\Traits\Mediable;
use Nh\LaravelLauncher\Traits\Positionable;
use Nh\LaravelLauncher\Traits\Roleable;
use Nh\LaravelLauncher\Traits\Sluggable;
use Nh\LaravelLauncher\Traits\Translatable;
use Nh\LaravelLauncher\Traits\Typeable;

class Page extends Model
{
    use Categorizable, Colorable, HasFactory, Levelable, Loggable, Mediable, Positionable, Roleable, Sluggable, SoftDeletes, Translatable, Typeable;

    /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): PageFactory
    {
        return PageFactory::new();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'level',
        'parent_id',
        'position',
        'name',
        'slug',
        'label',
        'title',
        'description',
        'published',
        'in_menu',
        'in_footer',
        'color',
        'type',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'published' => 'boolean',
            'in_menu' => 'boolean',
            'in_footer' => 'boolean',
        ];
    }

    /**
     * Retrieve the model for a bound value.
     * -> Use this for retrieve softDeleted model
     *
     * @param  mixed  $value
     * @param  string|null  $field
     */
    public function resolveRouteBinding($value, $field = null): ?\Illuminate\Database\Eloquent\Model
    {
        return $this->withTrashed()->where('id', $value)->firstOrFail();
    }

    /**
     * Get the the subpages with all descendants.
     */
    public function subpages(): HasMany
    {
        return $this->children()->inMenu()->with('subpages');
    }

    /**
     * Check if the page is protected.
     * -> Make it impossible to delete via the backend except for the superadmin
     */
    public function isProtected(): Attribute
    {
        return Attribute::make(
            get: fn () => in_array($this->name, (array) config('laravel-launcher.page.protected'))
        );
    }

    /**
     * Check if the page is published.
     */
    public function isPublished(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->published
        );
    }

    /**
     * Get the URL of the page.
     */
    public function url(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->name === 'index' ? route('home') : route('page', ['slugs' => Arr::join($this->slugs, '/')])
        );
    }

    /**
     * Check if the page is the active one.
     */
    public function isCurrent(): Attribute
    {
        $current = Route::current()->parameters['slugs'] ?? 'index';
        $current = explode('/', $current);

        return Attribute::make(
            get: fn () => in_array($this->slug, (array) $current)
        );
    }

    /**
     * Get published resources.
     */
    public function scopePublished(Builder $query): void
    {
        $query->where('published', 1);
    }

    /**
     * Get in menu resources.
     */
    public function scopeInMenu(Builder $query): void
    {
        $query->where('published', 1)->where('in_menu', 1);
    }

    /**
     * Get in footer resources.
     */
    public function scopeInFooter(Builder $query): void
    {
        $query->where('published', 1)->where('in_footer', 1);
    }

    /**
     * Filter the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeFilter(Builder $query, string $filter): void
    {
        switch ($filter) {
            case 'all':
                // Do nothing
                break;
            case 'trashed':
                $query->onlyTrashed();
                break;
            case 'published':
                $query->published();
                break;
            case 'in-menu':
                $query->where('in_menu', 1);
                break;
            case 'in-footer':
                $query->where('in_footer', 1);
                break;
            case 'not-in':
                $query->where('in_menu', 0)->where('in_footer', 0);
                break;
            default:
                $query->byTypes($filter);
                break;
        }
    }

    /**
     * Search the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeSearch(Builder $query, array $search): void
    {
        $query->where(function ($q) use ($search) {
            if (array_key_exists('keyword', $search)) {
                $q->where('name', 'LIKE', '%'.$search['keyword'].'%')
                    ->orWhere('label', 'LIKE', '%'.$search['keyword'].'%')
                    ->orWhere('title', 'LIKE', '%'.$search['keyword'].'%')
                    ->orWhere('description', 'LIKE', '%'.$search['keyword'].'%');
            }

            if (array_key_exists('color', $search)) {
                $search['color'] == 'empty' ? $q->whereNull('color')->orWhere('color', '') : $q->byColors($search['color']);
            }

            if (array_key_exists('category', $search)) {
                $search['category'] == 'empty' ? $q->doesntHave('categories') : $q->byCategories($search['category']);
            }

            if (array_key_exists('role', $search)) {
                $search['role'] == 'empty' ? $q->doesntHave('roles') : $q->byRoles($search['role']);
            }
        });
    }
}
