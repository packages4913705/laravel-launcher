<?php

namespace Nh\LaravelLauncher\Models;

use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Nh\LaravelLauncher\Database\Factories\PermissionFactory;
use Nh\LaravelLauncher\Observers\PermissionObserver;
use Nh\LaravelLauncher\Traits\Loggable;
use Nh\LaravelLauncher\Traits\Translatable;

#[ObservedBy([PermissionObserver::class])]
class Permission extends Model
{
    use HasFactory, Loggable, Translatable;

    /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): PermissionFactory
    {
        return PermissionFactory::new();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'label',
        'model',
        'action',
    ];

    /**
     * The roles that belong to the permission.
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Check if the permission is protected.
     * -> Make it impossible to delete via the backend except for the superadmin
     */
    public function isProtected(): Attribute
    {
        return Attribute::make(
            get: fn () => in_array($this->name, (array) config('laravel-launcher.permission.protected'))
        );
    }

    /**
     * Search the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeSearch(Builder $query, array $search): void
    {
        $query->where(function ($q) use ($search) {
            if (array_key_exists('keyword', $search)) {
                $q->where('name', 'LIKE', '%'.$search['keyword'].'%')
                    ->orWhere('label', 'LIKE', '%'.$search['keyword'].'%');
            }

            if (array_key_exists('action', $search) && ! empty($search['action'])) {
                $search['action'] == 'empty' ? $q->whereNull('action')->orWhere('action', '') : $q->where('action', $search['action']);
            }

            if (array_key_exists('model', $search) && ! empty($search['model'])) {
                $search['model'] == 'empty' ? $q->whereNull('model')->orWhere('model', '') : $q->where('model', $search['model']);
            }
        });
    }
}
