<?php

namespace Nh\LaravelLauncher\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Database\Factories\CategoryFactory;
use Nh\LaravelLauncher\Traits\Colorable;
use Nh\LaravelLauncher\Traits\Levelable;
use Nh\LaravelLauncher\Traits\Loggable;
use Nh\LaravelLauncher\Traits\Mediable;
use Nh\LaravelLauncher\Traits\Positionable;
use Nh\LaravelLauncher\Traits\Roleable;
use Nh\LaravelLauncher\Traits\Sluggable;
use Nh\LaravelLauncher\Traits\Translatable;
use Nh\LaravelLauncher\Traits\Typeable;

class Category extends Model
{
    use Colorable, HasFactory, Levelable, Loggable, Mediable, Positionable, Roleable, Sluggable, SoftDeletes, Translatable, Typeable;

    /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): CategoryFactory
    {
        return CategoryFactory::new();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'level',
        'parent_id',
        'position',
        'name',
        'slug',
        'label',
        'color',
        'type',
    ];

    /**
     * Retrieve the model for a bound value.
     * -> Use this for retrieve softDeleted model
     *
     * @param  mixed  $value
     * @param  string|null  $field
     */
    public function resolveRouteBinding($value, $field = null): ?\Illuminate\Database\Eloquent\Model
    {
        return $this->withTrashed()->where('id', $value)->firstOrFail();
    }

    /**
     * Check if the category is protected.
     * -> Make it impossible to delete via the backend except for the superadmin
     */
    public function isProtected(): Attribute
    {
        return Attribute::make(
            get: fn () => in_array($this->name, (array) config('laravel-launcher.category.protected'))
        );
    }

    /**
     * Check the number of used categories
     */
    public function usedBy(): Attribute
    {
        return Attribute::make(
            get: fn () => DB::table('categorizables')->where('category_id', $this->id)->count()
        );
    }

    /**
     * Filter the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeFilter(Builder $query, string $filter): void
    {
        switch ($filter) {
            case 'all':
                // Do nothing
                break;
            case 'trashed':
                $query->onlyTrashed();
                break;
            default:
                $query->byTypes($filter);
                break;
        }
    }

    /**
     * Search the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeSearch(Builder $query, array $search): void
    {
        $query->where(function ($q) use ($search) {
            if (array_key_exists('keyword', $search)) {
                $q->where('name', 'LIKE', '%'.$search['keyword'].'%')
                    ->orWhere('label', 'LIKE', '%'.$search['keyword'].'%');
            }

            if (array_key_exists('color', $search)) {
                $search['color'] == 'empty' ? $q->whereNull('color')->orWhere('color', '') : $q->byColors($search['color']);
            }

            if (array_key_exists('role', $search)) {
                $search['role'] == 'empty' ? $q->doesntHave('roles') : $q->byRoles($search['role']);
            }
        });

    }
}
