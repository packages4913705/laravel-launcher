<?php

namespace Nh\LaravelLauncher\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Str;

class ActivityLog extends Model
{
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['description'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'event',
        'value',
        'comment',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'value' => 'array',
        ];
    }

    /**
     * The user associated with the activity log.
     */
    public function user(): BelongsTo
    {
        return $this->BelongsTo(User::class);
    }

    /**
     * Get the owning loggable model.
     */
    public function loggable(): MorphTo
    {
        return $this->morphTo()->withoutGlobalScope(SoftDeletingScope::class);
    }

    /**
     * Get the translated event of the log.
     */
    public function label(): Attribute
    {
        return Attribute::make(
            get: fn () => translate($this->event, 'log')
        );
    }

    /**
     * Get the name of the user that belong to the log.
     * -> In case of deleted user => set [deleted user]
     * -> In case of no user => [system]
     */
    public function by(): Attribute
    {
        return Attribute::make(
            get: fn () => is_null($this->user_id) && str_contains($this->comment, '[system]') ? trans('laravel-launcher::backend.log.system') : $this->user->name ?? trans('laravel-launcher::backend.log.no-user')
        );
    }

    /**
     * Get the name of the model that belong to the log.
     */
    public function model(): Attribute
    {
        return Attribute::make(
            get: fn () => getSingleModelName($this->loggable_type)
        );
    }

    /**
     * Get the name of the related model that belong to the log.
     * ! Media should not be use with Str::singular, otherwise it become medium
     */
    public function relation(): Attribute
    {
        return Attribute::make(
            get: fn () => count(array_keys($this->value)) == 1 ? (array_keys($this->value)[0] === 'media' ? 'media' : Str::singular(array_keys($this->value)[0])) : null
        );
    }

    /**
     * Get the full description of the log.
     * -> Translated model
     * -> Label/Title/Name of the item
     * -> Translated relation
     */
    public function description(): Attribute
    {
        $model = translate($this->model, 'model');
        $relation = $this->relation ? '> '.(translate($this->relation, 'model')) : '';
        $name = $this->loggable ? Str::limit($this->loggable->label ?? $this->loggable->name ?? $this->loggable->id, 15) : trans('laravel-launcher::backend.log.no-item');

        return Attribute::make(
            get: fn () => "{$model} : {$name} {$relation}"
        );
    }

    /**
     * Get the icon of the log.
     */
    public function icon(): Attribute
    {

        switch ($this->event) {
            case 'created':
                $icon = 'plus-lg';
                break;

            case 'restored':
                $icon = 'arrow-counterclockwise';
                break;

            case 'updated':
                $icon = 'pencil';
                break;

            case 'destroyed':
            case 'deleted':
                $icon = 'trash';
                break;

            case 'synchronized':
            case 'attached':
            case 'detached':
                $icon = 'diagram-2';
                break;

            default:
                $icon = 'rocket';
                break;
        }

        return Attribute::make(
            get: fn () => $icon
        );
    }

    /**
     * Filter the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeFilter(Builder $query, string $filter): void
    {
        switch ($filter) {
            case 'all':
                $query->whereBetween('created_at', [Carbon::now()->subMonth(3)->startOfMonth(), Carbon::now()->endOfMonth()]);
                break;
            case 'today':
                $query->whereDate('created_at', Carbon::today());
                break;
            case 'week':
                $query->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
                break;
            case 'month':
                $query->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()]);
                break;
            case 'last-month':
                $query->whereBetween('created_at', [Carbon::now()->subMonth()->startOfMonth(), Carbon::now()->subMonth()->endOfMonth()]);
                break;
        }
    }

    /**
     * Search the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeSearch(Builder $query, array $search): void
    {
        $query->where(function ($q) use ($search) {
            if (array_key_exists('keyword', $search)) {
                $q->whereHas('user', function ($q) use ($search) {
                    $q->where('name', 'LIKE', '%'.$search['keyword'].'%');
                });
            }

            if (array_key_exists('event', $search) && ! empty($search['event'])) {
                $search['event'] == 'empty' ? $q->whereNull('event')->orWhere('event', '') : $q->where('event', $search['event']);
            }

            if (array_key_exists('model', $search) && ! empty($search['model'])) {
                $search['model'] == 'empty' ? $q->whereNull('model')->orWhere('model', '') : $q->where('loggable_type', $search['model']);
            }
        });
    }
}
