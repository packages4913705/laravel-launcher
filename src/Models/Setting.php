<?php

namespace Nh\LaravelLauncher\Models;

use Illuminate\Database\Eloquent\Model;
use Nh\LaravelLauncher\Traits\Loggable;

class Setting extends Model
{
    use Loggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'value',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'value' => 'array',
        ];
    }
}
