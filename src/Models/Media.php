<?php

namespace Nh\LaravelLauncher\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Laravel\Facades\Image as LaravelImage;
use Nh\LaravelLauncher\Observers\MediaObserver;
use Nh\LaravelLauncher\Traits\Loggable;
use Nh\LaravelLauncher\Traits\Positionable;
use Nh\LaravelLauncher\Traits\Translatable;

#[ObservedBy([MediaObserver::class])]
class Media extends Model
{
    use Loggable, Positionable, Translatable;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['filename'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'position',
        'label',
        'description',
        'extension',
        'format',
        'type',
    ];

    /**
     * Get the owning mediable model.
     */
    public function mediable(): MorphTo
    {
        return $this->morphTo()->withoutGlobalScope(SoftDeletingScope::class);
    }

    /**
     * Set the extension.
     * -> Transform jpeg to jpg
     */
    public function extension(): Attribute
    {
        return Attribute::make(
            set: fn (string $value) => $value == 'jpeg' ? 'jpg' : $value
        );
    }

    /**
     * Get the filename.
     * -> By default it is {id}-{label}.{extention}
     * -> The label is slugified to avoid uppercase and white space
     */
    public function filename(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->id.'-'.Str::slug($this->label).'.'.$this->extension
        );
    }

    /**
     * Get the folder.
     * -> By default it is {pluralLowerClassName}/{pluralFormat}
     */
    public function folder(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => getPluralModelName($attributes['mediable_type']).'/'.Str::pluralStudly($this->format).'/'
        );
    }

    /**
     * Get the url.
     */
    public function url(): Attribute
    {
        return Attribute::make(
            get: fn () => Storage::url($this->folder.$this->filename)
        );
    }

    /**
     * Get the file.
     */
    public function file(): Attribute
    {
        return Attribute::make(
            get: fn () => Storage::get($this->folder.$this->filename)
        );
    }

    /**
     * Get the url by size.
     */
    public function urlBySize(string $size): string
    {
        return Storage::url($this->folder.$size.'/'.$this->filename);
    }

    /**
     * Get the file by size.
     */
    public function fileBySize(string $size): string
    {
        return Storage::get($this->folder.$size.'/'.$this->filename);
    }

    /**
     * Upload a file
     * -> If image, check for watermark and resize
     */
    public function uploadFile($file): void
    {

        if ($this->format === 'image') {
            // Create an Intervention/Image
            $file = LaravelImage::read($file);

            // Get the config per model and type
            $config = config('laravel-launcher.'.$this->mediable_type.'.mediable.'.$this->type);

            // Add a watermark, if necessary
            if (! empty($config['watermark'])) {
                $watermark = public_path($config['watermark']['path'] ?? 'images/watermark.png');
                $file->place(
                    $watermark,
                    $config['watermark']['position'] ?? 'bottom-right',
                    $config['watermark']['padding']['x'] ?? 0,
                    $config['watermark']['padding']['y'] ?? 0,
                    $config['watermark']['opacity'] ?? 100
                );
            }

            // Create folder if not already exist
            Storage::makeDirectory($this->folder);

            // Save the file inside the folder
            $file->save(Storage::path($this->folder.$this->filename));

            // Get the resizes in config and process
            $resizes = $config['sizes'] ?? [];

            foreach ($resizes as $type => $sizes) {
                foreach ($sizes as $size) {
                    // Make a copy of original
                    $copy = LaravelImage::read($this->file);

                    // Resize by type
                    switch ($type) {
                        case 'fit':
                            $copy->cover($size, $size);
                            break;
                        case 'height':
                            $copy->scale(height: $size);
                            break;
                        case 'width':
                            $copy->scale(width: $size);
                            break;
                    }

                    // Define folder name by size
                    $folder = $this->folder.mb_substr($type, 0, 1).'-'.$size.'/';

                    // Create the folder if not already exist
                    Storage::makeDirectory($folder);

                    // Save the file inside the folder
                    $copy->save(Storage::path($folder.$this->filename));
                }
            }
        } else {
            // Or simply upload the file !
            Storage::putFileAs($this->folder, $file, $this->filename);
        }

    }

    /**
     * Filter the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeFilter(Builder $query, string $filter): void
    {
        switch ($filter) {
            case 'all':
                // Do nothing
                break;
            default:
                $query->where('format', $filter);
                break;
        }
    }

    /**
     * Search the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeSearch(Builder $query, array $search): void
    {
        $query->where(function ($q) use ($search) {
            if (array_key_exists('keyword', $search)) {
                $q->where('label', 'LIKE', '%'.$search['keyword'].'%')
                    ->orWhere('description', 'LIKE', '%'.$search['keyword'].'%');
            }
        });
    }
}
