<?php

namespace Nh\LaravelLauncher\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Nh\LaravelLauncher\Database\Factories\UserFactory;
use Nh\LaravelLauncher\Notifications\ResetPasswordNotification;
use Nh\LaravelLauncher\Traits\Loggable;
use Nh\LaravelLauncher\Traits\Mediable;
use Nh\LaravelLauncher\Traits\Roleable;

class User extends Authenticatable
{
    use HasFactory, Loggable, Mediable, Notifiable, Roleable, SoftDeletes;

    /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): UserFactory
    {
        return UserFactory::new();
    }

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['roles'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    /**
     * Retrieve the model for a bound value.
     * -> Use this for retrieve softDeleted model
     *
     * @param  mixed  $value
     * @param  string|null  $field
     */
    public function resolveRouteBinding($value, $field = null): ?\Illuminate\Database\Eloquent\Model
    {
        return $this->withTrashed()->where('id', $value)->firstOrFail();
    }

    /**
     * Send a password reset notification to the user.

     *

     * @param  string  $token
     */
    public function sendPasswordResetNotification($token): void
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Get all the accessible permissions for a user.
     */
    protected function permissions(): Attribute
    {
        return Attribute::make(
            get: fn () => Permission::whereHas('roles', function ($query) {
                $query->whereIn('id', $this->roles->modelKeys());
            })->orderBy('name')->get()
        );
    }

    /**
     * Check if the user has superpowers.
     */
    protected function hasSuperpowers(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->hasRoles('superadmin'),
        );
    }

    /**
     * Check if the user is an administrator.
     */
    protected function isAdministrator(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->hasRoles(config('laravel-launcher.role.administrator')),
        );
    }

    /**
     * Filter the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeFilter(Builder $query, string $filter): void
    {
        switch ($filter) {
            case 'trashed':
                $query->onlyTrashed();
                break;
        }
    }

    /**
     * Search the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeSearch(Builder $query, array $search): void
    {
        $query->where(function ($q) use ($search) {
            if (array_key_exists('keyword', $search)) {
                $q->where('name', 'LIKE', '%'.$search['keyword'].'%');
            }

            if (array_key_exists('role', $search)) {
                $search['role'] == 'empty' ? $q->doesntHave('roles') : $q->byRoles($search['role']);
            }
        });
    }
}
