<?php

namespace Nh\LaravelLauncher\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Nh\LaravelLauncher\Database\Factories\RoleFactory;
use Nh\LaravelLauncher\Traits\Loggable;
use Nh\LaravelLauncher\Traits\Translatable;

class Role extends Model
{
    use HasFactory, Loggable, Translatable;

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['permissions'];

    /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): RoleFactory
    {
        return RoleFactory::new();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'label',
    ];

    /**
     * The permissions that belong to the role.
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Check if the role is protected.
     * -> Make it impossible to delete via the backend except for the superadmin
     */
    public function isProtected(): Attribute
    {
        return Attribute::make(
            get: fn () => in_array($this->name, (array) config('laravel-launcher.role.protected'))
        );
    }

    /**
     * Check the number of used roles
     */
    public function usedBy(): Attribute
    {
        return Attribute::make(
            get: fn () => DB::table('roleables')->where('role_id', $this->id)->count()
        );
    }

    /**
     * Get the accessible roles by user.
     * -> To make sure that a user can only set other role as himself
     */
    public function scopeByUser(Builder $query): void
    {
        if (Auth::user()->has_superpowers) {
            return;
        }

        if (Auth::user()->is_administrator) {
            $query->where('name', '!=', 'superadmin');
        } else {
            $query->whereIn('id', Auth::user()->roles->pluck('id'));
        }
    }

    /**
     * Search the resources.
     * -> Used for Livewire Datalist
     */
    public function scopeSearch(Builder $query, array $search): void
    {
        if (array_key_exists('keyword', $search)) {
            $query->where('name', 'LIKE', '%'.$search['keyword'].'%')
                ->orWhere('label', 'LIKE', '%'.$search['keyword'].'%');
        }
    }
}
