<?php

namespace Nh\LaravelLauncher\View\Components\Backend;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Card extends Component
{
    /**
     * An icon from bootstrap library.
     *
     * @var string
     */
    public $icon;

    /**
     * The title of the card.
     *
     * @var string
     */
    public $title;

    /**
     * The model of the card.
     * -> Auto translation from Model then Field
     *
     * @var string
     */
    public $model;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $icon = null,
        $title = null,
        $model = null
    ) {
        $this->icon = $icon;
        $this->title = $title;
        $this->model = ! empty($model) ? translate($model, ['model', 'field']) : null;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.backend.card');
    }
}
