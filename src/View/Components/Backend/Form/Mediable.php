<?php

namespace Nh\LaravelLauncher\View\Components\Backend\Form;

use Closure;
use Illuminate\Contracts\View\View;

class Mediable extends Template
{
    /**
     * Define the helper message per options in config
     * -> Format & weight are always visible
     * -> Priority of the 'size' property, then 'width' + 'height' and at last the 'fit'
     */
    public function getHelperMessage($options): string
    {
        $helperTranslation = 'laravel-launcher::form.helpers.';

        $help[] = trans($helperTranslation.'format', ['format' => $options['format'] ?? 'jpg, png, svg, pdf, doc, docx, xls, xlsx, csv, ics']);
        $help[] = trans($helperTranslation.'weight', ['weight' => $options['weight'] ?? '16000']);

        if (isset($options['size'])) {
            $help[] = trans($helperTranslation.'size', ['size' => $options['size']]);
        } elseif (isset($options['sizes'])) {

            $width = ! empty($options['sizes']['width']) ? max($options['sizes']['width']) : null;
            $help[] = ! empty($width) ? trans($helperTranslation.'width', ['width' => $width]) : '';

            $height = ! empty($options['sizes']['height']) ? max($options['sizes']['height']) : null;
            $help[] = ! empty($height) ? trans($helperTranslation.'height', ['height' => $height]) : '';

            $fit = ! empty($options['sizes']['fit']) ? max($options['sizes']['fit']) : null;
            if (empty($width) && empty($height) && ! empty($fit)) {
                $help[] = trans($helperTranslation.'size', ['size' => $fit]);
            }

        }

        return implode(' | ', array_filter($help));
    }

    /**
     * Create a new component instance.
     */
    public function __construct(
        $model = null,
        $current = []
    ) {

        // Run the parent in Template
        parent::__construct($model, 'mediable', $current);

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.backend.form.mediable');
    }
}
