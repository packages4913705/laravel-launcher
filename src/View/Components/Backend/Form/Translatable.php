<?php

namespace Nh\LaravelLauncher\View\Components\Backend\Form;

use Closure;
use Illuminate\Contracts\View\View;

class Translatable extends Template
{
    /**
     * The array of languages.
     *
     * @var array
     */
    public $languages;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $model = null
    ) {

        // Run the parent in Template
        parent::__construct($model, 'translatable', null);

        // Set the additionnal attributes
        $this->languages = array_diff(config('laravel-launcher.languages'), [config('app.locale')]);

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.backend.form.translatable');
    }
}
