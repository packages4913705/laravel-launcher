<?php

namespace Nh\LaravelLauncher\View\Components\Backend\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Route;

class Levelable extends Template
{
    /**
     * The array of parents.
     *
     * @var array
     */
    public $parents;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $model = null,
        $parents = [],
        $current = null
    ) {
        // Run the parent in Template
        parent::__construct($model, 'levelable', $current);

        // Set the additionnal attributes

        if ($parents) {
            $this->parents = $parents;
        } else {
            // Get the current item if exist
            $currentItem = Route::current()->parameters[$this->model] ?? null;

            // Get the model class
            $modelClass = Relation::getMorphedModel($this->model);

            if ($currentItem) {
                $this->parents = $modelClass::whereNot('id', $currentItem->id)->whereNotIn('id', $currentItem->children_keys)->availableLevels($this->config)->get()->pluck('hierarchy', 'id');
            } else {
                $this->parents = $modelClass::availableLevels($this->config)->get()->pluck('hierarchy', 'id');
            }
        }

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.backend.form.levelable');
    }
}
