<?php

namespace Nh\LaravelLauncher\View\Components\Backend\Form;

use Closure;
use Illuminate\Contracts\View\View;

class Colorable extends Template
{
    /**
     * The array of colors.
     *
     * @var array
     */
    public $colors;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $model = null,
        $colors = [],
        $current = null
    ) {

        // Run the parent in Template
        parent::__construct($model, 'colorable', $current);

        // Set the additionnal attributes
        $this->colors = empty($colors) ? ($this->config ?? []) : $colors;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.backend.form.colorable');
    }
}
