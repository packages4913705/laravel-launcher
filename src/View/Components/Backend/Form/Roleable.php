<?php

namespace Nh\LaravelLauncher\View\Components\Backend\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Nh\LaravelLauncher\Models\Role;

class Roleable extends Template
{
    /**
     * The array of roles.
     *
     * @var array
     */
    public $roles;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $model = null,
        $roles = [],
        $current = null
    ) {

        // Run the parent in Template
        parent::__construct($model, 'roleable', $current);

        // Set the additionnal attributes
        $this->roles = empty($roles) ? Role::byUser()->get() : $roles;

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.backend.form.roleable');
    }
}
