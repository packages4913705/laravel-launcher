<?php

namespace Nh\LaravelLauncher\View\Components\Backend\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class Template extends Component
{
    /**
     * The model/module name.
     *
     * @var string
     */
    public $model;

    /**
     * The config.
     *
     * @var string
     */
    public $config;

    /**
     * The current value.
     *
     * @var mixed
     */
    public $current;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $model = null,
        $config = null,
        $current = null
    ) {

        if ($model) {
            $this->model = $model;
        } else {
            // Get the current route name and explode it !
            $routeName = explode('.', Route::currentRouteName());

            // Define the model by route
            $this->model = Str::singular($routeName[1]) ?? null;
        }

        $this->config = config('laravel-launcher.'.$this->model.'.'.$config);

        $this->current = $current;

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return false;
    }
}
