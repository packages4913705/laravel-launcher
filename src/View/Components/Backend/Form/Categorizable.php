<?php

namespace Nh\LaravelLauncher\View\Components\Backend\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Nh\LaravelLauncher\Models\Category;

class Categorizable extends Template
{
    /**
     * The array of categories.
     *
     * @var array
     */
    public $categories;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $model = null,
        $categories = [],
        $current = null
    ) {

        // Run the parent in Template
        parent::__construct($model, 'categorizable', $current);

        // Set the additionnal attributes
        if ($this->config === true || $this->config > 1) {
            $categories = Category::with('children')->byTypes($this->model, true)->whereNull('parent_id')->orderBy('position', 'asc')->get();
        } else {
            $categories = Category::byTypes($this->model, true)->orderBy('position', 'asc')->get()->pluck('hierarchy', 'id');
        }

        $this->categories = $categories;

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.backend.form.categorizable');
    }
}
