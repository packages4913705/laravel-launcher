<?php

namespace Nh\LaravelLauncher\View\Components\Backend\Form;

use Closure;
use Illuminate\Contracts\View\View;

class Typeable extends Template
{
    /**
     * The array of types.
     *
     * @var array
     */
    public $types;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $model = null,
        $types = [],
        $current = null
    ) {

        // Run the parent in Template
        parent::__construct($model, 'typeable', $current);

        // Set the additionnal attributes
        $this->types = empty($types) ? ($this->config ?? []) : $types;

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.backend.form.typeable');
    }
}
