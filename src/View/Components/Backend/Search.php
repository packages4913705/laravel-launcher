<?php

namespace Nh\LaravelLauncher\View\Components\Backend;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use Nh\LaravelLauncher\Models\Category;
use Nh\LaravelLauncher\Models\Role;

class Search extends Component
{
    /**
     * The name of the model.
     *
     * @var string
     */
    public $model;

    /**
     * Array of options.
     *
     * @var array
     */
    public $options = [];

    /**
     * Array of colors.
     *
     * @var array
     */
    public $colors = [];

    /**
     * Array of categories.
     * -> Only if there is less than 15 categories in DB
     *
     * @var array
     */
    public $categories = [];

    /**
     * Array of roles.
     * -> Only if there is less than 15 roles in DB
     *
     * @var array
     */
    public $roles = [];

    /**
     * Create a new component instance.
     */
    public function __construct(
        $model = null
    ) {

        // Define the propertied
        $this->model = $model;
        $config = config('laravel-launcher.'.$model);

        if ($config) {

            // Get the colors
            $this->options['colorable'] = isset($config['colorable']) && $config['colorable'];
            if ($this->options['colorable']) {
                $this->colors['empty'] = '- '.trans_choice('laravel-launcher::field.empty', 1).' -';
                foreach ($config['colorable'] as $color) {
                    $this->colors[$color] = translate($color, 'color');
                }
            }

            // Get categories
            $this->options['categorizable'] = isset($config['categorizable']) && $config['categorizable'];
            if ($this->options['categorizable'] && Category::count() < 15) {
                $this->categories = Category::byTypes($this->model, true)->orderBy('position', 'asc')->get()->pluck('hierarchy', 'id');
                $this->categories->prepend('- '.trans_choice('laravel-launcher::field.empty', 1).' -', 'empty');
            }

            // Get roles
            $this->options['roleable'] = isset($config['roleable']) && $config['roleable'];
            if ($this->options['roleable'] && Role::count() < 15) {
                $this->roles = Role::get()->pluck('label', 'id');
                $this->roles->prepend('- '.trans_choice('laravel-launcher::field.empty', 1).' -', 'empty');
            }

        }

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.backend.search');
    }
}
