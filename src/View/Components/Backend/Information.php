<?php

namespace Nh\LaravelLauncher\View\Components\Backend;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\Component;

class Information extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(public Model $item) {}

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.backend.information');
    }
}
