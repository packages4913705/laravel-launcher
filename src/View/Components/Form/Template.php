<?php

namespace Nh\LaravelLauncher\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class Template extends Component
{
    /**
     * The id of the field.
     *
     * @var string
     */
    public $id;

    /**
     * The label of the field.
     *
     * @var string
     */
    public $label;

    /**
     * The name of the field.
     *
     * @var string
     */
    public $name;

    /**
     * The clean name of the field.
     *
     * @var string
     */
    public $cleanName;

    /**
     * The default value of the field.
     *
     * @var string
     */
    public $value;

    /**
     * The description message of the field.
     * -> Will be placed under the field
     *
     * @var string
     */
    public $description;

    /**
     * The info message of the field.
     * -> Will be placed next to the label
     *
     * @var string
     */
    public $info;

    /**
     * Is the field required.
     *
     * @var bool
     */
    public $required;

    /**
     * Is the field disabled.
     *
     * @var bool
     */
    public $disabled;

    /**
     * The invalid ID.
     *
     * @var string
     */
    public $invalidId;

    /**
     * The message ID.
     *
     * @var string
     */
    public $descriptionId;

    /**
     * Define if the field if before the label
     *
     * @var bool
     */
    public $fieldIsBefore;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $id,
        $label,
        $name,
        $value = null,
        $description = null,
        $info = null,
        $required = false,
        $disabled = false,
        $fieldIsBefore = false
    ) {
        $this->id = $id;
        $this->label = $label;
        $this->name = $name;
        $this->value = $value;
        $this->description = $description;
        $this->info = $info;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->invalidId = 'invalid'.ucfirst($this->id);
        $this->descriptionId = 'description'.ucfirst($this->id);
        $this->cleanName = (string) Str::of($name)->replace('[]', '')->replace('[', '.')->replace(']', '');
        $this->fieldIsBefore = $fieldIsBefore;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return false;
    }
}
