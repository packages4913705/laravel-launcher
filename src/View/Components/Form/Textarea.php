<?php

namespace Nh\LaravelLauncher\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;

class Textarea extends Template
{
    /**
     * Is the field readonly.
     *
     * @var bool
     */
    public $readonly;

    /**
     * The placeholder.
     *
     * @var string
     */
    public $placeholder;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $id,
        $label,
        $name,
        $value = null,
        $description = null,
        $info = null,
        $required = false,
        $disabled = false,
        $readonly = false,
        $placeholder = null
    ) {

        // Run the parent in Template
        parent::__construct($id, $label, $name, $value, $description, $info, $required, $disabled);

        // Set the additionnal attributes
        $this->readonly = $readonly;
        $this->placeholder = $placeholder;

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.form.template', ['field' => 'textarea']);
    }
}
