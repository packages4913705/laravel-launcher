<?php

namespace Nh\LaravelLauncher\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;

class Select extends Template
{
    /**
     * The options of the field.
     *
     * @var array
     */
    public $options;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $id,
        $label,
        $name,
        $value = null,
        $description = null,
        $info = null,
        $required = false,
        $disabled = false,
        $options = []
    ) {

        // Run the parent in Template
        parent::__construct($id, $label, $name, $value, $description, $info, $required, $disabled);

        // Set the additionnal attributes
        $this->options = $options;

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.form.template', ['field' => 'select']);
    }
}
