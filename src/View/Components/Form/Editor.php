<?php

namespace Nh\LaravelLauncher\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;

class Editor extends Template
{
    /**
     * The toolbar options.
     *
     * @var array
     */
    public $toolbar;

    /**
     * The heading options.
     *
     * @var array
     */
    public $headings;

    /**
     * The divs options.
     *
     * @var array
     */
    public $divs;

    /**
     * The spans options.
     *
     * @var array
     */
    public $spans;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $id,
        $label,
        $name,
        $value = null,
        $description = null,
        $info = null,
        $required = false,
        $disabled = false,
        $toolbar = null,
        $headings = null,
        $divs = null,
        $spans = null
    ) {

        // Run the parent in Template
        parent::__construct($id, $label, $name, $value, $description, $info, $required, $disabled);

        // Set the additionnal attributes
        $this->toolbar = is_null($toolbar) ? config('laravel-launcher.editor.toolbar') : $toolbar;
        $this->headings = is_null($headings) ? config('laravel-launcher.editor.headings') : $headings;
        $this->divs = is_null($divs) ? config('laravel-launcher.editor.divs') : $divs;
        $this->spans = is_null($spans) ? config('laravel-launcher.editor.spans') : $spans;

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.form.template', ['field' => 'editor']);
    }
}
