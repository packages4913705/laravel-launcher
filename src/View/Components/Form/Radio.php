<?php

namespace Nh\LaravelLauncher\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;

class Radio extends Template
{
    /**
     * The default options of the field.
     *
     * @var array
     */
    public $default;

    /**
     * Create a new component instance.
     */
    public function __construct(
        $id,
        $label,
        $name,
        $value = null,
        $description = null,
        $info = null,
        $required = false,
        $disabled = false,
        $default = []
    ) {

        // Run the parent in Template
        parent::__construct($id, $label, $name, $value, $description, $info, $required, $disabled, true);

        // Set the additionnal attributes
        $this->default = $default;

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('laravel-launcher::components.form.template', ['field' => 'radio']);
    }
}
