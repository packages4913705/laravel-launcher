<?php

namespace Nh\LaravelLauncher\View\Composers;

use Illuminate\View\View;
use Nh\LaravelLauncher\Models\Permission;

class SearchPermissionComposer
{
    /**
     * Array of actions.
     *
     * @var array
     */
    protected $actions = [];

    /**
     * Array of models.
     *
     * @var array
     */
    protected $models = [];

    /**
     * Create a menu composer.
     */
    public function __construct()
    {

        // Get the list of all actions
        $actions = Permission::select('action')->whereNotNull('action')->where('action', '!=', '')->distinct()->get()->pluck('action');
        $this->actions['empty'] = '- '.trans_choice('laravel-launcher::field.empty', 1).' -';
        foreach ($actions as $action) {
            $this->actions[$action] = translate($action, 'action');
        }

        // Get the list of all models
        $models = Permission::select('model')->whereNotNull('model')->where('model', '!=', '')->distinct()->get()->pluck('model');
        $this->models['empty'] = '- '.trans_choice('laravel-launcher::field.empty', 1).' -';
        foreach ($models as $model) {
            $this->models[$model] = translate($model, 'model');
        }

    }

    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        $view->with(['actions' => $this->actions, 'models' => $this->models]);
    }
}
