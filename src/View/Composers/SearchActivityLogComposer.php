<?php

namespace Nh\LaravelLauncher\View\Composers;

use Illuminate\View\View;
use Nh\LaravelLauncher\Models\ActivityLog;

class SearchActivityLogComposer
{
    /**
     * Array of events.
     *
     * @var array
     */
    protected $events = [];

    /**
     * Array of models.
     *
     * @var array
     */
    protected $models = [];

    /**
     * Create a menu composer.
     */
    public function __construct()
    {

        // Get the list of all events
        $events = ActivityLog::select('event')->distinct()->get()->pluck('event');
        $this->events['empty'] = '- '.trans_choice('laravel-launcher::field.empty', 1).' -';
        foreach ($events as $event) {
            $this->events[$event] = translate($event, 'log');
        }

        // Get the list of all models
        $models = ActivityLog::select('loggable_type')->distinct()->get()->pluck('model', 'loggable_type');
        $this->models['empty'] = '- '.trans_choice('laravel-launcher::field.empty', 1).' -';
        foreach ($models as $model) {
            $this->models[$model] = translate($model, 'model');
        }

    }

    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        $view->with(['events' => $this->events, 'models' => $this->models]);
    }
}
