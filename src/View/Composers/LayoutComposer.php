<?php

namespace Nh\LaravelLauncher\View\Composers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\View\View;

class LayoutComposer
{
    /**
     * Array of crumbs.
     *
     * @var array
     */
    protected $crumbs = [];

    /**
     * Name of the model.
     *
     * @var ?string
     */
    protected $model = null;

    /**
     * Name of the action.
     *
     * @var ?string
     */
    protected $action = null;

    /**
     * Name of the item.
     *
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $item = null;

    /**
     * Array of values to display the <title>, the <h1> and the <h2> of the page.
     *
     * @var array
     */
    protected $layout = null;

    /**
     * Create a menu composer.
     */
    public function __construct()
    {

        // Get the current route name
        $routeName = Route::currentRouteName();

        // Explode the route to get the values
        // -> eg.: backend.pages.show
        $explode = explode('.', $routeName);

        // $explode[0] == 'backend'

        // $explode[1] == model
        $this->model = Str::singular($explode[1]) ?? null;

        // $explode[2] == action
        $this->action = $explode[2] ?? null;

        // Route parameters == item
        $this->item = Route::current()->parameters[$this->model] ?? null;

        // Define the attribute $layout
        $this->defineLayout();

        // Define the crumbs
        $this->defineCrumbs();

    }

    /**
     * Define the layout properties
     * -> <title> the title of the page
     * -> <h1> the model
     * -> <h2> the action or the name of the item
     */
    private function defineLayout(): void
    {

        // Get the model to define the <span> in <h1>
        if ($this->model) {
            $this->layout['model'] = translate(Str::pluralStudly($this->model), 'model');
        }

        // Get the action to define the <h1>
        if ($this->action) {
            $this->layout['h1'] = translate($this->action, 'action');
        }

        // But if there is an item and it's the show page, use the item to define the <h1>
        if ($this->item && $this->action == 'show') {
            $this->layout['h1'] = $this->item->label ?? $this->item->name ?? $this->item->id;
        }

        // Start the <title> with website name and backend
        $this->layout['title'] = config('app.name').' : '.trans('laravel-launcher::backend.admin');

        // Add the <h1> to the <title>
        if (isset($this->layout['model'])) {
            $this->layout['title'] .= ' > '.$this->layout['model'];
        }

        // Add the <h2> to the <title>
        if (isset($this->layout['h1'])) {
            $this->layout['title'] .= ' > '.$this->layout['h1'];
        }

    }

    /**
     * Define the breadcrumbs
     * -> Backend/Dashboard
     * -> Model/Index
     * -> Item/Show
     * -> Action
     */
    private function defineCrumbs(): void
    {

        // Go to dashboard
        $this->crumbs[] = [
            'label' => trans('laravel-launcher::backend.admin'),
            'url' => route('backend.dashboard'),
        ];

        // Go to model list
        if ($this->model) {
            $routePath = 'backend.'.Str::pluralStudly($this->model).'.index';
            $this->crumbs[] = [
                'label' => translate(Str::pluralStudly($this->model), ['model', 'backend.sidebar']),
                'url' => Route::has($routePath) ? route($routePath) : null,
            ];
        }

        if ($this->item) {
            $this->crumbs[] = [
                'label' => $this->item->label ?? $this->item->name ?? $this->item->id,
                'url' => route('backend.'.Str::pluralStudly($this->model).'.show', $this->item->id) ?? null,
            ];
        }

        if ($this->action) {
            $this->crumbs[] = [
                'label' => translate($this->action, 'action'),
                'url' => null,
            ];
        }

    }

    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        $view->with(['crumbs' => $this->crumbs, 'layout' => $this->layout]);
    }
}
