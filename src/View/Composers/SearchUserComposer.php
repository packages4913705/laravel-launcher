<?php

namespace Nh\LaravelLauncher\View\Composers;

use Illuminate\View\View;
use Nh\LaravelLauncher\Models\Role;

class SearchUserComposer
{
    /**
     * Array of roles.
     *
     * @var array
     */
    protected $roles = [];

    /**
     * Create a menu composer.
     */
    public function __construct()
    {
        $this->roles = Role::select('id', 'label')->get()->pluck('label', 'id');
    }

    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        $view->with(['roles' => $this->roles]);
    }
}
