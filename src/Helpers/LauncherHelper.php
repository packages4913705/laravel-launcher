<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * Return the correct model name.
 * -> FooBar => foo-bars
 */
function getPluralModelName(mixed $key): string
{
    return Str::pluralStudly(Str::kebab(class_basename($key)));
}

/**
 * Return the correct model name.
 * -> FooBar => foo-bar
 */
function getSingleModelName(mixed $key): string
{
    return Str::kebab(class_basename($key));
}

/**
 * Return a clean array
 * -> [0 => null, 1 => [0 => 1, 1 => null], 2 => 2] => [1 => [0 => 1], 2 => 2]
 */
function cleanArray(array $array): array
{
    return array_filter(array_map(function ($value) {
        if (is_array($value)) {
            return cleanArray($value);
        }

        return $value !== null ? $value : null;
    }, $array));
}

/**
 * Delete all files into the storage by his filename
 */
function deleteFilesInStorage(string $filename)
{
    foreach (Storage::allFiles() as $file) {
        if (basename($file) == $filename) {
            Storage::delete($file);
        }
    }
}

/**
 * Rename all files into the storage
 */
function renameFilesInStorage(string $oldname, string $newname)
{
    foreach (Storage::allFiles() as $file) {
        if (basename($file) == $oldname) {
            $newfile = str_replace($oldname, $newname, $file);
            Storage::move($file, $newfile);
        }
    }
}
