<?php

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

/**
 * Return a translation, and check between multiple files.
 */
function translate(string $key, string|array $files, string $package = 'laravel-launcher::'): string
{
    // If the first letter of the key is uppercase, no need to translate.
    if (ctype_upper($key[0])) {
        return $key;
    }

    // By default the translation is null
    $translation = null;

    // Determine if the word must be plural.
    $isPlural = Str::pluralStudly($key) === $key;

    // Transform the $key into a singular kebab case.
    $key = Str::replace('_', '-', $key);
    $word = $key == 'media' ? 'media' : Str::singular(Str::kebab($key));

    // Transform the files into array and make a foreach
    foreach ((array) $files as $file) {
        $keyA = $file.'.'.$word;
        $keyB = $package.$file.'.'.$word;

        if (Lang::has($keyA)) {
            $translation = $keyA;
        } elseif (Lang::has($keyB)) {
            $translation = $keyB;
        }

        if (! is_null($translation)) {
            break;
        }
    }

    return is_null($translation) ? $key : trans_choice($translation, $isPlural ? 2 : 1);
}
