<?php

namespace Nh\LaravelLauncher\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class WithoutHtml implements ValidationRule
{
    /**
     * Check that some string as no HTML.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        /*
        This regular expression is designed to capture a complete HTML or XML tag, whether it is: Opening, Closing or Self-closing.
        It also recognizes attributes inside tags, whether with or without values ​​surrounded by single or double quotes.
        */

        if (preg_match('/<\/?[\w-]+(?:\s+[\w-]+(?:\s*=\s*(?:"[^"]*"|\'[^\']*\'|[^\'">\s]+))?)*\s*\/?>/', $value)) {
            $fail('validation.without-html')->translate();
        }

    }
}
