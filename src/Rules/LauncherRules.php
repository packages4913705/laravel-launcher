<?php

namespace Nh\LaravelLauncher\Rules;

use Illuminate\Validation\Rule;

class LauncherRules
{
    /**
     * Define somes rules to validate the relationship.
     */
    public static function rules(string $model): array
    {

        // Define an array
        $rules = [];

        // Categories
        $categorizable = config('laravel-launcher.'.$model.'.categorizable');
        if ($categorizable && is_numeric($categorizable)) {
            $rules['categories'] = ['array', 'max:'.$categorizable];
        }

        // Media
        $mediable = config('laravel-launcher.'.$model.'.mediable');
        if ($mediable) {

            // Define the default file rules
            $rules['media_to_add.*.*.file'] = ['file', 'max:16000'];

            // Foreach types of media define it's own rules
            foreach ($mediable as $type => $options) {

                if (isset($options['min']) || isset($options['max'])) {
                    $array = ['array'];

                    if (isset($options['min'])) {
                        $array[] = 'min:'.$options['min'];
                    }

                    if (isset($options['max'])) {
                        $array[] = 'max:'.$options['max'];
                    }

                    $rules['media_to_add.'.$type] = $array;
                }

                $rules['media_to_add.'.$type.'.*.file'] = ['mimes:'.(isset($options['format']) ? preg_replace('/\s+/', '', $options['format']) : 'jpg,png,svg,pdf,doc,docx,xls,xlsx,csv,ics')];

            }

        }

        // Roles
        $roleable = config('laravel-launcher.'.$model.'.roleable');
        if ($roleable && is_numeric($roleable)) {
            $rules['roles'] = ['array', 'max:'.$roleable];
        }

        // Types
        $typeable = config('laravel-launcher.'.$model.'.typeable');
        if ($typeable && count($typeable)) {
            $rules['type'] = ['nullable', Rule::in($typeable)];
        }

        // Colors
        $colorable = config('laravel-launcher.'.$model.'.colorable');
        if ($colorable && count($colorable)) {
            $rules['color'] = ['nullable', Rule::in($colorable)];
        }

        // Return the array
        return $rules;

    }
}
