<?php

namespace Nh\LaravelLauncher\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Str;

class Slug implements ValidationRule
{
    /**
     * Check if a string is as a slug.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        if (Str::slug($value) !== $value) {
            $fail('validation.slug')->translate();
        }

    }
}
