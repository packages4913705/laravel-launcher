<?php

namespace Nh\LaravelLauncher\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class WithoutUrl implements ValidationRule
{
    /**
     * Check that a string as no url.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        if (preg_match('#\b((https?|ftp|file):\/\/[^\s@]+(?:\.[^\s@]+)+[^\s]*)#', $value)) {
            $fail('validation.without-url')->translate();
        }

    }
}
