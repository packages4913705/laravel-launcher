<?php

namespace Nh\LaravelLauncher\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class Phone implements ValidationRule
{
    /**
     * Check the phone number (Swiss).
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        /*
        This regular expression checks if a string possibly starts with a +,
        followed by at least 9 characters that are numbers (0-9), hyphens (-), or spaces.
        */

        if (! preg_match("/^\+?([0-9\-\s]){9,}$/", $value)) {
            $fail('validation.phone')->translate();
        }

    }
}
