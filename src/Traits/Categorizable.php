<?php

namespace Nh\LaravelLauncher\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Nh\LaravelLauncher\Models\Category;
use Nh\LaravelLauncher\Observers\CategorizableObserver;

trait Categorizable
{
    /**
     * Bootstrap any application services.
     */
    protected static function bootCategorizable(): void
    {
        static::observe(CategorizableObserver::class);
    }

    /**
     * Get all of the categories for the model.
     */
    public function categories(): MorphToMany
    {
        return $this->morphToMany(Category::class, 'categorizable');
    }

    /**
     * Get the first category of the model.
     */
    public function category(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->categories->first()
        );
    }

    /**
     * Check if the model has any categories.
     */
    public function hasCategories(mixed $categories = null, string $column = 'name', bool $strict = false): bool
    {

        // Check if there is ANY categories
        if (is_null($categories)) {
            return $this->categories()->exists();
        }

        // Count how many category matches
        $count = $this->categories->whereIn($column, (array) $categories)->count();

        // Return the count
        return $strict ? $count === count((array) $categories) : $count;

    }

    /**
     * Scope model that have some specific categories.
     */
    public function scopeByCategories(Builder $query, mixed $values = null, mixed $columns = ['id', 'name', 'label']): void
    {
        $query->whereHas('categories', function ($q) use ($values, $columns) {
            $q->where(function ($subquery) use ($values, $columns) {
                foreach ((array) $columns as $column) {
                    if (is_array($values)) {
                        $subquery->orWhereIn($column, (array) $values);
                    } else {
                        $subquery->orWhere($column, 'LIKE', '%'.$values.'%');
                    }
                }
            });
        });
    }
}
