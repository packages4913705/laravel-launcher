<?php

namespace Nh\LaravelLauncher\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Colorable
{
    /**
     * Scope model by colors.
     */
    public function scopeByColors(Builder $query, mixed $colors = null): void
    {
        $query->whereIn('color', (array) $colors);
    }
}
