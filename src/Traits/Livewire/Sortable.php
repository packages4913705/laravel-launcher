<?php

namespace Nh\LaravelLauncher\Traits\Livewire;

trait Sortable
{
    /**
     * Have the items some position in Database.
     *
     * @var bool
     */
    public $hasPosition = false;

    /**
     * Are the items draggable.
     *
     * @var bool
     */
    public $draggable = false;

    /**
     * Move an item up.
     */
    public function goUp(int $index)
    {
        if ($index > 0) {

            // Swap between the current item and the previous one
            $this->swapItems($index, $index - 1);

            // Save in DB
            $this->updatePosition($this->getItemIds());

        }
    }

    /**
     * Move an item down.
     */
    public function goDown(int $index)
    {
        if ($index < count($this->items) - 1) {

            // Swap between the current item and the next one
            $this->swapItems($index, $index + 1);

            // Save in DB
            $this->updatePosition($this->getItemIds());

        }
    }

    /**
     * Swap between to items.
     */
    private function swapItems($one, $two)
    {
        $item1 = $this->items[$one];
        $item2 = $this->items[$two];
        $this->items[$one] = $item2;
        $this->items[$two] = $item1;
    }

    /**
     * Get the ids as array.
     */
    private function getItemIds()
    {
        return $this->items->pluck('id')->toArray();
    }

    /**
     * Update the position of all items.
     * -> The $ids is the list of ids in the new order.
     */
    public function updatePosition(array $ids, bool $byJS = false)
    {

        if ($this->hasPosition && ! empty($ids)) {

            foreach ($ids as $position => $id) {
                $this->items->find($id)->update(['position' => $position]);
            }

            if ($byJS) {
                $this->items = $this->items->sortBy('position');
            }

            $this->dispatch('position-updated', trans('laravel-launcher::notification.success.sorted'));

        }

    }
}
