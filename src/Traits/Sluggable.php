<?php

namespace Nh\LaravelLauncher\Traits;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Str;

trait Sluggable
{
    /**
     * Set the slug by label/title/name if no specific slug request.
     * -> Use for url
     */
    public function slug(): Attribute
    {
        return Attribute::make(
            set: fn (mixed $value) => is_null($value) ? Str::slug(request()->label ?? request()->title ?? request()->name) : $value
        );
    }

    /**
     * Get all the slugs of the model in case of multilevel.
     * -> Will return an array
     */
    public function slugs(): Attribute
    {
        $slugs = [$this->slug];

        if (in_array('Nh\LaravelLauncher\Traits\Levelable', class_uses($this)) && $this->parent) {
            $parent = $this->parent;

            while ($parent) {
                array_unshift($slugs, $parent->slug);
                $parent = $parent->parent;
            }
        }

        return Attribute::make(
            get: fn () => $slugs
        );
    }
}
