<?php

namespace Nh\LaravelLauncher\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Nh\LaravelLauncher\Models\Media;
use Nh\LaravelLauncher\Observers\MediableObserver;

trait Mediable
{
    /**
     * Bootstrap any application services.
     */
    protected static function bootMediable(): void
    {
        static::observe(MediableObserver::class);
    }

    /**
     * Get all of the media for the model.
     */
    public function media(): MorphMany
    {
        return $this->morphMany(Media::class, 'mediable')->orderBy('position', 'asc');
    }

    /**
     * Check if the model has any media.
     */
    public function hasMedia(mixed $media = null, string $column = 'type', bool $strict = false): bool
    {

        // Check if there is ANY categories
        if (is_null($media)) {
            return $this->media()->exists();
        }

        // Count how many media matches
        $count = $this->media->whereIn($column, (array) $media)->count();

        // Return the count
        return $strict ? $count === count((array) $media) : $count;

    }

    /**
     * Get the media by types.
     */
    public function mediaByTypes(mixed $types = null): Collection
    {
        return $this->media->whereIn('type', (array) $types);
    }

    /**
     * Get the media by formats.
     */
    public function mediaByFormats(mixed $formats = null): Collection
    {
        return $this->media->whereIn('format', (array) $formats);
    }
}
