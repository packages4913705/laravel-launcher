<?php

namespace Nh\LaravelLauncher\Traits;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Facades\App;
use Nh\LaravelLauncher\Models\Translation;
use Nh\LaravelLauncher\Observers\TranslatableObserver;

trait Translatable
{
    /**
     * Bootstrap any application services.
     */
    protected static function bootTranslatable(): void
    {
        static::observe(TranslatableObserver::class);
    }

    /**
     * Get all of the translations for the model.
     */
    public function translations(): MorphMany
    {
        return $this->morphMany(Translation::class, 'translatable');
    }

    /**
     * Get the translation by lang.
     */
    public function getTranslation(?string $lang = null): ?Translation
    {
        return $this->translations()->firstWhere('lang', $lang);
    }

    /**
     * Get the translation by the current lang.
     */
    public function translation(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->getTranslation(App::getLocale())
        );
    }

    /**
     * Get the translated label.
     */
    public function label(): Attribute
    {
        return Attribute::make(
            get: fn (?string $value) => $this->translation->value['label'] ?? $value
        );
    }

    /**
     * Get the translated title.
     */
    public function title(): Attribute
    {
        return Attribute::make(
            get: fn (?string $value) => $this->translation->value['title'] ?? $value
        );
    }

    /**
     * Get the translated description.
     */
    public function description(): Attribute
    {
        return Attribute::make(
            get: fn (?string $value) => $this->translation->value['description'] ?? $value
        );
    }

    /**
     * Set the translations for a model and save the changes in activity log.
     * -> Because it is use in multiple places as MediableObserver and TranslatableObserver
     */
    public function setTranslations(array $translations): void
    {
        $changes = [];

        foreach (array_filter($translations) as $lang => $values) {

            if (empty($this->getTranslation($lang)) && empty(array_filter($values))) {
                continue;
            }

            $translation = $this->translations()->updateOrCreate([
                'lang' => $lang,
            ], [
                'value' => $values,
            ]);

            // Add changes in an array
            $changes[$lang] = $translation->wasRecentlyCreated ? $translation->getAttributes() : $translation->getChanges();

        }

        // Add the activity log
        if (in_array('Nh\LaravelLauncher\Traits\Loggable', class_uses($this)) && ! empty(array_filter($changes))) {
            $this->addLog('synchronized', ['translations' => array_filter($changes)]);
        }

    }
}
