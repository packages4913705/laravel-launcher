<?php

namespace Nh\LaravelLauncher\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection as SupportCollection;
use Nh\LaravelLauncher\Observers\LevelableObserver;

trait Levelable
{
    /**
     * Bootstrap any application services.
     */
    protected static function bootLevelable(): void
    {
        static::observe(LevelableObserver::class);
    }

    /**
     * The parent that belong to the model.
     */
    public function parent(): ?BelongsTo
    {
        return $this->belongsTo(get_class($this), 'parent_id');
    }

    /**
     * The children that belong to the model.
     */
    public function children(): ?HasMany
    {
        return $this->hasMany(get_class($this), 'parent_id');
    }

    /**
     * Get all parents.
     */
    public function getAllParents(): SupportCollection
    {
        $collection = collect();
        $parent = $this->parent;

        while (! is_null($parent)) {
            $collection = $collection->push($parent);
            $parent = $parent->parent;
        }

        return $collection;
    }

    /**
     * Get all children and subchildren.
     */
    public function getAllChildren(): Collection
    {
        $collection = $this->children;

        foreach ($this->children as $child) {
            if ($child->children()->exists()) {
                $collection = $collection->merge($child->getAllChildren());
            }
        }

        return $collection;
    }

    /**
     * Get all children and subchildren modelkeys (=id).
     * -> Return an array
     */
    public function childrenKeys(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->getAllChildren()->modelKeys()
        );
    }

    /**
     * Get all parents keys.
     * -> Return an array
     */
    public function parentsKeys(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->getAllParents()->pluck('id')->toArray()
        );
    }

    /**
     * Get the hierarchy as string.
     */
    public function hierarchy(): Attribute
    {
        $string = '';

        foreach ($this->getAllParents()->reverse() as $parent) {
            $string .= ($parent->label ?? $parent->title ?? $parent->name).' > ';
        }

        $string .= $this->label ?? $this->title ?? $this->name;

        return Attribute::make(
            get: fn () => $string
        );
    }

    /**
     * Scope model to get all that are able to be a parent.
     */
    public function scopeAvailableLevels(Builder $query, mixed $level): void
    {
        $query->where('level', '<=', is_numeric($level) ? $level - 1 : 1000)->orderBy('parent_id')->orderBy('name');
    }

    /**
     * Return a list of values from children.
     * -> For get the list of ids related to use in aria-owns
     */
    public function getChildrenString(?string $prepend = null, ?string $column = 'id'): string
    {
        if (! $this->children->count()) {
            return '';
        }

        return $this->children()->pluck($column)->map(function ($x) use ($prepend) {
            return $prepend.$x;
        })->implode(' ');
    }
}
