<?php

namespace Nh\LaravelLauncher\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Typeable
{
    /**
     * Scope model by types.
     */
    public function scopeByTypes(Builder $query, mixed $types = null, bool $nullable = false): void
    {
        $query->whereIn('type', (array) $types);

        if ($nullable) {
            $query->orWhereNull('type');
        }
    }
}
