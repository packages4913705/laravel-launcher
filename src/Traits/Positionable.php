<?php

namespace Nh\LaravelLauncher\Traits;

use Illuminate\Database\Eloquent\Builder;
use Nh\LaravelLauncher\Observers\PositionableObserver;

trait Positionable
{
    /**
     * Bootstrap any application services.
     */
    protected static function bootPositionable(): void
    {
        static::observe(PositionableObserver::class);
    }

    /**
     * Scope model ordered by position.
     */
    public function scopeByPosition(Builder $query, string $direction = 'asc'): void
    {
        $query->orderBy('position', $direction);
    }
}
