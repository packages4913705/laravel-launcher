<?php

namespace Nh\LaravelLauncher\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Facades\Auth;
use Nh\LaravelLauncher\Models\Permission;
use Nh\LaravelLauncher\Models\Role;
use Nh\LaravelLauncher\Models\User;
use Nh\LaravelLauncher\Observers\RoleableObserver;

trait Roleable
{
    /**
     * Bootstrap any application services.
     */
    protected static function bootRoleable(): void
    {
        static::observe(RoleableObserver::class);
    }

    /**
     * Get all of the roles for the model.
     */
    public function roles(): MorphToMany
    {
        return $this->morphToMany(Role::class, 'roleable');
    }

    /**
     * Get the first role of the model.
     */
    public function role(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->roles->first()
        );
    }

    /**
     * Check if the model has any roles.
     */
    public function hasRoles(mixed $roles = null, string $column = 'name', bool $strict = false): bool
    {

        // Check if there is ANY roles
        if (is_null($roles)) {
            return $this->roles()->exists();
        }

        // Count how many role matches
        $count = $this->roles->whereIn($column, (array) $roles)->count();

        // Return the count
        return $strict ? $count === count((array) $roles) : $count;

    }

    /**
     * Check if the model has any permissions.
     * ! Only for user
     */
    public function hasPermissions(mixed $permissions, string $column = 'name', bool $strict = false): bool
    {

        // If not an User or have no roles, return false
        if (! ($this  instanceof User) || ! $this->roles()->exists()) {
            return false;
        }

        // Count how many permissions matches
        $count = $this->roles()->whereHas('permissions', function (Builder $query) use ($permissions, $column) {
            $query->whereIn($column, (array) $permissions);
        })->count();

        // Return the count
        return $strict ? $count === count((array) $permissions) : $count;
    }

    /**
     * Check if model has access to ALL model/action permission.
     * ! Only for user
     */
    public function hasAccess(string $model, mixed $actions = null, bool $strict = false): bool
    {
        // If not an User or have no roles, return false
        if (! ($this  instanceof User) || ! $this->roles()->exists()) {
            return false;
        }

        // If no actions is passed, get an array of all permissions for the model
        if (is_null($actions)) {
            $actions = Permission::select('action')->where('model', $model)->pluck('action')->toArray();
        }

        // Count how many permissions matches
        $count = $this->roles()->whereHas('permissions', function (Builder $query) use ($model, $actions) {
            $query->where('model', $model)->whereIn('action', (array) $actions);
        })->count();

        // Return the count
        return $strict ? $count === count((array) $actions) : $count;

    }

    /**
     * Scope model that have some specific categories.
     */
    public function scopeByRoles(Builder $query, mixed $values = null, mixed $columns = ['id', 'name', 'label']): void
    {
        $query->whereHas('roles', function ($q) use ($values, $columns) {
            $q->where(function ($subquery) use ($values, $columns) {
                foreach ((array) $columns as $column) {
                    if (is_array($values)) {
                        $subquery->orWhereIn($column, (array) $values);
                    } else {
                        $subquery->orWhere($column, 'LIKE', '%'.$values.'%');
                    }
                }
            });
        });
    }

    /**
     * Scope model that are accessible by an Auth user.
     * ! NOT for user
     */
    public function scopeAuthorized(Builder $query): void
    {
        if (! ($this  instanceof User) && Auth::check() && ! Auth::user()->has_superpowers) {
            $query->doesntHave('roles')->orWhereHas('roles', function (Builder $q) {
                $q->whereIn('role_id', Auth::user()->roles->modelKeys());
            });
        }
    }
}
