<?php

namespace Nh\LaravelLauncher\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Facades\Auth;
use Nh\LaravelLauncher\Models\ActivityLog;
use Nh\LaravelLauncher\Observers\LoggableObserver;

trait Loggable
{
    /**
     * Bootstrap any application services.
     */
    protected static function bootLoggable(): void
    {
        static::observe(LoggableObserver::class);
    }

    /**
     * Get all of the activity logs for the model.
     */
    public function activity_logs(): MorphMany
    {
        return $this->morphMany(ActivityLog::class, 'loggable')->latest()->orderBy('id', 'desc');
    }

    /**
     * Get the last activity log for the model.
     */
    public function activity_log(): MorphOne
    {
        return $this->morphOne(ActivityLog::class, 'loggable')->latestOfMany();
    }

    /**
     * Add an activity log with event, json value and a comment.
     */
    public function addLog(string $event, ?array $value = null, ?string $comment = null): void
    {

        $log = new ActivityLog;

        if (! Auth::check()) {
            $comment = '[system]'.(! is_null($comment) ? ' - '.$comment : '');
        }

        $log->fill([
            'event' => $event,
            'value' => empty($value) ? $this->getDirty() : $value,
            'comment' => $comment,
        ]);

        if (Auth::check()) {
            $log->user()->associate(Auth::user());
        }

        $this->activity_logs()->save($log);

    }
}
