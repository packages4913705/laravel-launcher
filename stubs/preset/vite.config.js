import { defineConfig } from 'vite'
import laravel from 'laravel-vite-plugin'
import { resolve } from 'path'

export default defineConfig({
    resolve: {
        alias: {
            '@natachah': resolve(__dirname, 'node_modules/@natachah'),
            '@tiptap': resolve(__dirname, 'node_modules/@tiptap'),
        },
    },
    plugins: [
        laravel({
            input: ['resources/scss/app.scss', 'resources/js/app.js', 'resources/scss/backend.scss', 'resources/js/backend.js', 'resources/scss/auth.scss', 'resources/js/auth.js'],
            refresh: true,
        }),
    ]
})
