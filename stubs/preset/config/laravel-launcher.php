<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Localization
    |--------------------------------------------------------------------------
    |
    | This value determine if the website is multilanguage and which languages are supported.
    |
    */

    'languages' => ['fr', 'en', 'de', 'it'],

    /*
    |--------------------------------------------------------------------------
    | Format
    |--------------------------------------------------------------------------
    |
    | This value determine the date format.
    |
    */

    'formats' => [
        'datetime' => 'd/m/Y H:i:s',
        'date' => 'd/m/Y',
    ],

    /*
    |--------------------------------------------------------------------------
    | Editor
    |--------------------------------------------------------------------------
    |
    | This value determine the default Tiptap editor settings.
    |
    */

    'editor' => [
        'toolbar' => ['typography', 'div', 'span', 'table', 'blockquote', 'list', 'format', 'abbr', 'link', 'align'],
        'headings' => [2, 3, 4, 5, 6],
        'divs' => [],
        'spans' => [],
    ],

    /*
    |--------------------------------------------------------------------------
    | Settings
    |--------------------------------------------------------------------------
    |
    | This value determine if the website have some default settings.
    |
    */

    'settings' => [
        'seo' => true,
        'social-network' => true,
        'contact' => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | This value determine the sidebar navigation per module.
    | NOTE: The dashboard, profile, settings, history, permissions, roles and users are by default
    |
    */

    'sidebar' => [
        'settings' => [
            'media',
            'category',
        ],
        'content' => [
            'page',
            // !NAME_PLACEHOLDER
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Settings per model
    |--------------------------------------------------------------------------
    |
    | This values determine the configuration by model.
    |
    | categorizable => boolean or int of maximum categories
    | levelable => boolean or int of maximum level depth
    | roleable => boolean or int of maximum roles
    | mediable => array with settings per types
    | translatable => boolean
    | typeable => array of available types
    | colorable => array of available colors
    |
    | protected => array of protected name (can't be deleted exept by a superadmin)
    |
    */

    'role' => [
        'translatable' => false,
        'protected' => ['superadmin', 'admin'],
        'administrator' => ['admin'], // Admin that can set all other roles (exept superadmin)
    ],

    'permission' => [
        'translatable' => false,
        'protected' => ['superadmin', 'admin'],
        'actions' => ['view', 'create', 'update', 'delete', 'restore', 'destroy', 'activity'], // Default actions to create the permissions
    ],

    'user' => [
        'roleable' => 1,
        'mediable' => false,
    ],

    'category' => [
        'levelable' => false,
        'roleable' => false,
        'translatable' => false,
        'mediable' => false,
        'typeable' => [],
        'colorable' => [],
        'protected' => [],
    ],

    'page' => [
        'categorizable' => false,
        'levelable' => false,
        'roleable' => false,
        'translatable' => false,
        'mediable' => [
            'picture' => [
                'format' => 'jpg,png',
                'size' => '1800 x 800',
                'sizes' => [
                    'fit' => [],
                    'height' => [800],
                    'width' => [1800],
                ],
            ],
        ],
        'typeable' => [],
        'colorable' => [],
        'protected' => ['index'],
    ],

    // !PLACEHOLDER

];
