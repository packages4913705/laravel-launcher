<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Rules\Password;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

        // Adapt string length in DB for Mysql lower than v5.7.7
        Schema::defaultStringLength(255);

        // Default rule for the passwors
        Password::defaults(function () {
            return Password::min(6)
                // ->letters()
                ->numbers();
            // ->mixedCase()
            // ->uncompromised();
        });

        // Add relation mapping to save in DB.
        // -> e.g: "Nh\LaravelLauncher\Models\Page" will be transform into "page"
        Relation::enforceMorphMap([
            'activity-log' => 'Nh\LaravelLauncher\Models\ActivityLog',
            'category' => 'Nh\LaravelLauncher\Models\Category',
            'media' => 'Nh\LaravelLauncher\Models\Media',
            'page' => 'Nh\LaravelLauncher\Models\Page',
            'permission' => 'Nh\LaravelLauncher\Models\Permission',
            'role' => 'Nh\LaravelLauncher\Models\Role',
            'setting' => 'Nh\LaravelLauncher\Models\Setting',
            'translation' => 'Nh\LaravelLauncher\Models\Translation',
            'user' => 'Nh\LaravelLauncher\Models\User',
            // !PLACEHOLDER
        ]);

    }
}
