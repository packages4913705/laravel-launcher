<?php

namespace App\View\Composers;

use Illuminate\Support\Facades\Schema;
use Illuminate\View\View;
use Nh\LaravelLauncher\Models\Page;
use Nh\LaravelLauncher\Models\Setting;

class LayoutComposer
{
    /**
     * Array of header links.
     *
     * @var array
     */
    protected $headerLinks = [];

    /**
     * Array of footer links.
     *
     * @var array
     */
    protected $footerLinks = [];

    /**
     * Array of SEO information.
     *
     * @var array
     */
    protected $seo = [];

    /**
     * Create a menu composer.
     */
    public function __construct()
    {
        if (Schema::hasTable('pages')) {
            $this->headerLinks = Page::with('subpages')->inMenu()->where('level', 0)->byPosition()->get();
            $this->footerLinks = Page::inFooter()->byPosition()->get();
        }

        if (Schema::hasTable('settings')) {
            $this->seo = Setting::firstWhere('name', 'seo');
        }
    }

    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        $view->with(['headerLinks' => $this->headerLinks, 'footerLinks' => $this->footerLinks, 'seo' => $this->seo]);
    }
}
