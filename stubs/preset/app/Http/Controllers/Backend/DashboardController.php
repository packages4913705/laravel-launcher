<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Nh\LaravelLauncher\Models\ActivityLog;
use Nh\LaravelLauncher\Models\Category;
use Nh\LaravelLauncher\Models\Page;
use Nh\LaravelLauncher\Models\User;

class DashboardController extends Controller
{
    public function __invoke(): View
    {
        // Get the last activity log
        $logs = ActivityLog::latest('id')->take(5)->get();

        // Define the shortcuts
        $users = User::get()->count();
        $shortcuts['user'] = [
            'icon' => 'people',
            'count' => $users,
            'label' => trans_choice('laravel-launcher::model.user', $users),
            'link' => Auth::user()->hasAccess('user') ? route('backend.users.index') : null,
        ];

        $categories = Category::get()->count();
        $shortcuts['category'] = [
            'icon' => 'tags',
            'count' => $categories,
            'label' => trans_choice('laravel-launcher::model.category', $categories),
            'link' => Auth::user()->hasAccess('category') ? route('backend.categories.index') : null,
        ];

        $pages = Page::get()->count();
        $shortcuts['page'] = [
            'icon' => 'body-text',
            'count' => $pages,
            'label' => trans_choice('laravel-launcher::model.page', $pages),
            'link' => Auth::user()->hasAccess('page') ? route('backend.pages.index') : null,
        ];

        return view('backend.dashboard', compact('logs', 'shortcuts'));
    }
}
