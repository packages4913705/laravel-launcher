<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Nh\LaravelLauncher\Models\Page;

class PageController extends Controller
{
    /**
     * Array of compact to pass to the view
     *
     * @var array
     */
    private $compact;

    /**
     * View to render
     *
     * @var string
     */
    private $view = 'page';

    /**
     * Return a page
     */
    public function __invoke(string $slugs): RedirectResponse|View
    {

        $slugArray = explode('/', $slugs);

        // If Homepage redirect
        if (reset($slugArray) == 'index') {
            return redirect()->route('home');
        }

        // Get the page from last slug
        $page = Page::firstWhere('slug', end($slugArray)) ?? null;

        // If not exist return 404
        if (! $page) {
            return abort(404);
        }

        // If not Published return 403
        if (! $page->is_published) {
            return abort(403);
        }

        // Get the page
        $this->compact['page'] = $page;

        /*
        * You can customize the compact per slug or name like this:
        switch ($page->name) {
            case 'aaa':
                $this->compact['subpage'] = ...;
                break;
            case 'bbb':
                $this->compact['subpage'] = ...;
                break;
        }
        */

        // Return the page !
        return view($this->view, $this->compact);

    }
}
