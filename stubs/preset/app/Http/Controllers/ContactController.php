<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Nh\LaravelLauncher\Models\Setting;

class ContactController extends Controller
{
    /**
     * Get the Email
     */
    public function email(): RedirectResponse
    {
        $contact = Setting::firstWhere('name', 'contact');
        $sendTo = $contact ? $contact->value['email'] : '';

        return redirect()->to('mailto:'.$sendTo);
    }

    /**
     * Get the Phone
     */
    public function phone(): RedirectResponse
    {
        $contact = Setting::firstWhere('name', 'contact');
        $sendTo = $contact ? $contact->value['phone'] : '';

        return redirect()->to('tel:'.$sendTo);
    }
}
