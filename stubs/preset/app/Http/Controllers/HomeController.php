<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Nh\LaravelLauncher\Models\Page;

class HomeController extends Controller
{
    public function __invoke(): View
    {
        $page = Page::firstWhere('name', 'index');

        return view('home', compact('page'));
    }
}
