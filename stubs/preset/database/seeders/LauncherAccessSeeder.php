<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Nh\LaravelLauncher\Models\Permission;
use Nh\LaravelLauncher\Models\Role;
use Nh\LaravelLauncher\Models\User;

class LauncherAccessSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        $user = User::create([
            'name' => 'Natacha',
            'email' => 'info@natachaherth.ch',
            'password' => '123456',
        ]);

        $superadmin = Role::create([
            'name' => 'superadmin',
            'label' => 'Superadmin',
        ]);

        $admin = Role::create([
            'name' => 'admin',
            'label' => 'Administrateur',
        ]);

        $user->roles()->attach($superadmin);

        // Create default permission per Model
        $allActions = config('laravel-launcher.permission.actions');
        $basicActions = array_diff($allActions, ['delete', 'restore']);

        $this->createPermissions(['view', 'update'], 'setting');
        $this->createPermissions(['view'], 'activity-log');
        $this->createPermissions($allActions, 'user');
        $this->createPermissions($basicActions, 'role');
        $this->createPermissions($basicActions, 'permission');
        $this->createPermissions(['view', 'update', 'activity'], 'media');
        $this->createPermissions($allActions, 'category');
        $this->createPermissions($allActions, 'page');
        // !PLACEHOLDER

    }

    /**
     * Create all permissions for model.
     *
     * @param  array  $actions
     * @param  string  $model
     */
    private function createPermissions($actions, $model): void
    {
        foreach ($actions as $action) {
            Permission::create([
                'name' => $model.'-'.$action,
                'label' => $model.' '.$action,
                'model' => $model,
                'action' => $action,
            ]);
        }
    }
}
