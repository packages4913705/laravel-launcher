<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Call the Laravel Launcher package Seeder
        // -> this will create the default permissions, roles and user
        $this->call(LauncherAccessSeeder::class);

        // -> this will create the default pages
        $this->call(LauncherPageSeeder::class);
    }
}
