<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Nh\LaravelLauncher\Models\Page;

class LauncherPageSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        // Create default index page
        Page::create([
            'slug' => 'index',
            'name' => 'index',
            'label' => 'Accueil',
            'title' => 'Bienvenue sur votre nouveau site !',
            'description' => '<p>Ce site est un installation de <b>Laravel Launcher</b> !<br>Accèder à l\'administration afin de modifier le contenu !<p>',
            'published' => 1,
            'in_menu' => 1,
            'in_footer' => 0,
        ]);

        // Create the default rgpd page
        Page::create([
            'slug' => 'politique-de-confidentialite',
            'name' => 'rgpd',
            'label' => 'Politique de confidentialité',
            'title' => 'Politique de confidentialité',
            'description' => "<p>Les cookies sont de petits fichiers texte placés sur votre ordinateur par les sites Internet que vous visitez. Ils sont largement utilisés pour permettre aux sites de fonctionner, ou de fonctionner de manière plus efficace, ainsi que pour fournir des informations aux propriétaires du site.</p><h2>Les cookies suivant sont utilisés sur notre site</h2><ul><li><b>_XXX-consent (session)</b> : Ce cookie permet de savoir si vous avez pris connaissance de l'utilisation des cookies sur notre site.</li><li><b>_XXX-comfort (session)</b> : Ce cookie permet de mémoriser vos préférences d'affichage et d'accessibilité.</li><li><b>_XXX_session (session)</b> : Ce cookie sert à stocker la session de visite. Cela est utile, par exemple, pour accéder à une zone privée du site (connexion à l'administration) et pour d'autres fonctionnalités nécessaires au bon fonctionnement du site.</li><li><b>XSRF-TOKEN (session)</b> : Ce cookie est une fonctionnalité de sécurité propre aux applications web Laravel. Il est utilisé pour protéger contre les attaques de falsification de requêtes inter-sites (CSRF).</li><li><b>_backend-comfort (session)</b> : Ces cookies sont utilisés dans la partie d'administration du site pour mémoriser vos préférences d'affichage.</li></ul><p>Ces cookies sont <b>strictement nécessaires</b> au fonctionnement du site Internet. Ils ne stockent <b>aucune donnée personnelle</b> et ne nécessitent aucune acceptation de l'utilisateur.</p>",
            'published' => 1,
            'in_menu' => 0,
            'in_footer' => 1,
        ]);

    }
}
