<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Comfort Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the comfort settings dialog.
    |
    */

    'accessibility' => 'Outils d\'accessibilité',

    'themes' => 'Thèmes',
    'theme' => [
        'light' => 'Thème clair',
        'dark' => 'Thème sombre',
        'gray' => 'Niveau de gris',
        'contrast' => 'Thème contrasté',
    ],

    'font-sizes' => 'Taille de la police',
    'font' => [
        'default' => 'Normale',
        'small' => 'Petite',
        'mid' => 'Moyenne',
        'large' => 'Grande',
    ],

    'line-height' => 'Espacement entre les lignes',
    'line' => [
        'default' => 'Normal',
        'small' => 'Petit',
        'mid' => 'Moyen',
        'large' => 'Grand',
    ],

    'font-family' => 'Police de caractère',

];
