<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Consent Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the consent dialog.
    |
    */

    'cookies' => 'Cookies',
    'only-necessary' => 'Ce site utilise <strong>uniquement</strong> les cookies nécessaires pour vous offrir la meilleure expérience.',

];
