<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Layout Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the frontend interface.
    |
    */

    'main-menu' => 'Menu principal',
    'website-by' => 'Site créé par',

];
