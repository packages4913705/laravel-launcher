<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Layout Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the frontend interface.
    |
    */

    'main-menu' => 'Main menu',
    'website-by' => 'Website created by',

];
