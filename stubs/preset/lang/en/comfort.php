<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Comfort Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the comfort settings dialog.
    |
    */

    'accessibility' => 'Accessibility tools',

    'themes' => 'Themes',
    'theme' => [
        'light' => 'Light theme',
        'dark' => 'Dark theme',
        'gray' => 'Grayscale theme',
        'contrast' => 'High contrast theme',
    ],

    'font-sizes' => 'Font size',
    'font' => [
        'default' => 'Normal',
        'small' => 'Small',
        'mid' => 'Medium',
        'large' => 'Large',
    ],

    'line-height' => 'Line height',
    'line' => [
        'default' => 'Normal',
        'small' => 'Small',
        'mid' => 'Medium',
        'large' => 'Large',
    ],

    'font-family' => 'Font family',

];
