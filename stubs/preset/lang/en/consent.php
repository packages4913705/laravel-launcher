<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Consent Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for the consent dialog.
    |
    */

    'cookies' => 'Cookies',
    'only-necessary' => 'This website uses <strong>only</strong> the necessary cookies to ensure you get the best experience.',

];
