<footer>
	<p>
		© 2024-{{ now()->year }} {{ config('app.name') }} |
		@if (isset($footerLinks) && count($footerLinks))
			@foreach ($footerLinks as $footerLink)
				<a href="{{ $footerLink->url }}">{{ $footerLink->label }}</a>
			@endforeach
			|
		@endif
		@lang('frontend.website-by') <a href="https://www.natachaherth.ch" target="_blank">Natacha Herth</a>
	</p>
</footer>
