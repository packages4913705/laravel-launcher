<dialog id="comfortDialog" aria-modal="true" role="alertdialog">
	<h1>@lang('comfort.accessibility')</h1>

	<fieldset>
		<legend>@lang('comfort.themes')</legend>
		<button data-theme value="light">
			<x-ll::svg icon="sun" /> @lang('comfort.theme.light')
		</button>
		<button data-theme value="dark">
			<x-ll::svg icon="moon-stars" /> @lang('comfort.theme.dark')
		</button>
	</fieldset>

	<fieldset>
		<legend>@lang('comfort.font-sizes')</legend>
		<button data-style="--font-size" value="14px">
			@lang('comfort.font.small')
		</button>
		<button data-style="--font-size" value="16px">
			@lang('comfort.font.default')
		</button>
		<button data-style="--font-size" value="18px">
			@lang('comfort.font.large')
		</button>
	</fieldset>

	<fieldset>
		<legend>@lang('comfort.font-family')</legend>
		<button data-style="--font-family" value="arial">Arial</button>
		<button data-style="--font-family" value="times">Times</button>
		<button data-style="--font-family" value="Open-Dyslexic">Open Dyslexic</button>
	</fieldset>

	<fieldset>
		<legend>@lang('comfort.line-height')</legend>
		<button data-style="--line-height" value="1.5">
			@lang('comfort.line.default')
		</button>
		<button data-style="--line-height" value="1.75">
			@lang('comfort.line.mid')
		</button>
		<button data-style="--line-height" value="2">
			@lang('comfort.line.large')
		</button>
	</fieldset>

	<footer>
		<button type="reset" id="comfortReset">@lang('laravel-launcher::action.reset')</button>
		<button class="primary" data-dialog-close>@lang('laravel-launcher::action.save')</button>
	</footer>

</dialog>
