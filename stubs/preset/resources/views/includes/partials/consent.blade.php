<dialog id="cookies">
	<h1>@lang('consent.cookies')</h1>
	<p>
		@lang('consent.only-necessary')
		@if (route('page', ['slugs' => 'politique-de-confidentialite']))
			<a tabindex="-1" href="{{ route('page', ['slugs' => 'politique-de-confidentialite']) }}">
				@lang('laravel-launcher::action.learn-more')
			</a>
		@endif
	</p>
	<button tabindex="0" data-dialog-close>Ok</button>
</dialog>
