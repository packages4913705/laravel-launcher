@forelse ($links as $link)
	@if ($link->subpages->count())
		<li>
			<button role="link" aria-controls="submenu{{ $link->id }}" aria-expanded="false" @if ($link->is_current) aria-current="section" @endif @if ($loop->first) tabindex="0" @endif>
				{{ $link->label }}
				<x-ll::svg icon="chevron-down" />
			</button>
			<ul id="submenu{{ $link->id }}" tabindex="0" hidden>
				@include('includes.partials.link', ['links' => $link->subpages])
			</ul>
		</li>
	@else
		<li>
			<a href="{{ $link->url }}" @if ($link->is_current) aria-current="page" @endif @if ($loop->first) tabindex="0" @endif>
				{{ $link->label }}
			</a>
		</li>
	@endif
@empty
	<li><a href="{{ route('backend.dashboard') }}">Backend</a></li>
@endforelse
