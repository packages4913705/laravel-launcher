<header>

	<div>
		<a id="logo" href="{{ route('home') }}">
			LOGO {{ config('app.name') }}
		</a>

		<nav>

			<ul id="menu" class="drawer" tabindex="0" hidden>

				@include('includes.partials.link', ['links' => $headerLinks])

				@if (count(config('laravel-launcher.languages')) > 1)
					<li>
						<button role="link" aria-controls="languages" aria-expanded="false">
							{{ config('language.' . App::getLocale()) }}
							<x-ll::svg icon="chevron-down" />
						</button>
						<ul id="languages" tabindex="0" hidden>
							@foreach (config('laravel-launcher.languages') as $lang)
								<li>
									<a href="{{ route('language.change', $lang) }}" @if (App::getLocale() == $lang) aria-current="true" @endif>
										{{ config('language.' . $lang) }}
									</a>
								</li>
							@endforeach
						</ul>
					</li>
				@endif

			</ul>

			<button aria-controls="comfortDialog" title="@lang('comfort.accessibility')" aria-label="@lang('comfort.accessibility')">
				<x-ll::svg icon="universal-access-circle" />
			</button>

			<button id="toggleMenu" class="drawer-button" aria-expanded="false" aria-pressed="false" aria-controls="menu" aria-label="@lang('laravel-launcher::action.toggle-menu')" title="@lang('laravel-launcher::action.toggle-menu')">
				<svg aria-hidden="true" focusable="false" viewbox="0 0 100 100" width="100%">
					<rect fill="currentColor" width="100" height="10" x="0" y="10" rx="0"></rect>
					<rect fill="currentColor" width="100" height="10" x="0" y="45" rx="0"></rect>
					<rect fill="currentColor" width="100" height="10" x="0" y="80" rx="0"></rect>
				</svg>
			</button>

		</nav>
	</div>

</header>
