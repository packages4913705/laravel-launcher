@extends('laravel-launcher::layouts.backend')

@section('model', trans('laravel-launcher::backend.admin'))
@section('title', trans('laravel-launcher::backend.sidebar.dashboard'))

@section('grid')
	@include('laravel-launcher::includes.cards.logs')
	<x-ll::backend.card icon="bug" :title="trans('laravel-launcher::backend.help.title')">
		<p>@lang('laravel-launcher::backend.help.message')</p>
		<a href="mailto:info@natachaherth.ch?subject=BUG%20{{ config('app.name') }}" class="primary" role="button">@lang('laravel-launcher::action.send-email')</a>
	</x-ll::backend.card>
@endsection

@section('dashboard')
	@foreach ($shortcuts as $key => $shortcut)
		<x-ll::backend.card :icon="$shortcut['icon']">
			<b>{{ $shortcut['count'] }}</b>
			{{ $shortcut['label'] }}
			@if (!empty($shortcut['link']))
				<a href="{{ $shortcut['link'] }}" aria-label="@lang('laravel-launcher::action.skip-to-list')"></a>
			@endif
		</x-ll::backend.card>
	@endforeach
@endsection
