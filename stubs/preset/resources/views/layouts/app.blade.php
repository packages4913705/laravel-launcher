<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		@isset($seo)
			<meta name="description" content="{{ $seo->value['description'] ?? '' }}">
		@endisset

		<link rel="shortcut icon" href="/favicon.ico">

		<title>
			@hasSection('title')
				@yield('title') :
			@endif
			{{ config('app.name') }}
		</title>

		{{-- * Exemple of Google Font connection
        <link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link rel="preload" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap">
         --}}

		{{-- * Add the Open dyslexic font by default
		<link href="https://fonts.cdnfonts.com/css/open-dyslexic" rel="stylesheet">
         --}}

		@vite('resources/js/app.js')
		@vite('resources/scss/app.scss')
	</head>

	<body data-preload>

		<a class="skip" href="#content">@lang('laravel-launcher::action.skip-to-content')</a>

		<div id="backdrop"></div>

		@include('includes.partials.header')

		<main id="content">
			@yield('content')
		</main>

		@include('includes.partials.footer')

		@include('includes.partials.consent')
		@include('includes.partials.comfort')

	</body>

</html>
