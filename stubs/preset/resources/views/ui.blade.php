@extends('layouts.app')

@section('title', 'UI')

@section('content')

	<section>
		<h1>Typography</h1>
		<p>Lorem ipsum <b>dolor sit amet</b> consectetur adipisicing elit. Aperiam, ipsam est! Corrupti animi in dolores recusandae officia placeat veniam nesciunt quibusdam possimus provident voluptas hic exercitationem minus vel id quod corporis qui ratione eius odit aspernatur, suscipit doloremque nemo est! Incidunt reiciendis voluptatum recusandae harum corporis provident in aliquid autem?</p>
		<p>Vel <a href="#">cupiditate nostrum</a> sequi quam, amet suscipit! Exercitationem quae praesentium voluptatem, optio minima delectus provident magni corporis debitis ipsam et, totam nulla, dolorum veritatis error iusto reprehenderit? Odit autem voluptate dicta minus facere voluptates dolorum quis, molestias, officia, earum consectetur labore. Modi expedita aliquid animi nam recusandae corrupti alias itaque!</p>
		<p>Explicabo quasi animi itaque sunt expedita? Aperiam, illo. Iure natus ipsum exercitationem culpa accusamus inventore eius placeat nulla pariatur ea ratione animi libero delectus obcaecati, eveniet aliquam aliquid dolorum voluptatem laudantium nihil minima, facilis unde excepturi necessitatibus. Deleniti itaque eos, atque expedita mollitia magni cupiditate, commodi aliquid hic molestias iste.</p>
		<ul>
			<li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Qui, quis.</li>
			<li>Hic et atque vel magnam quaerat fugit assumenda adipisci ducimus.</li>
			<li>Necessitatibus iusto facilis consequatur consectetur eligendi veniam, deleniti cum temporibus.</li>
			<li>Consequuntur, vel? Deleniti fugiat, omnis aspernatur mollitia quos illum eius.</li>
			<li>Quisquam ipsum assumenda consectetur autem quidem provident expedita officia dolorem!</li>
			<li>Vero assumenda magni nihil molestias asperiores officiis eaque optio minus.</li>
		</ul>
		<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus iste tenetur repudiandae ipsum. Molestiae ea fugit soluta, vitae dolor vero.</p>
		<h2>Secondary title</h2>
		<p>Nisi deserunt animi reprehenderit excepturi numquam vel harum totam quo, ratione, ex dolore minima distinctio aliquam fugiat, perspiciatis et repellendus!</p>
		<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus iste tenetur repudiandae ipsum. Molestiae ea fugit soluta, vitae dolor vero.</p>
		<h3>Thirdary title</h3>
		<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum quis possimus impedit dicta temporibus iure nisi ullam, mollitia libero earum odio reprehenderit beatae doloribus suscipit quia laborum dolorem voluptatum debitis vero pariatur! Architecto doloribus nulla nostrum facere repellendus tenetur perferendis?</p>
	</section>

	<section>
		<h1>Components</h1>
		<article class="card">
			<h2>Card title</h2>
			<p>Lorem ipsum dolor sit <a href="#">another link</a> amet consectetur adipisicing elit. Est, facere!</p>
            <ul>
				<li>Lorem, ipsum dolor.</li>
                <li>Est, voluptatibus animi!</li>
                <li>Aliquam, ipsa nesciunt?</li>
			</ul>
			<a href="#" role="button">Link button</a>
			<button>Button</button>
		</article>
	</section>

	<section>
		<h1>Form</h1>

		<form action="null">

            <x-ll::form.input id="testA" name="firstname" label="firstname"/>
            <x-ll::form.input id="testB" name="lastname" label="lastname"/>
            <x-ll::form.select id="testC" name="type" label="type" :options="['Type A', 'Type B', 'Type C']"/>

            <fieldset>
                <legend>Radios</legend>
                <x-ll::form.radio id="testD1" name="sex" label="Female"/>
                <x-ll::form.radio id="testD2" name="sex" label="Male"/>
            </fieldset>

            <fieldset>
                <legend>Checkboxes</legend>
                <x-ll::form.checkbox id="testE1" name="animal[]" label="Cat"/>
                <x-ll::form.checkbox id="testE2" name="animal[]" label="Dog"/>
            </fieldset>

           <div>
             <x-ll::form.switcher id="testF" name="aggree" label="I agreed"/>
           </div>

            <x-ll::form.textarea id="testG" name="message" label="message"/>

            <button type="reset">Cancel</button>
            <button type="submit">Submit</button>
        </form>
			
	</section>

@endsection
