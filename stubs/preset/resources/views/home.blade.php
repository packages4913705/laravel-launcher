@extends('layouts.app')

@section('content')
	<section>
		@isset($page)
			<h1>{{ $page->title }}</h1>
			{!! $page->description !!}
		@endisset
	</section>
@endsection
