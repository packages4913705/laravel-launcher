@extends('layouts.app')

@section('title', $page->label)

@section('content')

	<section>
		<h1>{{ $page->title }}</h1>
		{!! $page->description !!}
		@if ($page->hasMedia('picture'))

			@php
				$pictures = $page->mediaByTypes('picture');
				$picture = $pictures->first();
			@endphp

			@if ($pictures->count() > 1)
				<div id="slider" class="slider">
					@foreach ($pictures as $slide)
						<div id="slide{{ $slide->id }}" role="tabpanel" aria-hidden="true">
							<picture>
								<source srcset="{{ $slide->urlBySize('h-800') }}, {{ $slide->urlBySize('h-800') }} 2x" media="(max-width: 580px)">
								<source srcset="{{ $slide->urlBySize('h-800') }}, {{ $slide->urlBySize('h-800') }} 2x" media="(max-width: 960px)">
								<img src="{{ $slide->urlBySize('w-1800') }}" srcset="{{ $slide->urlBySize('w-1800') }} 2x" alt="{{ $slide->description ?? 'Une image' }}">
							</picture>
						</div>
					@endforeach
				</div>
			@else
				<picture>
					<source srcset="{{ $picture->urlBySize('h-800') }}, {{ $picture->urlBySize('h-800') }} 2x" media="(max-width: 580px)">
					<source srcset="{{ $picture->urlBySize('h-800') }}, {{ $picture->urlBySize('h-800') }} 2x" media="(max-width: 960px)">
					<img src="{{ $picture->urlBySize('w-1800') }}" srcset="{{ $picture->urlBySize('w-1800') }} 2x" alt="{{ $picture->description ?? 'Une image' }}">
				</picture>
			@endif

		@endif
	</section>

@endsection
