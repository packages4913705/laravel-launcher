/**
 * ------------------------------------------------------------------
 * Auth
 * ------------------------------------------------------------------
 * This script is for the backend auth of your website.
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

// Import the default scripts
import '../../vendor/nh/laravel-launcher/resources/js/auth'

// Customize 