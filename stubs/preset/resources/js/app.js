/**
 * ------------------------------------------------------------------
 * App
 * ------------------------------------------------------------------
 * This script is for the frontend of your website.
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

// Here the helpers
import FormHelper from "@natachah/vanilla-frontend/js/utilities/_form-helper"

// Here the components
import Autofill from "@natachah/vanilla-frontend/js/_autofill"
import CheckAll from "@natachah/vanilla-frontend/js/_check-all"
import Comfort from "@natachah/vanilla-frontend/js/_comfort"
import Consent from "@natachah/vanilla-frontend/js/_consent"
import Dialog from "@natachah/vanilla-frontend/js/_dialog"
import Dropdown from "@natachah/vanilla-frontend/js/_dropdown"
import Scroll from "@natachah/vanilla-frontend/js/_scroll"
import Drawer from "@natachah/vanilla-frontend/js/_drawer"
import Slider from "@natachah/vanilla-frontend/js/_slider"
import Sortable from "@natachah/vanilla-frontend/js/_sortable"
import Toggle from "@natachah/vanilla-frontend/js/_toggle"
import Tree from "@natachah/vanilla-frontend/js/_tree"

// * Set a timeout to remove data-preload
// This is for avoid animation on first load
setTimeout(() => {
    document.body.removeAttribute('data-preload')
}, 1000)

// * Create the menu as drawer for mobile
// Add the new Tree only if multi-level menu
const menu = document.getElementById('menu')
if (menu) {
    new Drawer(menu, { breakpoint: 960 })
    new Tree(menu)
}

// * Set the default Consent <dialog>
if (document.getElementById('cookies')) {
    const cookieConsent = new Consent('_frontend-consent')
}

// * Set the default Comfort <dialog>
const comfortDialog = document.getElementById('comfortDialog')
if (comfortDialog) {

    // Define the Comfort
    const frontendComfort = new Comfort('_frontend-comfort')

    // Define the default values
    defineDefaultComfortSettings(frontendComfort)

    // Create the dialog
    new Dialog(comfortDialog)

    // Add the reset button, and reset to default values
    const resetComfort = document.getElementById('comfortReset')
    if (resetComfort) resetComfort.addEventListener('click', () => {
        frontendComfort.reset()
        defineDefaultComfortSettings(frontendComfort)
    })

}

// Function to reset the default settings
function defineDefaultComfortSettings(comfort) {
    if (!comfort.has('theme')) comfort.setTheme(window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light')
    if (!comfort.has('style')) {
        comfort.setStyle('--font-size', '16px')
        comfort.setStyle('--line-height', '1.5')
        comfort.setStyle('--font-family', 'arial')
    }
}

// * Define default slider
const sliders = document.querySelectorAll('.slider')
if (sliders) sliders.forEach(slider => new Slider(slider, { behavior: 'instant', loop: true, autoplay: 2500 }))
