<?php

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;
use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Http\Middleware\Localization;

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        commands: __DIR__.'/../routes/console.php',
        health: '/up',
        using: function () {
            Route::middleware('web')
                ->group(base_path('routes/auth.php'));
            Route::middleware(['web', 'auth'])
                ->prefix('backend')
                ->name('backend.')
                ->group(base_path('routes/backend.php'));
            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        }
    )
    ->withMiddleware(function (Middleware $middleware) {
        $middleware->redirectGuestsTo('/login');
        $middleware->redirectUsersTo('/backend');
        $middleware->appendToGroup('web', Localization::class);
    })
    ->withExceptions(function (Exceptions $exceptions) {
        //
    })->create();
