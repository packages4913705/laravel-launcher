<?php

use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Auth - Routes
|--------------------------------------------------------------------------
|
| Here is the auth routes.
|
*/

// Auth
Route::middleware(['guest'])->group(function () {
    Route::get('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/login', [AuthController::class, 'authenticate'])->name('authenticate');
    Route::get('/password/forgot', [AuthController::class, 'passwordForgot'])->name('password.forgot');
    Route::post('/password/forgot', [AuthController::class, 'passwordSend'])->name('password.send');
    Route::get('/password/reset/{token}', [AuthController::class, 'passwordReset'])->name('password.reset');
    Route::post('/password/reset', [AuthController::class, 'passwordUpdate'])->name('password.update');
});

Route::middleware(['auth'])->group(function () {
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
});
