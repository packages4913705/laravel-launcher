<?php

use App\Http\Controllers\Backend\DashboardController;
use Illuminate\Support\Facades\Route;
use Nh\LaravelLauncher\Http\Controllers\ActivityLogController;
use Nh\LaravelLauncher\Http\Controllers\CategoryController;
use Nh\LaravelLauncher\Http\Controllers\MediaController;
use Nh\LaravelLauncher\Http\Controllers\PageController;
use Nh\LaravelLauncher\Http\Controllers\PermissionController;
use Nh\LaravelLauncher\Http\Controllers\RoleController;
use Nh\LaravelLauncher\Http\Controllers\SettingController;
use Nh\LaravelLauncher\Http\Controllers\UserController;

// !USE_PLACEHOLDER

/*
|--------------------------------------------------------------------------
| Backend - Routes
|--------------------------------------------------------------------------
|
| Here is where you can register backend routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

Route::get('/', DashboardController::class)->name('dashboard');

// Setting
Route::patch('settings/update', [SettingController::class, 'update'])->name('settings.update');
Route::get('settings/edit', [SettingController::class, 'edit'])->name('settings.edit');
Route::get('settings', [SettingController::class, 'index'])->name('settings.index');

// Activity logs
Route::get('/logs/{log}', [ActivityLogController::class, 'show'])->name('activity-logs.show');
Route::get('/logs', [ActivityLogController::class, 'index'])->name('activity-logs.index');

// User
Route::patch('/users/{user}/restore', [UserController::class, 'restore'])->name('users.restore');
Route::delete('/users/{user}/delete', [UserController::class, 'delete'])->name('users.delete');
Route::resource('users', UserController::class);

// Role
Route::resource('roles', RoleController::class);

// Permission
Route::resource('permissions', PermissionController::class);

// Media
Route::patch('/media/{media}', [MediaController::class, 'update'])->name('media.update');
Route::get('/media/{media}/edit', [MediaController::class, 'edit'])->name('media.edit');
Route::get('/media/{media}', [MediaController::class, 'show'])->name('media.show');
Route::get('/media', [MediaController::class, 'index'])->name('media.index');

// Category
Route::patch('/categories/{category}/restore', [CategoryController::class, 'restore'])->name('categories.restore');
Route::delete('/categories/{category}/delete', [CategoryController::class, 'delete'])->name('categories.delete');
Route::resource('categories', CategoryController::class);

// Page
Route::patch('/pages/{page}/unpublish', [PageController::class, 'unpublish'])->name('pages.unpublish');
Route::patch('/pages/{page}/publish', [PageController::class, 'publish'])->name('pages.publish');
Route::patch('/pages/{page}/restore', [PageController::class, 'restore'])->name('pages.restore');
Route::delete('/pages/{page}/delete', [PageController::class, 'delete'])->name('pages.delete');
Route::resource('pages', PageController::class);

// !PLACEHOLDER
