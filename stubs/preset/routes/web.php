<?php

use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web - Routes
|--------------------------------------------------------------------------
|
| Here is where you can register frontend routes for your application.
|
*/

// Page to test the UI
Route::get('/ui', function(){
    return view('ui');
})->name('test.ui');

// Return the contact
Route::get('/email', [ContactController::class, 'email'])->name('contact.email');
Route::get('/phone', [ContactController::class, 'phone'])->name('contact.phone');

// Return the homepage
Route::get('/', HomeController::class)->name('home');

// Return the pages
Route::get('/{slugs}', PageController::class)->where('slugs', '.*')->name('page');
